package com.tuya.appsdk.sample;

import android.content.Context;
import android.text.TextUtils;

import com.tuya.smart.android.base.provider.ApiUrlProvider;
import com.tuya.smart.home.sdk.TuyaHomeSdk;
import com.tuya.smart.sdk.api.ITuyaUser;

/**
 * PrivateCloudUrlProvider
 *
 * @author qinyun <a href="qunyun.miao@@tuya.com"/>
 * @since 2022/3/18 16:40 PM
 */
public class PrivateCloudUrlProvider extends ApiUrlProvider {
    private final String baseUrl;

    public PrivateCloudUrlProvider(Context context, String baseUrl) {
        super(context);
        this.baseUrl = baseUrl;
    }

    @Override
    public String getApiUrl() {
        if (!this.isUserLogin()) {
            String url = baseUrl;
            if (!TextUtils.isEmpty(url) && !url.endsWith("/api.json")) {
                url = url + "/api.json";
            }
            return url;
        }
        return super.getApiUrl();
    }

    @Override
    public String getApiUrlByCountryCode(String countryCode) {
        if (!this.isUserLogin()) {
            String url = baseUrl;
            if (!TextUtils.isEmpty(url) && !url.endsWith("/api.json")) {
                url = url + "/api.json";
            }
            return url;
        }
        return super.getApiUrlByCountryCode(countryCode);
    }

    private boolean isUserLogin() {
        ITuyaUser userInstance = TuyaHomeSdk.getUserInstance();
        return null != userInstance && userInstance.isLogin();
    }

}