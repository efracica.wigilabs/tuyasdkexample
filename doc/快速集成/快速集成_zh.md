本文介绍如何将涂鸦安卓 App SDK 集成到您的开发环境中，例如 Android Studio，并介绍初始化方法以及如何启用调试模式。然后，您可以尝试运行 Demo，快速上手 App SDK 开发。

## 集成 SDK

- 开始操作前，请确保您已经完成了 [准备工作](https://developer.tuya.com/cn/docs/app-development/preparation?id=Ka69nt983bhh5)。
- 如果您还未安装 Android Studio，请访问 [安卓官网](https://developer.android.com/studio) 进行下载安装。

### 第一步：创建 Android 工程

在 Android Studio 中新建工程。

### 第二步：配置 build.gradle 文件

在安卓项目的 `build.gradle` 文件里，添加集成准备中下载的 `dependencies` 依赖库。

```groovy
android {
	defaultConfig {
		ndk {
			abiFilters "armeabi-v7a", "arm64-v8a"
		}
	}
	packagingOptions {
		pickFirst 'lib/*/libc++_shared.so' // 多个 AAR（Android Library）文件中存在此 .so 文件，请选择第一个
		pickFirst 'lib/*/libyuv.so'
		pickFirst 'lib/*/libopenh264.so'
	}
}
dependencies {
	implementation 'com.alibaba:fastjson:1.1.67.android'
	implementation 'com.squareup.okhttp3:okhttp-urlconnection:3.14.9'

	// App SDK 最新稳定安卓版：
	implementation 'com.tuya.smart:tuyasmart:1.0.0-cube'
}
```

在根目录的 `build.gradle` 文件，中增加涂鸦 IoT Maven 仓库地址，进行仓库配置。

```groovy
repositories {
	jcenter()
	google()
	// 涂鸦 IoT 仓库地址
	maven {
		url "https://maven-other.tuya.com/repository/maven-releases/"
	}
	maven {
		url 'https://maven-other.tuya.com/repository/maven-private/'
		credentials {
			Properties properties = new Properties()
			properties.load(project.rootProject.file('local.properties').newDataInputStream())
			username "${properties.getProperty("username")}"
			password "${properties.getProperty("password")}"
		}
	}
}
```

>**注意**：
> * 3.10.0 版本之前的 SDK 只支持安卓 armeabi-v7a。3.11.0 版本后已经将 armeabi-v7a、arm64-v8a 集成进 SDK。您需要将本地自行放入的 SDK 的相关 so 库移除，使用 SDK 中提供的。
> * 如果集成新版本 so 库，请移除之前老版本手动集成的库，防止冲突或者代码版本不一致导致的问题。
> * 如有其他平台需要，请前往 [GitHub](https://github.com/tuya/tuya-home-android-sdk/tree/master/so_libs) 获取。

<a id="bmp&keySetting"></a>

### 第三步：集成安全图片和设置 Appkey 和 AppSecret

1. 在 [App 工作台](https://iot.tuya.com/oem/sdkList)，找到您创建的 SDK。
2. 在 **获取密钥** 中，点击 **下载安全图片** > **安全图片下载** 下载安全图片。

	![image.png](https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/content-platform/hestia/1629354107c0dedbc5e3a.png)

    :::important
    Android端从3.29.5版本开始，需要设置SHA256。关于如何获取        SHA256密钥，可以参考文档[如何获取SHA256密钥](https://developer.tuya.com/cn/docs/app-development/iot_app_sdk_core_sha1?id=Kao7c7b139vrh)。
    :::

3. 将下载的安全图片命名为 `t_s.bmp`，放置到工程目录的 `assets` 文件夹下。

	<img src="https://images.tuyacn.com/fe-static/docs/img/2de282a3-5498-479e-bb31-3688e3ac1eb2.png" alt="涂鸦技术文档示意图" width="300"  style="border:1px solid black"/>

4. 返回安卓项目，在 `AndroidManifest.xml` 文件里配置 appkey 和 appSecret，在配置相应的权限等。

```xml
	<meta-data
		android:name="TUYA_SMART_APPKEY"
		android:value="应用 Appkey" />
	<meta-data
		android:name="TUYA_SMART_SECRET"
		android:value="应用密钥 AppSecret" />
```

### 第四步：混淆配置

在 `proguard-rules.pro` 文件配置相应混淆配置。

```bash
#fastJson
-keep class com.alibaba.fastjson.**{*;}
-dontwarn com.alibaba.fastjson.**

#mqtt
-keep class com.tuya.smart.mqttclient.mqttv3.** { *; }
-dontwarn com.tuya.smart.mqttclient.mqttv3.**

#OkHttp3
-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-dontwarn okhttp3.**

-keep class okio.** { *; }
-dontwarn okio.**

-keep class com.tuya.**{*;}
-dontwarn com.tuya.**
```

### 第五步：初始化 SDK 并设置私有域名

您需要在 `Application` 的主线程中初始化 SDK，确保所有进程都能初始化。示例代码如下：

```java
public class TuyaSmartApp extends Application {
	@Override
	public void onCreate() {
		super.onCreate();
		initTuyaHomeSdk("host:port");
	}

	/**
	 * @param baseUrl 私有域名
	 */
	private void initTuyaHomeSdk(String baseUrl) {
		TuyaSmartSdk.init(this);
		TuyaSdk.init(
				this,
				TuyaSmartSdk.getAppkey(),
				TuyaSmartSdk.getAppSecret(),
				"android",
				"sdk",
				null,
				new PrivateCloudUrlProvider(this, baseUrl)
		);
		OptimusUtil.init(this);
	}
}

public class PrivateCloudUrlProvider extends ApiUrlProvider {
	private final String baseUrl;

	public PrivateCloudUrlProvider(Context context, String baseUrl) {
		super(context);
		this.baseUrl = baseUrl;
	}

	@Override
	public String getApiUrl() {
		if (!this.isUserLogin()) {
			String url = baseUrl;
			if (!TextUtils.isEmpty(url) && !url.endsWith("/api.json")) {
				url = url + "/api.json";
			}
			return url;
		}
		return super.getApiUrl();
	}

	@Override
	public String getApiUrlByCountryCode(String countryCode) {
		if (!this.isUserLogin()) {
			String url = baseUrl;
			if (!TextUtils.isEmpty(url) && !url.endsWith("/api.json")) {
				url = url + "/api.json";
			}
			return url;
		}
		return super.getApiUrlByCountryCode(countryCode);
	}
	
	private boolean isUserLogin() {
		ITuyaUser userInstance = TuyaHomeSdk.getUserInstance();
		return null != userInstance && userInstance.isLogin();
	}

}
```

### 第六步：注销云连接

在退出应用的时候，调用以下接口可以注销云连接。

```java
TuyaHomeSdk.onDestroy();
```

### 第七步：开启或关闭日志

* 在 debug 模式下，您可以开启 SDK 的日志开关，查看更多的日志信息，帮助您快速定位问题。
* 在 release 模式下，建议关闭日志开关。

	```java
	TuyaHomeSdk.setDebugMode(true);
	```

## 运行 Demo App

>**注意**：在完成快速集成 SDK 后，您将获取到 SDK 使用的 `AppKey`、 `AppSecret`、安全图片信息。集成 SDK 时请确认 `AppKey`、`AppSecret`、安全图片是否与平台上的信息一致，任意一个不匹配会导致 SDK 无法使用。详细操作，请参考 [第三步：集成安全图片和设置 Appkey 和 AppSecret](#bmp&keySetting)。

Demo App 演示了 App SDK 的开发流程。在开发 App 之前，建议您先按照以下流程完成 Demo App 的操作。

### Demo App 介绍

Demo App 主要包括了：

- 用户管理：使用手机号或者邮箱进行登录和注册。
- 家庭管理和设备管理：
	- 创建家庭，切换用户所属的当前家庭。
	- 展现家庭中设备列表，控制设备功能点。
	- 设备重命名和设备移除。
- 设备配网：包括 Wi-Fi 快连配网模式，热点配网模式，有线网关配网，网关子设备配网，蓝牙配网，Mesh子设备配网。
	<img src="https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/content-platform/hestia/16214050939ab3d5873b4.png" style="zoom: 25%;border:1px solid black"/>

更多详情，请参考 [tuya-home-android-sdk-sample-java](https://github.com/tuya/tuya-home-android-sdk-sample-java) 或者 [tuya-home-android-sdk-sample-kotlin](https://github.com/tuya/tuya-home-android-sdk-sample-kotlin) GitHub 项目。

### 运行 Demo

1. 替换 `app` 目录下 `build.gradle` 文件中的 `applicationId` 为您的应用包名。

	![image-20191101112723293](https://images.tuyacn.com/fe-static/docs/img/e4a39c6a-35f2-4a66-a82d-c5aace2889f4.png)

2. 确认您已经完成了 [第三步：集成安全图片和设置 Appkey 和 AppSecret](#bmp&keySetting)。

3. 然后点击运行，运行 Demo。
<!-- 
2. 将您的安全图片命名为：`t_s.bmp`，放到 app 目录下 `src` > `main` > `assets` 文件夹下。

	<img src="https://images.tuyacn.com/fe-static/docs/img/8f8ca63c-7a41-4d7d-8301-6f4843dbf614.png" alt="image-20191101112851418" style="zoom:40%;" />

3. 将您的 AppKey、AppSecret 填写到 `AndroidManifest.xml` 中的对应 <meta-data> 标签中。

	![image-20191101113051694](https://images.tuyacn.com/fe-static/docs/img/28872d1c-4a6b-44b7-9b3b-2c70e44b20ad.png) 
-->

### 故障排查：API 接口请求提示签名错误

* **问题现象**：运行 Demo 时提示以下错误：

	```json
	{
		"success": false,
		"errorCode" : "SING_VALIDATE_FALED",
		"status" : "error",
		"errorMsg" : "Permission Verification Failed",
		"t" : 1583208740059
	}
	```

* **解决方法**：

	* 请检查您的 AppKey 、AppSecret 和 安全图片是否正确配置，是否和 [准备工作](https://developer.tuya.com/cn/docs/app-development/preparation/preparation?id=Ka69nt983bhh5) 中获取到的一致。
	* 安全图片是否放在正确目录，文件名是否为 `t_s.bmp`。