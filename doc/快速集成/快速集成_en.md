This topic describes how to quickly integrate Tuya App SDK for Android into your development environment, such as Android Studio. It also sheds light on the initialization method and how to enable the debugging mode with a few simple steps. This allows you to run the demo app and get started with your app development by using the App SDK.

## Integrate the SDK

- Before you start, make sure that you have performed the steps in [Preparation](https://developer.tuya.com/en/docs/app-development/preparation?id=Ka69nt983bhh5).
- If you have not installed Android Studio, visit the [Android Studio official website](https://developer.android.com/studio) to download Android Studio.

### Step 1: Create an Android project

Create a project in Android Studio.

### Step 2: Configure build.gradle

Add `dependencies` to the `build.gradle` file of the Android project.

```groovy
android {
	defaultConfig {
		ndk {
			abiFilters "armeabi-v7a", "arm64-v8a"
		}
	}
	packagingOptions {
		pickFirst 'lib/*/libc++_shared.so' // An Android Archive (AAR) file contains an Android library. If the .so file exists in multiple AAR files, select the first AAR file.
        pickFirst 'lib/*/libyuv.so'
        pickFirst 'lib/*/libopenh264.so'
	}
}
dependencies {
	implementation 'com.alibaba:fastjson:1.1.67.android'
	implementation 'com.squareup.okhttp3:okhttp-urlconnection:3.14.9'

	// The latest stable App SDK for Android.
	implementation 'com.tuya.smart:tuyasmart:1.0.0-cube'
}
```

Add the Tuya IoT Maven repository URL to the `build.gradle` file in the root directory.

```groovy
repositories {
	jcenter()
	google()
	// The Tuya IoT Maven repository URL.
	maven {
		url "https://maven-other.tuya.com/repository/maven-releases/"
	}
    maven {
        url 'https://maven-other.tuya.com/repository/maven-private/'
        credentials {
            Properties properties = new Properties()
            properties.load(project.rootProject.file('local.properties').newDataInputStream())
            username "${properties.getProperty("username")}"
            password "${properties.getProperty("password")}"
        }
    }
}
```

> **Note**:
> * App SDK v3.10.0 and earlier only support `armeabi-v7a`. App SDK v3.11.0 and later have integrated `armeabi-v7a` and `arm64-v8a`. If you have added `.so` libraries to the project, you must remove them and only use the library included in the SDK.
> * If you want to integrate the `.so` library of a later version, you must remove the library of an earlier version to avoid conflicts or other possible issues.
> * For more information, see [GitHub](https://github.com/tuya/tuya-home-android-sdk/tree/master/so_libs).

<a id="bmp&keySetting"></a>

### Step 3: Configure the security image, AppKey, and AppSecret

1. Log in to the [Tuya IoT Platform](https://iot.tuya.com/), choose **App** > **App SDK** > **SDK Development**, and then click the newly created SDK.
2. On the page that appears, click the **Get Key** tab and click **Download** in the **App Security Image for Android** field.

    ![Get Key](https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/content-platform/hestia/162935428707a87f44ab0.png)

    :::important
    From version 3.29.5 on Android side, you need to set SHA256 before you can use it. How to get SHA256 key, you can refer to the document [How to get SHA256 key](https://developer.tuya.com/en/docs/app-development/iot_app_sdk_core_sha1?id=Kao7c7b139vrh).
    :::

3. Rename the security image as `t_s.bmp` and put the image in the `assets` folder of your project.

   <img src="https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/content-platform/hestia/16213492036a2d2d32a44.png" alt="Security image" style="zoom: 30%;" />

4. Return to the Android project, configure `Appkey` and `AppSecret` in `AndroidManifest.xml`, and then set permissions for the app.

```xml
	<meta-data
        android:name="TUYA_SMART_APPKEY"
        android:value="AppKey" />
	<meta-data
        android:name="TUYA_SMART_SECRET"
        android:value="AppSecret" />
```

### Step 4: Obfuscate the code

Configure obfuscation in `proguard-rules.pro`.

```bash
#fastJson
-keep class com.alibaba.fastjson.**{*;}
-dontwarn com.alibaba.fastjson.**

#mqtt
-keep class com.tuya.smart.mqttclient.mqttv3.** { *; }
-dontwarn com.tuya.smart.mqttclient.mqttv3.**

#OkHttp3
-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-dontwarn okhttp3.**

-keep class okio.** { *; }
-dontwarn okio.**

-keep class com.tuya.**{*;}
-dontwarn com.tuya.**
```

### Step 5: Initialize the SDK and inject private host 

Initialize the SDK in the main thread of `Application`. Make sure that all processes are initialized. Sample code:

```java
public class TuyaSmartApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        initTuyaHomeSdk("host:port");
    }
    
    /**
     * @param baseUrl private host
     */
    private void initTuyaHomeSdk(String baseUrl) {
        TuyaSmartSdk.init(this);
        TuyaSdk.init(
                this,
                TuyaSmartSdk.getAppkey(),
                TuyaSmartSdk.getAppSecret(),
                "android",
                "sdk",
                null,
                new PrivateCloudUrlProvider(this, baseUrl)
        );
        OptimusUtil.init(this);
    }
}

public class PrivateCloudUrlProvider extends ApiUrlProvider {
    private final String baseUrl;

    public PrivateCloudUrlProvider(Context context, String baseUrl) {
        super(context);
        this.baseUrl = baseUrl;
    }

    @Override
    public String getApiUrl() {
        if (!this.isUserLogin()) {
            String url = baseUrl;
            if (!TextUtils.isEmpty(url) && !url.endsWith("/api.json")) {
                url = url + "/api.json";
            }
            return url;
        }
        return super.getApiUrl();
    }

    @Override
    public String getApiUrlByCountryCode(String countryCode) {
        if (!this.isUserLogin()) {
            String url = baseUrl;
            if (!TextUtils.isEmpty(url) && !url.endsWith("/api.json")) {
                url = url + "/api.json";
            }
            return url;
        }
        return super.getApiUrlByCountryCode(countryCode);
    }

    private boolean isUserLogin() {
        ITuyaUser userInstance = TuyaHomeSdk.getUserInstance();
        return null != userInstance && userInstance.isLogin();
    }

}
```

### Step 6: Disable the cloud connection

Before you exit the app, you must call the following operation to disable the cloud connection.

```java
TuyaHomeSdk.onDestroy();
```

### Step 7: Enable or disable logging

* In debug mode, you can enable SDK logging to facilitate troubleshooting.
* We recommend that you disable logging in release mode.

```java
	TuyaHomeSdk.setDebugMode(true);
```

## Run the demo app

> **Note**: After you integrate the SDK, you can get the security image, `AppKey`, and `AppSecret`. Make sure the security image, `AppKey`, and `AppSecret` are consistent with those used on the Tuya IoT Platform. Any mismatch will cause the SDK development to be failed. For more information, see [Step 3: Configure the security image, AppKey, and AppSecret](#bmp&keySetting).

In the following example, a demo app is used to describe the process of app development with the App SDK. Before the development of your app, we recommend that you run the demo app.

### Demo app description

The demo app supports the following functions:

- User management: Register and log in to the app account by mobile phone number or email address.
- Home and device management
   - Create a home and switch between homes.
   - Display a list of devices in the home and control data points (DPs) of the devices.
   - Rename and remove devices.
- Pair devices: Multiple pairing methods are supported, including **EZ Mode**, **AP Mode**, **Bluetooth**, **Zigbee Gateway**, **Zigbee Subdevice** and **Mesh Subdevice**,.

   <img src="https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/content-platform/hestia/162140528121c16d9cde1.png" alt="Demo app" style="zoom: 50%;" />

For more information, see the GitHub projects [tuya-home-android-sdk-sample-java](https://github.com/tuya/tuya-home-android-sdk-sample-java) or [tuya-home-android-sdk-sample-kotlin](https://github.com/tuya/tuya-home-android-sdk-sample-kotlin).

### Run the demo

1. Choose `app` > `build.gradle` and change the value of `applicationId` to your app package name.

   ![Change the value of the app package name](https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/content-platform/hestia/1621349712a3e8f0a0aab.png)

2. Make sure that you have completed [ Step 3: Configure the security image, AppKey, and AppSecret](#bmp&keySetting).

3. Click **Run** to run the demo app.
<!-- 
2. 将您的安全图片命名为：`t_s.bmp`，放到 app 目录下 `src` > `main` > `assets` 文件夹下。

	<img src="https://images.tuyacn.com/fe-static/docs/img/8f8ca63c-7a41-4d7d-8301-6f4843dbf614.png" alt="image-20191101112851418" style="zoom:40%;" />

3. 将您的 AppKey、AppSecret 填写到 `AndroidManifest.xml` 中的对应 <meta-data> 标签中。

	![image-20191101113051694](https://images.tuyacn.com/fe-static/docs/img/28872d1c-4a6b-44b7-9b3b-2c70e44b20ad.png) 
-->

### Troubleshooting the error message: Permission Verification Failed

* **Problem**: When the system runs the demo app, an error message is returned in the following response:

   	```json
   {
   	"success": false,
   	"errorCode" : "SING_VALIDATE_FALED",
   	"status" : "error",
   	"errorMsg" : "Permission Verification Failed",
   	"t" : 1583208740059
   }

   ```

* **Solutions**:

   * Check whether your security image, AppKey, and AppSecret are correctly configured and consistent with those obtained in [Preparation](https://developer.tuya.com/en/docs/app-development/preparation/preparation?id=Ka69nt983bhh5).
   * Check whether the security image is placed in the correct directory and whether the file name is `t_s.bmp`.