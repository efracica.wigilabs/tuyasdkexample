Home management mainly refers to the management of home-related information under the user, such as:

- Get home list.
- Add, modify, or remove individual home actions.
- Management of home name, geographic location, room list information, member information, etc.
- At home, monitoring of device addition, information modification, and removal, monitoring of device status change, etc.