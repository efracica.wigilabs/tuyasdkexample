## 添加家庭成员

> **注意**：拥有者可以添加管理员及以下角色，管理员仅仅可以添加普通成员及以下角色。

**接口说明**

`MemberWrapperBean` 对象中的 autoAccept 字段用于控制是否需要受邀者同意，若为 false 则需要受邀者调用 `processInvitation()` 方法接受或者同意邀请后才能加入家庭。

```java
void addMember(MemberWrapperBean memberWrapperBean, ITuyaDataCallback<MemberBean> callback)
```

**参数说明**

| 参数              | 说明     |
| :---------------- | :------- |
| memberWrapperBean | 成员信息 |

**MemberWrapperBean数据模型**

| 字段             | 类型    | 描述                                                         |
| :-------------- | :------ | :----------------------------------------------------------- |
| homeId          | long    | 要添加成员的家庭的 ID                                        |
| nickName        | String  | 为受邀请者设置的昵称                                         |
| admin           | boolean | 是否为家庭管理员                                            |
| memberId        | long    | 家庭成员 ID                                              |
| headPic         | String  | 为受邀请者设置的头像 nil 时使用受邀请者个人头像              |
| account         | String  | 受邀请账号                                                   |
| countryCode     | String  | 受邀请者账号对应国家码                                       |
| invitationCode  | String  | 邀请码                             |
| role            | int     | 成员角色(查看MemberRole定义类型)                             |
| autoAccept      | boolean | 是否需要受邀请者同意接受邀请 true-受邀请账号自动接受该家庭邀请，无需受邀请者确认 false-需要受邀请者同意后才可加入该家庭 |


**示例代码**

```java
TuyaHomeSdk.getMemberInstance().addMember(memberWrapperBean, new ITuyaDataCallback<MemberBean>() {
		@Override
		public void onSuccess(MemberBean result) {
			// do something
		}
		@Override
		public void onError(String errorCode, String errorMessage) {
			// do something
		}
	});
```

## 删除家庭成员

>**注意**：拥有者可以删除管理员及以下角色，管理员仅仅可以删除普通成员及以下角色。

**接口说明**

若成员传入自身 memberId，家庭管理员，普通成员，自定义角色，调用此接口为离开家庭，此时该家庭未注销，设备也不会被重置。拥有者为注销家庭，同时该家庭下所有设备会被重置，效果与上文注销家庭一致。

```java
void removeMember(long memberId, IResultCallback callback)
```

**参数说明**

| 参数     | 说明    |
| :------- | :------ |
| memberId | 成员 ID |

**示例代码**

```java
TuyaHomeSdk.getMemberInstance().removeMember(memberId, new IResultCallback() {
		@Override
		public void onSuccess() {
			// do something
		}
		@Override
		public void onError(String code, String error) {
			// do something
		}
	});
```

## 获取家庭成员列表

**接口说明**

```java
void queryMemberList(long mHomeId, ITuyaGetMemberListCallback callback)
```

**参数说明**

| 参数    | 说明    |
| :------ | :------ |
| mHomeId | 家庭 ID |

**示例代码**

```java
TuyaHomeSdk.getMemberInstance().queryMemberList(mHomeId, new ITuyaGetMemberListCallback() {
		@Override
		public void onSuccess(List<MemberBean> memberBeans) {
			// do something
		}
		@Override
		public void onError(String errorCode, String error) {
			// do something
		}
	});
```


## 更新成员信息

**接口说明**

```java
void updateMember(long memberId, String name, boolean admin, IResultCallback callback)
```

**参数说明**

| 参数              |  参数类型     |  说明                 |
| :---------------- | ------------| :-----------------   |
| memberId          |  long       |  家庭成员ID            |
| name              |  name       |  成员名称              |
| admin             |  boolean    |  是否是家庭管理员       |

**示例代码**

```java
TuyaHomeSdk.getMemberInstance().updateMember(memberId, name, admin, new IResultCallback() {
        @Override
        public void onSuccess() {
            // do something
        }
        @Override
        public void onError(String code, String error) {
            // do something
        }
     });
```
## 更新成员信息(推荐)

**接口说明**

```java
void updateMember(MemberWrapperBean memberWrapperBean, IResultCallback callback)
```

**参数说明**

| 参数              |  参数类型              |  说明                 |
| :---------------- | ---------------------| :-----------------   |
| memberWrapperBean |  MemberWrapperBean   |  成员信息              |

**示例代码**

```java
TuyaHomeSdk.getMemberInstance().updateMember(memberWrapperBean, new IResultCallback() {
		@Override
		public void onSuccess() {
			// do something
		}
		@Override
		public void onError(String code, String error) {
			// do something
		}
	});
```

## 获取添加家庭成员邀请码

**接口说明**

```java
void getInvitationMessage(long homeId, ITuyaDataCallback callback)
```

**参数说明**

| 参数   | 说明    |
| ------ | ------- |
| homeId | 家庭 ID |

**示例代码**

```java
TuyaHomeSdk.getMemberInstance().getInvitationMessage(homeId, new ITuyaDataCallback<InviteMessageBean>() {
			@Override
			public void onSuccess(InviteMessageBean result) {
				// onSuc
			}

			@Override
			public void onError(String errorCode, String errorMessage) {
				// onErr
			}
		});
```
## 通过邀请码加入一个家庭

**接口说明**

```java
void joinHomeByInviteCode(String code, IResultCallback callback);
```
**参数说明**

| 参数         | 说明    |
| :----------- | :------ |
| code | 邀请码 |

**示例代码**

```java
TuyaHomeSdk.getHomeManagerInstance().joinHomeByInviteCode("code" , new IResultCallback() {
            @Override
            public void onError(String code, String error) {

            }

            @Override
            public void onSuccess() {

            }
        });
```



## 撤销家庭邀请

**接口说明**

```java
void cancelMemberInvitationCode(long invitationId, IResultCallback callBack)
```

**参数说明**

| 参数         | 说明    |
| :----------- | :------ |
| invitationId | 邀请 ID |

**示例代码**

```java
TuyaHomeSdk.getMemberInstance().cancelMemberInvitationCode(invitationId, new IResultCallback() {
		@Override
		public void onSuccess() {
			// do something
		}
		@Override
		public void onError(String code, String error) {
			// do something
		}
	});
```

## 接受或拒绝家庭邀请

**接口说明**

```java
void processInvitation(long homeId, boolean action, IResultCallback callBack)
```

**参数说明**

| 参数   | 说明      |
| :----- | :-------- |
| homeId | 家庭 ID   |
| action | 接受/拒绝 |

**示例代码**

```java
TuyaHomeSdk.getMemberInstance().processInvitation(homeId, action, new IResultCallback() {
		@Override
		public void onSuccess() {
			// do something
		}
		@Override
		public void onError(String code, String error) {
			// do something
		}
	});
```

## 获取邀请成员记录

**接口说明**

```java
void getInvitationList(long homeId, ITuyaDataCallback callback)
```

**参数说明**

| 参数   | 说明    |
| :----- | :------ |
| homeId | 家庭 ID |

**示例代码**

```java
TuyaHomeSdk.getMemberInstance().getInvitationList(homeId, new ITuyaDataCallback() {
		@Override
		public void onSuccess(List<InvitedMemberBean> memberBeans) {
			// do something
		}
		@Override
		public void onError(String errorCode, String error) {
			// do something
		}
	});
```

## 修改被邀请者信息

**接口说明**

```java
void updateInvitedMember(long invitationId, String memberName, int memberRole, IResultCallback callBack)
```

**参数说明**

| 参数         | 说明                 |
| :----------- | :------------------- |
| invitationId | 邀请 ID              |
| memberName   | 受邀者昵称           |
| memberRole   | 受邀者在家庭下的角色 |

**示例代码**

```java
TuyaHomeSdk.getMemberInstance().updateInvitedMember(invitationId, memberName, membeRole, new IResultCallback() {
		@Override
		public void onSuccess() {
			// do something
		}
		@Override
		public void onError(String code, String error) {
			// do something
		}
	});
```