Get the family list, family management, room and equipment management under the family and information monitoring, and so forth.

**HomeBean description**

| Parameters       | Type                     | Description                                                  |
| :--------------- | :----------------------- | :----------------------------------------------------------- |
| name             | String                   | The home name                                                |
| lon              | double                   | The home lon                                                 |
| lat              | double                   | The home lat                                                 |
| geoName          | String                   | The home location name                                       |
| homeId           | long                     | The home Id                                                  |
| admin            | boolean                  | Administrator status                                         |
| role             | int                      | The role of members：<br/>-1:Custom role<br/>0:Member<br/>1:Admin<br/>2:Owner<br/>-999:Invalid role |
| rooms            | List&lt;RoomBean&gt;     | All the rooms                                                |
| deviceList       | List&lt;DeviceBean&gt;   | All the devices                                              |
| groupList        | List&lt;GroupBean&gt;    | All the groups                                               |
| meshList         | List&lt;BlueMeshBean&gt; | All of the mesh devices                                      |
| sharedDeviceList | List&lt;DeviceBean&gt;   | Shared devices received                                      |
| sharedGroupList  | List&lt;GroupBean&gt;    | Shared groups received                                       |
| homeStatus       | int                      | The home status (1:to be accepted 2:accepted 3:reject)       |

## Create a family

**Description**

```java
void createHome(String name, double lon, double lat, String geoName, List rooms, ITuyaHomeResultCallback callback)
```

**Parameters**

| Parameters | Description                                                  |
| :--------- | :----------------------------------------------------------- |
| name       | Family name (Support up to **25** characters)                |
| lon        | longitude (Pass **`0`** if you don't set family location information) |
| lat        | latitude (Pass **`0`** if you don't set family location information) |
| geoName    | Home location name                                           |
| rooms      | Room list                                                    |
| callback   | Result callback                                              |

**Example**

```java
TuyaHomeSdk.getHomeManagerInstance().createHome(name, lon, lat, geoName, rooms, new ITuyaHomeResultCallback() {
		@Override
		public void onError(String errorCode, String errorMsg) {
			// do something
		}
		@Override
		public void onSuccess(HomeBean bean) {
			// do something
		}
	});
```

## 

## Get the family list

**Description**

```java
void queryHomeList(ITuyaGetHomeListCallback callback)
```

**Parameters**

| Parameters | Description     |
| :--------- | :-------------- |
| callback   | Result callback |

**Example**

```java
TuyaHomeSdk.getHomeManagerInstance().queryHomeList(new ITuyaGetHomeListCallback() {
		@Override
		public void onError(String errorCode, String error) {
			// do something
		}
		@Override
		public void onSuccess(List<HomeBean> homeBeans) {
			// do something
		}
	});
```

## Change the home information

Change of registered home information includes:

inviting home, adding home and removing home, change of home information, change of shared lists, and monitoring of successfully server connection.

**Description**

```java
public interface ITuyaHomeChangeListener {
	/**
	* Family added successfully.
	* Used for multi-device data synchronization
	*
	* @param homeId Family ID
	*/
	void onHomeAdded(long homeId);

	/**
	* Family invitation.
	* @param homeId    Family ID
	* @param homeName  Family name
	*/
	void onHomeInvite(long homeId,String homeName);

	/**
	* Family deleted successfully.
	* Used for multi-device data synchronization
	*
	* @param homeId  Family ID
	*/
	void onHomeRemoved(long homeId);

	/**
	* Family information changed.
	* Used for multi-device data synchronization
	*
	* @param homeId  Family ID
	*/
	void onHomeInfoChanged(long homeId);

	/**
	* Shared device list change.
	* Used for multi-device data synchronization
	*
	* @param sharedDeviceList shared device list
	*/
	void onSharedDeviceList(List<DeviceBean> sharedDeviceList);

	/**
	* The mobile phone successfully connects to the Tuya Cloud server and receives this notification.
	* Local data and server data may be inconsistent or the device cannot be controlled,
	* You can call the getHomeDetail interface under Home to reinitialize the data.
	*/
	void onServerConnectSuccess();
}
```

### Register the information change of a home

**Description**

```java
void registerTuyaHomeChangeListener(ITuyaHomeChangeListener listener);
```

**Parameters**

| Parameters | Description |
| :--------- | :---------- |
| listener   | Monitor     |

**Example**

```java
ITuyaHomeChangeListener listener = new ITuyaHomeChangeListener() {
		@Override
		public void onHomeInvite(long homeId, String homeName) {
			// do something
		}
		@Override
		public void onHomeRemoved(long homeId) {
			// do something
		}
		@Override
		public void onHomeInfoChanged(long homeId) {
			// do something
		}
		@Override
		public void onSharedDeviceList(List<DeviceBean> sharedDeviceList) {
			// do something
		}
		@Override
		public void onSharedGroupList(List<GroupBean> sharedGroupList) {
			// do something
		}
		@Override
		public void onServerConnectSuccess() {
			// do something
		}
		@Override
		public void onHomeAdded(long homeId) {
			// do something
		}
	};
TuyaHomeSdk.getHomeManagerInstance().registerTuyaHomeChangeListener(listener);
```

### Cancel the information registration of home

**Description**

```java
void unRegisterTuyaHomeChangeListener(ITuyaHomeChangeListener listener)
```

**Parameters**

| Parameters | Description |
| :--------- | :---------- |
| listener   | Monitor     |

**Example**

```java
ITuyaHomeChangeListener listener = new ITuyaHomeChangeListener() {
		@Override
		public void onHomeInvite(long homeId, String homeName) {
			// do something
		}
		@Override
		public void onHomeRemoved(long homeId) {
			// do something
		}
		@Override
		public void onHomeInfoChanged(long homeId) {
			// do something
		}
		@Override
		public void onSharedDeviceList(List<DeviceBean> sharedDeviceList) {
			// do something
		}
		@Override
		public void onSharedGroupList(List<GroupBean> sharedGroupList) {
			// do something
		}
		@Override
		public void onServerConnectSuccess() {
			// do something
		}
		@Override
		public void onHomeAdded(long homeId) {
			// do something
		}
	};
TuyaHomeSdk.getHomeManagerInstance().registerTuyaHomeChangeListener(listener);
// ...
TuyaHomeSdk.getHomeManagerInstance().unRegisterTuyaHomeChangeListener(listener);
```

### Callback of MQTT service connection success

**Description**

```java
void onServerConnectSuccess()
```

**Example**

```java
ITuyaHomeChangeListener listener = new ITuyaHomeChangeListener() {
		@Override
		public void onHomeInvite(long homeId, String homeName) {
			// do something
		}
		@Override
		public void onHomeRemoved(long homeId) {
			// do something
		}
		@Override
		public void onHomeInfoChanged(long homeId) {
			// do something
		}
		@Override
		public void onSharedDeviceList(List<DeviceBean> sharedDeviceList) {
			// do something
		}
		@Override
		public void onSharedGroupList(List<GroupBean> sharedGroupList) {
			// do something
		}
		@Override
		public void onServerConnectSuccess() {
			// do something
		}
		@Override
		public void onHomeAdded(long homeId) {
			// do something
		}
	};

TuyaHomeSdk.getHomeManagerInstance().registerTuyaHomeChangeListener(listener);
// ...
TuyaHomeSdk.getHomeManagerInstance().unRegisterTuyaHomeChangeListener(listener);
```

## Get family details

Obtain all data under the family, including equipment, groups, rooms, etc

**Description**

```java
void getHomeDetail(ITuyaHomeResultCallback callback)
```

**Parameters**

| Parameters | Description             |
| :--------- | :---------------------- |
| callback   | Callback to get results |

**Example**

```java
TuyaHomeSdk.newHomeInstance(10000).getHomeDetail(new ITuyaHomeResultCallback() {
		@Override
		public void onError(String errorCode, String errorMsg) {
			// do something
		}
		@Override
		public void onSuccess(HomeBean bean) {
			// do something
		}
	});
```

## Get family offline details

Obtain all offline data under the family, including equipment, groups, rooms, and more.

**Description**

```java
void getHomeLocalCache(ITuyaHomeResultCallback callback)
```

**Parameters**

| Parameters | Description             |
| :--------- | :---------------------- |
| callback   | Callback to get results |

**Example**

```java
TuyaHomeSdk.newHomeInstance(10000).getHomeLocalCache(new ITuyaHomeResultCallback() {
		@Override
		public void onSuccess(HomeBean bean) {
			// do something
		}
		@Override
		public void onError(String errorCode, String errorMsg) {
			//sdk cache error do not deal
		}
	});
```

## Operate family cached data

`ITuyaHomeDataManager` provides the ability to access cached data, the interface entry is `TuyaHomeSdk.getDataInstance()`.

**Note**: Before obtaining this data, you should call the family's initialization interface `TuyaHomeSdk.newHomeInstance("homeId").getHomeDetail()` or `TuyaHomeSdk.newHomeInstance("homeId").getHomeLocalCache()`, then you can get it.

## Update family information

**Description**

```java
void updateHome(String name, double lon, double lat, String geoName, List<String> rooms, IResultCallback callback)
```

**Parameters**

| Parameters    | Description                                   |
| :------------ | :-------------------------------------------- |
| name          | Family name (Support up to **25** characters) |
| lon           | Longitude of current family                   |
| lat           | Latitude of the current family                |
| geoName       | Address of geographical location              |
| rooms         | Room information                              |
| callback      | Result callback                               |

**Example**

```java
TuyaHomeSdk.newHomeInstance(10000).updateHome(name, lon, lat, geoName, rooms, new IResultCallback() {
		@Override
		public void onError(String code, String error) {
			// do something
		}
		@Override
		public void onSuccess() {
			// do something
		}
	});
```

## Home weather

### Get home's weather simple summary parameters

Such as the state of weather(clear, cloudy, rainy, and so on), weather icon.

**Description**

```java
void getHomeWeatherSketch(double lon,double lat,IIGetHomeWetherSketchCallBack callback);
```

**Parameters**

| **Parameters** | **Description** |
| -------------- | --------------- |
| lon            | longitude       |
| lat            | latitude        |
| callback       | callback        |

WeatherBean Description

| **Parameters** | **Description**     |
| -------------- | ------------------- |
| condition      | weather description |
| temp           | temperature         |
| iconUrl        | weather icon        |
| inIconUrl      | weather detail icon        |

**Example**

```java
TuyaHomeSdk.newHomeInstance(mHomeId).getHomeWeatherSketch(120.075652,30.306265
new IIGetHomeWetherSketchCallBack() {
	@Override
	public void onSuccess(WeatherBean result) {
	}
	@Override
	public void onFailure(String errorCode, String errorMsg) {
	}
});
```

### Get home's weather summary parameters with more detail

Such as temperature, humidity, ultraviolet index, air quality.

**Description**

```java
void getHomeWeatherDetail(int limit, Map<String,Object> unit, IGetHomeWetherCallBack callback);
```

**Parameters**

| **Parameters** | **Description** |
| :------------- | :-------------- |
| limit          | list count      |
| unit           | parameter unit  |
| callback       | callback        |

More explanations about the unit:

| key           | description      | value                               |
| ------------- | ---------------- |------------------------------------ |
| tempUnit      | temperature unit |°C：1<br>°F：2                        |
| pressureUnit  | pressure unit    | hPa：1<br>inHg：2<br>mmHg：3<br>mb：4 |
| windspeedUnit | windspeed unit   | mph：1<br>m/s：2<br>kph：3<br>km/h：4 |

For example, when the unit of the data you want to obtain is Celsius unit:

```java
Map<String,Object> units = new HashMap<>();
units.put("tempUnit",1);
```

**Example**

```java
Map<String,Object> units = new HashMap<>();
units.put("tempUnit",1);   // °C
units.put("pressureUnit",1);  // hPa
units.put("windspeedUnit",2); // m/s

TuyaHomeSdk.newHomeInstance(mHomeId).getHomeWeatherDetail(10, units, new IGetHomeWetherCallBack() {
	@Override
	public void onSuccess(ArrayList<DashBoardBean> result) {

	}

	@Override
	public void onFailure(String errorCode, String errorMsg) {

	}
});
```



## Delete home

**Description**

```java
void dismissHome(IResultCallback callback)
```

**Parameters**

| Parameters | Description     |
| :--------- | :-------------- |
| callback   | Result callback |

**Example**

```java
TuyaHomeSdk.newHomeInstance(10000).dismissHome(new IResultCallback() {
		@Override
		public void onError(String code, String error) {
			// do something
		}
		@Override
		public void onSuccess() {
			// do something
		}
	});
```

## Sort groups or devices in the home

**Description**

```java
void sortDevInHome(String homeId, List<DeviceAndGroupInHomeBean> list, IResultCallback callback)
```

**Parameters**

| Parameters | Description                                                  |
| :--------- | :----------------------------------------------------------- |
| homeId     | Home ID                                                      |
| list       | Room or group list. The list element DeviceAndGroupInHomeBean here contains two fields. The one is `bizType`, which is the target type to sort. You can refer to BizParentTypeEnum for details. Another is `bizId`, which is the ID of the target, for example, group id or device id. |
| callback   | result callback                                              |

Meanings of the enum BizParentTypeEnum:

- LOCATION
- MESH
- ROOM
- GROUP
- DEVICE

**Example**

```java
List<DeviceAndGroupInHomeBean> list = new ArrayList<>();
List<DeviceBean> deviceList = homeBean.getDeviceList();
List<GroupBean> groupList = homeBean.getGroupList();
for (GroupBean bean : groupList) {
	DeviceAndGroupInHomeBean deviceInRoomBean = new DeviceAndGroupInHomeBean();
	deviceInRoomBean.setBizId(bean.getDevId());
	deviceInRoomBean.setBizType(BizParentTypeEnum.GROUP.getType());
	list.add(deviceInRoomBean);
}
for (DeviceBean bean: deviceList) {
	DeviceAndGroupInHomeBean deviceInRoomBean = new DeviceAndGroupInHomeBean();
	deviceInRoomBean.setBizId(bean.getDevId());
	deviceInRoomBean.setBizType(BizParentTypeEnum.DEVICE.getType());
	list.add();
}
TuyaHomeSdk.newHomeInstance(10000).sortDevInHome(homeId, list, new IResultCallback() {
		@Override
		public void onSuccess() {
			// do something
		}
		@Override
		public void onError(String code, String error) {
			// do something
		}
	});

		// @param allDeviceBeans Contains all devices and groups in current home.
Collections.sort(allDeviceBeans, new Comparator<DeviceBean>() {
                @Override
                public int compare(DeviceBean o1, DeviceBean o2) {
                    return o1.getHomeDisplayOrder() - o2.getHomeDisplayOrder();
                }
            });
```

## Log off monitoring of information changes below the home

**Description**

```java
void unRegisterHomeStatusListener(ITuyaHomeStatusListener listener)
```

**Parameters**

| Parameters | Description |
| :--------- | :---------- |
| listener   | Monitor     |

**Example**

```java
// define a listener
ITuyaHomeStatusListener listener = new ITuyaHomeStatusListener() {
		@Override
		public void onDeviceAdded(String devId) {
			// do something
		}
		@Override
		public void onDeviceRemoved(String devId) {
			// do something
		}
		@Override
		public void onGroupAdded(long groupId) {
			// do something
		}
		@Override
		public void onGroupRemoved(long groupId) {
			// do something
		}
		@Override
		public void onMeshAdded(String meshId) {
			// do something
		}
	};
// register a listener somewhere
TuyaHomeSdk.newHomeInstance(10000).registerHomeStatusListener(listener);
// ...
// unregister a listener
TuyaHomeSdk.newHomeInstance(10000).unRegisterHomeStatusListener(listener);
```

##
