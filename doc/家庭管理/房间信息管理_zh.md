## 新增房间

**接口说明**

```java
void addRoom(String name, ITuyaRoomResultCallback callback)
```

**参数说明**

| 参数     | 说明     |
| :------- | :------- |
| name     | 房间名称 |
| callback | 结果回调 |

**示例代码**

```java
TuyaHomeSdk.newHomeInstance(10000).addRoom("房间名称", new ITuyaRoomResultCallback() {
		@Override
		public void onSuccess(RoomBean bean) {
			// do something
		}
		@Override
		public void onError(String errorCode, String errorMsg) {
			// do something
		}
	});
```

## 删除房间

**接口说明**

```java
void removeRoom(long roomId, IResultCallback callback)
```

**参数说明**

| 参数     | 说明     |
| :------- | :------- |
| roomId   | 房间 ID  |
| callback | 结果回调 |

**示例代码**

```java
TuyaHomeSdk.newHomeInstance(10000).removeRoom(roomId, new IResultCallback() {
		@Override
		public void onSuccess() {
			// do something
		}
		@Override
		public void onError(String code, String error) {
			// do something
		}
	});
```

## 房间排序

**接口说明**

```java
void sortRoom(List<Long> idList, IResultCallback callback)
```

**参数说明**

| 参数     | 说明         |
| :------- | :----------- |
| idList   | 房间 ID 列表 |
| callback | 结果回调     |

**示例代码**

```java
TuyaHomeSdk.newHomeInstance(10000).sortRoom(idList, new IResultCallback() {
		@Override
		public void onSuccess() {
			// do something
		}
		@Override
		public void onError(String code, String error) {
			// do something
		}
	});
```

## 根据设备获取房间信息

**接口说明**

通过设备ID查询房间信息。

```java
RoomBean getDeviceRoomBean(String devId)
```

**参数说明**

| 参数  | 说明   |
| :---- | :----- |
| devId | 设备ID |

**示例代码**

```java
TuyaHomeSdk.getDataInstance().getDeviceRoomBean("设备ID");
```

## 更新房间名称

**接口说明**

```java
void updateRoom(String name, IResultCallback callback)
```

**参数说明**

| 参数 | 说明       |
| :--- | :--------- |
| name | 新房间名称 |

**示例代码**

```java
TuyaHomeSdk.newRoomInstance(10000).updateRoom(name, new IResultCallback() {
		@Override
		public void onSuccess() {
			// do something
		}
		@Override
		public void onError(String code, String error) {
			// do something
		}
	});
```

## 自定义房间图片

房间可支持自定义背景图片，上传成功后可通过下述方法获取房间图片地址。（此方法为 3.19 版本新增）。

```java
RoomBean roomBean = homeBean.rooms.get(index);
String roomBgImageurl = roomBean.iconUrl;
```

**接口说明**

```java
void updateIcon(File file, IResultCallback callback);
```

**参数说明**

| 参数     | 说明     |
| -------- | -------- |
| file     | 房间图片 |
| callback | 回调     |

**示例代码**

```java
TuyaHomeSdk.newRoomInstance(10000).updateIcon(file, new IResultCallback() {
		@Override
		public void onSuccess() {
			// do something
		}
		@Override
		public void onError(String code, String error) {
			// do something
		}
	});
```

## 添加设备到房间

**接口说明**

```java
void addDevice(String devId, IResultCallback callback)
```

**参数说明**

| 参数  | 说明    |
| :---- | :------ |
| devId | 设备 ID |

**示例代码**

```java
TuyaHomeSdk.newRoomInstance(10000).addDevice(devId, new IResultCallback() {
		@Override
		public void onSuccess() {
			// do something
		}
		@Override
		public void onError(String code, String error) {
			// do something
		}
	});
```

## 从房间中移除设备

**接口说明**

```java
void removeDevice(String devId, IResultCallback callback)
```

**参数说明**

| 参数  | 说明    |
| :---- | :------ |
| devId | 设备 ID |

**示例代码**

```java
TuyaHomeSdk.newRoomInstance(10000).removeDevice(devId, new IResultCallback() {
		@Override
		public void onSuccess() {
			// do something
		}
		@Override
		public void onError(String code, String error) {
			// do something
		}
	});
```

## 添加群组到房间

**接口说明**

```java
void addGroup(long groupId, IResultCallback callback)
```

**参数说明**

| 参数    | 说明    |
| :------ | :------ |
| groupId | 群组 ID |

**示例代码**

```java
TuyaHomeSdk.newRoomInstance(10000).addGroup(groupId, new IResultCallback() {
		@Override
		public void onSuccess() {
			// do something
		}
		@Override
		public void onError(String code, String error) {
			// do something
		}
	});
```

## 从房间中移除群组

**接口说明**

```java
void removeGroup(Long groupId, IResultCallback resultCallback)
```

**参数说明**

| 参数    | 说明    |
| :------ | :------ |
| groupId | 群组 ID |

**示例代码**

```java
TuyaHomeSdk.newRoomInstance(10000).removeGroup(groupId, new IResultCallback() {
		@Override
		public void onSuccess() {
			// do something
		}
		@Override
		public void onError(String code, String error) {
			// do something
		}
	});
```

## 批量修改房间与群组、设备的关系

**接口说明**

此方法可以用来批量地将设备和群组移入或者移出房间。

> 注意：借助此方法，可以实现房间下的设备排序。调用此方法，传入你要排序的设备集合`DeviceAndGroupInRoomBean`对象，然后通过[查询家庭详细信息](https://developer.tuya.com/cn/docs/app-development/familyrelations?id=Ka6ki8h2c2yo5#title-7-%E6%9F%A5%E8%AF%A2%E5%AE%B6%E5%BA%AD%E8%AF%A6%E7%BB%86%E4%BF%A1%E6%81%AF)获取到`HomeBean`,从中获取到所有设备列表，再从所有设备列表中根据设备id查找出你调用`moveDevGroupListFromRoom`方法传入的的设备列表，再根据`DeviceBean`中的`displayOrder`字段本地自己排序。

```java
void moveDevGroupListFromRoom(List<DeviceAndGroupInRoomBean> list, IResultCallback callback)
```

**参数说明**

| 参数 | 说明         |
| :--- | :----------- |
| list | 群组或者设备 |

其中，**DeviceAndGroupInRoomBean** 的数据格式如下所示：

| 参数 | 参数类型 | 说明                               |
| ---- | -------- | ---------------------------------- |
| id   | String   | 设备或者群组ID                     |
| type | int      | 类型（设备为 **6**，群组为 **5**） |

**示例代码**

```java
TuyaHomeSdk.newRoomInstance(10000).moveDevGroupListFromRoom(list, new IResultCallback() {
		@Override
		public void onSuccess() {
			// do something
		}
		@Override
		public void onError(String code, String error) {
			// do something
		}
	});
```