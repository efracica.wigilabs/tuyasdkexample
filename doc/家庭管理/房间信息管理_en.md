## Add room

**Description**

```java
void addRoom(String name, ITuyaRoomResultCallback callback)
```

**Parameters**

| Parameters | Description     |
| :--------- | :-------------- |
| name       | Room name       |
| callback   | Result callback |

**Example**

```java
TuyaHomeSdk.newHomeInstance(10000).addRoom(name, new ITuyaRoomResultCallback() {
		@Override
		public void onError(String errorCode, String errorMsg) {
			// do something
		}
		@Override
		public void onSuccess(RoomBean bean) {
			// do something
		}
	});
```

## Remove room

**Description**

```java
void removeRoom(long roomId, IResultCallback callback)
```

**Parameters**

| Parameters | Description     |
| :--------- | :-------------- |
| roomId     | Room ID         |
| callback   | Result callback |

**Example**

```java
TuyaHomeSdk.newHomeInstance(10000).removeRoom(roomId, new IResultCallback() {
		@Override
		public void onError(String code, String error) {
			// do something
		}
		@Override
		public void onSuccess() {
			// do something
		}
	});
```

## Sorting rooms

**Description**

```java
void sortRoom(List<Long> idList, IResultCallback callback)
```

**Parameters**

| Parameters | Description     |
| :--------- | :-------------- |
| idList     | Room ID list    |
| callback   | Result callback |

**Example**

```java
TuyaHomeSdk.newHomeInstance(10000).sortRoom(idList, new IResultCallback() {
		@Override
		public void onError(String code, String error) {
			// do something
		}
		@Override
		public void onSuccess() {
			// do something
		}
	});
```

## Get room information based on devId

**Description**

```java
void getDeviceRoomBean(String devId)
```

**Parameters**

| Parameters | Description |
| :--------- | :---------- |
| devId      | device ID   |

**Example**

```java
TuyaHomeSdk.getDataInstance().getDeviceRoomBean(devId);
```

## Update room name

**Description**

```java
void updateRoom(String name, IResultCallback callback)
```

**Parameters**

| Parameters | Description   |
| :--------- | :------------ |
| name       | New room name |

**Example**

```java
TuyaHomeSdk.newRoomInstance(10000).updateRoom(name, new IResultCallback() {
		@Override
		public void onError(String code, String error) {
			// do something
		}
		@Override
		public void onSuccess() {
			// do something
		}
	});
```

## Update room icon

Room support custom background image,  you can get the room background image URL as below after upload succeeded. (This method is new in version 3.19)

```java
RoomBean roomBean = homeBean.rooms.get(index);
String roomBgImageurl = roomBean.iconUrl;
```

**Description**

```java
void updateIcon(File file, IResultCallback callback);
```

**Parameters**

| Parameters | Description |
| ---------- | ----------- |
| file       | Room image  |
| callback   | callback    |

**Example**

```java
TuyaHomeSdk.newRoomInstance(10000).updateIcon(file, new IResultCallback() {
		@Override
		public void onSuccess() {
			// do something
		}
		@Override
		public void onError(String code, String error) {
			// do something
		}
	});
```

## Add device to a room

**Description**

```java
void addDevice(String devId, IResultCallback callback)
```

**Parameters**

| Parameters | Description |
| :--------- | :---------- |
| devId      | Device ID   |

**Example**

```java
TuyaHomeSdk.newRoomInstance(10000).addDevice(devId, new IResultCallback() {
		@Override
		public void onError(String code, String error) {
			// do something
		}
		@Override
		public void onSuccess() {
			// do something
		}
	});
```

## Remove a device from a room

**Description**

```java
void removeDevice(String devId, IResultCallback callback)
```

**Parameters**

| Parameters | Description |
| :--------- | :---------- |
| devId      | Device ID   |

**Example**

```java
TuyaHomeSdk.newRoomInstance(10000).removeDevice(devId, new IResultCallback() {
		@Override
		public void onError(String code, String error) {
			// do something
		}
		@Override
		public void onSuccess() {
			// do something
		}
	});
```

## Add group in a room

**Description**

```java
void addGroup(long groupId, IResultCallback callback)
```

**Parameters**

| Parameters | Description |
| :--------- | :---------- |
| groupId    | Group ID    |

**Example**

```java
TuyaHomeSdk.newRoomInstance(10000).addGroup(groupId, new IResultCallback() {
		@Override
		public void onError(String code, String error) {
			// do something
		}
		@Override
		public void onSuccess() {
			// do something
		}
	});
```

## Remove group in a room

**Description**

```java
void removeGroup(Long groupId, IResultCallback resultCallback)
```

**Parameters**

| Parameters | Description |
| :--------- | :---------- |
| groupId    | Group ID    |

**Example**

```java
TuyaHomeSdk.newRoomInstance(10000).removeGroup(groupId, new IResultCallback() {
		@Override
		public void onError(String code, String error) {
			// do something
		}
		@Override
		public void onSuccess() {
			// do something
		}
	});
```

## Remove group or device from room

**Description**

This method can be used to move devices and groups into or out of the room in batches.

> Note: With the help of this method, you can sort the devices under the room. Call this method, pass in the device collection `DeviceAndGroupInRoomBean` object that you want to sort, then get `HomeBean` by [query family details](https://developer.tuya.com/en/docs/app-development/familyrelations?id=Ka6ki8h2c2yo5#title-7-%E6%9F%A5%E8%AF%A2%E5%AE%B6%E5%BA%AD%E8%AF%A6%E7%BB%86%E4%BF%A1%E6%81%AF), get the list of all devices from it, then find out the list of devices you call ` moveDevGroupListFromRoom` method, and then sort it locally according to the `displayOrder` field in the `DeviceBean`.

```java
void moveDevGroupListFromRoom(List list, IResultCallback callback)
```

**Parameters**

| Parameters | Description     |
| :--------- | :-------------- |
| list       | Group or device |

The data format of **DeviceAndGroupInRoomBean** is as follows:

| Parameters | Type   | Description                                                  |
| ---------- | ------ | ------------------------------------------------------------ |
| id         | String | Device or group ID                                           |
| type       | int    | Type ( device type returns **6**, group type returns **5** ) |

**Example**

```java
TuyaHomeSdk.newRoomInstance(10000).moveDevGroupListFromRoom(list, new IResultCallback() {
		@Override
		public void onError(String code, String error) {
			// do something
		}
		@Override
		public void onSuccess() {
			// do something
		}
	});
```

## Query room info by devId

**Description**

```java
RoomBean getDeviceRoomBean(String devId);
```

**Parameters**

| Parameters | Description |
| :--------- | :---------- |
| devId      | device Id   |

**Example**

```java
TuyaHomeSdk.getDataInstance().getDeviceRoomBean(String devId);
```

## Query room info by groupId

**Description**

```java
RoomBean getGroupRoomBean(String devId);
```

**Parameters**

| Parameters | Description |
| :--------- | :---------- |
| groupId    | groupd Id   |

**Example**

```java
TuyaHomeSdk.getDataInstance().getGroupRoomBean(long groupId);
```