## Add home member

**Description**

The field autoAccept in `MemberWrapperBean` was used to control if the member added should accept the invitation. If the value was false, the added member should call `processInvitation()`, and then he/she can join the family.

```java
void addMember(MemberWrapperBean memberWrapperBean, ITuyaDataCallback<MemberBean> callback)
```

**Parameters**

| Parameters        | Description        |
| :---------------- | :----------------- |
| memberWrapperBean | Member information |

**MemberWrapperBean description**

| Parameters       | Type    | Description                                                  |
| :--------------- | :------ | :----------------------------------------------------------- |
| homeId           | long    | ID of the family to add members to                                     |
| nickName         | String  | Nicknames for invitees                                        |
| admin            | boolean | Whether is family administrator or not                                          |
| memberId         | long    | Family member ID                                              |
| headPic          | String  | Invitees' personal avatars are used when the avatar set for the invitee is nil             |
| account          | String  | Invited account                                                   |
| countryCode      | String  | Invited account corresponding to the country code                                      |
| invitationCode   | String  | Invited code                                               |
| role             | int     | MemberRole (view MemberRole definition type)                              |
| autoAccept       | boolean | Does the invitee need to agree to accept the invitation true - the invited account automatically accepts the family invitation without confirmation from the invitee false - the invitee needs to agree before joining the family |

**Example**

```java
TuyaHomeSdk.getMemberInstance().addMember(memberWrapperBean, new ITuyaDataCallback<MemberBean>() {
		@Override
		public void onError(String errorCode, String errorMessage) {
			// do something
		}
		@Override
		public void onSuccess(MemberBean result) {
			// do something
		}
	});
```

## Delete home member

**Description**

```java
void removeMember(long memberId, IResultCallback callback)
```

**Parameters**

| Parameters | Description |
| :--------- | :---------- |
| memberId   | Member ID   |

**Example**

```java
TuyaHomeSdk.getMemberInstance().removeMember(memberId, new IResultCallback() {
		@Override
		public void onError(String code, String error) {
			// do something
		}
		@Override
		public void onSuccess() {
			// do something
		}
	});
```

## Query the member list under home

**Description**

```java
void queryMemberList(long mHomeId, ITuyaGetMemberListCallback callback)
```

**Parameters**

| Parameters | Description |
| :--------- | :---------- |
| mHomeId    | Family ID   |

**Example**

```java
TuyaHomeSdk.getMemberInstance().queryMemberList(mHomeId, new ITuyaGetMemberListCallback() {
		@Override
		public void onError(String errorCode, String error) {
			// do something
		}
		@Override
		public void onSuccess(List<MemberBean> memberBeans) {
			// do something
		}
	});
```
## Update member information

**Description**

```java
void updateMember(MemberWrapperBean memberWrapperBean, IResultCallback callback)
```

**Parameters**

| Parameters        |  Type       |  Description         |
| :---------------- | ------------| :-----------------   |
| memberId          |  long       |  Family member id     |
| name              |  name       |  Member name             |
| admin             |  boolean    |  Set to true if he is an administrator  |

**Example**

```java
TuyaHomeSdk.getMemberInstance().updateMember(memberId, name, admin, new IResultCallback() {
        @Override
        public void onSuccess() {
            // do something
        }
        @Override
        public void onError(String code, String error) {
            // do something
        }
     });
```

## Update member information(Recommend)

**Description**

```java
void updateMember(MemberWrapperBean memberWrapperBean, IResultCallback callback)
```

**Parameters**

| Parameters        |  Type                |  Description         |
| :---------------- | ---------------------|   :----------------- |
| memberWrapperBean |  MemberWrapperBean   |  Member information  |

**Example**

```java
TuyaHomeSdk.getMemberInstance().updateMember(memberWrapperBean, new IResultCallback() {
		@Override
		public void onError(String code, String error) {
			// do something
		}
		@Override
		public void onSuccess() {
			// do something
		}
	});
```

## Get the invitation code to add a family member

**Description**

```java
void getInvitationMessage(long homeId, ITuyaDataCallback callback)
```

**Parameters**

| Parameters | Description |
| ---------- | ----------- |
| homeId     | Family ID   |

**Example**

```java
TuyaHomeSdk.getMemberInstance().getInvitationMessage(homeId, new ITuyaDataCallback<InviteMessageBean>() {
			@Override
			public void onSuccess(InviteMessageBean result) {
				// onSuc
			}

			@Override
			public void onError(String errorCode, String errorMessage) {
				// onErr
			}
		});
```
## Add a family by the invitation code

**Description**

```java
void joinHomeByInviteCode(String code, IResultCallback callback);
```
**Parameters**

| Parameters | Description |
| ---------- | ----------- |
| code     |  invitation code |

**Example**

```java
TuyaHomeSdk.getHomeManagerInstance().joinHomeByInviteCode("code" , new IResultCallback() {
            @Override
            public void onError(String code, String error) {

            }

            @Override
            public void onSuccess() {

            }
        });
```

## Withdraw family invitation

**Description**

```java
void cancelMemberInvitationCode(long invitationId, IResultCallback callBack)
```

**Parameters**

| Parameters   | Description   |
| :----------- | :------------ |
| invitationId | Invitation ID |

**Example**

```java
TuyaHomeSdk.getMemberInstance().cancelMemberInvitationCode(invitationId, new IResultCallback() {
		@Override
		public void onSuccess() {
			// do something
		}
		@Override
		public void onError(String code, String error) {
			// do something
		}
	});
```

## Accept or refuse to join the family

**Description**

```java
void processInvitation(long homeId, boolean action, IResultCallback callBack)
```

**Parameters**

| Parameters | Description      |
| :--------- | :--------------- |
| homeId     | Family ID        |
| action     | Accept \/ reject |

**Example**

```java
TuyaHomeSdk.getMemberInstance().processInvitation(homeId, action, new IResultCallback() {
		@Override
		public void onError(String code, String error) {
			// do something
		}
		@Override
		public void onSuccess() {
			// do something
		}
	});
```

## Get the record of invited members

**Description**

```java
void getInvitationList(long homeId, ITuyaDataCallback callback)
```

**Parameters**

| Parameters | Description |
| :--------- | :---------- |
| homeId     | Family ID   |

**Example**

```java
TuyaHomeSdk.getMemberInstance().getInvitationList(homeId, new ITuyaDataCallback() {
		@Override
		public void onSuccess(List<InvitedMemberBean> memberBeans) {
			// do something
		}
		@Override
		public void onError(String errorCode, String error) {
			// do something
		}
	});
```

## Update information of invited member

**Description**

```java
void updateInvitedMember(long invitationId, String memberName, int memberRole, IResultCallback callBack)
```

**Parameters**

| Parameters   | Description                   |
| :----------- | :---------------------------- |
| invitationId | Invitation ID                 |
| memberName   | Remark name of invited member |
| memberRole   | Role of invited member        |

**Example**

```java
TuyaHomeSdk.getMemberInstance().updateInvitedMember(invitationId, memberName, membeRole, new IResultCallback() {
		@Override
		public void onSuccess() {
			// do something
		}
		@Override
		public void onError(String code, String error) {
			// do something
		}
	});
```