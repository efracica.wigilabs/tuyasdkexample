
本文介绍了家庭信息的获取和管理能力等 Android 版接口信息。包括创建家庭、查询家庭列表、家庭信息变化监听、查询家庭信息、修改家庭信息、查询家庭天气信息、设置家庭中设备和群组的顺序、注册或注销家庭信息变更的监听等。

## 功能说明

- `ITuyaHomeManager` 提供了添加家庭、获取家庭列表以及监听家庭相关的变更的能力，类为 `TuyaHomeSdk.getHomeManagerInstance()`。

- `HomeBean` 数据类型：

	| 属性 | 类型 | 说明 |
	| :---- | :---- | :---- |
	| name | String | 家庭名称 |
	| geoName | String | 家庭地理位置名称 |
	| lon | Double | 经度 |
	| lat | Double | 纬度 |
	| homeId | Long | 家庭 ID |
	| admin | Boolean | 管理员身份 |
	| homeStatus | Integer | 加入家庭要求的状态 <ul><li> 1：等待接受 </li><li> 2：接受 </li><li> 3：拒绝 </li></ul> |
	| role | Integer | 家庭成员权限： <ul><li> -1：自定义成员 </li><li> 0：家庭成员 </li><li> 1：管理员 </li><li> 2：拥有者 </li><li> -999：无效成员，成员会失效 </li></ul> |
	| rooms | List&lt;RoomBean&gt; | 所有房间列表 |
	| deviceList | List&lt;DeviceBean&gt; | 所有设备列表 |
	| groupList | List&lt;GroupBean&gt; | 所有群组 |
	| meshList | List&lt;BlueMeshBean&gt; | 网关设备 |
	| sharedDeviceList | List&lt;DeviceBean&gt; | 收到的共享设备 |
	| sharedGroupList | List&lt;GroupBean&gt; | 收到的共享群组 |

## 创建家庭

**接口说明**

```java
void createHome(String name, double lon, double lat, String geoName, List<String> rooms, ITuyaHomeResultCallback callback)
```

**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| name | 家庭名称，最多支持 **25** 个字符 |
| lon | 经度，如果不设置家庭位置信息，请设置为 **`0`** |
| lat | 纬度，如果不设置家庭位置信息，请设置为 **`0`** |
| geoName | 家庭地理位置名称 |
| rooms | 房间列表 |
| callback | 获取结果的回调 |

**Java 示例**

```java
TuyaHomeSdk.getHomeManagerInstance().createHome(name, lon, lat, geoName, rooms, new ITuyaHomeResultCallback() {
		@Override
		public void onSuccess(HomeBean bean) {
			// do something
		}
		@Override
		public void onError(String errorCode, String errorMsg) {
			// do something
		}
	});
```

## 查询家庭列表

**接口说明**

```java
void queryHomeList(ITuyaGetHomeListCallback callback)
```

**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| callback | 获取结果的回调 |

**Java 示例**

```java
TuyaHomeSdk.getHomeManagerInstance().queryHomeList(new ITuyaGetHomeListCallback() {
		@Override
		public void onSuccess(List<HomeBean> homeBeans) {
			// do something
		}
		@Override
		public void onError(String errorCode, String error) {
			// do something
		}
	});
```

## 家庭信息变化监听

家庭信息变化的时机包括家庭增加、邀请、删除、信息变更、分享列表的变更、服务器连接成功的监听等。

**接口说明**

```java
public interface ITuyaHomeChangeListener {
	/**
	* 家庭添加成功
	* 用于多设备数据同步
	*
	* @param homeId
	*/
	void onHomeAdded(long homeId);

	/**
	* 家庭邀请
	* @param homeId    家庭ID
	* @param homeName  家庭名称
	*/
	void onHomeInvite(long homeId,String homeName);

	/**
	* 家庭删除成功
	* 用于多设备数据同步
	*
	* @param homeId
	*/
	void onHomeRemoved(long homeId);

	/**
	* 家庭信息变更
	* 用于多设备数据同步
	*
	* @param homeId
	*/
	void onHomeInfoChanged(long homeId);

	/**
	* 分享设备列表变更
	* 用于多设备数据同步
	*
	* @param sharedDeviceList
	*/
	void onSharedDeviceList(List<DeviceBean> sharedDeviceList);

	/**
	*
	* 手机连接涂鸦 IoT 云成功，接收到此通知，
	* 本地数据与服务端数据可能会不一致或者无法控制设备，
	* 您可以调用 Home 下 getHomeDetail 接口重新初始化数据。
	*/
	void onServerConnectSuccess();
}
```

### 新增一个家庭监听

**接口说明**

```java
void registerTuyaHomeChangeListener(ITuyaHomeChangeListener listener)
```

**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| listener | 监听器 |

**Java 示例**

```java
// 定义监听
ITuyaHomeChangeListener listener = new ITuyaHomeChangeListener() {
		@Override
		public void onHomeInvite(long homeId, String homeName) {
			// do something
		}
		@Override
		public void onHomeRemoved(long homeId) {
			// do something
		}
		@Override
		public void onHomeInfoChanged(long homeId) {
			// do something
		}
		@Override
		public void onSharedDeviceList(List<DeviceBean> sharedDeviceList) {
			// do something
		}
		@Override
		public void onSharedGroupList(List<GroupBean> sharedGroupList) {
			// do something
		}
		@Override
		public void onServerConnectSuccess() {
			// do something
		}
		@Override
		public void onHomeAdded(long homeId) {
			// do something
		}
	};
// 注册监听
TuyaHomeSdk.getHomeManagerInstance().registerTuyaHomeChangeListener(listener);
```

### 删除一个家庭监听

**接口说明**

```java
void unRegisterTuyaHomeChangeListener(ITuyaHomeChangeListener listener)
```

**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| listener | 监听器 |

**Java 示例**

```java
// 定义监听
ITuyaHomeChangeListener listener = new ITuyaHomeChangeListener() {
		@Override
		public void onHomeInvite(long homeId, String homeName) {
			// do something
		}
		@Override
		public void onHomeRemoved(long homeId) {
			// do something
		}
		@Override
		public void onHomeInfoChanged(long homeId) {
			// do something
		}
		@Override
		public void onSharedDeviceList(List<DeviceBean> sharedDeviceList) {
			// do something
		}
		@Override
		public void onSharedGroupList(List<GroupBean> sharedGroupList) {
			// do something
		}
		@Override
		public void onServerConnectSuccess() {
			// do something
		}
		@Override
		public void onHomeAdded(long homeId) {
			// do something
		}
	};
// 注册监听
TuyaHomeSdk.getHomeManagerInstance().registerTuyaHomeChangeListener(listener);
// ...
// 取消注册监听
TuyaHomeSdk.getHomeManagerInstance().unRegisterTuyaHomeChangeListener(listener);
```

### 注册 MQTT 服务连接成功回调

**接口说明**

```java
void onServerConnectSuccess()
```

**Java 示例**

```java
// 定义监听
ITuyaHomeChangeListener listener = new ITuyaHomeChangeListener() {
		@Override
		public void onHomeInvite(long homeId, String homeName) {
			// do something
		}
		@Override
		public void onHomeRemoved(long homeId) {
			// do something
		}
		@Override
		public void onHomeInfoChanged(long homeId) {
			// do something
		}
		@Override
		public void onSharedDeviceList(List<DeviceBean> sharedDeviceList) {
			// do something
		}
		@Override
		public void onSharedGroupList(List<GroupBean> sharedGroupList) {
			// do something
		}
		@Override
		public void onServerConnectSuccess() {
			// do something
		}
		@Override
		public void onHomeAdded(long homeId) {
			// do something
		}
	};
// 注册监听
TuyaHomeSdk.getHomeManagerInstance().registerTuyaHomeChangeListener(listener);
// ...
// 取消注册监听
TuyaHomeSdk.getHomeManagerInstance().unRegisterTuyaHomeChangeListener(listener);
```

## 查询家庭详细信息

获取家庭下的所有数据，包括设备、群组、房间等。

**接口说明**

```java
void getHomeDetail(ITuyaHomeResultCallback callback)
```

**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| callback | 获取结果的回调 |

**Java 示例**

```java
TuyaHomeSdk.newHomeInstance(10000).getHomeDetail(new ITuyaHomeResultCallback() {
		@Override
		public void onSuccess(HomeBean bean) {
			// do something
		}
		@Override
		public void onError(String errorCode, String errorMsg) {
			// do something
		}
	});
```

## 查询家庭离线的详细信息

获取家庭下的所有离线数据，包括设备、群组、房间等。

**接口说明**

```java
void getHomeLocalCache(ITuyaHomeResultCallback callback)
```

**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| callback | 获取结果的回调 |

**Java 示例**

```java
TuyaHomeSdk.newHomeInstance(10000).getHomeLocalCache(new ITuyaHomeResultCallback() {
		@Override
		public void onSuccess(HomeBean bean) {
			// do something
		}
		@Override
		public void onError(String errorCode, String errorMsg) {
			//sdk cache error do not deal
		}
	});
```

## 家庭缓存数据

`ITuyaHomeDataManager` 提供了缓存数据的访问能力，接口为 `TuyaHomeSdk.getDataInstance()`。

<!-- ITuyaHomeDataManager 是个啥？有没有访问链接 -->

>**注意**：获取家庭缓存数据前，您应该调用家庭的初始化接口 `TuyaHomeSdk.newHomeInstance("homeId").getHomeDetail()` 或者 `TuyaHomeSdk.newHomeInstance("homeId").getHomeLocalCache()`，然后 `TuyaHomeSdk.getDataInstance()` 接口才会有缓存数据。

## 修改家庭信息

### 新版接口

<!-- 以什么版本号为界限 -->

**接口说明**

```java
void updateHome(String name, double lon, double lat, String geoName, List<String> rooms, IResultCallback callback)
```

**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| name | 家庭名称，最多支持 **25** 个字符 |
| lon | 当前家庭的经度 |
| lat | 当前家庭的纬度 |
| geoName | 地理位置的地址 |
| rooms | 房间信息 |
| callback | 获取结果的回调 |

**Java 示例**

```java
TuyaHomeSdk.newHomeInstance(10000).updateHome(name, lon, lat, geoName, rooms, new IResultCallback() {
		@Override
		public void onError(String code, String error) {
			// do something
		}
		@Override
		public void onSuccess() {
			// do something
		}
	});
```

### 旧版接口

<!-- 以什么版本号为界限 -->

**接口说明**

```java
void updateHome(String name, double lon, double lat, String geoName, IResultCallback callback)
```

**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| name | 家庭名称 |
| lon | 当前家庭的经度 |
| lat | 当前家庭的纬度 |
| geoName | 地理位置的地址 |
| callback | 获取结果的回调 |

**Java 示例**

```java
TuyaHomeSdk.newHomeInstance(10000).updateHome(name, lon, lat, geoName, new IResultCallback() {
		@Override
		public void onSuccess() {
			// do something
		}
		@Override
		public void onError(String code, String error) {
			// do something
		}
	});
```

<!-- 为什么有这么多 do something？ -->

## 家庭天气

### 查询家庭天气的简要参数

**接口说明**

查询家庭所在城市的简要天气参数。包括天气状况（晴、多云、雨等）和天气图片信息。

```java
void getHomeWeatherSketch(double lon,double lat,IIGetHomeWetherSketchCallBack callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| lon | 经度 |
| lat | 纬度 |
| callback | 获取结果的回调 |

**`WeatherBean` 说明**

| 参数 | 说明 |
| ---- | ---- |
| condition | 天气情况，例如晴，阴，雨等 |
| temp | 温度 |
| iconUrl | 天气图标 |
| inIconUrl | 天气详情页图标 |

**Java 示例**

```java
TuyaHomeSdk.newHomeInstance(mHomeId).getHomeWeatherSketch(120.075652,30.306265
new IIGetHomeWetherSketchCallBack() {
	@Override
	public void onSuccess(WeatherBean result) {
	}
	@Override
	public void onFailure(String errorCode, String errorMsg) {
	}
});
```

### 查询家庭天气的详细参数

- 获取家庭天气详细参数，如温度、湿度、紫外线指数、空气质量等。

- 由于天气服务在不同地区的使用的服务不同，不同地区返回的参数有可能不同。

- 如果当前家庭账号在中国地区，则不会返回风速和气压信息。

**接口说明**

```java
void getHomeWeatherDetail(int limit, Map<String,Object> unit, IGetHomeWetherCallBack callback);
```

**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| limit | 获取的数量 |
| unit | 单位 |
| callback | 获取结果的回调 |

**unit 说明**

| unit 键 | unit 说明 | unit 值 |
| ---- | ---- |---- |
| tempUnit | 温度单位 |°C：1<br>°F：2 |
| pressureUnit | 气压单位| hPa：1<br>inHg：2<br>mmHg：3<br>mb：4 |
| windspeedUnit | 风速单位| mph：1<br>m/s：2<br>kph：3<br>km/h：4 |


例如，获取摄氏度时，可以将 `tempUnit` 设置为 `1`：

```java
Map<String,Object> units = new HashMap<>();
units.put("tempUnit",1);
```

**Java 示例**

```java
Map<String,Object> units = new HashMap<>();
units.put("tempUnit",1);   // °C
units.put("pressureUnit",1);  // hPa
units.put("windspeedUnit",2); // m/s

TuyaHomeSdk.newHomeInstance(mHomeId).getHomeWeatherDetail(10, units, new IGetHomeWetherCallBack() {
	@Override
	public void onSuccess(ArrayList<DashBoardBean> result) {

	}

	@Override
	public void onFailure(String errorCode, String errorMsg) {

	}
});
```

## 注销家庭

**接口说明**

```java
void dismissHome(IResultCallback callback)
```

**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| callback | 获取结果的回调 |

**Java 示例**

```java
TuyaHomeSdk.newHomeInstance(10000).dismissHome(new IResultCallback() {
		@Override
		public void onSuccess() {
			// do something
		}
		@Override
		public void onError(String code, String error) {
			// do something
		}
	});
```

## 设置家庭中设备和群组的顺序

**接口说明**

```java
void sortDevInHome(String homeId, List<DeviceAndGroupInHomeBean> list, IResultCallback callback)
```

**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| homeId | 家庭 ID |
| list | 房间或者群组列表。元素 `DeviceAndGroupInHomeBean` 包含两个字段：<ul><li>`bizType`：被排序的对象的类型，例如是群组或者设备，是一个整数类型的枚举，参考 `BizParentTypeEnum` 对象</li><li>`bizId`：被排序的对象的 ID，例如群组 ID 或者设备 ID</li></ul> |
| callback | 获取结果的回调 |

`BizParentTypeEnum` 对象的枚举值：

- LOCATION
- MESH
- ROOM
- GROUP
- DEVICE

**Java 示例**

```java
List<DeviceAndGroupInHomeBean> list = new ArrayList<>();
List<DeviceBean> deviceList = homeBean.getDeviceList();
List<GroupBean> groupList = homeBean.getGroupList();
for (GroupBean bean : groupList) {
	DeviceAndGroupInHomeBean deviceInRoomBean = new DeviceAndGroupInHomeBean();
	deviceInRoomBean.setBizId(bean.getDevId());
	deviceInRoomBean.setBizType(BizParentTypeEnum.GROUP.getType());
	list.add(deviceInRoomBean);
}
for (DeviceBean bean : deviceList) {
	DeviceAndGroupInHomeBean deviceInRoomBean = new DeviceAndGroupInHomeBean();
	deviceInRoomBean.setBizId(bean.getDevId());
	deviceInRoomBean.setBizType(BizParentTypeEnum.DEVICE.getType());
	list.add();
}
TuyaHomeSdk.newHomeInstance(10000).sortDevInHome(homeId, list, new IResultCallback() {
		@Override
		public void onSuccess() {
			// do something
		}
		@Override
		public void onError(String code, String error) {
			// do something
		}
	});

	// @param allDeviceBeans Contains all devices and groups in current home.
Collections.sort(allDeviceBeans, new Comparator<DeviceBean>() {
                @Override
                public int compare(DeviceBean o1, DeviceBean o2) {
                    return o1.getHomeDisplayOrder() - o2.getHomeDisplayOrder();
                }
            });
```

## 注册或注销家庭信息变更的监听

**接口说明**

```java
void unRegisterHomeStatusListener(ITuyaHomeStatusListener listener)
```

**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| listener | 监听器 |

**Java 示例**

```java
// 定义监听
ITuyaHomeStatusListener listener = new ITuyaHomeStatusListener() {
		@Override
		public void onDeviceAdded(String devId) {
			// do something
		}
		@Override
		public void onDeviceRemoved(String devId) {
			// do something
		}
		@Override
		public void onGroupAdded(long groupId) {
			// do something
		}
		@Override
		public void onGroupRemoved(long groupId) {
			// do something
		}
		@Override
		public void onMeshAdded(String meshId) {
			// do something
		}
	};
// 注册监听
TuyaHomeSdk.newHomeInstance(10000).registerHomeStatusListener(listener);
// ...
// 取消注册监听
TuyaHomeSdk.newHomeInstance(10000).unRegisterHomeStatusListener(listener);
```