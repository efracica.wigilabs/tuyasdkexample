设备离线是指设备完成配网后，出现掉线、无网络响应的情况。您可以为设备开启离线告警，发生设备离线的情况后，设备会向指定用户发送通知。用户可以订阅消息推送功能来开启该功能。

## 查询设备是否支持离线提醒
**接口说明**

```
void getOfflineReminderSupportStatus(String hdId,ITuyaResultCallback<IsSupportOffLineBean> callback);
```

**参数说明**

| 参数     | 说明     |
| -------- | -------- |
| hdId     | 设备id   |
| callback | 结果回调 |

**代码示例**

```
mDevice.getOfflineReminderSupportStatus(hdId, new ITuyaResultCallback<IsSupportOffLineBean>() {
            @Override
            public void onSuccess(IsSupportOffLineBean result) {
                if(result.isOfflineReminder()){
                    Toast.makeText(mContext, "支持离线提醒", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(String errorCode, String errorMessage) {
                Toast.makeText(mContext, "查询失败", Toast.LENGTH_SHORT).show();
            }
        });
```



## 获取设备离线提醒的设置状态

**接口说明**

```
void getOfflineReminderStatus(String devId, ITuyaResultCallback<Boolean> callback);
```

**参数说明**

| 参数     | 说明     |
| -------- | -------- |
| devId    | 设备id   |
| callback | 结果回调 |

**代码示例**

```
mDevice.getOfflineReminderStatus(devId, new ITuyaResultCallback<Boolean>() {
            @Override
            public void onSuccess(Boolean result) {
                if(result){
                    Toast.makeText(mContext, "已设置离线提醒", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(mContext, "未设置离线提醒", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(String errorCode, String errorMessage) {
                Toast.makeText(mContext, "查询失败", Toast.LENGTH_SHORT).show();
            }
        });
```



## 设置设备离线提醒

**接口说明**

```
void setOfflineReminderStatus(String devId, boolean isWarn, IResultCallback callback);
```

**参数说明**

| 参数     | 说明             |
| -------- | ---------------- |
| devId    | 设备id           |
| isWarn   | 是否开启离线提醒 |
| callback | 结果回调         |

**代码示例**

```
mDevice.setOfflineReminderStatus(devId, true, new IResultCallback() {
            @Override
            public void onError(String code, String error) {
                Toast.makeText(mContext, "开启离线提醒失败", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess() {
                Toast.makeText(mContext, "开启离线提醒成功", Toast.LENGTH_SHORT).show();
            }
        });
```