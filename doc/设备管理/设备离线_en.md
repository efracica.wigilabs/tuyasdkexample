A device might get offline unexpectedly or do not respond over the network after the device is paired. In this case, you can enable offline alerts for the device. If the device goes offline, it will send a notification to a specified user. To enable this feature, you can subscribe to push notifications.

## Check support for offline alerts

**API description****

```
void getOfflineReminderSupportStatus(String hdId,ITuyaResultCallback<IsSupportOffLineBean> callback);
```

**Parameters**

| **Parameter** | **Description** |
| ------------- | --------------- |
| hdId          | device id       |
| callback      | Callback        |

**Example**

```
mDevice.getOfflineReminderSupportStatus(hdId, new ITuyaResultCallback<IsSupportOffLineBean>() {
            @Override
            public void onSuccess(IsSupportOffLineBean result) {
                if(result.isOfflineReminder()){
                    Toast.makeText(mContext, "支持离线提醒", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(String errorCode, String errorMessage) {
                Toast.makeText(mContext, "查询失败", Toast.LENGTH_SHORT).show();
            }
        });
```

## Query status of offline alerts

**API description****

```
void getOfflineReminderStatus(String devId, ITuyaResultCallback<Boolean> callback);
```

**Parameters**

| **Parameter** | **Description** |
| ------------- | --------------- |
| devId         | device id       |
| callback      | callback        |

**Example**

```
mDevice.getOfflineReminderStatus(devId, new ITuyaResultCallback<Boolean>() {
            @Override
            public void onSuccess(Boolean result) {
                if(result){
                    Toast.makeText(mContext, "Offline reminder set", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(mContext, "Offline reminder not set", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(String errorCode, String errorMessage) {
                Toast.makeText(mContext, "Query failed", Toast.LENGTH_SHORT).show();
            }
        });
```

## Set status of offline alerts

**API description***

```
void setOfflineReminderStatus(String devId, boolean isWarn, IResultCallback callback);
```

**Parameters**

| **Parameter** | **Description**                    |
| ------------- | ---------------------------------- |
| devId         | device id                          |
| isWarn        | Whether to enable offline reminder |
| callback      | callback                           |

**Example**

```
mDevice.setOfflineReminderStatus(devId, true, new IResultCallback() {
            @Override
            public void onError(String code, String error) {
                Toast.makeText(mContext, "Failed to enable offline reminder", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess() {
                Toast.makeText(mContext, "Offline reminder turned on successfully", Toast.LENGTH_SHORT).show();
            }
        });
```