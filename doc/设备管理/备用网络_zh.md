一些安全场景，可能会定期修改Wi-Fi密码，如果没有Wi-Fi多组连接功能，意味着所有连接的Wi-Fi设备都需要重新配网。通过提前预设多组Wi-Fi密码，则可以让设置自动尝试连接，从而无需经过重新配网，Wi-Fi在线切换连接网络环境。

:::important
通过`deviceBean.getDevAttribute() & (1 << 12)`判断当前设备是否支持设置备用网络，1表示支持，0表示不支持。
 :::

## 网络信息
### 获取当前设备连接的WIFI信息
**初始化**

```java
ITuyaWifiBackup wifiBackupManager = TuyaHomeSdk.getWifiBackupManager(devId);
```


**接口说明**

```java
void getCurrentWifiInfo(ITuyaDataCallback<CurrentWifiInfoBean> dataCallback);
```

**参数说明**

| 参数         | 描述 |
| ------------ | ---- |
| dataCallback | 回调 |

**示例代码**

```java

wifiBackupManager.getCurrentWifiInfo(new ITuyaDataCallback<CurrentWifiInfoBean>() {
    @Override
    public void onSuccess(CurrentWifiInfoBean result) {

    }

    @Override
    public void onError(String errorCode, String errorMessage) {

    }
});

```

### 获取当前设备中已有的备用Wi-Fi列表

**接口说明**

```java
void getBackupWifiList(ITuyaDataCallback<CurrentWifiInfoBean> dataCallback);
```

**参数说明**

| 参数         | 描述 |
| ------------ | ---- |
| dataCallback | 回调 |

**示例代码**

```java

wifiBackupManager.getBackupWifiList(new ITuyaDataCallback<List<BackupWifiBean>>() {
    @Override
    public void onSuccess(List<BackupWifiBean> result) {

    }

    @Override
    public void onError(String errorCode, String errorMessage) {

        wifiBackupManager.onDestroy();
    }
});

```

### 备用网络

**接口说明**

```java
void setBackupWifiList(List<BackupWifiBean> backupWifiList, ITuyaDataCallback<BackupWifiResultBean> dataCallback);
```

**参数说明**

| 参数           | 描述              |
| -------------- | ----------------- |
| backupWifiList | 备用Wi-Fi信息bean |
| dataCallback   | 回调              |

**示例代码**

```java
ArrayList<BackupWifiBean> backupWifiList = new ArrayList<>();

// 新添加的，设置密码
BackupWifiBean backupWifiBean = new BackupWifiBean();
backupWifiBean.ssid = "test1";
backupWifiBean.passwd = "12345678";
backupWifiList.add(backupWifiBean);

// 之前已添加过的，设置hash
String ssid="test2";
String pwd="123123";
DeviceBean dev = TuyaHomeSdk.getDataInstance().getDeviceBean(devId);
String hashStr=SHA256Util.getBase64Hash(dev.getLocalKey() + ssid + pwd);
BackupWifiBean backupWifiBean2 = new BackupWifiBean();
backupWifiBean2.ssid = "test2";
backupWifiBean2.hash = hashStr;
backupWifiList.add(backupWifiBean2);

final ITuyaDataCallback<BackupWifiResultBean> setBackupCallback = new ITuyaDataCallback<BackupWifiResultBean>() {
    @Override
    public void onSuccess(BackupWifiResultBean result) {

    }

    @Override
    public void onError(String errorCode, String errorMessage) {

    }
};
wifiBackupManager.setBackupWifiList(backupWifiList, setBackupCallback);


```


### 注销监听

**接口说明**

退出页面销毁监听。

```java
wifiBackupManager.onDestory();
```


## 切换网络

### 切换到新的Wi-Fi
**初始化**

```java
ITuyaWifiSwitch wifiSwitchManager = TuyaHomeSdk.getWifiSwitchManager(devId);
```

**接口说明**


``` java
void switchToNewWifi(String ssid, String password, ITuyaDataCallback<SwitchWifiResultBean> callback);
```

**参数说明**

| 参数     | 说明      |
| -------- | --------- |
| ssid     | WI-FI名称 |
| password | WI-FI密码 |
| callback | 回调      |

**示例代码**

```java

wifiSwitchManager.switchToNewWifi(String ssid, String password, new ITuyaDataCallback<SwitchWifiResultBean>() {
            @Override
            public void onSuccess(SwitchWifiResultBean result) {
                
            }

            @Override
            public void onError(String errorCode, String errorMessage) {

            }
        });

```

### 切换到已备份的Wi-Fi

**接口说明**

``` java
void switchToBackupWifi(String hash, ITuyaDataCallback<SwitchWifiResultBean> callback);
```

**参数说明**

| 参数     | 说明            |
| -------- | --------------- |
| hash     | Wi-Fi密码的hash |
| callback | 回调            |

**示例代码**

```java

wifiSwitchManager.switchToBackupWifi(hash,new ITuyaDataCallback<SwitchWifiResultBean>() {
            @Override
            public void onSuccess(SwitchWifiResultBean result) {
                
            }

            @Override
            public void onError(String errorCode, String errorMessage) {

            }
        });
```

### 注销监听

**接口说明**

退出页面销毁监听。

```java
wifiSwitchManager.onDestory();
```