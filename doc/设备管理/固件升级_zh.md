
固件升级主要用于增强设备能力以及修复 bug；

固件升级分为“设备升级”和“MCU 升级”两种；

## 初始化固件信息

**接口说明**

Wi-Fi，Zigbee 网关，摄像头等设备：

```java
ITuyaOta newOTAInstance(String devId);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| devId | 设备 id |

**实例代码**

```java
ITuyaOta iTuyaOta = TuyaHomeSdk.newOTAInstance("devId");
```

## 初始化子设备固件信息

NOTE：从SDK 2.9.1开始支持；

**接口说明**

子设备：

```java
ITuyaOta newOTAInstance(String meshId, String devId, String nodeId);
```

**参数说明**

| 参数  | 说明                                             |
| ---- | ---- |
| meshId | Zigbee 网关 id                                   |
| devId | Zigbee 子设备 id                                 |
| nodeId | Zigbee 子设备 mac 地址（从子设备的 DeviceBean 获取） |

**实例代码**

```java
ITuyaOta iTuyaOta = TuyaHomeSdk.newOTAInstance("meshId", "devId", "nodeId");
```

## 获取固件升级信息

**接口说明**

```java
iTuyaOta.getOtaInfo(new IGetOtaInfoCallback({
	@Override
	void onSuccess(List<UpgradeInfoBean> list) {
	
	}
	@Override
	void onFailure(String code, String error) {
	
	}
});
```

**参数说明**

| 字段        | 类型  | 描述                                                  |
| ---- | ---- | ---- |
| upgradeStatus  | int | 升级状态，0 :无新版本 1 :有新版本 2 :在升级中         |
| version     | String | 最新版本                                              |
| currentVersion | String | 当前版本                                              |
| timeout     | int | 超时时间，单位：秒                                    |
| upgradeType | int | 0 :app 提醒升级  2: app 强制升级  3: 检测升级         |
| type        | int | 0: Wi-Fi 设备 1:蓝牙设备 2:GPRS 设备 3:zigbee 设备 9: MCU |
| typeDesc    | String | 模块描述                                              |
| lastUpgradeTime | long  | 上次升级时间，单位：毫秒                              |

## 添加固件升级监听

**接口说明**

```java
iTuyaOta.setOtaListener(new IOtaListener() {
        @Override
        public void onSuccess(int otaType) {

        }

        @Override
        public void onFailure(int otaType, String code, String error) {

        }

        @Override
        public void onFailureWithText(int otaType, String code,
                                      OTAErrorMessageBean messageBean) {

        }

        @Override
        public void onProgress(int otaType, int progress) {

        }

        @Override
        public void onTimeout(int otaType) {

        }
  
  		@Override
        public void onStatusChanged(int otaStatus, int otaType) {
			// 3.23.0 版本新增，针对于低功耗类设备会通过该方法返回设备等待唤醒状态。此时 otaStatus 参数返回 5。
        }
});
```

**参数说明**

| 参数 | 描述                                            |
| ---- | ---- |
| otaType | 升级的设备类型，同 UpgradeInfoBean 中的 type 字段； |

## 开始升级

**接口说明**

调用该方法开始升级,调用后注册的 OTA 监听会把升级状态返回回来，以便开发者构建 UI；

```java
iTuyaOta.startOta();
```

## 销毁

**接口说明**

离开升级页面后要销毁，回收内存；

```java
iTuyaOta.onDestory();
```

## 完整示例代码
**该示例为普通的 Wi-Fi 设备升级流程，[蓝牙单点](https://developer.tuya.com/cn/docs/app-development/android-bluetooth-ble?id=Karv7r2ju4c21#title-18-%E5%9B%BA%E4%BB%B6%E5%8D%87%E7%BA%A7)及[蓝牙 Mesh](https://developer.tuya.com/cn/docs/app-development/meshsig?id=Ka6km4aeuygxr#title-35-%E5%9B%BA%E4%BB%B6%E5%8D%87%E7%BA%A7)请参考对应章节;**

```java
private ITuyaOta iTuyaOta;
    private String mDevId = "your_devId";

    public void startOTA() {
        initOta();
        initListener();
        checkOTAInfo();
    }

    //初始化固件信息
    private void initOta() {
        iTuyaOta = TuyaHomeSdk.newOTAInstance(mDevId);
    }

    //添加固件升级监听
    private void initListener() {
        iTuyaOta.setOtaListener(new IOtaListener() {
            @Override
            public void onSuccess(int otaType) {
                iTuyaOta.onDestroy();
                Log.i(TAG, "upgrade success");
            }

            @Override
            public void onFailure(int otaType, String code, String error) {
                iTuyaOta.onDestroy();
                Log.i(TAG, "upgrade failure, errorCode = " + code
                        + ",errorMessage = " + error);
            }

            @Override
            public void onFailureWithText(int otaType, String code, OTAErrorMessageBean messageBean) {
                iTuyaOta.onDestroy();
                Log.i(TAG, "upgrade failure, errorCode = " + code 
                        + ",errorMessage = " + messageBean.text);
            }

            @Override
            public void onProgress(int otaType, int progress) {
                Log.i(TAG, "upgrade progress = " + progress);
            }

            @Override
            public void onTimeout(int otaType) {
                iTuyaOta.onDestroy();
                Log.i(TAG, "upgrade timeout");
            }

            @Override
            public void onStatusChanged(int otaStatus, int otaType) {

            }
        });
    }

    //获取固件升级信息
    private void checkOTAInfo() {
        iTuyaOta.getOtaInfo(new IGetOtaInfoCallback() {
            @Override
            public void onSuccess(List<UpgradeInfoBean> upgradeInfoBeans) {
                if (hasHardwareUpdate(upgradeInfoBeans)) {
                    //开始升级
                    iTuyaOta.startOta();
                }
            }

            @Override
            public void onFailure(String code, String error) {
                Log.i(TAG, "Get OTA info failed,errorCode=" + code
                        + ",errorMessage=" + error);
            }
        });
    }

    private boolean hasHardwareUpdate(List<UpgradeInfoBean> list) {
        if (null == list || list.size() == 0)
            return false;
        for (UpgradeInfoBean upgradeInfoBean : list) {
            if (upgradeInfoBean != null && upgradeInfoBean.getUpgradeStatus() == 1)
                return true;
        }
        return false;
    }
```

