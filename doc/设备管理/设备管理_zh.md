本文介绍设备信息的获取和管理能力等 Android 版接口信息。包含设备初始化、设备监听、设备控制、查询设备信息、修改设备名称、移除设备、设备恢复出厂设置、查询 Wi-Fi 信号强度、回收设备资源等。

## 功能说明

- 设备相关的返回数据都采用异步消息的方式通知接收者。

- 同时，也提供了控制指令下发，设备固件升级的接口。

- `ITuyaDevice` 类提供了设备状态通知能力，通过注册回调函数，您可以方便的获取设备数据接受、设备移除、设备上下线、手机网络变化的通知。

- `DeviceBean` 数据类型：

	| 属性 | 类型 | 说明 |
	| ---- | ---- | ---- |
	| devId | String | 设备 ID。 |
	| name | String | 设备名称。 |
	| iconUrl | String | 图标地址。 |
	| schema | String | 设备控制数据点的类型信息。 |
	| productId | String | 产品 ID，同一个产品 ID 的设备的 `schema` 信息一致。 |
	| timezoneId | String | 设备所在的时区。 |
	| category | String | 设备类型（已弃用）。获取设备类型需要使用产品信息的category字段。 |
	| pv | String | 网关协议版本。 |
	| bv | String | 网关通用固件版本。 |
	| time | Long | 设备激活时间。 |
	| schemaMap | Map | `schema` 的缓存数据。 |
	| dps | Map | 设备当前数据信息。key 是 DP ID，value 是取值。详情请参考 [设备功能点](#DeviceFunction) 章节。 |
	| getIsOnline | Boolean | 设备是否在线，指局域网或者云端在线。 |
	| isLocalOnline| Boolean | 设备的局域网在线状态。 |
	| supportGroup | Boolean | 设备是否支持群组，如果不支持请到 [涂鸦 IoT 平台](https://iot.tuya.com) 开启此功能。 |
	| isShare | Boolean | 是否是分享的设备。 |
	| virtual| Boolean | 是否是虚拟设备。 |
	| isZigBeeWifi | Boolean | 是否是 Zigbee 网关设备。 |
	| hasZigBee | Boolean | 是否有 Zigbee 设备。 |
	| nodeId | String | 用于网关和子设备类型的设备。属于子设备的一个属性，标识其短地址 ID，一个网关的子设备的 nodeId 都是唯一的。 |
	| meshId | String | 用于网关和子设备类型的设备。属于子设备的一个属性，标识其网关 ID。 |
	| lon | String | 设备所在经度信息。 |
	| lat | String | 设备所在纬度信息。 |
	| productBean | ProductBean | 产品信息。 |

	>**注意**：设备控制时，如果涉及到经纬度 `lon` 和 `lat`，请在配网前调用 `setLatAndLong` 设置经纬度：
	>```java
	>TuyaSdk.setLatAndLong(String latitude, String longitude)
	>```

- `ProductBean` 产品信息：

  | 属性          | 类型    | 说明                                                         |
  | ------------- | ------- | ------------------------------------------------------------ |
  | category      | String  | 设备类型缩写，用于标注该产品是什么设备。例：‘kg’表示开关、‘cz’表示插座。 |
  | capability    | int     | 联网通信能力标位：0=wifi；1=cable；2=gprs；3=nb-iot；10=bluetooth；11=blemesh；12=zigbee；13=hongwai；14=ZigBee配网。 |
  | resptime      | long    | 拉取服务端数据相应时间                                       |
  | supportSGroup | boolean | 是否支持标准群组                                             |

  
  
## 初始化设备

### 初始化家庭数据

* 设备控制前必须初始化数据，调用下面的方法获取家庭下的设备信息。
* 每次 App 存活期间，初始化一次即可。
* 此外，切换家庭也需要进行初始化。

	```java
	TuyaHomeSdk.newHomeInstance(homeId).getHomeDetail(new ITuyaHomeResultCallback() {
		@Override
		public void onSuccess(HomeBean homeBean) {
	
		}
	
		@Override
		public void onError(String errorCode, String errorMsg) {
	
		}
	});
	```

- 该接口的 `onSuccess` 方法中将返回 `HomeBean`。
- 然后调用 `HomeBean` 的 `getDeviceList` 即可获得设备列表。

	```java
	List<DeviceBean> deviceList = homeBean.getDeviceList();
	```

### 初始化设备控制类

根据设备 ID 初始化设备控制类 `ITuyaDevice`。

```java
TuyaHomeSdk.newDeviceInstance(String devId);
```

**参数说明**

| 参数| 说明|
| ---- | --- |
| devId | 设备 ID。 |

**Java 示例**

```java
ITuyaDevice mDevice = TuyaHomeSdk.newDeviceInstance(deviceBean.getDevId());
```

## 监听设备

### 注册设备监听

`ITuyaDevice` 提供设备相关信息的监听，包含：

- DP 数据
- 设备名称
- 设备在线状态和设备移除

	```java
	ITuyaDevice.registerDevListener(IDevListener listener)
	```

**参数说明**

| 参数| 说明|
| ---- | --- |
| listener | 设备状态监听 |

**`IDevListener` 接口**

```java
public interface IDevListener {

	/**
	* DP 数据更新
	*
	* @param devId 设备 ID
	* @param dpStr 设备发生变动的功能点，为 JSON 字符串，数据格式：{"101": true}
	*/
	void onDpUpdate(String devId, String dpStr);

	/**
	* 设备移除回调
	*
	* @param devId 设备id
	*/
	void onRemoved(String devId);

	/**
	* 设备上下线回调。如果设备断电或断网，服务端将会在3分钟后回调到此方法。
	*
	* @param devId  设备 ID
	* @param online 是否在线，在线为 true
	*/
	void onStatusChanged(String devId, boolean online);

	/**
	* 网络状态发生变动时的回调
	*
	* @param devId  设备 ID
	*  @param status 网络状态是否可用，可用为 true
	*/
	void onNetworkStatusChanged(String devId, boolean status);

	/**
	* 设备信息更新回调
	*
	* @param devId  设备 ID
	*/
	void onDevInfoUpdate(String devId);

}
```

>**说明**：其中，设备功能点说明请参考 [设备功能点](#DeviceFunction) 章节。

**Java 示例**

```java
mDevice.registerDevListener(new IDevListener() {
	@Override
	public void onDpUpdate(String devId, String dpStr) {

	}
	@Override
	public void onRemoved(String devId) {

	}
	@Override
	public void onStatusChanged(String devId, boolean online) {

	}
	@Override
	public void onNetworkStatusChanged(String devId, boolean status) {

	}
	@Override
	public void onDevInfoUpdate(String devId) {

	}
});
```

>**注意**：请勿使用 `void registerDeviceListener(IDeviceListener listener)` 方法，此方法需要配合标准设备使用，该 API 暂未开放使用。

### 取消设备监听

当不需要监听设备时，取消设备监听。

**接口说明**

```java
ITuyaDevice.unRegisterDevListener()
```

**Java 示例**


```java
mDevice.unRegisterDevListener();
```

<div style="display:none">
<!--## 查询设备是否支持离线提醒-->

查询设备是否支持离线提醒。

```java
void getOfflineReminderSupportStatus(String hdId, ITuyaResultCallback<IsSupportOffLineBean> callback);
```

**参数说明**

| 参数     | 说明         |
| -------- | ------------ |
| hdId     | 设备 ID      |
| callback | 查询结果回调 |

**Java 示例**

```java
 mDevice.getOfflineReminderSupportStatus(deviceId, new ITuyaResultCallback<IsSupportOffLineBean>() {
            @Override
            public void onSuccess(IsSupportOffLineBean result) {

            }

            @Override
            public void onError(String errorCode, String errorMessage) {

            }
        });
    }
```
</div>

## 查询设备信息

查询单个 DP 数据，查询后的数据会通过 `IDevListener.onDpUpdate()` 接口进行异步回调。

> **注意**：该接口主要是针对不主动发送数据的设备 DP，例如倒计时信息查询。常规查询 DP 数据值时，可通过 `DeviceBean` 中的 `getDps()` 获取。

```java
mDevice.getDp(String dpId, IResultCallback callback);
```

**Java 示例**

```java
mDevice.getDp(dpId, new IResultCallback() {
	@Override
	public void onError(String code, String error) {

	}

	@Override
	public void onSuccess() {

	}
});
```

## 修改设备名称

重命名设备，并支持多设备同步。

```java
// 修改设备名称
mDevice.renameDevice(String name,IResultCallback callback);
```

**Java 示例**

```java
mDevice.renameDevice("设备名称", new IResultCallback() {
	@Override
	public void onError(String code, String error) {
		// 修改设备名称失败
	}
	@Override
	public void onSuccess() {
		// 修改设备名称成功
	}
});
```

**后续步骤**

重命名成功后，`IDevListener.onDevInfoUpdate()` 会收到通知。调用以下方法可以获取最新数据，然后刷新设备信息即可。

```java
TuyaHomeSdk.getDataInstance().getDeviceBean(String devId);
```

## 移除设备

从用户设备列表中移除设备。

```java
void removeDevice(IResultCallback callback);
```

**Java 示例**

```java
mDevice.removeDevice(new IResultCallback() {
	@Override
	public void onError(String errorCode, String errorMsg) {
	}

	@Override
	public void onSuccess() {
	}
});
```

## 恢复出厂设置

设备恢复出厂设置后，设备的相关数据会被清除掉，并重新进入待配网状态。如果是 Wi-Fi 设备，默认进入 Wi-Fi 快连模式。

```java
void resetFactory(IResultCallback callback)；
```

**Java 示例**

```java
mDevice.resetFactory(new IResultCallback() {
	@Override
	public void onError(String errorCode, String errorMsg) {
	}

	@Override
	public void onSuccess() {
	}
});
```

## 查询 Wi-Fi 信号强度

查询设备 Wi-Fi 的信号强度。

```java
void requestWifiSignal(WifiSignalListener listener);
```

**Java 示例**

```java
mDevice.requestWifiSignal(new WifiSignalListener() {

	@Override
	public void onSignalValueFind(String signal) {

	}

	@Override
	public void onError(String errorCode, String errorMsg) {

	}
});;
```

## 回收设备资源

应用或者 Activity 关闭时，可以调用此接口，回收设备占用的资源。

```java
void onDestroy()
```

**Java 示例**

```java
mDevice.onDestroy();
```