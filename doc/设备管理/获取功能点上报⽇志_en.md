Fetch the recent log of the DP report. It is only available for the result of 7 days, The method is to call the [common interface](https://developer.tuya.com/cn/docs/app-development/android-app-sdk/commoninterface?id=Ka6o3bst0te1r) to send a request.

## Description

| Name | Version | Description |
| ---- | ---- | ---- |
| tuya.m.smart.operate.all.log | 1.0 | Fetch the log of DP report |

## Parameters

| Name | Type | Description | Is Required? |
| ---- | ---- | ---- | ---- |
| devId | String | Device ID | true|
| dpIds | String | The DP id for querying, splitted by comma. For example: `@“1，2”` | true|
| offset | Integer | The offset of pagination | true|
| limit | Integer | The number of fetched data per page | true|
| startTime | String | The start time for the query (unit in milliseconds) | false|
| endTime | String | The end time for the query (unit in milliseconds) | false|
| sortType | String | The order of sorted results, sorted by time. (`ASC` or `DESC`, Default is `DESC`) | false|

## Request example

```json
{
	"devId" : "05200020b4e62d16ce8b",
	"dpIds" : "1,2",
	"offset" : 0,
	"limit" : 10,
	"startTime" : "1542800401000",
	"endTime" : "1542886801000",
	"sortType" : "DESC"
}
```

## Response example

```json
{
"result" : {
	"total" : 11055,
	"dps" : [ {
		"timeStamp" : 1542829972,
		"dpId" : 5,
		"timeStr" : "2018-11-21 20:52:52",
		"value" : "311"
	}, {
		"timeStamp" : 1542829970,
		"dpId" : 5,
		"timeStr" : "2018-11-21 20:52:50",
		"value" : "323"
	}, {
		"timeStamp" : 1542829966,
		"dpId" : 5,
		"timeStr" : "2018-11-21 20:52:46",
		"value" : "230"
	}, {
		"timeStamp" : 1542829964,
		"dpId" : 5,
		"timeStr" : "2018-11-21 20:52:44",
		"value" : "231"
	}, {
		"timeStamp" : 1542829960,
		"dpId" : 5,
		"timeStr" : "2018-11-21 20:52:40",
		"value" : "307"
	}, {
		"timeStamp" : 1542829958,
		"dpId" : 5,
		"timeStr" : "2018-11-21 20:52:38",
		"value" : "320"
	}, {
		"timeStamp" : 1542829954,
		"dpId" : 5,
		"timeStr" : "2018-11-21 20:52:34",
		"value" : "229"
	}, {
		"timeStamp" : 1542829950,
		"dpId" : 5,
		"timeStr" : "2018-11-21 20:52:30",
		"value" : "325"
	}, {
		"timeStamp" : 1542829948,
		"dpId" : 5,
		"timeStr" : "2018-11-21 20:52:28",
		"value" : "292"
	}, {
		"timeStamp" : 1542829942,
		"dpId" : 5,
		"timeStr" : "2018-11-21 20:52:22",
		"value" : "231"
	} ],
	"hasNext" : true
  },
  "t" : 1542959314632,
  "success" : true,
  "status" : "ok"
}
```

## Java example

```java
Map<String, Object> map = new HashMap<>();
map.put("devId", "your_devId");
map.put("dpIds", "your_device_dpId_str"); //"1,2"
//map.put("..","..");

TuyaHomeSdk.getRequestInstance().requestWithApiName("tuya.m.smart.operate.all.log", "1.0", map, String.class, new ITuyaDataCallback<String>() {
	@Override
	public void onSuccess(String result) {

	}

	@Override
	public void onError(String s, String s1) {

	}
});
```