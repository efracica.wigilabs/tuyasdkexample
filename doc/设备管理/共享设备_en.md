When a device needs to be provided for other users to operate, the device can be shared with other users, and the user who receives the shared device can simply operate the device.

| Class Name       | Description                                   |
| ---- | ---- |
| ITuyaHomeDeviceShare | Share device, get share info, manage share device |

## Add Device Share
### Add Multiple Device Sharing (coverage)

Sharing multiple devices to users will override all previous user sharing

**Declaration**

```java
void addShare(long homeId, String countryCode, final String userAccount, ShareIdBean bean, boolean autoSharing, final ITuyaResultCallback<SharedUserInfoBean> callback);
```

**Parameters**

| Parameter  | Description                                              |
| ---- | ---- |
| homeId  | device home id                                           |
| countryCode | share member country code                                |
| userAccount | share member user account                                |
| bean    | share device id list                                     |
| autoSharing | auto sharing                                             |
| callback | callback, including sharing success or failure, cannot be null |

What's more, the data structure of  `ShareIdBean`  is as follows:

| Parameter | Type           | Description      |
| ---- | ---- | ---- |
| devIds | List&lt;String&gt; | share device id list |
| meshIds  | List&lt;String&gt; | share mesh id list  |

**Example**

```java
TuyaHomeSdk.getDeviceShareInstance().addShare(homeId, countryCode, userAccount,
                shareIdBean, autoSharing, new ITuyaResultCallback<SharedUserInfoBean>() {
                    @Override
                    public void onSuccess(SharedUserInfoBean sharedUserInfoBean) {}
                    @Override
                    public void onError(String errorCode, String errorMsg) {}
                });
```



### Add Shares (new, not overwriting old Shares)

Sharing multiple devices to users will add the devices to all user sharing. If you have previously shared the device with a specified user, you can obtain the target user id through the following **getting Shared Relationships API**, and then use this method to quickly share the device with the user.

**Declaration**

```java
void addShareWithMemberId(long memberId,List<String> devIds,IResultCallback callback);
```

**Parameters**

| Parameter | Description                                              |
| ---- | ---- |
| memberId | share user member id                                     |
| devIds | share device id list                                     |
| callback | Callback, including sharing success or failure, cannot be null |

**Example**

```java
TuyaHomeSdk.getDeviceShareInstance().addShareWithMemberId(userId, devIdList, new IResultCallback() {
            @Override
            public void onError(String errorCode, String errorMsg) {
            }
            @Override
            public void onSuccess() {
            }
});
```


### Device Add Sharing

Sharing single or multiple devices to users will add the devices to all user shareing

**Declaration**

```java

void addShareWithHomeId(long homeId, String countryCode, String userAccount, List<String> devIds, ITuyaResultCallback<SharedUserInfoBean> callback);
```

**Parameters**

| Parameters    | Description                              |
| ---- | ---- |
| homeId  | device home id |
| countryCode | share member country code |
| userAccount | share member user account                                |
| devIds  | share device id list                                     |
| callback | Callback, including sharing success or failure, cannot be null |

**Example**

```java
TuyaHomeSdk.getDeviceShareInstance().addShareWithHomeId(homeId, countryCode,
        userAccount, devsIdList, new ITuyaResultCallback<SharedUserInfoBean>() {
            @Override
            public void onSuccess(SharedUserInfoBean sharedUserInfoBean) {
            }
    
            @Override
            public void onError(String errorCode, String errorMsg) {                       
            }
});
```



## Getting Shared Relationships

### Get List of All Active Shared Users in the Home

**Declaration**

```java
void queryUserShareList(long homeId, final ITuyaResultCallback<List<SharedUserInfoBean>> callback);
```

**Parameters**

| Parameter | Description                                        |
| ---- | ---- |
| homeId | home id                                            |
| callback | callback, including success or failure, cannot be null |

**Example**

```java
TuyaHomeSdk.getDeviceShareInstance().queryUserShareList(homeId, new ITuyaResultCallback<List<SharedUserInfoBean>>() {
            @Override
            public void onSuccess(List<SharedUserInfoBean> sharedUserInfoBeans) {}
            @Override
            public void onError(String errorCode, String errorMsg) {}
        });
```



### Get List of All Shared Uers Received

**Declaration**

```java
void queryShareReceivedUserList(final ITuyaResultCallback<List<SharedUserInfoBean>> callback);
```

**Parameters**

| Parameter | Description                                        |
| ---- | ---- |
| callback | callback, including success or failure, cannot be null |

**Example**

```java
TuyaHomeSdk.getDeviceShareInstance().queryShareReceivedUserList(new ITuyaResultCallback<List<SharedUserInfoBean>>() {
    @Override
    public void onSuccess(List<SharedUserInfoBean> sharedUserInfoBeans) {
    }
    @Override
    public void onError(String errorCode, String errorMsg) {
    }
});
```



### Obtaining User-shared Data that is Actively Shared by a Single User

**Declaration**

```java
void getUserShareInfo(long memberId, final ITuyaResultCallback<ShareSentUserDetailBean> callback);
```

**Parameters**

| Parameter | Description                                        |
| ---- | ---- |
| memberId | share member id                                    |
| callback | callback, including success or failure, cannot be null |

**Example**

```java
TuyaHomeSdk.getDeviceShareInstance().getUserShareInfo(mRelationId, new ITuyaResultCallback<ShareSentUserDetailBean>() {
                @Override
                public void onSuccess(ShareSentUserDetailBean shareSentUserDetailBean) {}
                @Override
                public void onError(String errorCode, String errorMsg) {}
            });
```



### Getting Shared Data from a Single Recipient

**Declaration**

```java
void getReceivedShareInfo(long memberId, final ITuyaResultCallback<ShareReceivedUserDetailBean> callback);
```

**Parameters**

| Parameter | Description                                        |
| ---- | ---- |
| memberId | member id                                          |
| callback | callback, including success or failure, cannot be null |

**Example**

```java
 TuyaHomeSdk.getDeviceShareInstance().getReceivedShareInfo(mRelationId, new ITuyaResultCallback<ShareReceivedUserDetailBean>() {
                @Override
                public void onSuccess(ShareReceivedUserDetailBean shareReceivedUserDetailBean) {}
                @Override
                public void onError(String errorCode, String errorMsg) {}
            });
```



### Get a Single Device Shared User List

**Declaration**

```java
void queryDevShareUserList(String devId, final ITuyaResultCallback<List<SharedUserInfoBean>> callback);
```

**Parameters**

| Parameter | Description                                        |
| ---- | ---- |
| devId | device id                                          |
| callback | callback, including success or failure, cannot be null |

**Example**

```java
TuyaHomeSdk.getDeviceShareInstance().queryDevShareUserList(mDevId, new ITuyaResultCallback<List<SharedUserInfoBean>>() {
        @Override
        public void onError(String errorCode, String errorMsg) {}
        @Override
        public void onSuccess(List<SharedUserInfoBean> shareUserBeen) {}
    });
```

### Who Shares Access Devices

**Declaration**

```java
void queryShareDevFromInfo(String devId, final ITuyaResultCallback<SharedUserInfoBean> callback);
```

**Parameters**

| Parameter | Description                                        |
| ---- | ---- |
| devId | receive share device id                            |
| callback | callback, including success or failure, cannot be null |

**Example**

```java
TuyaHomeSdk.getDeviceShareInstance().queryShareDevFromInfo(devId, new ITuyaResultCallback<SharedUserInfoBean>() {
            @Override
            public void onSuccess(SharedUserInfoBean result) {
            }
            @Override
            public void onError(String errorCode, String errorMessage) {
            }
        });
```

## Remove Sharing

### Delete Active Sharers

The sharer deletes all shared relationships with this user through memberId (user dimension delete)

**Declaration**

```java
void removeUserShare(long memberId, IResultCallback callback);
```

**Parameters**

| Parameter | Description                                              |
| ---- | ---- |
| memberId | share memer id                                           |
| callback | callback, including delete success or failure, cannot be null |

**Example**

```java
TuyaHomeSdk.getDeviceShareInstance().removeUserShare(memberId, new IResultCallback() {
    @Override
    public void onError(String code, String error) {
    }
    @Override
    public void onSuccess() {
    }
})
```



### Delete Receiving Sharer

The shared party obtains the information of all shared devices of this user through memberId

**Declaration**

```java
void removeReceivedUserShare(long memberId, IResultCallback callback);
```

**Parameters**

| Parameter | Description                                              |
| ---- | ---- |
| memberId | revice share member id                                   |
| callback | callback, including delete success or failure, cannot be null |

**Example**

```java
TuyaHomeSdk.getDeviceShareInstance().removeReceivedUserShare(memberId, new IResultCallback() {
    @Override
    public void onError(String code, String error) {
    }
    @Override
    public void onSuccess() {
    }
})
```



### Single Device Remove Sharing

**Declaration**

```java
void disableDevShare(String devId, long memberId, IResultCallback callback);
```

**Parameters**

| Parameter | Description                                              |
| ---- | ---- |
| memberId | share member id                                          |
| devId | share device id                                          |
| callback | callback, including delete success or failure, cannot be null |

**Example**

```java
TuyaHomeSdk.getDeviceShareInstance().disableDevShare (devId, memberId, new IResultCallback() {
    @Override
    public void onError(String code, String error) {
    }
    @Override
    public void onSuccess() {
    }
});
```



### Remove Shared Devices Received

**Declaration**

```java
void removeReceivedDevShare(String devId, IResultCallback callback);
```

**Parameters**

| Parameter | Description                                              |
| ---- | ---- |
| devId | reviced device id                                        |
| callback | callback, including delete success or failure, cannot be null |

**Example**

```java
TuyaHomeSdk.getDeviceShareInstance().removeReceivedDevShare(devId,new IResultCallback() {
    @Override
    public void onError(String code, String error) {}
    @Override
    public void onSuccess() {}
})
```



## Modify Note Name

### Modify the nickname of the active sharer

The sharer modifies the remark name of the person being shared. If you share the device with other people, you can modify the remark name of the person.

**Declaration**

```java
void renameShareNickname(long memberId, String name, IResultCallback callback);
```

**Parameters**

| Parameters    | Description                              |
| ---- | ---- |
| memberId | share member id  |
| name   | new name                                                 |
| callback | callback, including modification success and failure, cannot be null |

**Example**

```java
TuyaHomeSdk.getDeviceShareInstance().renameShareNickname(mRelationId, inputText, new IResultCallback() {
                @Override
                public void onError(String s, String s1) {}
                @Override
                public void onSuccess() {}
            });
```

### Modify the Nickname of the Recipient Sharer

**Declaration**

```java
void renameReceivedShareNickname(long memberId, String name, IResultCallback callback);
```

**Parameters**

| Parameters | Description |
| ---- | ---- |
| memberId  | revice share member id                                   |
| name | new name                                                 |
| callback | callback, including modification success and failure, cannot be null |

**Example**

```java
TuyaHomeSdk.getDeviceShareInstance().renameReceivedShareNickname(mRelationId, inputText, new IResultCallback() {
                @Override
                public void onError(String s, String s1) {}
                @Override
                public void onSuccess() {}
            });
```



## Share invitation

### Send invitations to share devices to other users

**Interface Description**

```java
void inviteShare(String devId, String userAccount, String countryCode, ITuyaResultCallback<Integer> callback);
```

**Parameter Description**

| Parameters | Description |
| ----------- | ------------------ |
| countryCode | Country Code |
| userAccount | User Account |
| devId | device id |
| success | Success callback |
| failure | Failure callback |

**Sample Code**

```java
    TuyaHomeSdk.getDeviceShareInstance().inviteShare(devId, userAccount, countryCode, new ITuyaResultCallback<Integer>() {
                @Override
                public void onSuccess(Integer result) {
                    
                }
    
                @Override
                public void onError(String errorCode, String errorMessage) {
    
                }
            });
```


### Confirm sharing invitation

**Interface Description**

```java
 void confirmShareInviteShare(int shareId, final IResultCallback callback);
```

**Parameter Description**

| Parameters | Description |
| ----------- | ------------------ |
| shareId | share id |
| success | Success callback |
| failure | Failure callback |

**Sample Code**

```java
 TuyaHomeSdk.getDeviceShareInstance().confirmShareInviteShare(shareId, new IResultCallback() {
            @Override
            public void onError(String code, String error) {
                
            }

            @Override
            public void onSuccess() {

            }
        });
```



## Group sharing

### Return to device sharing user list by group ID

Note: The group ID is displayed on the panel

**Interface Description**

```java
void queryGroupSharedUserList(long groupId, ITuyaResultCallback<List<SharedUserInfoBean>> callback);
```

**Parameter Description**

| Parameters | Description |
| ----------- | ------------------ |
| groupId | group id |
| success | Success callback |
| failure | Failure callback |

**Sample Code**

```java
TuyaHomeSdk.getDeviceShareInstance().queryGroupSharedUserList(mGroupId, new ITuyaResultCallback<List<SharedUserInfoBean>>() {
            @Override
            public void onSuccess(List<SharedUserInfoBean> result) {
                
            }

            @Override
            public void onError(String errorCode, String errorMessage) {

            }
        });
```



### Share group

**Interface Description**

```java
void addShareUserForGroup(long homeId, String countryCode, String userAccount, long groupId, IResultCallback callback);
```

**Parameter Description**

| Parameters | Description |
| ----------- | ------------------ |
| homeId | family id |
| countryCode | Country Code |
| userAccount | User Account |
| groupId | group id |
| success | Success callback |
| failure | Failure callback |


**Sample Code**

```java
    TuyaHomeSdk.getDeviceShareInstance().addShareUserForGroup(homeId, countryCode, userAccount, groupId, new IResultCallback() {
            @Override
            public void onError(String code, String error) {
                
            }

            @Override
            public void onSuccess() {

            }
        });
```



### Remove device sharing group from members

**Interface Description**


```java
void removeGroupShare(long groupId, long memberId, IResultCallback callback);
```

**Parameter Description**

| Parameters | Description |
| ----------- | ------------------ |
| memberId | member id |
| groupId | group id |
| success | Success callback |
| failure | Failure callback |

**Sample Code**

```java
      TuyaHomeSdk.getDeviceShareInstance().removeGroupShare(groupId, memberId, new IResultCallback() {
            @Override
            public void onError(String code, String error) {

            }

            @Override
            public void onSuccess() {

            }
        });
```