To ensure network security, Wi-Fi passwords might be regularly changed. If backup Wi-Fi networks are unavailable, all smart devices connected to the Wi-Fi network must be paired again after the Wi-Fi password is changed. If you predefine multiple Wi-Fi passwords to create backup Wi-Fi networks, the system automatically switches to backup Wi-Fi networks and reconnects to the smart devices. They do not need to be paired again.

:::important
You can determine if the current device can use the alternate networks by the`deviceBean.getDevAttribute() & (1 << 12)`. `1` indicating yes and `0` means not.
:::

## Get network information

### Get current Wi-Fi information

**Initialization**

```java
ITuyaWifiBackup wifiBackupManager = TuyaHomeSdk.getWifiBackupManager(devId);
```


**API description**

```java
void getCurrentWifiInfo(ITuyaDataCallback<CurrentWifiInfoBean> dataCallback);
```

**Parameter description**

| Parameter | Description |
| ------------ | ---- |
| dataCallback | The callback. |

**Example**

```java

wifiBackupManager.getCurrentWifiInfo(new ITuyaDataCallback<CurrentWifiInfoBean>() {
    @Override
    public void onSuccess(CurrentWifiInfoBean result) {

    }

    @Override
    public void onError(String errorCode, String errorMessage) {

    }
});

```

### Get current backup Wi-Fi networks

**API description**

```java
void getBackupWifiList(ITuyaDataCallback<CurrentWifiInfoBean> dataCallback);
```

**Parameter description**

| Parameter | Description |
| ------------ | ---- |
| dataCallback | The callback. |

**Example**

```java

wifiBackupManager.getBackupWifiList(new ITuyaDataCallback<List<BackupWifiBean>>() {
    @Override
    public void onSuccess(List<BackupWifiBean> result) {

    }

    @Override
    public void onError(String errorCode, String errorMessage) {

        wifiBackupManager.onDestroy();
    }
});

```

### Set a list of backup Wi-Fi networks

**API description**

```java
void setBackupWifiList(List<BackupWifiBean> backupWifiList, ITuyaDataCallback<BackupWifiResultBean> dataCallback);
```

**Parameter description**

| Parameter | Description |
| -------------- | ----------------- |
| backupWifiList | The bean of backup Wi-Fi networks. |
| dataCallback | The callback. |

**Example**

```java
ArrayList<BackupWifiBean> backupWifiList = new ArrayList<>();

// Adds a backup Wi-Fi network and sets its password.
BackupWifiBean backupWifiBean = new BackupWifiBean();
backupWifiBean.ssid = "test1";
backupWifiBean.passwd = "12345678";
backupWifiList.add(backupWifiBean);

// Sets a hash value for an existing backup Wi-Fi network.
String ssid="test2";
String pwd="123123";
DeviceBean dev = TuyaHomeSdk.getDataInstance().getDeviceBean(devId);
String hashStr=SHA256Util.getBase64Hash(dev.getLocalKey() + ssid + pwd);
BackupWifiBean backupWifiBean2 = new BackupWifiBean();
backupWifiBean2.ssid = "test2";
backupWifiBean2.hash = hashStr;
backupWifiList.add(backupWifiBean2);

final ITuyaDataCallback<BackupWifiResultBean> setBackupCallback = new ITuyaDataCallback<BackupWifiResultBean>() {
    @Override
    public void onSuccess(BackupWifiResultBean result) {

    }

    @Override
    public void onError(String errorCode, String errorMessage) {

    }
};
wifiBackupManager.setBackupWifiList(backupWifiList, setBackupCallback);


```


### Remove a listener

**API description**

Exits the page and destroys the listener.

```java
wifiBackupManager.onDestory();
```


## Switch between Wi-Fi networks

### Switch to a new Wi-Fi network
**Initialization**

```java
ITuyaWifiSwitch wifiSwitchManager = TuyaHomeSdk.getWifiSwitchManager(devId);
```

**API description**


``` java
void switchToNewWifi(String ssid, String password, ITuyaDataCallback<SwitchWifiResultBean> callback);
```

**Parameter description**

| Parameter | Description |
| -------- | --------- |
| ssid | The name of the target Wi-Fi network. |
| password | The password of the target Wi-Fi network. |
| callback | The callback. |

**Example**

```java

wifiSwitchManager.switchToNewWifi(String ssid, String password, new ITuyaDataCallback<SwitchWifiResultBean>() {
            @Override
            public void onSuccess(SwitchWifiResultBean result) {
                
            }

            @Override
            public void onError(String errorCode, String errorMessage) {

            }
        });

```

### Switch to a backup Wi-Fi network

**API description**

``` java
void switchToBackupWifi(String hash, ITuyaDataCallback<SwitchWifiResultBean> callback);
```

**Parameter description**

| Parameter | Description |
| -------- | --------------- |
| hash | The hash value of the target Wi-Fi password. |
| callback | The callback. |

**Example**

```java

wifiSwitchManager.switchToBackupWifi(hash,new ITuyaDataCallback<SwitchWifiResultBean>() {
            @Override
            public void onSuccess(SwitchWifiResultBean result) {
                
            }

            @Override
            public void onError(String errorCode, String errorMessage) {

            }
        });
```

### Remove a listener

**API description**

Exits the current Wi-Fi page and destroys the listener for the current Wi-Fi connection.
```java
wifiSwitchManager.onDestory();
```