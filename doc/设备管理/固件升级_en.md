
Firmware upgrades are mainly used to add new device features and repair device bugs;

There are two main types of firmware upgrades. The first is device upgrade and the second is MCU upgrade. 

## Initialize the firmware information

**Declaration**

wifi, zigbee gateway, camera and other equipment:

```java
ITuyaOta newOTAInstance(String devId);
```

**Parameters**

| Parameters | Description |
| ---- | ---- |
| devId | device id |

**Example**

```java
ITuyaOta iTuyaOta = TuyaHomeSdk.newOTAInstance("devId");
```

## Initialize sub-device firmware information

NOTE：Zigbe sub-device upgrades are supported from SDK version 2.9.1.

**Declaration**

sub-device：

```java
ITuyaOta newOTAInstance(String meshId, String devId, String nodeId);
```

**Parameters**

| Parameters  | Description                                  |
| ---- | ---- |
| meshId | Zigbee gateway id                 |
| devId | Zigbee child device id                  |
| nodeId | Zigbee sub-device mac address (obtained from the DeviceBean of the sub-device) |

**Example**

```java
ITuyaOta iTuyaOta = TuyaHomeSdk.newOTAInstance("meshId", "devId", "nodeId");
```

## Obtaining Firmware Upgrade Information

**Declaration**

```java
iTuyaOta.getOtaInfo(new IGetOtaInfoCallback({
	@Override
	void onSuccess(List<UpgradeInfoBean> list) {
	
	}
	@Override
	void onFailure(String code, String error) {
	
	}
});
```

**Parameters**

| Parameters        | Type | Description                                       |
| ---- | ---- | ---- |
| upgradeStatus  | int | upgrade status, 0: no new version 1: new version available 2: in upgrade |
| version     | String | Latest version |
| currentVersion | String | Current version |
| timeout     | int | timeout, unit: second |
| upgradeType | int | 0: app reminder upgrade 2-app forced upgrade 3-detect upgrade |
| type        | int | 0: wifi device 1: Bluetooth device 2: GPRS device 3: zigbee device 9: MCU |
| typeDesc    | String | module description |
| lastUpgradeTime | long  | last upgrade time in milliseconds |

## Set upgrade status callback

**Declaration**

```java
iTuyaOta.setOtaListener(new IOtaListener() {
        @Override
        public void onSuccess(int otaType) {

        }

        @Override
        public void onFailure(int otaType, String code, String error) {

        }

        @Override
        public void onFailureWithText(int otaType, String code,
                                      OTAErrorMessageBean messageBean) {

        }

        @Override
        public void onProgress(int otaType, int progress) {

        }

        @Override
        public void onTimeout(int otaType) {

        }
  
  		@Override
        public void onStatusChanged(int otaStatus, int otaType) {
			// New in version 3.23.0, for low-power devices, this method will return to 
            // the device waiting to be awakened. At this time, the otaStatus parameter returns 5.
        }
});
```

**Parameters**

| Parameters | Description |
| ---- | ---- |
| otaType | The device type to be upgraded, the same as the type field of `UpgradeInfoBean` |

## Start upgrade

**Declaration**

Call this method to start the upgrade. The ota listener registered after the call will return the upgrade status so that the developer can build the UI.

```java
iTuyaOta.startOta();
```

## Destroy

**Declaration**

Destroy after leaving the upgrade page and reclaim memory.

```java
iTuyaOta.onDestory();
```

## Full example
**This example is a normal Wi-Fi device upgrade process，[Bluetooth](https://developer.tuya.com/en/docs/app-development/android-bluetooth-ble?id=Karv7r2ju4c21#title-18-Bluetooth%20OTA) and [Bluetooth Mesh](https://developer.tuya.com/en/docs/app-development/meshsig?id=Ka6km4aeuygxr#title-35-Firmware%20upgrade) please refer to the corresponding chapter.**

```java
		private ITuyaOta iTuyaOta;
    private String mDevId = "your_devId";

    public void startOTA() {
        initOta();
        initListener();
        checkOTAInfo();
    }

    // Initialize firmware information.
    private void initOta() {
        iTuyaOta = TuyaHomeSdk.newOTAInstance(mDevId);
    }

    // Add firmware upgrade listener.
    private void initListener() {
        iTuyaOta.setOtaListener(new IOtaListener() {
            @Override
            public void onSuccess(int otaType) {
                iTuyaOta.onDestroy();
                Log.i(TAG, "upgrade success");
            }

            @Override
            public void onFailure(int otaType, String code, String error) {
                iTuyaOta.onDestroy();
                Log.i(TAG, "upgrade failure, errorCode = " + code
                        + ",errorMessage = " + error);
            }

            @Override
            public void onFailureWithText(int otaType, String code, OTAErrorMessageBean messageBean) {
                iTuyaOta.onDestroy();
                Log.i(TAG, "upgrade failure, errorCode = " + code 
                        + ",errorMessage = " + messageBean.text);
            }

            @Override
            public void onProgress(int otaType, int progress) {
                Log.i(TAG, "upgrade progress = " + progress);
            }

            @Override
            public void onTimeout(int otaType) {
                iTuyaOta.onDestroy();
                Log.i(TAG, "upgrade timeout");
            }

            @Override
            public void onStatusChanged(int otaStatus, int otaType) {

            }
        });
    }

    // Get firmware upgrade information.
    private void checkOTAInfo() {
        iTuyaOta.getOtaInfo(new IGetOtaInfoCallback() {
            @Override
            public void onSuccess(List<UpgradeInfoBean> upgradeInfoBeans) {
                if (hasHardwareUpdate(upgradeInfoBeans)) {
                    // Start to upgrade.
                    iTuyaOta.startOta();
                }
            }

            @Override
            public void onFailure(String code, String error) {
                Log.i(TAG, "Get OTA info failed,errorCode=" + code
                        + ",errorMessage=" + error);
            }
        });
    }

    private boolean hasHardwareUpdate(List<UpgradeInfoBean> list) {
        if (null == list || list.size() == 0)
            return false;
        for (UpgradeInfoBean upgradeInfoBean : list) {
            if (upgradeInfoBean != null && upgradeInfoBean.getUpgradeStatus() == 1)
                return true;
        }
        return false;
    }
```