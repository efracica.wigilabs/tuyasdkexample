Tuya Smart provides a wealth of interfaces for developers to achieve device information acquisition and management capabilities (removal, etc.). The device-related return data is notified to the receiver in the form of asynchronous messages. The ITuyaDevice class provides the device status notification capability. By registering the callback function, developers can easily obtain notifications of device data acceptance, device removal, device online and offline, and mobile phone network changes. It also provides an interface for issuing control commands and upgrading device firmware

## DeviceBean data model

| Field|Type|Description|
| :--| :--| :--|
| devId |String|Device unique identification id|
| name |String|device name|
| iconUrl |String|Icon address|
| getIsOnline |Boolean|Whether the device is online (LAN or cloud online)|
| schema |String|Type information of device control data points|
| productId |String|Product ID, the same product ID, Schema information is consistent|
| supportGroup |Boolean|Does the device support groups, if not, please go to the open platform to enable this function|
| time | Long |Device activation time|
| pv | String |Gateway Protocol Version|
| bv | String |Gateway general firmware version|
| schemaMap | Map |Schema cache data|
| dps | Map | Device current data information. The key is dpId and value is value|
| isShare | boolean |Is it a sharing device|
| virtual|boolean |Is it a virtual device|
| lon, lat |String| are used to mark the latitude and longitude information, the user needs to call TuyaSdk.setLatAndLong to set the latitude and longitude information before using the SDK |
| isLocalOnline|boolean|Device LAN online status|
| nodeId |String|Used for devices of the gateway and sub-device type. It is an attribute of the sub-device and identifies its short address ID. The nodeId under a gateway is unique|
| timezoneId |String|Device time zone|
| category | String |Device Type|
| meshId |String|Used for devices of the gateway and sub-device types. It is an attribute of sub-devices and identifies its gateway ID|
| isZigBeeWifi |boolean|Is it a ZigBee gateway device|
| hasZigBee |boolean|hasZigBee|

## Precautions

If you need to use the latitude and longitude for device control, you need to call the method to set the latitude and longitude before network distribution:

```java
TuyaSdk.setLatAndLong(String latitude, String longitude)
```

## Device initialization
### Initialize family data


The device control must first initialize the data, call the following method to obtain the device information under the family, and it is sufficient to initialize it once every time the APP is alive. In addition, the switching family also needs to be initialized:

```java
TuyaHomeSdk.newHomeInstance(homeId).getHomeDetail(new ITuyaHomeResultCallback() {
    @Override
    public void onSuccess(HomeBean homeBean) {
    To
    }

    @Override
    public void onError(String errorCode, String errorMsg) {

    }
});
```

The onSuccess method of this interface will return `HomeBean`, and then call `getDeviceList` of `HomeBean` to get the device list:

```java
List<DeviceBean> deviceList = homeBean.getDeviceList();
```

### Initialize device control

**Interface Description**

Initialize the device control class according to the device id

```java
TuyaHomeSdk.newDeviceInstance(String devId);
```

**Parameter Description**

| Parameters | Description |
| ---- | --- |
| devId |device id|

**Sample Code**

```java
ITuyaDevice mDevice = TuyaHomeSdk.newDeviceInstance(deviceBean.getDevId());
```

## Device monitoring

### Register device monitoring

**Interface Description**

ITuyaDevice provides monitoring of device-related information (DP data, device name, device online status, and device removal), which will be synchronized here in real-time.

```java
ITuyaDevice.registerDevListener(IDevListener listener)
```

**Parameter Description**

| Parameters | Description |
| ---- | --- |
| listener| Device status monitoring|

The interface of `IDevListener` is as follows:

```java
public interface IDevListener {

    /**
     * dp data update
     *
     * @param devId device id
     * @param dpStr The function point of the device change, it is a json string, the data format: {"101": true}
     */
    void onDpUpdate(String devId, String dpStr);

    /**
     * Device removal callback
     *
     * @param devId device id
     */
    void onRemoved(String devId);

    /**
     * Device offline callback. If the device is powered off or disconnected from the network, the server will call back to this method after 3 minutes.
     *
     * @param devId device id
     * @param online is online, online is true
     */
    void onStatusChanged(String devId, boolean online);

    /**
     * Callback when the network status changes
     *
     * @param devId device id
     * @param status Whether the network status is available, can be true
     */
    void onNetworkStatusChanged(String devId, boolean status);

    /**
     * Device information update callback
     *
     * @param devId device id
     */
    void onDevInfoUpdate(String devId);

}
```

Among them, the description of equipment function points can be found in the section [Device Function Point Description](#DeviceFunctionPoint) in the document.

**Sample Code**

```java
mDevice.registerDevListener(new IDevListener() {
    @Override
    public void onDpUpdate(String devId, String dpStr) {

    }
    @Override
    public void onRemoved(String devId) {

    }
    @Override
    public void onStatusChanged(String devId, boolean online) {

    }
    @Override
    public void onNetworkStatusChanged(String devId, boolean status) {

    }
    @Override
    public void onDevInfoUpdate(String devId) {

    }
});
```

>**Note**: Do not use the `void registerDeviceListener(IDeviceListener listener)` method. This method needs to be used with standard equipment. The API is not yet open to the public.

### Cancel device monitoring

When you do not need to monitor the device, log off the device monitor.

**Interface Description**

```java
ITuyaDevice.unRegisterDevListener()
```

**Sample Code**


```java
mDevice.unRegisterDevListener();
```
<div style="display:none">
<!--## Device offline remind ability query-->

device offline remind ability query

**Interface Description**

```java
void getOfflineReminderSupportStatus(String hdId, ITuyaResultCallback<IsSupportOffLineBean> callback);
```
**Parameter Description**

| Parameters | Description |
| ---- | --- |
| hdId| device ID|
| callback| device ability motoring |

**Sample Code**

```java
 mDevice.getOfflineReminderSupportStatus(deviceId, new ITuyaResultCallback<IsSupportOffLineBean>() {
            @Override
            public void onSuccess(IsSupportOffLineBean result) {

            }

            @Override
            public void onError(String errorCode, String errorMessage) {

            }
        });
    }
```
</div>

## Device information query

**Interface Description**

Query single DP data.

This interface is not a synchronous interface, and the queried data will be called back through the `IDevListener.onDpUpdate()` interface.

```java
mDevice.getDp(String dpId, IResultCallback callback);
```

**Sample code**

```java
mDevice.getDp(dpId, new IResultCallback() {
    @Override
    public void onError(String code, String error) {

    }

    @Override
    public void onSuccess() {

    }
});
```


> **Important**
>
> This interface is mainly for DPs where data is not reported actively, such as countdown information query. Regular query DP value can be obtained through getDps() in DeviceBean.

## Modify device name

**Interface Description**

Rename the device and support multi-device synchronization.

```java
//Rename
mDevice.renameDevice(String name,IResultCallback callback);
```

**Sample Code**

```java
mDevice.renameDevice("device name", new IResultCallback() {
    @Override
    public void onError(String code, String error) {
        //Rename failed
    }
    @Override
    public void onSuccess() {
        //Rename successfully
    }
});
```

After success, `IDevListener.onDevInfoUpdate()` will be notified.

Call the following method to get the latest data, and then refresh the device information.

```java
TuyaHomeSdk.getDataInstance().getDeviceBean(String devId);
```

## Remove device

**Interface Description**

Used to remove a device from the user device list

```java
void removeDevice(IResultCallback callback);
```

**Sample Code**

```java
mDevice.removeDevice(new IResultCallback() {
    @Override
    public void onError(String errorCode, String errorMsg) {
    }

    @Override
    public void onSuccess() {
    }
});
```

## Reset

**Interface Description**

It is used to reset the device and restore it to the factory state. After the device is restored to the factory settings, it will re-enter the network-ready state (quick connection mode), and the related data of the device will be cleared.

```java
void resetFactory(IResultCallback callback);
```

**Sample Code**

```java
mDevice.resetFactory(new IResultCallback() {
    @Override
    public void onError(String errorCode, String errorMsg) {
    }

    @Override
    public void onSuccess() {
    }
});
```

## Query Wi-Fi signal strength

Get the current device Wi-Fi signal strength

**Interface Description**

```java
void requestWifiSignal(WifiSignalListener listener);
```

**Sample Code**

```java
mDevice.requestWifiSignal(new WifiSignalListener() {
     
     @Override
     public void onSignalValueFind(String signal) {
      
     }
     
     @Override
     public void onError(String errorCode, String errorMsg) {

     }
 });;
```

## Recycling device resources

**Interface Description**

When the application or activity is closed, you can call this interface to reclaim the resources occupied by the device.

```java
void onDestroy()
```

**Sample Code**

```java
mDevice.onDestroy();
```
