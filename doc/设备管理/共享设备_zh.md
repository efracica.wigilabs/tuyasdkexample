
当需要将家庭下某一个设备单独提供给其他用户操作时，可将设备共享给其他用户，收到共享设备的用户可在自身账号下对设备进行简单操作

| 类名             | 说明               |
| ---- | ---- |
| ITuyaHomeDeviceShare | 提供设备分享相关的功能 |

## 添加共享

### 添加多个设备共享（覆盖）

分享多个设备给指定用户，会将指定用户的以前所有分享覆盖掉

**接口说明**

```java
void addShare(long homeId, String countryCode, final String userAccount, ShareIdBean bean, boolean autoSharing, final ITuyaResultCallback<SharedUserInfoBean> callback);
```

**参数说明**

| 参数    | 说明                              |
| ---- | ---- |
| homeId  | 设备所属的家庭 id                 |
| countryCode | 手机区号码,例如中国是 “86”        |
| userAccount | 分享对象的账号                    |
| bean    | 分享的设备 id 列表                |
| autoSharing | 自动分享                          |
| callback | 回调，包括分享成功或失败，不能为 null |

此外，`ShareIdBean` 的数据结构如下所示：

| 参数 | 类型           | 说明                 |
| ---- | ---- | ---- |
| devIds | List&lt;String&gt; | 分享的设备 id 列表   |
| meshIds | List&lt;String&gt; | 分享的 mesh 设备 id 列表 |

**示例代码**

```java
TuyaHomeSdk.getDeviceShareInstance().addShare(homeId, countryCode, userAccount,
                shareIdBean, autoSharing, new ITuyaResultCallback<SharedUserInfoBean>() {
                    @Override
                    public void onSuccess(SharedUserInfoBean sharedUserInfoBean) {}
                    @Override
                    public void onError(String errorCode, String errorMsg) {}
                });
```


### 添加共享（新增，不覆盖旧的分享）

分享多个设备给指定用户，会将要分享的设备追加到指定用户的所有分享中。如果之前向某个指定用户分享过设备，那么您可以通过下面的 **分享关系 API** 获得目标用户 id，从而使用本方法向该用户快捷分享设备。

**接口说明**

```java
void addShareWithMemberId(long memberId,List<String> devIds,IResultCallback callback);
```

**参数说明**

| 参数 | 说明                              |
| ---- | ---- |
| memberId | 分享目标用户 id                   |
| devIds  | 分享的设备 id 列表                |
| callback | 回调，包括分享成功或失败，不能为 null |

**示例代码**

```java
TuyaHomeSdk.getDeviceShareInstance().addShareWithMemberId(userId, devIdList, new IResultCallback() {
            @Override
            public void onError(String errorCode, String errorMsg) {
            }
            @Override
            public void onSuccess() {
            }
});
```


### 设备添加共享
分享单个或多个设备给指定用户，会将要分享的设备追加到指定用户的所有分享中

**接口说明**

```java
void addShareWithHomeId(long homeId, String countryCode, String userAccount, List<String> devIds, ITuyaResultCallback<SharedUserInfoBean> callback);
```

**参数说明**

| 参数    | 说明                              |
| ---- | ---- |
| homeId  | 设备的家庭 id                     |
| countryCode | 手机区号码,例如中国是 “86”         |
| userAccount | 被分享者账号                |
| devIds  | 分享的设备 id 列表                  |
| callback | 回调，包括分享成功或失败，不能为 null |

**示例代码**

```java
TuyaHomeSdk.getDeviceShareInstance().addShareWithHomeId(homeId, countryCode,
                userAccount, devsIdList, new ITuyaResultCallback<SharedUserInfoBean>() {
                    @Override
                    public void onSuccess(SharedUserInfoBean sharedUserInfoBean) {
                    }

                    @Override
                    public void onError(String errorCode, String errorMsg) {                       
                    }
});
```

## 分享关系获取

### 获取家庭下所有主动共享的用户列表

**接口说明**

```java
void queryUserShareList(long homeId, final ITuyaResultCallback<List<SharedUserInfoBean>> callback);
```

**参数说明**

| 参数 | 说明                              |
| ---- | ---- |
| homeId  | 家庭id                            |
| callback | 回调，包括获取成功或失败，不能为 null |

**示例代码**

```java
TuyaHomeSdk.getDeviceShareInstance().queryUserShareList(homeId, new ITuyaResultCallback<List<SharedUserInfoBean>>() {
            @Override
            public void onSuccess(List<SharedUserInfoBean> sharedUserInfoBeans) {}
            @Override
            public void onError(String errorCode, String errorMsg) {}
        });
```



### 获取所有收到共享的用户列表

**接口说明**

```java
void queryShareReceivedUserList(final ITuyaResultCallback<List<SharedUserInfoBean>> callback);
```

**参数说明**

| 参数 | 说明                             |
| ---- | ---- |
| callback | 回调，包括获取成功或失败，不能为null |

**示例代码**

```java
TuyaHomeSdk.getDeviceShareInstance().queryShareReceivedUserList(new ITuyaResultCallback<List<SharedUserInfoBean>>() {
    @Override
    public void onSuccess(List<SharedUserInfoBean> sharedUserInfoBeans) {
    }
    @Override
    public void onError(String errorCode, String errorMsg) {
    }
});
```



### 获取单个主动共享的用户共享数据

**接口说明**

```java
void getUserShareInfo(long memberId, final ITuyaResultCallback<ShareSentUserDetailBean> callback);
```

**参数说明**

| 参数 | 说明                             |
| ---- | ---- |
| memberId | 分享用户的id                     |
| callback | 回调，包括获取成功或失败，不能为null |

**示例代码**

```java
TuyaHomeSdk.getDeviceShareInstance().getUserShareInfo(mRelationId, new ITuyaResultCallback<ShareSentUserDetailBean>() {
                @Override
                public void onSuccess(ShareSentUserDetailBean shareSentUserDetailBean) {}
                @Override
                public void onError(String errorCode, String errorMsg) {}
            });
```



### 获取单个收到共享的用户共享数据

**接口说明**

```java
void getReceivedShareInfo(long memberId, final ITuyaResultCallback<ShareReceivedUserDetailBean> callback);
```

**参数说明**

| 参数 | 说明                             |
| ---- | ---- |
| memberId | 用户id                           |
| callback | 回调，包括获取成功或失败，不能为null |

**示例代码**

```java
 TuyaHomeSdk.getDeviceShareInstance().getReceivedShareInfo(mRelationId, new ITuyaResultCallback<ShareReceivedUserDetailBean>() {
                @Override
                public void onSuccess(ShareReceivedUserDetailBean shareReceivedUserDetailBean) {}
                @Override
                public void onError(String errorCode, String errorMsg) {}
            });
```



### 获取单设备共享用户列表

**接口说明**

```java
void queryDevShareUserList(String devId, final ITuyaResultCallback<List<SharedUserInfoBean>> callback);
```

**参数说明**

| 参数 | 说明                             |
| ---- | ---- |
| devId | 分享的设备id                     |
| callback | 回调，包括获取成功或失败，不能为null |

**示例代码**

```java
TuyaHomeSdk.getDeviceShareInstance().queryDevShareUserList(mDevId, new ITuyaResultCallback<List<SharedUserInfoBean>>() {
        @Override
        public void onError(String errorCode, String errorMsg) {}
        @Override
        public void onSuccess(List<SharedUserInfoBean> shareUserBeen) {}
    });
```



### 获取设备分享来自哪里

**接口说明**

```java
void queryShareDevFromInfo(String devId, final ITuyaResultCallback<SharedUserInfoBean> callback);
```

**参数说明**

| 参数 | 说明                             |
| ---- | ---- |
| devId | 设备id                           |
| callback | 回调，包括获取成功或失败，不能为null |

**示例代码**

```java
TuyaHomeSdk.getDeviceShareInstance().queryShareDevFromInfo(devId, new ITuyaResultCallback<SharedUserInfoBean>() {
            @Override
            public void onSuccess(SharedUserInfoBean result) {
            }
            @Override
            public void onError(String errorCode, String errorMessage) {
            }
        });
```

## 移除共享

### 删除主动共享者

共享者通过memberId 删除与这个关系用户的所有共享关系（用户维度删除）。

**接口说明**

```java
void removeUserShare(long memberId, IResultCallback callback);
```

**参数说明**

| 参数 | 说明                              |
| ---- | ---- |
| memberId | 分享用户 id                       |
| callback | 回调，包括删除成功或失败，不能为 null |

**示例代码**

```java
TuyaHomeSdk.getDeviceShareInstance().removeUserShare(memberId, new IResultCallback() {
    @Override
    public void onError(String code, String error) {
    }
    @Override
    public void onSuccess() {
    }
})
```



### 删除收到共享者

被共享者通过 memberId 获取收到这个关系用户的所有共享设备信息。

**接口说明**

```java
void removeReceivedUserShare(long memberId, IResultCallback callback);
```

**参数说明**

| 参数 | 说明                              |
| ---- | ---- |
| memberId | 收到分享的用户id                  |
| callback | 回调，包括删除成功或失败，不能为 null |

**示例代码**

```java
TuyaHomeSdk.getDeviceShareInstance().removeReceivedUserShare(memberId, new IResultCallback() {
    @Override
    public void onError(String code, String error) {
    }
    @Override
    public void onSuccess() {
    }
})
```



### 单设备删除共享

**接口说明**

```java
void disableDevShare(String devId, long memberId, IResultCallback callback);
```

**参数说明**

| 参数 | 说明                              |
| ---- | ---- |
| devId | 分享设备的 id                     |
| memberId | 分享用户的 id                     |
| callback | 回调，包括删除成功或失败，不能为 null |

**示例代码**

```java
TuyaHomeSdk.getDeviceShareInstance().disableDevShare (devId, memberId, new IResultCallback() {
    @Override
    public void onError(String code, String error) {
    }
    @Override
    public void onSuccess() {
    }
});
```



### 删除收到的共享设备

**接口说明**

```java
void removeReceivedDevShare(String devId, IResultCallback callback);
```

**参数说明**

| 参数 | 说明                              |
| ---- | ---- |
| devId | 分享的设备 id                     |
| callback | 回调，包括删除成功或失败，不能为 null |

**示例代码**

```java
TuyaHomeSdk.getDeviceShareInstance().removeReceivedDevShare(devId,new IResultCallback() {
    @Override
    public void onError(String code, String error) {}
    @Override
    public void onSuccess() {}
})

```

## 修改备注名

### 修改主动共享者的昵称

**接口说明**

```java
void renameShareNickname(long memberId, String name, IResultCallback callback);
```

**参数说明**

| 参数    | 说明                              |
| ---- | ---- |
| memberId | 分享用户的id                      |
| name | 分享用户的新昵称 |
| callback | 回调，包括修改成功和失败，不能为 null  |

**示例代码**

```java
TuyaHomeSdk.getDeviceShareInstance().renameShareNickname(mRelationId, inputText, new IResultCallback() {
                @Override
                public void onError(String s, String s1) {}
                @Override
                public void onSuccess() {}
            });
```



### 修改收到共享者的昵称

**接口说明**

```java
void renameReceivedShareNickname(long memberId, String name, IResultCallback callback);
```

**参数说明**

| 参数 | 说明                               |
| ---- | ---- |
| memberId | 共享者家庭 id 从 SharedUserInfoBean 中获取 |
| name | 分享用户的新昵称                       |
| callback | 回调，包括修改成功和失败，不能为 null |

**示例代码**

```java
TuyaHomeSdk.getDeviceShareInstance().renameReceivedShareNickname(mRelationId, inputText, new IResultCallback() {
                @Override
                public void onError(String s, String s1) {}
                @Override
                public void onSuccess() {}
            });
```

## 分享邀请

### 向其他用户发送共享设备的邀请

**接口说明**


```java
void inviteShare(String devId, String userAccount, String countryCode, ITuyaResultCallback<Integer> callback);
```

**参数说明**

| 参数        | 说明               |
| ----------- | ------------------ |
|  countryCode   |	国家码  |
|  userAccount   |	用户账号  |
|  devId     |	设备id  |
|  success     |	成功回调  |
|  failure     |	失败回调	  |

**示例代码**

```java
    TuyaHomeSdk.getDeviceShareInstance().inviteShare(devId, userAccount, countryCode, new ITuyaResultCallback<Integer>() {
                @Override
                public void onSuccess(Integer result) {
                    
                }
    
                @Override
                public void onError(String errorCode, String errorMessage) {
    
                }
            });
```


### 确认分享邀请

**接口说明**


```java
 void confirmShareInviteShare(int shareId, final IResultCallback callback);
```

**参数说明**

| 参数        | 说明               |
| ----------- | ------------------ |
| 	shareId     |		分享id	  |
| 	success     |		成功回调	  |
| 	failure     |		失败回调	  |

**示例代码**

```java
 TuyaHomeSdk.getDeviceShareInstance().confirmShareInviteShare(shareId, new IResultCallback() {
            @Override
            public void onError(String code, String error) {
                
            }

            @Override
            public void onSuccess() {

            }
        });
```


## 群组分享

### 按组ID返回设备共享用户列表

备注：群组ID显示在面板上

**接口说明**

```java
void queryGroupSharedUserList(long groupId, ITuyaResultCallback<List<SharedUserInfoBean>> callback);
```

**参数说明**

| 参数        | 说明               |
| ----------- | ------------------ |
| 	groupId     |		群组id	  |
| 	success     |		成功回调	  |
| 	failure     |		失败回调	  |


**示例代码**

```java
TuyaHomeSdk.getDeviceShareInstance().queryGroupSharedUserList(mGroupId, new ITuyaResultCallback<List<SharedUserInfoBean>>() {
            @Override
            public void onSuccess(List<SharedUserInfoBean> result) {
                
            }

            @Override
            public void onError(String errorCode, String errorMessage) {

            }
        });
```


### 分享群组

**接口说明**


```java
void addShareUserForGroup(long homeId, String countryCode, String userAccount, long groupId, IResultCallback callback);
```

**参数说明**

| 参数        | 说明               |
| ----------- | ------------------ |
| 	homeId     |		家庭id	  |
| 	countryCode     |		国家码	  |
| 	userAccount     |		用户账号	  |
| 	groupId     |		群组id	  |
| 	success     |		成功回调	  |
| 	failure     |		失败回调	  |


**示例代码**

```java
    TuyaHomeSdk.getDeviceShareInstance().addShareUserForGroup(homeId, countryCode, userAccount, groupId, new IResultCallback() {
            @Override
            public void onError(String code, String error) {
                
            }

            @Override
            public void onSuccess() {

            }
        });
```


### 将设备共享组从成员中移除

**接口说明**


```java
void removeGroupShare(long groupId, long memberId, IResultCallback callback);
```

**参数说明**

| 参数        | 说明               |
| ----------- | ------------------ |
| 	memberId     |		成员id	  |
| 	groupId     |		群组id	  |
| 	success     |		成功回调	  |
| 	failure     |		失败回调	  |


**示例代码**

```java
      TuyaHomeSdk.getDeviceShareInstance().removeGroupShare(groupId, memberId, new IResultCallback() {
            @Override
            public void onError(String code, String error) {

            }

            @Override
            public void onSuccess() {

            }
        });
```