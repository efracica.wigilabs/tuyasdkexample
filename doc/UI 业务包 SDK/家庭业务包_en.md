The home biz bundle includes businesses such as home, member, room, and more, which are the basic conditions for the management of the devices after devices are activated.

The device can set the room where the device is located in the home after the device configuration, meanwhile, the family members who have different authority under the home correspond to different operation authority, and the home is also the largest unit of scene intelligence execution.

## Integration

### Create project

Create your project in Android Studio, connect to the public SDK, and configure [Biz Bundle Framework](https://developer.tuya.com/en/docs/app-development/android-bizbundle-sdk/integration?id=Ka8qhzk13vkfq).

### The build.gradle configuration

``` groovy
dependencies {
  implementation 'com.tuya.smart:tuyasmart-bizbundle-family:1.0.0-cube.2'
}
```

## Functions

### Go to the home management page

The home management page supports redirection through the following routing method:

```java
UrlRouter.execute(UrlRouter.makeBuilder(FamilyManageActivity.this, "family_manage"));
```

### Accept or refuse to join home

At present, the business of accepting invitations is not within the scope of biz bundle processing. You can handle this part of the logic on your application homepage or other places where you want to receive invitations.

Since messages for invitations are sent down via MQTT push, the home biz bundle provides the following methods to listen for home invitation notifications.

```java
// register listener
TuyaHomeSdk.getHomeManagerInstance()
	.registerTuyaHomeChangeListener(HomeInviteListener listener);
// unregister listener
TuyaHomeSdk.getHomeManagerInstance()
	.unRegisterTuyaHomeChangeListener(HomeInviteListener listener);
```

you can implement it yourself in the following way:

```java
TuyaHomeSdk.getHomeManagerInstance()
	.registerTuyaHomeChangeListener(new HomeInviteListener() {
			@Override
			public void onHomeInvite(long homeId, String homeName) {
				// accept or reject invitation from this home
				TuyaHomeSdk.getMemberInstance()
					.processInvitation(homeId, isAccept, new IResultCallback() {
						@Override
						public void onError(String errorCode, String errorMsg) {

						}

						@Override
						public void onSuccess() {
							// Do something like refresh home list
						}
						});
			}
	});
```
