This topic describes generic configurations to integrate a UI BizBundle of Tuya Smart Life App SDK with your Android project.

> **Note**: Tuya has deprecated the `Classpath` field in `com.tuya.android.module:tymodule-config:atestVersion`. To achieve optimal compatibility, we recommend that you remove this field.

## Prerequisites

For the UI BizBundle, the value of `android:minSdkVersion` is `19` and the value of `android:targetSdkVersion` is `29`. Your app can only be built with AndroidX.

## Configure the UI BizBundle

This section describes the configuration files and Android dependencies that are required by the UI BizBundle.

### Configuration files

After the UI BizBundle is updated, you must download the configuration files again. For more information about the updates, see [Release Note](https://developer.tuya.com/en/docs/app-development/changelog?id=Ka8ugz8p5khu6).

| Item | Description | Required |
| --- | --- | --- |
| assets | [module_app.json](../../ui_bizbundle_android_config_values/assets/) and [x_platform_config.json](../../ui_bizbundle_android_config_values/assets/) are service configuration files. Copy and paste them to the `assets` folder in the `app` directory to make the configurations effective. | Yes |
| res | <ul><li> [res/values/compat-colors.xml](../../ui_bizbundle_android_config_values/res/values/compat-colors.xml): the configuration file and default color settings of the UI BizBundle.</li><li> [res/values](../../ui_bizbundle_android_config_values/res/values/): the resource configuration file.</li><li>	[res](../../ui_bizbundle_android_config_values/res/): the optional multilingual configuration file to support your desired features. | `res/values/compat-colors.xml` and `res/values` are required. |
| Theme colors | <ul><li> [res/values/colors.xml](../../ui_bizbundle_android_config_values/res/values/colors.xml): the theme resource configurations.</li><li> [assets/tyTheme/ui_theme_config.json](../../ui_bizbundle_android_config_values/assets/tyTheme/ui_theme_config.json): the assets configurations for theme colors.</li></ul><div styled-block data-style-important><div class="styled-block-title"><span class="styled-block-title-icon"></span><span class="styled-block-title-label"></span></div><div class="styled-block-content">The files `colors.xml` and `ui_theme_config.json` must be both modified to configure theme colors. </div></div> | No[No more maintenance, recommended to use the theme color configuration UI Bizbundle](https://developer.tuya.com/en/docs/app-development/android-colors-theme?id=Kapl9ox0ysvgs) |


**Theme colors** configurations:

- **`colors.xml` parameters**

   | Parameter | Description | Default value |
   | --- | --- | --- |
   | ty_theme_color_m1 | Theme color resources in normal state | #FFFF5A28 |
   | ty_theme_color_m1_1 | Theme color resources in disabled state | #33FF5A28 |
   | ty_theme_color_m1_2 | Theme color resources in pressed state | #FFFF6B3E |
   | ty_theme_color_m1_alpha_5 | Theme color resources with transparency set to 5% | #0DFF5A28 |
   | ty_theme_color_m1_alpha_10 | Theme color resources with transparency set to 10% | #1AFF5A28 |
   | ty_theme_color_m1_alpha_15 | Theme color resources with transparency set to 15% | #26FF5A28 |
   | ty_theme_color_m1_alpha_20 | Theme color resources with transparency set to 20% | #33FF5A28 |
   | ty_theme_color_m1_alpha_25 | Theme color resources with transparency set to 25% | #40FF5A28 |
   | ty_theme_color_m1_alpha_30 | Theme color resources with transparency set to 30% | #4DFF5A28 |
   | ty_theme_color_m1_alpha_35 | Theme color resources with transparency set to 35% | #59FF5A28 |
   | ty_theme_color_m1_alpha_40 | Theme color resources with transparency set to 40% | #66FF5A28 |
   | ty_theme_color_m1_alpha_45 | Theme color resources with transparency set to 45% | #73FF5A28 |
   | ty_theme_color_m1_alpha_50 | Theme color resources with transparency set to 50% | #80FF5A28 |
   | ty_theme_color_m1_alpha_55 | Theme color resources with transparency set to 55% | #8CFF5A28 |
   | ty_theme_color_m1_alpha_60 | Theme color resources with transparency set to 60% | #99FF5A28 |
   | ty_theme_color_m1_alpha_65 | Theme color resources with transparency set to 65% | #A6FF5A28 |
   | ty_theme_color_m1_alpha_70 | Theme color resources with transparency set to 70% | #B3FF5A28 |
   | ty_theme_color_m1_alpha_75 | Theme color resources with transparency set to 75% | #BFFF5A28 |
   | ty_theme_color_m1_alpha_80 | Theme color resources with transparency set to 80% | #CCFF5A28 |
   | ty_theme_color_m1_alpha_85 | Theme color resources with transparency set to 85% | #D9FF5A28 |
   | ty_theme_color_m1_alpha_90 | Theme color resources with transparency set to 90% | #E6FF5A28 |
   | ty_theme_color_m1_alpha_95 | Theme color resources with transparency set to 95% | #F2FF5A28 |

- **`ui_theme_config.json` configurations:**

	```json
	{
		"color_alpha":{
		"N1":0.9,
		"N2":0.7,
		"N3":0.5,
		"N4":0.3,
		"N5":0.7,
		"N6":0.2,
		"N7":0.1,
		"N8":0.4
		},
		"colors":{
		"B1":"#F8F8F8",
		"B2":"#FFFFFF",
		"B3":"#FFFFFF",
		"B4":"#FFFFFF",
		"B5":"#FFFFFF",
		"M1":"#FF5A28", // `M1` means the theme color setting. Default value: `#FF5A28`. Only `M1` can be modified. Other fields cannot be modified.
		"M2":"#FF4444",
		"M3":"#00CC99",
		"M4":"#1989FA",
		"M5":"#FF5A28"
		},
		"corner":{
		"alert_corner_type":"angle",
		"button_corner_type":"angle",
		"card_corner_type":"angle"
		},
		"font":{
		"app_bold_font":"",
		"app_font":""
		}
	}
	```

### Drawables

`bg_text_bts.xml`

``` xml
<?xml version="1.0" encoding="utf-8"?>
<selector xmlns:android="http://schemas.android.com/apk/res/android">

	<item android:state_focused="true">
		<shape android:shape="rectangle">
			<corners android:radius="@dimen/mg_2" />

			<solid android:color="@color/primary_button_select_color" />
		</shape>
	</item>
	<item android:state_selected="true">
		<shape android:shape="rectangle">
			<corners android:radius="@dimen/mg_2" />

			<solid android:color="@color/primary_button_select_color" />
		</shape>
	</item>
	<item android:state_pressed="true">
		<shape android:shape="rectangle">
			<corners android:radius="@dimen/mg_2" />

			<solid android:color="@color/primary_button_select_color" />
		</shape>
	</item>
	<item android:state_enabled="true">
		<shape android:shape="rectangle">
			<corners android:radius="@dimen/mg_2" />

			<solid android:color="@color/primary_button_bg_color" />
		</shape>
	</item>
	<item android:state_enabled="false">
		<shape>
			<corners android:radius="@dimen/mg_2" />

			<solid android:color="@color/gray_99" />
		</shape>
	</item>

</selector>
```

### Styles

[res/values/styles.xml](../../ui_bizbundle_android_config_values/res/values/)

``` xml
<!-- Required configurations -->
<!-- Base application theme -->
<style name="Base_BizBundle_Theme" parent="AppTheme">
	<item name="status_font_color">@color/status_font_color</item>
	<item name="status_bg_color">@color/status_bg_color</item>
	<item name="navbar_font_color">@color/navbar_font_color</item>
	<item name="navbar_bg_color">@color/navbar_bg_color</item>
	<item name="app_bg_color">@color/app_bg_color</item>
	<item name="fragment_bg_color">@color/app_bg_color</item>
	<item name="list_primary_color">@color/list_primary_color</item>
	<item name="list_sub_color">@color/list_sub_color</item>
	<item name="list_secondary_color">@color/list_secondary_color</item>
	<item name="list_line_color">@color/list_line_color</item>
	<item name="list_bg_color">@color/list_bg_color</item>
	<item name="primary_button_font_color">@color/primary_button_font_color</item>
	<item name="primary_button_bg_color">@color/primary_button_bg_color</item>
	<item name="secondary_button_font_color">@color/secondary_button_font_color</item>
	<item name="secondary_button_bg_color">@color/secondary_button_bg_color</item>
	<item name="notice_font_color">@color/notice_font_color</item>
	<item name="notice_bg_color">@color/notice_bg_color</item>
	<item name="bg_normal_text_bt">@drawable/bg_text_bts</item>
	<item name="app_name">@string/app_name</item>
	<item name="is_splash_used">false</item>
	<item name="ap_default_ssid">@string/ap_mode_ssid</item>
	<item name="ap_connect_description">@string/ty_ap_connect_description</item>
	<item name="is_scan_support">@bool/is_scan_support</item>
	<item name="is_need_blemesh_support">@bool/is_need_blemesh_support</item>
	<item name="status_bg_color_75">@color/status_bg_color_75</item>
	<item name="status_bg_color_90">@color/status_bg_color_90</item>
</style>
```

### Application declaration (AndroidManifest.xml)

``` xml
<!-- Required configurations -->
<application
	android:supportsRtl="false"
	android:theme="@style/Base_BizBundle_Theme"
	tools:replace="android:theme"/>
```

### Dependency sources

- `build.gradle` configurations in the root directory:

	```java
	allprojects {
		repositories {
			jcenter()
			maven { url 'https://maven-other.tuya.com/repository/maven-releases/' }
			maven { url 'https://jitpack.io' }
		}
	}
	```

- `build.gradle` configurations for the app:

   > Note: The Native Development Kit (NDK) configuration only supports CPU versions of Arm architectures.

	``` java
	android {
		packagingOptions {
			pickFirst 'lib/*/libc++_shared.so'
			pickFirst 'lib/*/libgnustl_shared.so'
		}

		lintOptions {
			abortOnError false
		}

		defaultConfig {
			ndk { abiFilters "armeabi-v7a", "arm64-v8a" }
		}

		compileOptions {
			sourceCompatibility 1.8
			targetCompatibility 1.8
		}
	}
	```

## Initialize the application

Generic configurations to initialize the UI BizBundle:

```java
// Do not modify the initialization sequence.
Fresco.initialize(this);
// Initializes the SDK.
TuyaHomeSdk.init(this);

// Initializes the UI BizBundle.
TuyaWrapper.init(this, new RouteEventListener() {
	@Override
	public void onFaild(int errorCode, UrlBuilder urlBuilder) {
			// The callback of an unimplemented route.
			// An unresponsive touch indicates an unimplemented route. To implement the route, set `urlBuilder.target` to the required route and set routing parameters in `urlBuilder.params`.
			Log.e("router not implement", urlBuilder.target + urlBuilder.params.toString());
	}
}, new ServiceEventListener() {
	@Override
	public void onFaild(String serviceName) {
			// The callback of an unimplemented service.
			Log.e("service not implement", serviceName);
	}
});
TuyaOptimusSdk.init(this);

// Registers the home service. This service is not required for the Mall UI BizBundle.
TuyaWrapper.registerService(AbsBizBundleFamilyService.class, new BizBundleFamilyServiceImpl());
	// Intercepts the event of clicking the upper-right button on a panel and navigates to a custom page based on the parameters of `urlBuilder`.
	RedirectService service = MicroContext.getServiceManager().findServiceByInterface(RedirectService.class.getName());
	service.registerUrlInterceptor(new RedirectService.UrlInterceptor() {
		@Override
		public void forUrlBuilder(UrlBuilder urlBuilder, RedirectService.InterceptorCallback interceptorCallback) {
			// Example:
			// Intercepts the event of clicking the upper-right button on a panel and navigates to a custom page based on the parameters of `urlBuilder`.
			if (urlBuilder.target.equals("panelAction") && urlBuilder.params.getString("action").equals("gotoPanelMore")) {
				interceptorCallback.interceptor("interceptor");
				Log.e("interceptor", urlBuilder.params.toString());
			} else {
				interceptorCallback.onContinue(urlBuilder);
			}
		}
	});
```

## Log in to or out of an account

After the UI BizBundle is integrated, to enable login to or logout of an account, call the following API methods:

```java
// Enables login.
TuyaWrapper.onLogin();
// Enables logout.
TuyaWrapper.onLogout(Context context);
```

## Implement the home service

You can inherit the abstract class `AbsBizBundleFamilyService` and override `shiftCurrentFamily` method to set the value of `homeId` and `homeName` for the current home. Example:

``` java
public class BizBundleFamilyServiceImpl extends AbsBizBundleFamilyService {

	private long mHomeId;
	private String mHomeName;

	@Override
	public long getCurrentHomeId() {
		return mHomeId;
	}

	@Override
	public void shiftCurrentFamily(long familyId, String curName) {
	    super.shiftCurrentFamily(familyId, curName);
		mHomeId = familyId;
		mHomeName = curName;
	}
}
```

## Set value for a home

After a list of homes is returned, you can call the home service to set `homeId` and `homeName` for a home. Example:

```java
AbsBizBundleFamilyService service = MicroServiceManager.getInstance().findServiceByInterface(AbsBizBundleFamilyService.class.getName());
// Sets homeId and homeName for the current home.
service.shiftCurrentFamily(homeBean.getHomeId(), homeBean.getName());
```