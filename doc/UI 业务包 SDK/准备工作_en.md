
Before using the Tuya android BizBundle for development, you need to register developer account on [Tuya Smart Intelligent IoT Platform](https://iot.tuya.com/index/), and integrated [TuyaSmart Home SDK](https://developer.tuya.com/en/docs/app-development/android-app-sdk/featureoverview?id=Ka69nt97vtsfu)


## Register Developer Accont

Before docking with different development programs of Tuya, you need to register a developer account, create products, create function points, etc. on the Tuya smart development platform, After creating an account, you will get the key to activate the SDK. For the specific operation process, please refer to [Register Tuya Developer Account](https://developer.tuya.com/en/docs/app-development/android-app-sdk/preparation?id=Ka7mqlxh7vgi9)


## Integrate Tuya Smart Home SDK

Integrating the Tuya Smart Home SDK is a necessary prerequisite for accessing the Tuya android BizBundle. For specific integration methods, please refer to [Integrate SDK](../快速集成/快速集成_en.md)









