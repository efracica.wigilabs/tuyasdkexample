设备控制 UI 业务包是涂鸦智能设备控制面板的核心容器。在涂鸦智能安卓版 IoT App SDK 的基础上，它提供了设备控制面板的加载和控制的接口封装，加速了应用开发过程。

## 功能说明

设备控制 UI 业务包主要包括以下功能：

* 面板加载：加载多种设备类型，例如 Wi-Fi、Zigbee、蓝牙、蓝牙 Mesh 等通讯协议设备
* 设备控制：支持单设备和群组的控制，不支持群组管理
* 设备定时任务：支持为设备设定定时任务
* 扫地机面板：扫地机智能产品的面板加载

## 前提条件

在 Android Studio 中建立您的工程，接入 **智能生活 App SDK** 和添加 UI 业务包框架。详情请参考 [框架接入](https://developer.tuya.com/cn/docs/app-development/integration?id=Ka8qhzk13vkfq)。

## 注意事项

涂鸦设备控制 UI 业务包包含了开源框架 [ReactNative](https://github.com/facebook/react-native)，版本为 0.59.10。

- 若您的 Android 工程未集成 ReactNative，请按本文步骤集成设备控制业务包。
- 若您的 Android 工程已集成 ReactNative，需按下面步骤操作，以确定是否可集成设备控制业务包：
    - 若已集成 ReactNative，且版本 > 0.59.10，因版本兼容性问题，则无法集成设备控制业务包。
    - 若已集成 ReactNative，且 0.59.10 ≥ 版本 > 0.51，因 ReactNative 库冲突，不保证能够正常接入涂鸦面板。按下面两种方式进行操作：
        1. 移除您 Android 工程中的 ReactNative，使用设备控制业务包依赖的 ReactNative 0.59.10。
        2. 将您 Android 工程中集成的 ReactNative 进行更名，从而避免与业务包集成的 ReactNative 冲突。

  若均无法集成成功，则您无法接入集成设备控制业务包。

## UI 业务包配置

- build.gradle 配置

  ```groovy
  dependencies {
      api 'com.tuya.smart:tuyasmart-bizbundle-panel_base:1.0.0-cube.2'
      api 'com.tuya.smart:tuyasmart-bizbundle-rn_core:1.0.0-cube.2'
      api 'com.tuya.smart:tuyasmart-bizbundle-rn_biz:1.0.0-cube.2'
      api 'com.tuya.smart:tuyasmart-bizbundle-panel_ttt:1.0.0-cube.2'
  }
  ```

- 可选配置（根据面板功能，选择所需依赖）：

    * **阿里巴巴高德地图依赖项**

      应用发布到 Google Play 时，请务必去除此依赖。

      ``` groovy
      api 'com.tuya.smart:tuyasmart-react-native-amap:3.31.0-rc.2'
      implementation 'com.amap.api:search:7.9.0'
      implementation 'com.amap.api:navi-3dmap:8.0.0_3dmap8.0.0'
      ```

    * **谷歌地图依赖项**

      应用发布到中国大陆应用市场时，请务必去除此依赖。

      ```groovy
      api 'com.tuya.smart:tuyasmart-react-native-googlemap:3.31.5-rc.1'
              api 'com.google.android.gms:play-services-maps:17.0.0'
      ```

    * **腾讯 QQ 音乐登录模块依赖项**

      ```groovy
      api project(':qqmusic')
               api("com.tencent.yunxiaowei.dmsdk:core:2.3.0") {
                      exclude group: 'com.squareup.okhttp3', module: 'okhttp'
                      }
              api("com.tencent.yunxiaowei.webviewsdk:webviewsdk:2.3.0") {
                      exclude group: 'com.squareup.okhttp3', module: 'okhttp'
                      }
      ```
      
    * **IPC面板相关依赖项**

      此依赖包含加载IPC面板所需的所有相关依赖，无IPC面板不建议添加此依赖，不然会增大包体积.

      ```groovy
      api 'com.tuya.smart:tuyasmart-bizbundle-camera_panel:1.0.0-cube.2'
      ```

- 混淆配置

  ```groovy
  # react-native
  -keep,allowobfuscation @interface com.facebook.common.internal.DoNotStrip
  -keep,allowobfuscation @interface com.facebook.proguard.annotations.DoNotStrip
  -keep,allowobfuscation @interface com.facebook.proguard.annotations.KeepGettersAndSetters
  # Do not strip any method/class that is annotated with @DoNotStrip
  -keep @com.facebook.proguard.annotations.DoNotStrip class *
  -keep @com.facebook.common.internal.DoNotStrip class *
  -keepclassmembers class * {
      @com.facebook.proguard.annotations.DoNotStrip *;
      @com.facebook.common.internal.DoNotStrip *;
  }
  -keepclassmembers @com.facebook.proguard.annotations.KeepGettersAndSetters class * {
      void set*(***);
      *** get*();
  }
  -keep class * extends com.facebook.react.bridge.JavaScriptModule { *; }
  -keep class * extends com.facebook.react.bridge.NativeModule { *; }
  -keepclassmembers,includedescriptorclasses class * { native <methods>; }
  -keepclassmembers class *  { @com.facebook.react.uimanager.UIProp <fields>; }
  -keepclassmembers class *  { @com.facebook.react.uimanager.annotations.ReactProp <methods>; }
  -keepclassmembers class *  { @com.facebook.react.uimanager.annotations.ReactPropGroup <methods>; }
  -dontwarn com.facebook.react.**
  -keep class com.facebook.** { *; }
  -keep,includedescriptorclasses class com.facebook.react.bridge.** { *; }

  #高德地图
  -dontwarn com.amap.**
  -keep class com.amap.api.maps.** { *; }
  -keep class com.autonavi.** { *; }
  -keep class com.amap.api.trace.** { *; }
  -keep class com.amap.api.navi.** { *; }
  -keep class com.autonavi.** { *; }
  -keep class com.amap.api.location.** { *; }
  -keep class com.amap.api.fence.** { *; }
  -keep class com.autonavi.aps.amapapi.model.** { *; }
  -keep class com.amap.api.maps.model.** { *; }
  -keep class com.amap.api.services.** { *; }

  #Google Play Services
  -keep class com.google.android.gms.common.** {*;}
  -keep class com.google.android.gms.ads.identifier.** {*;}
  -keepattributes Signature,*Annotation*,EnclosingMethod
  -dontwarn com.google.android.gms.**

  #MPAndroidChart
  -keep class com.github.mikephil.charting.** { *; }
  -dontwarn com.github.mikephil.charting.**

  -keep class com.tuya.**.**{*;}
  -dontwarn com.tuya.**.**

  -keep,includedescriptorclasses class com.facebook.v8.** { *; }
  
  #TTT
  -keep class * extends com.tuya.android.universal.base.TYBaseUniPlugin { *; }
  -keep class com.tuya.smart.plugin.*.bean.**{ *; }
  ```

## 打开设备面板

通过设备 ID 进入对应设备面板页面。

**接口说明**

```java
goPanelWithCheckAndTip(Activity activity, String devId)
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| activity | 当前页面 context |
| devId | 设备 ID 通过 IoT App SDK 接口获取 |

**示例代码**

```java
AbsPanelCallerService service = MicroContext.getServiceManager().findServiceByInterface(AbsPanelCallerService.class.getName());
                service.goPanelWithCheckAndTip(PanelActivity.this, bean.devId);
```

## 打开群组面板

通过群组 ID 进入对应设备面板页面。

**接口说明**

```java
goPanelWithCheckAndTip(Activity activity, String groupId, boolean isAdmin)
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| activity | 当前页面 context |
| groupId | 群组 ID |
| isAdmin | 是否家庭管理员 |

**示例代码**

```java
boolean isAdmin = null != TuyaHomeSdk.getDataInstance().getHomeBean(curFamilyId)
                        && TuyaHomeSdk.getDataInstance().getHomeBean(curFamilyId).isAdmin();
 AbsPanelCallerService service = MicroContext.getServiceManager().findServiceByInterface(AbsPanelCallerService.class.getName());
                service.goPanelWithCheckAndTip(PanelActivity.this, groupId, isAdmin);
```

## 跳转未实现路由地址

点击面板按钮无反应时，请查看 logcat 日志内容，再 [通过路由回调跳转到实现页面](https://developer.tuya.com/cn/docs/app-development/integration?id=Ka8qhzk13vkfq#title-8-Application%20%E5%88%9D%E5%A7%8B%E5%8C%96)。

:::info
此方法替代原有的面板右上角按钮监听。
:::

## 清除所有面板的缓存

面板文件会存放在当前 `app` 存储目录下，若需要清理缓存可调用本小节描述的方法。

**示例代码**

```java
  ClearCacheService service = MicroContext.getServiceManager().findServiceByInterface(ClearCacheService.class.getName());
      if (service != null) {
        service.clearCache(PanelActivity.this);
      }
```

## 关闭设备控制页面

```java
TuyaSmartSdk.getEventBus().post(new PageCloseEventModel());
```

## 错误码

| 错误码 | 说明 |
| ---- | ---- |
| 1901 | 面板下载失败 |
| 1902 | 多语言包下载失败 |
| 1903 | 设备类型不支持 |
| 1904 | 群组内无设备 |
| 1905 | home id 有误 |
| 1906 | 设备 DeviceBean 为 null |
| 1907 | 找不到可下载的面板资源 |
| 1908 | 固件版本不正确 |
| 1909 | 未知错误 |
