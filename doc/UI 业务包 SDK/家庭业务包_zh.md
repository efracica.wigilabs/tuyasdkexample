家庭 UI 业务包主要囊括家庭管理、成员管理、房间管理等业务，这些是对配网后的设备进行管理的基础条件。

设备在配网后可设置家庭中设备所处的房间，同时，家庭下拥有不同权限的家庭成员对应着不同的操作权限，家庭也是场景智能执行的最大单位。

## 家庭业务包集成

### 创建工程

在 Android Studio 中建立您的工程，接入公版 SDK 并完成业务包 [框架接入](https://developer.tuya.com/cn/docs/app-development/android-bizbundle-sdk/integration?id=Ka8qhzk13vkfq)。

### module 的 build.gradle 配置

``` groovy
dependencies {
	implementation 'com.tuya.smart:tuyasmart-bizbundle-family:1.0.0-cube.2'
}
```

## 功能使用

### 进入家庭管理页面

家庭管理页面支持通过以下 **路由** 方式跳转：

```java
UrlRouter.execute(UrlRouter.makeBuilder(FamilyManageActivity.this, "family_manage"));
```

### 接受或拒绝家庭邀请

目前家庭接受邀请的业务并不在业务包处理范围内，您可以在自己的应用首页或者其他希望接收家庭邀请的地方来处理这部分逻辑。由于家庭邀请的消息通过 MQTT 推送下发，家庭业务包提供了以下方法来实现家庭邀请通知的监听：

```java
// 注册监听
TuyaHomeSdk.getHomeManagerInstance()
	.registerTuyaHomeChangeListener(HomeInviteListener listener);
// 注销监听
TuyaHomeSdk.getHomeManagerInstance()
	.unRegisterTuyaHomeChangeListener(HomeInviteListener listener);
```

调用方式如下：

```java
TuyaHomeSdk.getHomeManagerInstance()
	.registerTuyaHomeChangeListener(new HomeInviteListener() {
		@Override
		public void onHomeInvite(long homeId, String homeName) {
			// accept or reject invitation from this family
				TuyaHomeSdk.getMemberInstance()
				.processInvitation(homeId, isAccept, new IResultCallback() {
					@Override
					public void onError(String errorCode, String errorMsg) {

					}

					@Override
					public void onSuccess() {
						// Do something like refresh family list
					}
					});
		}
	});
```
