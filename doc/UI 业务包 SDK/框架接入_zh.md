本文介绍了涂鸦 **智能生活 App SDK** 业务包通用配置说明。

:::info
涂鸦已经废弃了 `com.tuya.android.module:tymodule-config:latestVersion` 的 `classpath`，为了更好的兼容性，建议您及时移除。
:::

## 前提条件

业务包支持的安卓 `minSdkVersion` 为 19，`targetSdkVersion` 为 29，且仅支持通过 AndroidX 构建。

## 配置业务包

本小节介绍配置业务包涉及的文件介绍及修改说明、及相关安卓依赖的设置。

### 配置文件

业务包更新时，您必须重新下载配置文件。业务包的更新记录，请参考 [更新日志](https://developer.tuya.com/cn/docs/app-development/changelog?id=Ka8ugz8p5khu6)。

| 配置项 | 说明 | 是否必选 |
| --- | --- | --- |
| assets | [module_app.json](../../ui_bizbundle_android_config_values/assets/) 和 [x_platform_config.json](../../ui_bizbundle_android_config_values/assets/) 为服务化配置文件，请拷贝至 `app` 目录下的 `assets` 文件夹下，即可生效。 | 是 |
| res | <ul><li> [res/values/compat-colors.xml](../../ui_bizbundle_android_config_values/res/values/compat-colors.xml) ：业务包配置文件及默认颜色 </li><li> [res/values](../../ui_bizbundle_android_config_values/res/values/)：资源配置文件	</li><li>	[res](../../ui_bizbundle_android_config_values/res/)：多语言文件可根据情况选择配置| `res/values/compat-colors.xml` 和 `res/values` 为必选 |
| 主题色 |	<ul><li> [res/values/colors.xml](../../ui_bizbundle_android_config_values/res/values/colors.xml)：主题资源配置 </li><li> [assets/tyTheme/ui_theme_config.json](../../ui_bizbundle_android_config_values/assets/tyTheme/ui_theme_config.json)：主题色 assets 资源配置 </li></ul><div styled-block data-style-important><div class="styled-block-title"><span class="styled-block-title-icon"></span><span class="styled-block-title-label"></span></div><div class="styled-block-content">您需要同时修改`colors.xml`和`ui_theme_config.json` 两个文件的内容来实现主题色配置。</div></div>| 否（此资源不再维护，推荐使用[主题色配置UI业务包](https://developer.tuya.com/cn/docs/app-development/android-colors-theme?id=Kapl9ox0ysvgs)） |

**主题色** 配置文件修改说明：

- **`colors.xml` 参数说明：**

	| 参数 | 说明 | 默认值 |
	| --- | --- | --- |
	| ty_theme_color_m1 | 正常态主题色资源 | #FFFF5A28 |
	| ty_theme_color_m1_1 | 禁用态主题色资源 | #33FF5A28 |
	| ty_theme_color_m1_2 | 按压态主题色资源 | #FFFF6B3E |
	| ty_theme_color_m1_alpha_5 | 5% 透明度的主题色资源 | #0DFF5A28 |
	| ty_theme_color_m1_alpha_10 | 10% 透明度的主题色资源 | #1AFF5A28 |
	| ty_theme_color_m1_alpha_15 | 15% 透明度的主题色资源 | #26FF5A28 |
	| ty_theme_color_m1_alpha_20 | 20% 透明度的主题色资源 | #33FF5A28 |
	| ty_theme_color_m1_alpha_25 | 25% 透明度的主题色资源 | #40FF5A28 |
	| ty_theme_color_m1_alpha_30 | 30% 透明度的主题色资源 | #4DFF5A28 |
	| ty_theme_color_m1_alpha_35 | 35% 透明度的主题色资源 | #59FF5A28 |
	| ty_theme_color_m1_alpha_40 | 40% 透明度的主题色资源 | #66FF5A28 |
	| ty_theme_color_m1_alpha_45 | 45% 透明度的主题色资源 | #73FF5A28 |
	| ty_theme_color_m1_alpha_50 | 50% 透明度的主题色资源 | #80FF5A28 |
	| ty_theme_color_m1_alpha_55 | 55% 透明度的主题色资源 | #8CFF5A28 |
	| ty_theme_color_m1_alpha_60 | 60% 透明度的主题色资源 | #99FF5A28 |
	| ty_theme_color_m1_alpha_65 | 65% 透明度的主题色资源 | #A6FF5A28 |
	| ty_theme_color_m1_alpha_70 | 70% 透明度的主题色资源 | #B3FF5A28 |
	| ty_theme_color_m1_alpha_75 | 75% 透明度的主题色资源 | #BFFF5A28 |
	| ty_theme_color_m1_alpha_80 | 80% 透明度的主题色资源 | #CCFF5A28 |
	| ty_theme_color_m1_alpha_85 | 85% 透明度的主题色资源 | #D9FF5A28 |
	| ty_theme_color_m1_alpha_90 | 90% 透明度的主题色资源 | #E6FF5A28 |
	| ty_theme_color_m1_alpha_95 | 95% 透明度的主题色资源 | #F2FF5A28 |

- **`ui_theme_config.json` 修改说明：**

	```json
	{
		"color_alpha":{
		"N1":0.9,
		"N2":0.7,
		"N3":0.5,
		"N4":0.3,
		"N5":0.7,
		"N6":0.2,
		"N7":0.1,
		"N8":0.4
		},
		"colors":{
		"B1":"#F8F8F8",
		"B2":"#FFFFFF",
		"B3":"#FFFFFF",
		"B4":"#FFFFFF",
		"B5":"#FFFFFF",
		"M1":"#FF5A28", // M1表示主题色配置，默认值为#FF5A28；仅M1支持修改为其他值，其他属性字段不支持修改
		"M2":"#FF4444",
		"M3":"#00CC99",
		"M4":"#1989FA",
		"M5":"#FF5A28"
		},
		"corner":{
		"alert_corner_type":"angle",
		"button_corner_type":"angle",
		"card_corner_type":"angle"
		},
		"font":{
		"app_bold_font":"",
		"app_font":""
		}
	}
	```

### Drawable 配置

`bg_text_bts.xml`

``` xml
<?xml version="1.0" encoding="utf-8"?>
<selector xmlns:android="http://schemas.android.com/apk/res/android">

	<item android:state_focused="true">
		<shape android:shape="rectangle">
			<corners android:radius="@dimen/mg_2" />

			<solid android:color="@color/primary_button_select_color" />
		</shape>
	</item>
	<item android:state_selected="true">
		<shape android:shape="rectangle">
			<corners android:radius="@dimen/mg_2" />

			<solid android:color="@color/primary_button_select_color" />
		</shape>
	</item>
	<item android:state_pressed="true">
		<shape android:shape="rectangle">
			<corners android:radius="@dimen/mg_2" />

			<solid android:color="@color/primary_button_select_color" />
		</shape>
	</item>
	<item android:state_enabled="true">
		<shape android:shape="rectangle">
			<corners android:radius="@dimen/mg_2" />

			<solid android:color="@color/primary_button_bg_color" />
		</shape>
	</item>
	<item android:state_enabled="false">
		<shape>
			<corners android:radius="@dimen/mg_2" />

			<solid android:color="@color/gray_99" />
		</shape>
	</item>

</selector>
```

### 主题（style）配置

[res/values/styles.xml](../../ui_bizbundle_android_config_values/res/values/)

``` xml
<!-- 必选配置 -->
<!-- Base application theme. -->
<style name="Base_BizBundle_Theme" parent="AppTheme">
	<item name="status_font_color">@color/status_font_color</item>
	<item name="status_bg_color">@color/status_bg_color</item>
	<item name="navbar_font_color">@color/navbar_font_color</item>
	<item name="navbar_bg_color">@color/navbar_bg_color</item>
	<item name="app_bg_color">@color/app_bg_color</item>
	<item name="fragment_bg_color">@color/app_bg_color</item>
	<item name="list_primary_color">@color/list_primary_color</item>
	<item name="list_sub_color">@color/list_sub_color</item>
	<item name="list_secondary_color">@color/list_secondary_color</item>
	<item name="list_line_color">@color/list_line_color</item>
	<item name="list_bg_color">@color/list_bg_color</item>
	<item name="primary_button_font_color">@color/primary_button_font_color</item>
	<item name="primary_button_bg_color">@color/primary_button_bg_color</item>
	<item name="secondary_button_font_color">@color/secondary_button_font_color</item>
	<item name="secondary_button_bg_color">@color/secondary_button_bg_color</item>
	<item name="notice_font_color">@color/notice_font_color</item>
	<item name="notice_bg_color">@color/notice_bg_color</item>
	<item name="bg_normal_text_bt">@drawable/bg_text_bts</item>
	<item name="app_name">@string/app_name</item>
	<item name="is_splash_used">false</item>
	<item name="ap_default_ssid">@string/ap_mode_ssid</item>
	<item name="ap_connect_description">@string/ty_ap_connect_description</item>
	<item name="is_scan_support">@bool/is_scan_support</item>
	<item name="is_need_blemesh_support">@bool/is_need_blemesh_support</item>
	<item name="status_bg_color_75">@color/status_bg_color_75</item>
	<item name="status_bg_color_90">@color/status_bg_color_90</item>
</style>
```

### 应用清单（AndroidManifest.xml）

``` xml
<!-- 必选配置 -->
<application
	android:supportsRtl="false"
	android:theme="@style/Base_BizBundle_Theme"
	tools:replace="android:theme"/>
```

### 依赖源

- 根目录的 `build.gradle` 文件配置如下：

	```java
	allprojects {
		repositories {
			jcenter()
			maven { url 'https://maven-other.tuya.com/repository/maven-releases/' }
			maven { url 'https://jitpack.io' }
		}
	}
	```

- app 的 `build.gradle` 文件配置：

	> 注意：NDK 配置只支持 ARM 指令架构的 CPU 版本。

	``` java
	android {
		packagingOptions {
			pickFirst 'lib/*/libc++_shared.so'
			pickFirst 'lib/*/libgnustl_shared.so'
		}

		lintOptions {
			abortOnError false
		}

		defaultConfig {
			ndk { abiFilters "armeabi-v7a", "arm64-v8a" }
		}

		compileOptions {
			sourceCompatibility 1.8
			targetCompatibility 1.8
		}
	}
	```

## 初始化应用

在主线程初始化业务包通用设置：

```java
// 请不要修改初始化顺序
Fresco.initialize(this);
// SDK 初始化
TuyaHomeSdk.init(this);

// 业务包初始化
TuyaWrapper.init(this, new RouteEventListener() {
	@Override
	public void onFaild(int errorCode, UrlBuilder urlBuilder) {
			// 路由未实现回调
			// 点击无反应表示路由未现实，需要在此实现， urlBuilder.target 目标路由， urlBuilder.params 路由参数
			Log.e("router not implement", urlBuilder.target + urlBuilder.params.toString());
	}
}, new ServiceEventListener() {
	@Override
	public void onFaild(String serviceName) {
			// 服务未实现回调
			Log.e("service not implement", serviceName);
	}
});
TuyaOptimusSdk.init(this);

// 注册家庭服务，商城业务包可以不注册此服务
TuyaWrapper.registerService(AbsBizBundleFamilyService.class, new BizBundleFamilyServiceImpl());
	//拦截已存在的路由，通过参数跳转至自定义实现页面
	RedirectService service = MicroContext.getServiceManager().findServiceByInterface(RedirectService.class.getName());
	service.registerUrlInterceptor(new RedirectService.UrlInterceptor() {
		@Override
		public void forUrlBuilder(UrlBuilder urlBuilder, RedirectService.InterceptorCallback interceptorCallback) {
			//Such as:
			//Intercept the event of clicking the panel right menu and jump to the custom page with the parameters of urlBuilder
			//例如：拦截点击面板右上角按钮事件，通过 urlBuilder 的参数跳转至自定义页面
			if (urlBuilder.target.equals("panelAction") && urlBuilder.params.getString("action").equals("gotoPanelMore")) {
				interceptorCallback.interceptor("interceptor");
				Log.e("interceptor", urlBuilder.params.toString());
			} else {
				interceptorCallback.onContinue(urlBuilder);
			}
		}
	});
```

## 登录或退出账号

业务包接入后，请在登录和退出用户账号时分别调用如下方法：

```java
//登录
TuyaWrapper.onLogin();
//退出
TuyaWrapper.onLogout(Context context);
```

## 实现家庭服务

您可以通过继承 `AbsBizBundleFamilyService` 抽象类，实现设置当前家庭 `homeId` 和当前家庭名称 `homeName`。示例代码如下：

``` java
public class BizBundleFamilyServiceImpl extends AbsBizBundleFamilyService {

	private long mHomeId;
	private String mHomeName;

	@Override
	public long getCurrentHomeId() {
		return mHomeId;
	}

	@Override
	public void shiftCurrentFamily(long familyId, String curName) {
		super.shiftCurrentFamily(familyId, curName);
		mHomeId = familyId;
		mHomeName = curName;
	}
}
```

## 设置家庭数据

当获取家庭列表后，通过服务化调用设置家庭 `homeId` 和 `homeName`。示例代码如下：

```java
AbsBizBundleFamilyService service = MicroServiceManager.getInstance().findServiceByInterface(AbsBizBundleFamilyService.class.getName());
// 设置为当前家庭的 homeId 和 homeName
service.shiftCurrentFamily(homeBean.getHomeId(), homeBean.getName());
```