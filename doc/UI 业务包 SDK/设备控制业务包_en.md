Device Control UI BizBundle for Android is the core container of the control panel for each `Powered by Tuya` device. Based on Tuya IoT App SDK for Android, the UI BizBundle encapsulates the APIs to load and control panels and accelerates the application development process.

## Feature overview

The UI BizBundle supports the following features:

* Panel loading: supports device panels of multiple protocols, such as Wi-Fi, Zigbee, Bluetooth, and Bluetooth mesh.
* Device control: controls a single device and device groups. Device groups cannot be managed.
* Scheduled tasks: configures scheduled tasks for specific devices.
* Robot vacuum panel: loads panels for robot vacuums.

## Prerequisites

Tuya IoT App SDK for Android is integrated into your project with Android Studio. The **Smart Life App SDK** and Device Control UI BizBundle are added to your project. For more information, see [Integration with UI BizBundle SDK for Android](https://developer.tuya.com/en/docs/app-development/integration?id=Ka8qhzk13vkfq).

## Things to note

The UI BizBundle includes the open source [React Native](https://github.com/facebook/react-native) framework (v0.59.10).

- If your Android project does not integrate with React Native, follow the procedure in this topic to integrate with the UI BizBundle.
- If your Android project has integrated with React Native, perform the following steps to check whether the UI BizBundle can be integrated:
    - If React Native has been integrated and its version is later than v0.59.10, the UI BizBundle cannot be integrated due to version incompatibility.
    - If React Native has been integrated and its version is later than v0.51 and equal to or earlier than v0.59.10, the Tuya panels cannot be accessed as expected due to the repeated uses of the React Native library. Use either of the following methods to troubleshoot the problem:
        1. Remove React Native from your Android project and use React Native v0.59.10 that the UI BizBundle depends on.
        2. Rename React Native that is integrated with your Android project to avoid the repeated React Native library.

  If both methods failed, the UI BizBundle cannot be integrated.

## Configure the UI BizBundle

- Configure the `build.gradle` file.

  ```groovy
  dependencies {
      api 'com.tuya.smart:tuyasmart-bizbundle-panel_base:1.0.0-cube.2'
      api 'com.tuya.smart:tuyasmart-bizbundle-rn_core:1.0.0-cube.2'
      api 'com.tuya.smart:tuyasmart-bizbundle-rn_biz:1.0.0-cube.2'
      api 'com.tuya.smart:tuyasmart-bizbundle-panel_ttt:1.0.0-cube.2'
  }
  ```

- (Optional) Select the following desired dependencies to support panel features:

    * **Dependency of AutoNavi**

      Before you launch an application on Google Play, remove this dependency.

      ``` groovy
      api 'com.tuya.smart:tuyasmart-react-native-amap:3.31.0-rc.2'
               implementation 'com.amap.api:search:7.9.0'
               implementation 'com.amap.api:navi-3dmap:8.0.0_3dmap8.0.0'
      ```

    * **Dependency of Google Maps**

      Before you launch an application on app stores in mainland China, remove this dependency.

      ```groovy
      api 'com.tuya.smart:tuyasmart-react-native-googlemap:3.31.5-rc.1'
               api 'com.google.android.gms:play-services-maps:17.0.0'
      ```

    * **Dependency of Tencent's QQ Music login module**

      ```groovy
      api project(':qqmusic')
      api("com.tencent.yunxiaowei.dmsdk:core:2.3.0") {
          exclude group: 'com.squareup.okhttp3', module: 'okhttp'
      }
      api("com.tencent.yunxiaowei.webviewsdk:webviewsdk:2.3.0") {
          exclude group: 'com.squareup.okhttp3', module: 'okhttp'
      }
      ```
      
    * **IPC panel dependencies**

      This dependency contains all the relevant dependencies needed to load the IPC panel, it is not recommended to add this dependency without the IPC panel, otherwise it will increase the package size.

      ```groovy
      api 'com.tuya.smart:tuyasmart-bizbundle-camera_panel:1.0.0-cube.2'
      ```

- Obfuscate the code.

  ```groovy
  # react-native
  -keep,allowobfuscation @interface com.facebook.common.internal.DoNotStrip
  -keep,allowobfuscation @interface com.facebook.proguard.annotations.DoNotStrip
  -keep,allowobfuscation @interface com.facebook.proguard.annotations.KeepGettersAndSetters
  # Do not strip any method/class that is annotated with @DoNotStrip
  -keep @com.facebook.proguard.annotations.DoNotStrip class *
  -keep @com.facebook.common.internal.DoNotStrip class *
  -keepclassmembers class * {
      @com.facebook.proguard.annotations.DoNotStrip *;
      @com.facebook.common.internal.DoNotStrip *;
  }
  -keepclassmembers @com.facebook.proguard.annotations.KeepGettersAndSetters class * {
      void set*(***);
      *** get*();
  }
  -keep class * extends com.facebook.react.bridge.JavaScriptModule { *; }
  -keep class * extends com.facebook.react.bridge.NativeModule { *; }
  -keepclassmembers,includedescriptorclasses class * { native <methods>; }
  -keepclassmembers class *  { @com.facebook.react.uimanager.UIProp <fields>; }
  -keepclassmembers class *  { @com.facebook.react.uimanager.annotations.ReactProp <methods>; }
  -keepclassmembers class *  { @com.facebook.react.uimanager.annotations.ReactPropGroup <methods>; }
  -dontwarn com.facebook.react.**
  -keep class com.facebook.** { *; }
  -keep,includedescriptorclasses class com.facebook.react.bridge.** { *; }

     # AutoNavi
  -dontwarn com.amap.**
  -keep class com.amap.api.maps.** { *; }
  -keep class com.autonavi.** { *; }
  -keep class com.amap.api.trace.** { *; }
  -keep class com.amap.api.navi.** { *; }
  -keep class com.autonavi.** { *; }
  -keep class com.amap.api.location.** { *; }
  -keep class com.amap.api.fence.** { *; }
  -keep class com.autonavi.aps.amapapi.model.** { *; }
  -keep class com.amap.api.maps.model.** { *; }
  -keep class com.amap.api.services.** { *; }

  #Google Play Services
  -keep class com.google.android.gms.common.** {*;}
  -keep class com.google.android.gms.ads.identifier.** {*;}
  -keepattributes Signature,*Annotation*,EnclosingMethod
  -dontwarn com.google.android.gms.**

  #MPAndroidChart
  -keep class com.github.mikephil.charting.** { *; }
  -dontwarn com.github.mikephil.charting.**

  -keep class com.tuya.**.**{*;}
  -dontwarn com.tuya.**.**

  -keep,includedescriptorclasses class com.facebook.v8.** { *; }
  
  #TTT
  -keep class * extends com.tuya.android.universal.base.TYBaseUniPlugin { *; }
  -keep class com.tuya.smart.plugin.*.bean.**{ *; }
  ```

## Open a device panel

Enters a device panel by device ID.

**API description**

```java
goPanelWithCheckAndTip(Activity activity, String devId)
```

**Parameters**

| Parameter | Description |
| ---- | ---- |
| activity | The context of the current page. |
| devId | The device ID can be obtained from the IoT App SDK. |

**Example**

```java
AbsPanelCallerService service = MicroContext.getServiceManager().findServiceByInterface(AbsPanelCallerService.class.getName());
                service.goPanelWithCheckAndTip(PanelActivity.this, bean.devId);
```

## Open a group panel

Enters a device panel by group ID.

**API description**

```java
goPanelWithCheckAndTip(Activity activity, String groupId, boolean isAdmin)
```

**Parameters**

| Parameter | Description |
| ---- | ---- |
| activity | The context of the current page. |
| groupId | The group ID. |
| isAdmin | Specifies whether the user is an administrator. |

**Example**

```java
boolean isAdmin = null != TuyaHomeSdk.getDataInstance().getHomeBean(curFamilyId)
                        && TuyaHomeSdk.getDataInstance().getHomeBean(curFamilyId).isAdmin();
 AbsPanelCallerService service = MicroContext.getServiceManager().findServiceByInterface(AbsPanelCallerService.class.getName());
                service.goPanelWithCheckAndTip(PanelActivity.this, groupId, isAdmin);
```

## Navigate to an unresponsive URL

If a panel button is unresponsive after you touch it, check the logcat tool, and then use a route callback to navigate to the desired page. For more information, see [Initialize the application](https://developer.tuya.com/en/docs/app-development/integration?id=Ka8qhzk13vkfq#title-7-Initialize%20the%20application).

:::info
This method replaces the listener for the button in the top-right corner of the panel.
:::

## Clear all cached panel data

Panel files are stored in the current `app` storage directory. If you want to clear the cached data, you can call the method in this section.

**Example**

```java
  ClearCacheService service = MicroContext.getServiceManager().findServiceByInterface(ClearCacheService.class.getName());
      if (service != null) {
        service.clearCache(PanelActivity.this);
      }
```

## Close a device control page

```java
TuyaSmartSdk.getEventBus().post(new PageCloseEventModel());
```

## Error codes

| Error code | Description |
| ---- | ---- |
| 1901 | Failed to download a specified panel. |
| 1902 | Failed to download a specified multilingual package. |
| 1903 | The specified device type is not supported. |
| 1904 | No devices are found in a specified group. |
| 1905 | The specified home ID is incorrect. |
| 1906 | `DeviceBean` is set to `null`. |
| 1907 | No panel resources can be downloaded. |
| 1908 | The specified firmware version is incorrect. |
| 1909 | An unknown error has occurred. |
