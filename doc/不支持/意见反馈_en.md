
If user intends to submit feedback, he has to select the type of feedback first and fill the feedback content. After submission, a feedback talk will be generated based on the selected type of feedback. User may continue to fill feedback in the session, and the feedback will be displayed in the feedback list of the session.

All functions related to the feedback will be realized by using the ITuyaFeedbackManager Or ITuyaFeedbackMsg class. Obtaining feedback talk list, feedback content list in the session and feedback type list and adding feedback are supported.

|Class Name |         Description |
|----|----|
| ITuyaFeedbackManager | Feedback management class|
| ITuyaFeedbackMsg | A feedback management class for some conversation |

## Obtain Feedback Talk List

Obtain the feedback talk list submitted by user

**Declaration**

```java
void getFeedbackList(final ITuyaDataCallback<List<FeedbackBean>> callback);
```

**Parameters**

| Parameters | Description                                              |
| ---- | ---- |
| callback  | Callback, including obtain feedback list success or failure, cannot be null |

**FeedbackBean data model**

| field | type  | description                                              |
| ---- | ---- | ---- |
| dateTime | String | Date and time                                            |
| content | String | Feedback content                                         |
| hdId | String | Feedback id                                              |
| hdType  | String | Feedback Type                                            |
| title | String | Title (if it is equipment failure feedback, that is, equipment name) |

**Example**

```java
TuyaHomeSdk.getTuyaFeekback().getFeedbackManager().getFeedbackList(new ITuyaDataCallback<List<FeedbackBean>>() {
     @Override
     public void onSuccess(List<FeedbackBean> feedbackTalkBeans) {
     }
     @Override
     public void onError(String errorCode, String errorMessage) {
     }
}); 
```

## Obtain the list of feedback content

Obtain the feedback content list submitted by user。You can call `FeedbackBean` to get the `hdId` and `hdType` field values.

**Declaration**

```java
void getFeedbackMsg(hdId, hdType).getFeedbackList(final ITuyaDataCallback<List<FeedbackMsgBean>> callback);
```

**Parameters**

| Parameters     | Description                                         |
| -------- | -------------------------------------------- |
|   hdId   |            Feedback id               |
|  hdType  |           Feedback Type                |
| callback | Callback, including obtain feedback content list success or failure, cannot be null |


**FeedbackMsgBean data model**

|   field   |  type   |                  description                  |
| ------  | ----    | ------------------------------------ |
|    id    | int    |             Feedback content id               |
|  cTime   | int    |             Feedback time(timestamp，unit sec)    |
| content  | String |             Feedback content                |
|   hdId   | String |             Feedback id               |
|  hdType  | int    |             Feedback Type               |

**Example**

```java
TuyaHomeSdk.getTuyaFeekback().getFeedbackMsg(hdId, hdType).getMsgList(new ITuyaDataCallback<List<FeedbackMsgBean>>() {
    @Override
    public void onSuccess(List<FeedbackMsgBean> result) {}
    @Override 
    public void onError(String errorCode, String errorMessage) {}
});
```

## Obtain Feedback Type List

The feedback type can be selected first when adding feedback.

**Declaration**

```java
void getFeedbackType(final ITuyaDataCallback<List<FeedbackTypeRespBean>> callback);
```

**Parameters**

| Parameters | Description                                              |
| ---- | ---- |
| callback  | callback, including obtain feedback list success or failure, cannot be null |

**FeedbackTypeRespBean data model**

| field | type               | description                                     |
| ---- | ---- | ---- |
| list | List<FeedbackTypeBean> | Feedback type list                              |
| type | String             | List categories (currently only devices and others) |

**FeedbackTypeBean data model**

| field | type  | description                                              |
| ---- | ---- | ---- |
| hdId  | String | Feedback id                                              |
| hdType | String | feedback type                                            |
| title | String | Title (if it is equipment failure feedback, that is, equipment name) |

**Example**

```java
TuyaHomeSdk.getTuyaFeekback().getFeedbackManager().getFeedbackType(new ITuyaDataCallback<List<FeedbackTypeRespBean>>() {
    @Override
    public void onSuccess(List<FeedbackTypeRespBean> feedbackTypeRespBeans) {}
    @Override
    public void onError(String errorCode, String errorMsg) {}
});
```

## Add Feedback

Add and submit feedback.

**Declaration**

```java
void addFeedback(final String message,String contact, String hdId, int hdType, final ITuyaDataCallback<FeedbackMsgBean> callback);
```

**Parameters**

| Parameters | Description                                            |
| ---- | ---- |
| message | Feedback content                                       |
| contact | Contact information (phone or email)                   |
| hdId   | Feedback Id                                            |
| hdType | Feedback type                                          |
| callback  | Callback, including add success or failure, cannot be null |

**Example**

```java
TuyaHomeSdk.getTuyaFeekback().getFeedbackManager().addFeedback(
     “Thedevice fails ", //feedback
     "abc@qq.com",
     feebackTypeBean.getHdId(), 
     feebackTypeBean.getHdType(), 
     new ITuyaDataCallback<FeedbackMsgBean>() {
         @Override
         public void onSuccess(FeedbackMsgBean feedbackMsgBean) {
         }
         @Override
         public void onError(String errorCode, String errorMsg) {
         }
});
```