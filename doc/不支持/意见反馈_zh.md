
当用户有问题需要反馈时，可添加反馈，添加反馈时应先选择反馈类型，然后撰写反馈内容进行提交，提交后会按照之前选择的反馈类型生成相应的反馈会话，同时用户也可以在该会话中继续撰写反馈内容并提交，显示在该会话的反馈列表中。

意见反馈相关的所有功能对应 ITuyaFeedbackManager、ITuyaFeedbackMsg类，支持获取反馈会话列表，获取会话中反馈内容列表，获取反馈类型列表，以及添加反馈。

|         类名         |           说明           |
| ------------------ | ---------------------- |
| ITuyaFeedbackManager |      用户反馈管理类      |
|   ITuyaFeedbackMsg   | 针对某一会话的反馈管理类 |

## 获取反馈会话列表

获取用户已提交反馈会话列表。

**接口说明**

```java
void getFeedbackManager().getFeedbackList(final ITuyaDataCallback<List<FeedbackBean>> callback);
```

**参数说明**

| 参数     | 说明                                         |
| -------- | -------------------------------------------- |
| callback | 回调，包括获取反馈列表成功和失败，不能为 null |

**FeedbackBean 数据模型**

|   字段   |  类型  |                  描述                  |
| ------ | ---- | ------------------------------------ |
| dateTime | String |               日期和时间               |
| content  | String |                反馈内容                |
|   hdId   | String |               反馈类目 id               |
|  hdType  | String |                反馈类型                |
|  title   | String | 类目标题(如果为设备故障反馈即设备名称) |

**示例代码**

```java
TuyaHomeSdk.getTuyaFeekback().getFeedbackManager().getFeedbackList(new ITuyaDataCallback<List<FeedbackBean>>() {
    @Override
    public void onSuccess(List<FeedbackBean> feedbackTalkBeans) {}
    @Override
    public void onError(String errorCode, String errorMessage) {}
});
```

## 获取反馈内容列表

获取用户已提交反馈会话中对应的反馈内容列表。其中，`hdId` 和 `hdType` 字段可以从 `FeedbackBean` 中获取。

**接口说明**

```java
void getFeedbackMsg(hdId, hdType).getFeedbackList(final ITuyaDataCallback<List<FeedbackMsgBean>> callback);
```

**参数说明**

| 参数     | 说明                                         |
| -------- | -------------------------------------------- |
|   hdId   |              反馈类目 id               |
|  hdType  |              反馈类型                |
| callback | 回调，包括获取反馈内容列表成功和失败，不能为 null |


**FeedbackMsgBean 数据模型**

|   字段   |  类型   |                  描述                  |
| ------  | ----    | ------------------------------------ |
|    id    | int    |               反馈内容id               |
|  cTime   | int    |               反馈时间(时间戳，单位 秒)    |
| content  | String |                反馈内容                |
|   hdId   | String |               反馈类目 id               |
|  hdType  | int    |                反馈类型                |

**示例代码**

```java
TuyaHomeSdk.getTuyaFeekback().getFeedbackMsg(hdId, hdType).getMsgList(new ITuyaDataCallback<List<FeedbackMsgBean>>() {
    @Override
    public void onSuccess(List<FeedbackMsgBean> result) {}
    @Override 
    public void onError(String errorCode, String errorMessage) {}
});
```

## 获取反馈类型列表

添加反馈时，可先选择反馈类型。

**接口说明**

```java
void getFeedbackType(final ITuyaDataCallback<List<FeedbackTypeRespBean>> callback);
```

**参数说明**

| 参数     | 说明                                             |
| -------- | ------------------------------------------------ |
| callback | 回调，包括获取反馈类型列表成功和失败，不能为 null |

**FeedbackTypeRespBean 数据模型**

| 字段 |          类型          |             描述             |
| -- | -------------------- | -------------------------- |
| list | List<FeedbackTypeBean> |         反馈类型列表         |
| type |         String         | 列表类别(目前仅有设备和其它) |

**FeedbackTypeBean 数据模型**

|  字段  |  类型  |                  描述                  |
| ---- | ---- | ------------------------------------ |
|  hdId  | String |               反馈类目 id               |
| hdType | String |                反馈类型                |
| title  | String | 类目标题(如果为设备故障反馈即设备名称) |

**示例代码**

```java
TuyaHomeSdk.getTuyaFeekback().getFeedbackManager().getFeedbackType(new ITuyaDataCallback<List<FeedbackTypeRespBean>>() {
    @Override
    public void onSuccess(List<FeedbackTypeRespBean> feedbackTypeRespBeans) {}
    @Override
    public void onError(String errorCode, String errorMsg) {}
});
```

## 添加反馈

添加反馈，提交用户输入的反馈的内容。

**接口说明**

```java
void addFeedback(final String message,String contact, String hdId, int hdType, final ITuyaDataCallback<FeedbackMsgBean> callback);
```

**参数说明**

| 参数     | 说明                     |
| -------- | ------------------------ |
| message  | 反馈内容                 |
| contact  | 联系方式（电话或邮箱）   |
| hdId     | 反馈类目 id               |
| hdType   | 反馈类型                 |
| callback | 回调，包含新增成功和失败，不能为 null |

**示例代码**

```java
TuyaHomeSdk.getTuyaFeekback().getFeedbackManager().addFeedback(
    "设备存在故障",
    "abc@qq.com",
    feebackTypeBean.getHdId(), 
    feebackTypeBean.getHdType(), 
    new ITuyaDataCallback<FeedbackMsgBean>() {
        @Override
        public void onSuccess(FeedbackMsgBean feedbackMsgBean) {}
        @Override
        public void onError(String errorCode, String errorMsg) {}
});
```

