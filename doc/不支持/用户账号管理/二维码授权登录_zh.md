## 流程说明

> **说明**：以下流程 **中控设备**的app，需要添加白名单才能使用，否则会报错。
>
> 例如，如果您使用 "涂鸦智能" app 来扫描后台创建的 SDK 的二维码那么会报错，session 失效。

二维码授权登录功能适用于 APP 扫码授权另一台设备登录相同账号。设备可以是中控设备、TV 、平板设备等。完整授权流程如下：

![image.png](https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/goat/20201228/b61cf5a33f764fe3878bf385f6759245.png)

图中标注的关键步骤解释：

* ①：获取 token

	设备请求接口获取授权流程使用的 token，接口为`getQRCodeToken`

* ②：生成二维码

	将获取到的 token，使用特定格式生成二维码：

	格式为：`tuyaSmart--qrLogin?token=xxxxxxx`，例如：`tuyaSmart--qrLogin?token=AZc72de000-ec00-4000-9e51-b610fc300000`

	生成二维码后

	![二维码图片_6月28日11时52分25秒](https://images.tuyacn.com/fe-static/docs/img/b809c242-2b1e-418a-8906-dc2aba5d3a1d.png)

	将上面字符串生成二维码展示在设备的屏幕中。

* ③：获取登录状态

	向服务端轮训获取是否授权成功，如果授权成功后将返回用户信息，跳转进入应用主页，进入后续操作。

	接口为： `QRCodeLogin`

* ④：扫描二维码

	app 扫描设备上的二维码，将二维码中 token 解析出来，进行授权操作。

* ⑤：授权

	将解析出的二维码发送到云端，完成授权动作。

	授权接口为`QRcodeAuth`。

## 接口说明

### 用户获取 Token

```java
void getQRCodeToken(String countryCode, IGetQRCodeTokenCallback callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| countryCode | 国家区号，例如：86 |
| callback | 回调 |

**示例代码**

```java
TuyaHomeSdk.getUserInstance().getQRCodeToken("86", new IGetQRCodeTokenCallback() {
    @Override
    public void onSuccess(String token) {

    }

    @Override
    public void onError(String code, String error) {

    }
});
```

### 获取登录状态

```Java
void QRCodeLogin(String countryCode, String token, ILoginCallback callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| countryCode | 国家区号，例如：86 |
| token | token |
| callback | 回调 |

**示例代码**

```java
TuyaHomeSdk.getUserInstance().QRCodeLogin("86", "xxxx", new ILoginCallback() {
    @Override
    public void onSuccess(User user) {
        if (user != null && !TextUtils.isEmpty(user.getSid())){
            TuyaHomeSdk.getUserInstance().loginSuccess(user);
            //获取 homeId
            Object homeId = user.getExtras().get("homeId");

            gotoHomePage();
        }
    }

    @Override
    public void onError(String code, String error) {

    }
});
```

### 授权接口

```java
void QRcodeAuth(String countryCode, long homeId, String token, IBooleanCallback callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| countryCode | 国家区号，例如：86 |
| homeId | 家庭 ID。请参考家庭相关章节获取 |
| token | token |
| callback | 回调 |

**示例代码**

```java
TuyaHomeSdk.getUserInstance().QRcodeAuth("86", mHomeId, getActivityToken(), new IBooleanCallback() {
    @Override
    public void onSuccess() {

    }

    @Override
    public void onError(String code, String error) {

    }
});
```