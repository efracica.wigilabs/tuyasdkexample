
## 用户 UID 登录注册接口

**接口描述**

注册和登录为一体的接口，如果注册了就自动登录，如果没有注册就自动注册并且登录。

```java
TuyaHomeSdk.getUserInstance().loginOrRegisterWithUid(String countryCode, String uid, String passwd, ILoginCallback callback);

//支持创建默认家庭
TuyaHomeSdk.getUserInstance().loginOrRegisterWithUid(String countryCode, String uid, String passwd, boolean isCreateHome, IUidLoginCallback callback);
```
| 参数         | 说明              |
| ------------ | ----------------- |
| countryCode  | 国家区号,例如：86 |
| uid          | uid               |
| passwd       | 用户密码          |
| isCreateHome | 是否默认创建家庭  |
| callback     | 回调              |

**示例代码**

```java
//uid登录
TuyaHomeSdk.getUserInstance().loginOrRegisterWithUid("86", "1234", "123456", new ILoginCallback() {
    @Override
    public void onSuccess(User user) {
        Toast.makeText(mContext, "登录成功，用户名：" , Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(String code, String error) {
        Toast.makeText(mContext, "code: " + code + "error:" + error, Toast.LENGTH_SHORT).show();
    }
});
```
