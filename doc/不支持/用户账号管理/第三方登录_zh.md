
第三方登录需要将对应的第三方的 配置信息配置到 tuya iot 平台上，否则，调用下面接口会失败。

![thirdParty-login](https://images.tuyacn.com/fe-static/docs/img/b89af907-ced5-432a-ba95-cb6dfc4a2d32.png)

## 微信登录

**接口说明**

```java
TuyaHomeSdk.getUserInstance().loginByWechat(String countryCode, String code, ILoginCallback callback);
```

**参数说明**

| 参数  | 说明                |
| ---- | ---- |
| phoneCode | 手机区号,例如：86   |
| code  | 微信授权登录获取的 code |
| callback | 回调                |


## QQ 登录

**接口说明**

QQ 登录接口

```java
TuyaHomeSdk.getUserInstance().loginByQQ(String countryCode, String userId, String accessToken, ILoginCallback callback);
```

**参数说明**

| 参数    | 说明                     |
| ---- | ---- |
| countryCode | 国家区号,例如：86        |
| userId  | QQ授权登录获取的 userId  |
| accessToken | QQ授权登录获取的 accessToken |
| callback | 回调                     |

## Facebook 登录

**接口说明**


```java
TuyaHomeSdk.getUserInstance().loginByFacebook(String phoneCode, String token, ILoginCallback callback);
```

**参数说明**

| 参数    | 说明                      |
| ---- | ---- |
| countryCode | 手机区号,例如：86         |
| token   | facebook 授权登录获取的 token |


## Auth2登录

**接口说明**

Auth2 的接口是一个通用的登录接口，可以根据传参来确认正在使用 Auth2 的类型。

```java
void thirdLogin(String countryCode, String accessToken, String type, String extraInfo, ILoginCallback callback) 
```

**参数说明**

| 参数    | 说明                                      |
| ---- | ---- |
| countryCode | 国家码,例如：86                           |
| accessToken | 授权登录的 token                          |
| type    | Auth2 接口调用的类型，例如：Google登录用 "gg" |
| extraInfo  | 额外的参数                                |
| callback | 回调                                      |

**示例代码**

```java
TuyaHomeSdk.getUserInstance().thirdLogin("your_country_code","auth2_token","auth2_type","{"info_key":"info_value"}", new ILoginCallback() {
    @Override
    public void onSuccess(User user) {
       
    }
    @Override
    public void onError(String code, String error) {
      
    }
});
```



### Google登录

**接口说明**

Auth2接口支持三方登录，授权成功后通过 Auth2 的接口传入 token(这里是Google id Token) 和 extraInfo 等信息，可以实现Google登录。（建议国外用户使用）

**参数说明**

| 参数    | 说明              |
| ---- | ---- |
| type    | "gg"              |
| countryCode | 国家码,例如：86   |
| accessToken | Google 授权的id token |
| extraInfo  | {\"pubVersion\": 1}  |
| callback | 回调              |

**示例代码**

```java
TuyaHomeSdk.getUserInstance().thirdLogin(countryNumberCode,token,"gg","{\"pubVersion\":1}", new ILoginCallback() {
    @Override
    public void onSuccess(User user) {
       
    }
    @Override
    public void onError(String code, String error) {
      
    }
});
```