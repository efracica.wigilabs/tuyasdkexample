
## Terms

|  | Description |
| ---- | ---- |
| `dpCode` | An identifier of a data point (DP) for a device. Each data point is assigned a name and a `dpCode`. For more information, see the [List of Bluetooth lock DPs](#蓝牙门锁功能点列表) section of this topic. |
| Duress alert | The duress alert feature allows you to enroll a password or fingerprint as a duress code. If lock users are coerced by hostile persons, unlocking with the duress code can trigger alert messages to be sent to a list of contacts. |
| Lock member | Lock members are classified into home members and non-home members. <br />Home members are specified in a smart home. You can associate a lock password number with a home member. <br />Non-home members are the members that are associated with locks. You can create a non-home member and assign a lock to the non-home member to make them associated. |
| `lockUserId` and `userId` | A `lockUserId` is a firmware member ID that the cloud assigns to a lock when you create a lock member. Each `lockUserId` indicates the user ID that is recorded in the firmware. <br />A `userId` is the ID that the cloud assigns to a lock member when you create the member. Each `userId` is a unique ID of each user and recorded in a database. |


## Description

| Class name | Description |
| ---- | ---- |
| `TuyaOptimusSdk` | The entry that is used to initialize the SDK and get the lock management class. |
| `ITuyaLockManager` | The lock management class that is used to get different types of lock classes. |
| `ITuyaBleLock` | The Bluetooth lock class that includes all methods of Bluetooth locks. |

**Sample code**

The following code block shows how to create a Bluetooth lock class based on a device ID:

```java
// Initialize the SDK for only once.
TuyaOptimusSdk.init(getApplicationContext());
// Get the TuyaLockManager class.
ITuyaLockManager tuyaLockManager = TuyaOptimusSdk.getManager(ITuyaLockManager.class);
// Create the ITuyaBleLock class.
ITuyaBleLock tuyaLockDevice = tuyaLockManager.getBleLock(your_device_id);
```

## Manage lock members
Lock members are classified into home members and non-home members. Home members are specified in a smart home. For more information, see [Home Management.](https://developer.tuya.com/en/docs/app-development/android-app-sdk/home-management/homemanage?id=Ka6kjkgere4ae)

### Get a list of lock members
**Description**

```java
/**
 * get lock users
 */
public void getLockUsers(final ITuyaResultCallback<List<BLELockUser>> callback)
```

**Parameters**

**`BLELockUser `data model**

| Parameter | Type | Description |
|---|---|---|
| userId | String | The user ID. |
| lockUserId | int | The user ID that is associated with the lock. |
| userContact | String | The contact information of the user. |
| nickName | String | The nickname of the user. |
| avatarUrl | String | The avatar URL of the user. |
| userType | int | The type of user. Valid values:<br>`10`: an administrator<br>`20`: a common home member<br>`30`: a lock user |
| supportUnlockTypes | List<String> | The supported unlocking types. For more information, see the unlocking methods that are specified by `TuyaUnlockType`. |
| effectiveTimestamp | long | The timestamp in milliseconds when the user takes effect. |
| invalidTimestamp | long | The timestamp in milliseconds when the user expires. |


**Sample code**

```java
tuyaLockDevice.getLockUsers(new ITuyaResultCallback<List<BLELockUser>>() {
    @Override
    public void onError(String code, String message) {
        Log.e(TAG, "get lock users failed: code = " + code + "  message = " + message;
    }

   @Override
    public void onSuccess(List<BLELockUser> user) {
        Log.i(TAG, "get lock users success: lockUserBean = " + user);
    }
});
```

### Create a lock member

You can call this operation to add a member to a lock. Then, you can associate the member with an unlocking method.

```sequence
Title: Procedure to create a lock member

participant app
participant cloud

note over app: Enter the information about the member.
app->cloud: Call an API operation to create a member.
cloud-->app: Generate the lock user ID and hardware member ID and return the result to indicate whether the member is created.
note over app: Process the response and display the processing result.
```

> Note: A non-home member is created in this example. You can also create a home member. For more information, see [Home Management.](https://developer.tuya.com/en/docs/app-development/android-app-sdk/home-management/homemanage?id=Ka6kjkgere4ae)

**Description**

```java
/**
 * add lock user
 *
 * @param userName           User name.
 * @param allowedUnlock      Whether unlocking with Bluetooth is allowed.
  @param permanent          Whether the user is permanent.
 * @param effectiveTimestamp The time when a user takes effect.
 * @param invalidTimestamp   The time when a user expires.
 * @param avatarFile         Avatar.
 * @param callback           Callback.
 */
void addLockUser(final String userName, boolean allowedUnlock, boolean permanent, long effectiveTimestamp, long invalidTimestamp, File avatarFile, final ITuyaResultCallback<Boolean> callback);
```

**Parameters**

| Parameter | Description |
|---|---|
| userName | The username. |
| allowedUnlock | Allows you to unlock a door with Bluetooth. |
| unlockType | The unlocking method. For more information, see the unlocking methods that are specified by `TuyaUnlockType`. |
| permanent | Specifies whether the user is a permanent user. |
| effectiveTimestamp | The timestamp in milliseconds when the user takes effect. If `permanent` is set to `true`, ignore this parameter. |
| invalidTimestamp | The timestamp in milliseconds when the user expires. If `permanent` is set to `true`, ignore this parameter. |
| avatarFile | The user avatar file. |

**Sample code**

```java
tuyaLockDevice.addLockUser("your_user_name", true, true, 0, 0, null, new ITuyaResultCallback<Boolean>() {
    @Override
    public void onError(String code, String message) {
        Log.e(TAG, "add lock user failed: code = " + code + "  message = " + message);
    }

    @Override
    public void onSuccess(Boolean result) {
        Log.i(TAG, "add lock user success");
    }
});
```

### Update a lock member

You can call this operation to update a lock member. This feature requires communication between your application and the Bluetooth lock. Your application must be connected to the Bluetooth lock during the operation.

```sequence
Title: Procedure to update a lock member

participant cloud
participant app
participant lock

note over app: Enter the new information about the member.
app->lock: Create a Bluetooth connection.
app->lock: Send a command to update the information about the member.
lock-->app: Return the update result.
app->cloud: Call an API operation to update a member.
cloud-->app: Return the update result.
note over app: Process the response and display the processing result.
```

**Description**

```java
/**
 * update lock user
 *
 * @param userId             User ID.
 * @param userName           User name.
 * @param allowedUnlock      Whether unlocking with Bluetooth is allowed.
 * @param permanent          Whether the user is permanent.
 * @param effectiveTimestamp The time when a user takes effect.
 * @param invalidTimestamp   The time when a user expires.
 * @param avatarFile         Avatar.
 * @param callback           Callback.
 */
void updateLockUser(final String userId, boolean allowedUnlock, final String userName, final boolean permanent, long effectiveTimestamp, long invalidTimestamp, File avatarFile, final ITuyaResultCallback<Boolean> callback);

```

**Parameters**

| Parameter | Description |
|---|---|
| userId | The user ID. |
| allowedUnlock | Allows you to unlock a door with Bluetooth. |
| userName | The username. |
| permanent | Specifies whether the user is a permanent user. |
| effectiveTimestamp | The timestamp in milliseconds when the user takes effect. If `permanent` is set to `true`, ignore this parameter. |
| invalidTimestamp | The timestamp in milliseconds when the user expires. If `permanent` is set to `true`, ignore this parameter. |
| avatarFile | The user avatar file. |

**Sample code**

```java
tuyaLockDevice.updateLockUser("your_user_id", true, "your_user_name", true, 0, 0, null, new ITuyaResultCallback<Boolean>() {
    @Override
    public void onError(String code, String message) {
        Log.e(TAG, "update lock user failed: code = " + code + "  message = " + message);
    }

   @Override
    public void onSuccess(Boolean aBoolean) {
        Log.i(TAG, "update lock user success");
    }
});
```

### Delete a lock member

You can call this operation to delete a lock member. **This feature requires communication between your application and the Bluetooth lock. Your application must be connected to the Bluetooth lock during the operation. After a lock member is deleted, all unlocking methods and passwords that are associated with the member are deleted**.

```sequence
Title: Procedure to delete a lock member

participant cloud
participant app 
participant lock 

note over app: Confirm to delete the member.
app->lock: Create a Bluetooth connection.
app->lock: Send a command to delete the information about the member.
lock-->app: Return the deletion result.
app->cloud: Call an API operation to delete a user.
cloud-->app: Return the deletion result.
note over app: Process the response and display the processing result.
```

**Description**

```java
/**
 * delete lock user
 * @param user User bean.
 * @param callback  Callback.
 */
public void deleteLockUser(BLELockUser user, final ITuyaResultCallback<Boolean> callback)
```

## Manage the Bluetooth status
To use the features of a Bluetooth lock, you must enable the Bluetooth feature on your application.

### Check Bluetooth connection

**Description**

You can call this operation to check whether a Bluetooth lock is connected to your application.

This operation is required to check the Bluetooth lock status. You can manage a Bluetooth lock only when the Bluetooth lock is connected to your application.

```java
/**
 *  @return If the lock is online, return true.
 */
public boolean isBLEConnected() 
```

**Sample code**

```java
boolean online = tuyaLockDevice.isBLEConnected();
```

### Connect to a Bluetooth lock

**Description**

If a Bluetooth lock is not connected to your application, you can call this operation to connect to the Bluetooth lock.

```java
/**
 * connect to lock
 *
 * @param connectListener Callback of BLE lock connection status.
 */
public void connect(ConnectListener connectListener)
```

**Parameters**

`ConnectListener` is a callback of the lock status. The `onStatusChanged` method indicates whether the lock is connected to your application.

**Sample code**

```java
tuyaLockDevice.connect(new ConnectListener() {
    @Override
    public void onStatusChanged(boolean online) {
        Log.i(TAG, "onStatusChanged  online: " + online);
    }
});
```


## Manage dynamic passwords

You can call this operation to get a dynamic password and enter the password to unlock the door. Each dynamic password is valid for five minutes.

### Get dynamic password
**Description**

```java
public void getDynamicPassword(final ITuyaResultCallback<String> callback)
```
**Parameters**

| Parameter | Description |
| ---- | ---- |
| callback | The callback that is used to get a dynamic password. |

**Sample code**

```java
tuyaLockDevice.getDynamicPassword(new ITuyaResultCallback<String>() {
    @Override
    public void onError(String code, String message) {
        Log.e(TAG, "get lock dynamic password failed: code = " + code + "  message = " + message);
    }

   @Override
    public void onSuccess(String dynamicPassword) {
        Log.i(TAG, "get lock dynamic password success: dynamicPassword = " + dynamicPassword);
    }
});
```


## Unlock and lock a door with Bluetooth

### Unlock a door with Bluetooth

```sequence
Title: Procedure to unlock a door with Bluetooth

participant user
participant app
participant lock

note over app: Enable the Bluetooth feature and connect to the Bluetooth lock.
user->app: Tap Unlock on your application.
app->lock: Send a command to unlock a door with Bluetooth.
note over lock: Receive the command and unlock the door with Bluetooth.
lock-->app: Return the unlocking result.
note over app: Process the response and display the processing result.
```


**Description**

```java
/**
 * unlock the door
 */
public void unlock(String lockUserId)
```

**Parameters**

| Parameter | Description |
|---|---|
| lockUserId | The user ID that is associated with the lock. |

Each user of a lock is assigned an ID that starts from 1. The ID is increased by 1 and assigned to a new user.

**Sample code**

```java
// `1` indicates the current user ID that is associated with the lock.
tuyaLockDevice.unlock("1");
```

### Lock a door with Bluetooth
```sequence
Title: Procedure to lock a door with Bluetooth

participant user
participant app
participant lock

note over app: Enable the Bluetooth feature and connect to the Bluetooth lock.
user->app: Tap Lock on your application.
app->lock: Send a command to lock a door with Bluetooth.
note over lock: Receive the command and lock the door with Bluetooth.
lock-->app: Return the locking result.
note over app: Process the response and display the processing result.
```

**Description**

You can call this operation to lock a door with Bluetooth after your application is connected to the Bluetooth lock.

```java
/**
 * lock the door
 */
public void lock()
```

**Sample code**

```java
tuyaLockDevice.lock();
```
## Manage lock records

### Get alert records

**Description**

```java
/**
 * get alarm records
 * @param Offset page number.
 * @param Limit item count.
 * @param callback Callback.
 */
void getAlarmRecords(int offset, int limit, final ITuyaResultCallback<Record> callback);
```


**Parameters**

| Parameter | Description |
|---|---|
| offset | The page number of the records to be returned. |
| limit | The number of records to be returned. |

**`Record ` data model**

The following table describes the parameters of `Record`.

| Parameter | Type | Description |
|---|---|---|
| totalCount | int | The total number of records to be returned. |
| hasNext | boolean | Specifies whether to return the next page. |
| datas | List<DataBean> | The data that is included in a record. |

The following table describes the parameters of `DataBean`.

| Parameter | Type | Description |
| ---- | ---- | ---- |
| userId | String | The member ID. |
| userName | String | The nickname of the member. |
| unlockType | String | The type of unlocking. |
| devId | String | The ID of the device. |
| createTime | long | The timestamp of the record |
| tags | int | The tag. Valid values:<br>`1`: a duress alert<br>`0`: other scenes |
| unlockRelation | UnlockRelation | The instance that specifies the relationship between an unlocking type and an unlocking password number. Ignore this parameter for non-unlocking records. |

**Sample code**

```java
tuyaLockDevice. getAlarmRecords(0, 10, new ITuyaResultCallback<Record>() {
    @Override
    public void onError(String code, String message) {
        Log.e(TAG, "get lock records failed: code = " + code + "  message = " + message);
    }

    @Override
    public void onSuccess(Record recordBean) {
        Log.i(TAG, "get lock records success: recordBean = " + recordBean);
    }
});
```

### Get unlocking records

**Description**

```java
/**
 * get unlock records
 * @param unlockTypes The list of unlocking types.
 * @param Offset page number.
 * @param Limit item count.
 * @param callback Callback.
 */
void getUnlockRecords(int offset, int limit, final ITuyaResultCallback<Record> callback);
```

**Parameters**

| Parameter | Description |
|---|---|
| offset | The page number of the records to be returned. |
| limit | The number of records to be returned. |


**Sample code**

```java
tuyaLockDevice.getUnlockRecords(0, 10, new ITuyaResultCallback<Record>() {
    @Override
    public void onError(String code, String message) {
        Log.e(TAG, "get unlock records failed: code = " + code + "  message = " + message);
    }

   @Override
    public void onSuccess(Record recordBean) {
        Log.i(TAG, "get unlock records success: recordBean = " + recordBean);
    }
});
```


## Manage unlocking methods

This section describes the operations that are used to add, modify, or delete an unlocking method.

The following procedure shows how to add an unlocking method.

```mermaid
sequenceDiagram
SDK->>Lock: Connect
Note left of Lock: Connect via Bluetooth
Lock-->>SDK: Connect success
SDK->>Lock: Add unlock mode
Lock-->>SDK: Success
SDK->>Server: Notify to server
loop save info
	Server->>Server: 
end
Server-->>SDK: Success
```

### Get a list of unlocking methods

**Description**

```java
/**
 * get unlock mode by unlockType
 *
 * @param unlockType Unlocking type {@link com.tuya.smart.optimus.lock.api.TuyaUnlockType}.
 * @param callback Callback.
 */
void getUnlockModeList(String unlockType, final ITuyaResultCallback<ArrayList<UnlockMode>> callback);
```


**Parameters**

**`UnlockMode` data model**

| Parameter | Type | Description |
|---|---|---|
| userId | String | The user ID. |
| lockUserId | int | The user ID that is associated with the lock. |
| userName | String | The nickname of the member. |
| unlockAttr | int | The attribute of the unlocking method. Valid values:<br>`0`: a common password<br>`1`: a duress password |
| userType | int | The type of user. Valid values:<br>`10`: an administrator<br>`20`: a common home member<br>`30`: a lock user |
| unlockModeId | String | The ID of the current unlocking method on the service end. |
| unlockId | String | The ID of the current unlocking method in the lock. |
| unlockName | String | The name of the current unlocking method. |
| unlockType | String | The unlocking type. For more information, see the settings of `TuyaUnlockType`. |

**Sample code**

```java
tuyaLockDevice.getUnlockModeList(TuyaUnlockType.PASSWORD, new ITuyaResultCallback<ArrayList<UnlockMode>>() {
    @Override
    public void onSuccess(ArrayList<UnlockMode> result) {
        Log.i(TAG, "getUnlockModeList  onSuccess: " + result);
    }

    @Override
    public void onError(String errorCode, String errorMessage) {
        Log.e(TAG, "getUnlockModeList failed: code = " + errorCode + "  message = " + errorMessage);
    }
});
```


### Register the unlocking method listener


This operation is called to return a response after you call the asynchronous operations that are used to add, modify, or delete unlocking methods.

**Description**

You can call this operation to register the unlocking method listener.

```java
void setUnlockModeListener(UnlockModeListener unlockModeListener);
```

The following code block shows how to set `UnlockModeListener`:


```java
public interface UnlockModeListener {

    /**
    * The unlocking method parameter is missing.
     */
    int FAILED_STATUS_ILLEGAL_ARGUMENT = -1;
    /**
     * The current operation does not support the specified unlocking method.
     */
    int FAILED_STATUS_NOT_SUPPORT_UNLOCK_TYPE = -2;
    /**
     * Failed to send a control command.
     */
    int FAILED_STATUS_SEND_ERROR = -3;
    /**
     * Failed to send a request to the service end.
     */
    int FAILED_STATUS_REQUEST_SERVER_ERROR = -4;
    /**
     * The fingerprint is incomplete.
     */
    int FAILED_STATUS_FINGERPRINT_INCOMPLETE = -5;
    /**
     * The service end fails to return a response.
     */
    int FAILED_STATUS_SERVER_RESPONSE_FAILED = -6;
    /**
     * The lock fails to return a response.
     */
    int FAILED_STATUS_LOCK_RESPONSE_FAILED = -7;
    /*----The following types of status are defined in a lock. An error code is returned to indicate the status when the system fails to create an unlocking method.----*/
    int FAILED_STATUS_TIMEOUT = 0x00;
    int FAILED_STATUS_FAILED = 0x01;
    int FAILED_STATUS_REPEAT = 0x02;
    int FAILED_STATUS_LOCK_ID_EXHAUSTED = 0x03;
    int FAILED_STATUS_PASSWORD_NOT_NUMBER = 0x04;
    int FAILED_STATUS_PASSWORD_WRONG_LENGTH = 0x05;
    int FAILED_STATUS_NOT_SUPPORT = 0x06;
    int FAILED_STATUS_ALREADY_ENTERED = 0x07;
    int FAILED_STATUS_ALREADY_BOUND_CARD = 0x08;
    int FAILED_STATUS_ALREADY_BOUND_FACE = 0x09;
    int FAILED_STATUS_PASSWORD_TOO_SIMPLE = 0x0A;
    int FAILED_STATUS_WRONG_LOCK_ID = 0xFE;

    /**
     * The callback used to add, modify, or delete an unlocking method
     *
     * @param devId              The device ID.
     * @param userId             The user ID.
     * @param unlockModeResponse The response.
     */
    void onResult(String devId, String userId, UnlockModeResponse unlockModeResponse);
}
```

**Parameters**


| Parameter | Type | Description |
|---|---|---|
| unlockMethod | String | Valid values of lock management types:<br>/** Add an unlocking method.  \*/ <br/>public static final String UNLOCK\_METHOD\_CREATE = "unlock\_method\_create";<br/>/** Modify an unlocking method.  \*/ <br/>public static final String UNLOCK\_METHOD\_MODIFY = "unlock\_method\_modify";<br/> /** Delete an unlocking method.  \*/ <br/>public static final String UNLOCK\_METHOD\_DELETE = "unlock\_method\_delete"; |
| unlockType | String | The unlocking type. For more information, see the settings of `TuyaUnlockType`. |
| stage | int | The current stage in which the lock is managed. The value is defined by `BleLockConstant`. Valid values:<br>int STAGE_AFTER = -2; // Synchronizes data to the service end after the lock is managed.<br/>int STAGE_BEFORE = -1;// Runs tasks, such as command delivery and communication with the service end, before communication with the lock.<br/> int STAGE_START = 0x00; // Starts the unlocking method enrollment.<br/>int STAGE_CANCEL = 0xFE;// Cancels the unlocking method enrollment.<br/> int STAGE_FAILED = 0xFD;// Failed to enroll the unlocking method.<br/>int STAGE_ENTERING = 0xFC;// Enrolling the unlocking method.<br/> int STAGE_SUCCESS = 0xFF;// The unlocking method is enrolled. |
| lockUserId | int | The user ID that is associated with the lock. |
| unlockId | int | The ID of the current unlocking method in the lock. |
| unlockModeId | String | The ID of the current unlocking method on the service end. |
| admin | boolean | Specifies whether the user is an administrator. |
| times | int | Valid values:<br>`0`: specifies the specified password is permanently valid.<br>`1` to `254`: specifies the number of times the password can be used as expected. |
| status | int | The HTTP status code that specifies a failed unlocking method input. For more information, see the settings of `UnlockModeListener`. |
| failedStage | int | The stage in which the failure occurs. This parameter is required only when `stage` is set to `STAGE_FAILED`. |

**Sample code**

```java
tuyaLockDevice.setUnlockModeListener(new UnlockModeListener() {
  @Override
  public void onResult(String devId, String userId, UnlockModeResponse unlockModeResponse) {
    Log.i(TAG, "UnlockModeListener devId: " + devId);
    Log.i(TAG, "UnlockModeListener userId: " + userId);
    Log.i(TAG, "UnlockModeListener unlockType: " + unlockModeResponse.unlockType);
    Log.d(TAG, "UnlockModeListener: " + unlockModeResponse);
    if (unlockModeResponse.success) {
      if (TextUtils.equals(unlockModeResponse.unlockMethod, UnlockModeResponse.UNLOCK_METHOD_CREATE)) {
        Log.i(TAG, "Create unlock mode success");
      } else if (TextUtils.equals(unlockModeResponse.unlockMethod, UnlockModeResponse.UNLOCK_METHOD_MODIFY)) {
        Log.i(TAG, "Modify unlock mode success");
      } else if (TextUtils.equals(unlockModeResponse.unlockMethod, UnlockModeResponse.UNLOCK_METHOD_DELETE)) {
        Log.i(TAG, "Delete unlock mode success");
      }
    } else if (unlockModeResponse.stage == BleLockConstant.STAGE_FAILED) {
      if (TextUtils.equals(unlockModeResponse.unlockMethod, UnlockModeResponse.UNLOCK_METHOD_CREATE)) {
        Log.w(TAG, "Create unlock mode failed.");
        Log.w(TAG, "Create unlock mode failed reason: " + unlockModeResponse.status);
        Log.w(TAG, "Create unlock mode failed stage: " + unlockModeResponse.failedStage);
      } else if (TextUtils.equals(unlockModeResponse.unlockMethod, UnlockModeResponse.UNLOCK_METHOD_MODIFY)) {
        Log.w(TAG, "Modify unlock mode failed.");
        Log.w(TAG, "Modify unlock mode failed reason: " + unlockModeResponse.status);
        Log.w(TAG, "Modify unlock mode failed stage: " + unlockModeResponse.failedStage);
      } else if (TextUtils.equals(unlockModeResponse.unlockMethod, UnlockModeResponse.UNLOCK_METHOD_DELETE)) {
        Log.w(TAG, "Delete unlock mode failed.");
        Log.w(TAG, "Delete unlock mode failed reason: " + unlockModeResponse.status);
        Log.w(TAG, "Delete unlock mode failed stage: " + unlockModeResponse.failedStage);
      }
    }
  }
});
```

### Add an unlocking method


**Description**

You can call this operation to add an unlocking method based on the specified unlocking type. **Your application must be connected to the lock with Bluetooth.**

```java
/**
 * Add unlock method.
 *
 * @param unlockType Unlocking types {@link com.tuya.smart.optimus.lock.api.TuyaUnlockType}.
 * @param user       The lock user {@link com.tuya.smart.sdk.optimus.lock.bean.ble.BLELockUser}.
 * @param name       The name of unlocking mode.
 * @param password   The password of an unlocking mode. If it is not the password unlocking method, this field can be null.
 * @param times      The number of times the unlocking mode can be used. The value ranges from 0 to 254. 0 specifies unlimited times, and 1 to 254 specifies the actual number of times.
 * @param isHijack   The duress flag. If it is true, a duress alert will be triggered when unlocking with this unlock mode.
 */
void addUnlockMode(final String unlockType, final BLELockUser user, String name, String password, int times, boolean isHijack);
```

**Parameters**

| Parameter | Description |
|---|---|
| unlockType | The unlocking type. For more information, see the settings of `TuyaUnlockType`. |
| user | The BLELockUser type of data model. |
| name | The name of the unlocking method. |
| password | The password that is used to unlock the door. This parameter is required when `unlockType` is set to `password`. |
| times | The number of times the specified password can be used as expected. Valid values:<br>`0`: specifies that the specified password is permanently valid.<br>`1` to `254`: specifies the number of times the password can be used as expected. |
| isHijack | The tag that specifies whether duress occurs. When this parameter is set to `true`, if the specified password is used to unlock the door, a duress alert is triggered. |

**Sample code**

The following code block shows how to set a password-based unlocking method for the first home member:

```java
tuyaLockDevice.getHomeUsers(new ITuyaResultCallback<List<BLELockUser>>() {
    @Override
    public void onSuccess(List<BLELockUser> result) {
        Log.i(TAG, "getHomeUsers  onSuccess: " + result);
        // add password unlock mode
        tuyaLockDevice.addUnlockMode(TuyaUnlockType.PASSWORD, result.get(0), "test_unlock_mode1", "431232", 0, false);
    }

    @Override
    public void onError(String errorCode, String errorMessage) {
        Log.e(TAG, "getHomeUsers failed: code = " + errorCode + "  message = " + errorMessage);
    }
});
```

### Update unlocking method data

**Description**

You can call this operation to update the name of an unlocking method or modify the duress tag. **Your application does not need to be connected to the lock with Bluetooth.**

This operation does not require communication with the lock, but requires communication with only the service end.

```java
/**
 * Update name and hijack flag of the unlocking method. Only update server information, not communicate with door lock device
 *
 * @param unlockMode Unlock mode bean {@link com.tuya.smart.sdk.optimus.lock.bean.ble.UnlockMode}.
 * @param name       Unlock mode name.
 * @param isHijack   The duress flag. If it is true, a duress alert will be triggered when unlocking with this unlock mode.
 */
void updateUnlockModeServerInfo(UnlockMode unlockMode, String name, boolean isHijack);
```

**Parameters**

| Parameter | Description |
| ---- | ---- |
| unlockMode | The data model of the unlocking method. |
| name | The name of the unlocking method. |
| isHijack | The tag that specifies whether duress occurs. When this parameter is set to `true`, if the specified password is used to unlock the door, a duress alert is triggered. |

**Sample code**

```java
tuyaLockDevice.getUnlockModeList(TuyaUnlockType.FINGERPRINT, new ITuyaResultCallback<ArrayList<UnlockMode>>() {
    @Override
    public void onSuccess(ArrayList<UnlockMode> result) {
        Log.i(TAG, "getUnlockModeList  onSuccess: " + result);
        for (UnlockMode unlockMode : result) {
            if (TextUtils.equals(unlockMode.unlockName, "test_unlock_mode1")) {
                tuyaLockDevice.updateUnlockModeServerInfo(unlockMode, "test_unlock2", false);// rename unlock mode
            }
        }
    }

    @Override
    public void onError(String errorCode, String errorMessage) {
        Log.e(TAG, "getUnlockModeList failed: code = " + errorCode + "  message = " + errorMessage);
    }
});
```

### Delete an unlocking method

**Description**

You can call this operation to delete a specified unlocking method. **Your application must be connected to the lock with Bluetooth.**

```java
/**
 * Delete unlockMode.
 *
 * @param unlockMode Unlock mode.
 */
void deleteUnlockMode(UnlockMode unlockMode);
```

**Sample code**

```java
tuyaLockDevice.deleteUnlockMode(unlockMode);
```

### Cancel fingerprint enrollment

**Description**

You can call this operation to cancel fingerprint enrollment. To unlock a door with a fingerprint, you might need to enroll the fingerprint four to five times. **Your application must be connected to the lock with Bluetooth.**

```java
/**
 * Cancel fingerprint entry.
 * <p>
 * The fingerprint entry process will be repeated multiple times and can be cancelled during the entry process.
 *
 * @param user Lock user {@link com.tuya.smart.sdk.optimus.lock.bean.ble.BLELockUser}.
 */
void cancelFingerprintUnlockMode(final BLELockUser user);
```
### Update an unlocking password

**Description**

You can call this operation to update information about a password after you specify the password as the unlocking method. The information includes the password, password name, number of times the password can be used as expected, and duress tag. **Your application must be connected to the lock with Bluetooth.**

Note: This operation supports only the password-based unlocking method.

```java
/**
 * Update the name, password, validity period and other information of the unlocking method
 *
 * @param unlockMode Unlock mode bean {@link com.tuya.smart.sdk.optimus.lock.bean.ble.UnlockMode}.
 * @param name       Unlock mode name.
 * @param password   The password of an unlocking mode. If it is not the password unlocking method, this field can be null.
 * @param times      The number of times the unlocking mode can be used. The value ranges from 0 to 254. 0 specifies unlimited times, and 1 to 254 specifies the actual number of times.
 * @param isHijack   The duress flag. If it is true, a duress alert will be triggered when unlocking with this unlock mode.
 */
void updatePasswordUnlockMode(UnlockMode unlockMode, String name, String password, int times, boolean isHijack);
```

**Parameters**

| Parameter | Description |
| ---- | ---- |
| unlockMode | The data model of the unlocking method. |
| name | The name of the unlocking method. |
| password | The password that is used to unlock the door. This parameter is required when `unlockType` is set to `password`. |
| times | The number of times the specified password can be used as expected. Valid values:<br>`0`: specifies that the specified password is permanently valid.<br>`1` to `254`: specifies the number of times the password can be used as expected. |
| isHijack | The tag that specifies whether duress occurs. When this parameter is set to `true`, if the specified password is used to unlock the door, a duress alert is triggered. |

**Sample code**

```java
tuyaLockDevice.getUnlockModeList(TuyaUnlockType.PASSWORD, new ITuyaResultCallback<ArrayList<UnlockMode>>() {
    @Override
    public void onSuccess(ArrayList<UnlockMode> result) {
        Log.i(TAG, "getUnlockModeList  onSuccess: " + result);
        for (UnlockMode unlockMode : result) {
            if (TextUtils.equals(unlockMode.unlockName, "test_password")) {
                tuyaLockDevice.updatePasswordUnlockMode(unlockMode, "test_password", "131232", 0, false);// modify password
            }
        }
    }

    @Override
    public void onError(String errorCode, String errorMessage) {
        Log.e(TAG, "getUnlockModeList failed: code = " + errorCode + "  message = " + errorMessage);
    }
});
```

## Manage temporary passwords

Temporary passwords are added to the lock for temporary use. This type of password is classified into one-time passwords and periodic passwords. One-time passwords can be used only once and then become invalid. Periodic passwords are valid only within a specified period.

This section describes the operations that are used to add, modify, or delete a temporary password.

The following procedure shows how to add a temporary password.

![](https://images.tuyacn.com/fe-static/docs/img/980ae276-3dd4-4656-b14c-48778c71dfb7.png)

### Register a temporary password listener
You can call this operation to register a temporary password listener. This listener can be used as a callback to add, modify, or delete a temporary password.

**Description**

```java
/**
 * Set temporary password listener
 *
 * @param temporaryPasswordListener Temporary password listener.
 */
void setTemporaryPasswordListener(TemporaryPasswordListener temporaryPasswordListener);
```

**Parameters**

| Parameter | Description |
| ------------------------- | ------------------------------ |
| temporaryPasswordListener | The listener that can be used to add, modify, or delete a temporary password. |

The following code block shows the settings of the temporary password listener:

```java
public interface TemporaryPasswordListener {

    /**
     * Create a temporary password.
     */
    String TEMPORARY_PASSWORD_CREATE = "temporary_password_creat";
    /**
     * Modify the temporary password.
     */
    String TEMPORARY_PASSWORD_MODIFY = "temporary_password_modify";
    /**
     * Delete the temporary password.
     */
    String TEMPORARY_PASSWORD_DELETE = "temporary_password_delete";
  
    /*-------------------Error codes returned when the temporary password failed to be added, modified, or deleted-------------------------------------*/
    /**
     * illegal argument
     */
    int FAILED_STATUS_ILLEGAL_ARGUMENT = -1;
    /**
     * Device not support such method
     */
    int FAILED_STATUS_NOT_SUPPORT_UNLOCK_TYPE = -2;
    /**
     * Command send to device failed
     */
    int FAILED_STATUS_SEND_FAILED = -3;
    /**
     * Request server failed
     */
    int FAILED_STATUS_REQUEST_SERVER_FAILED = -4;
    /**
     * Device offline
     */
    int FAILED_STATUS_DEVICE_OFFLINE = -5;
    /**
     * Server response failed
     */
    int FAILED_STATUS_SERVER_RESPONSE_FAILED = -6;
    /** Lock response failed */
    int FAILED_STATUS_LOCK_RESPONSE_FAILED = 1;
    /** Lock sequence number exhausted */
    int FAILED_STATUS_SEQUENCE_NUMBER_EXHAUSTED = 2;

    /**
     * The callback used to add, modify, or delete an unlocking method
     *
     * @param sn The serial number of the password.
     */
    void onSuccess(String type, int sn);
 
    /**
     * The callback used to add, modify, or delete an unlocking method
     *
     * @param type       The type of operation, for example, to add, delete, or modify.
     * @param resultCode The HTTP status code.
     */
    void onFailed(String type, int resultCode);
}
```

**Sample code**

```java
tuyaLockDevice.setTemporaryPasswordListener(new TemporaryPasswordListener() {
    @Override
    public void onSuccess(String type, int sn) {
        switch (type) {
            case TemporaryPasswordListener.TEMPORARY_PASSWORD_CREATE:
                Log.i(TAG, "temporary password create success, passsword sequence number: " + sn);
                break;
            case TemporaryPasswordListener.TEMPORARY_PASSWORD_MODIFY:
                Log.i(TAG, "temporary password modify success, passsword sequence number: " + sn);
                break;
            case TemporaryPasswordListener.TEMPORARY_PASSWORD_DELETE:
                Log.i(TAG, "temporary password delete success, passsword sequence number: " + sn);
                break;
        }
    }

    @Override
    public void onFailed(String type, int resultCode) {
        switch (type) {
            case TemporaryPasswordListener.TEMPORARY_PASSWORD_CREATE:
                Log.w(TAG, "temporary password create failed, resultCode: " + resultCode);
                break;
            case TemporaryPasswordListener.TEMPORARY_PASSWORD_MODIFY:
                Log.w(TAG, "temporary password modify failed, resultCode: " + resultCode);
                break;
            case TemporaryPasswordListener.TEMPORARY_PASSWORD_DELETE:
                Log.w(TAG, "temporary password delete failed, resultCode: " + resultCode);
                break;
        }
    }
});
```

### Get a list of temporary passwords

You can call this operation to get a list of temporary passwords. Temporary passwords are classified into one-time passwords and periodic passwords.

**Description**

```java
/**
 * @param availTimes Specify a one-time password or a periodic password. Valid values: `0` that specifies a periodic password and `1` that specifies a one-time password.
 */
public void getTempPasswordList(int availTimes, final ITuyaDataCallback<List<TempPasswordBeanV3>> dataCallback);
```

**Parameters**

The following table describes the parameters of the `TempPasswordBeanV3` data model.

| Parameter | Description | Type |
| ------------- | ------------------------------------------------------------ | -------------- |
| effective | The status of the temporary password. Valid values:<br>`1`: pending to take effect<br>`2`: pending to be sent<br>`3`: in use<br>`4`: pending to be deleted<br>`5`: expired | int |
| availTimes | Specify a one-time password or a periodic password. Valid values:<br>`0`: a periodic password<br>`1`: a one-time password | int |
| passwordId | The ID of the temporary password on the service end. | int |
| sn | The serial number of the password in the lock. | int |
| name | The name of the temporary password. | String |
| scheduleBean | The period in which the password is valid. Ignore this parameter for a one-time password. | ScheduleBean |
| phone | The mobile number. This parameter can be set when you subscribe to the SMS service. | String |
| effectiveTime | The 13-digit timestamp when the password takes effect. | long |
| invalidTime | The 13-digit timestamp when the password expires. | long |

The following table describes the parameters of `ScheduleBean`.

| Parameter | Description | Type |
| ------------- | ------------------------------------------------------------ | -------------- |
| allDay | Specifies whether the password is valid for the whole day. When the value is set to `true`, ignore `effectiveTime` and `invalidTime`. | boolean |
| effectiveTime | The number of minutes starting from which the password becomes valid in a day. | int |
| invalidTime | The number of minutes starting from which the password becomes invalid in a day. | int |
| dayOfWeeks | The date on which the password becomes valid in a week. | Set<DayOfWeek> |

**Sample code**

```java
// Get a periodic password.
tuyaLockDevice.getTempPasswordList(0, new ITuyaDataCallback<List<TempPasswordBeanV3>>() {
    @Override
    public void onSuccess(List<TempPasswordBeanV3> result) {
        Log.i(TAG, "getTempPasswordList  onSuccess: " + result);
    }

    @Override
    public void onError(String errorCode, String errorMessage) {
        Log.e(TAG, "getTempPasswordList failed: code = " + errorCode + "  message = " + errorMessage);
    }
});
// Get a one-time password.
tuyaLockDevice.getTempPasswordList(1, new ITuyaDataCallback<List<TempPasswordBeanV3>>() {
    @Override
    public void onSuccess(List<TempPasswordBeanV3> result) {
        Log.i(TAG, "getTempPasswordList  onSuccess: " + result);
    }

    @Override
    public void onError(String errorCode, String errorMessage) {
        Log.e(TAG, "getTempPasswordList failed: code = " + errorCode + "  message = " + errorMessage);
    }
});
```

### Create a temporary password

You can call this operation to create a temporary password. Temporary passwords are classified into one-time passwords and periodic passwords. The following code block shows how to call this operation.

**Description**

```java
/**
 * @param availTimes    Specify a one-time password or a periodic password. Valid values: `0` that specifies a periodic password and `1` that specifies a one-time password.
 * @param name          The name of the temporary password.
 * @param password      The temporary password.
 * @param scheduleBean  The period in which the password is valid. Ignore this parameter for a one-time password.
 * @param phone         The mobile number. This parameter can be set when you subscribe to the SMS service.
 * @param countryCode   The country code. This parameter can be set when you subscribe to the SMS service
 * @param effectiveTime The 13-digit timestamp when the password takes effect.
 * @param invalidTime   The 13-digit timestamp when the password expires.
 */
public void createTempPassword(final int availTimes, final String name, final String password, final ScheduleBean scheduleBean, final String phone, final String countryCode, final long effectiveTime, final long invalidTime);
```

**Parameters**


| Parameter | Description | Required |
| ------------- | --------------------------------------------------------- | -------- |
| availTimes | Specify a one-time password or a periodic password. Valid values:<br>`0`: a periodic password<br>`1`: a one-time password | Yes |
| name | The name of the temporary password. | Yes |
| password | The password. | Yes |
| scheduleBean | The period in which the password is valid. Ignore this parameter for a one-time password. | No |
| phone | The mobile number. This parameter can be set when you subscribe to the SMS service. | No |
| countryCode | The country code. This parameter can be set when you subscribe to the SMS service. | No |
| effectiveTime | The 13-digit timestamp when the password takes effect. | Yes |
| invalidTime | The 13-digit timestamp when the password expires. | Yes |

The following table describes the parameters of `scheduleBean`.

| Parameter | Description | Type |
| ------------- | ------------------------------------------------------------ | -------------- |
| allDay | Specifies whether the password is valid for the whole day. When the value is set to `true`, ignore `effectiveTime` and `invalidTime`. | boolean |
| effectiveTime | The number of minutes starting from which the password becomes valid in a day. | int |
| invalidTime | The number of minutes starting from which the password becomes invalid in a day. | int |
| dayOfWeeks | The date on which the password becomes valid in a week. | Set<DayOfWeek> |

**Sample code**

The following code block shows how to add a one-time password:

```java
tuyaLockDevice.createTempPassword(1, "your_password_name", "111222", null, "", "", System.currentTimeMillis(), System.currentTimeMillis() + 24 * 60 * 60 * 1000);
```

The following code block shows how to add a periodic password:

```java
ScheduleBean scheduleBean = new ScheduleBean();
scheduleBean.allDay = false; // `false` specifies that the password is not valid for the whole day, but valid for some time of the day.
scheduleBean.effectiveTime = 480; // The password takes effect on the 480th minute of the day. The 480th minute means 08:00 (UTC+8).
scheduleBean.invalidTime = 1080; // The password expires on the 1,080th minute of the day. The 1,080th minute means 18:00 (UTC+8).
// Add the number of days for which the password is valid.
scheduleBean.dayOfWeeks.add(ScheduleBean.DayOfWeek.MONDAY);
scheduleBean.dayOfWeeks.add(ScheduleBean.DayOfWeek.TUESDAY);
scheduleBean.dayOfWeeks.add(ScheduleBean.DayOfWeek.WEDNESDAY);
scheduleBean.dayOfWeeks.add(ScheduleBean.DayOfWeek.THURSDAY);
scheduleBean.dayOfWeeks.add(ScheduleBean.DayOfWeek.FRIDAY);
scheduleBean.dayOfWeeks.add(ScheduleBean.DayOfWeek.SATURDAY);
scheduleBean.dayOfWeeks.add(ScheduleBean.DayOfWeek.SUNDAY);
tuyaLockDevice.createTempPassword(0, "your_password_name", "111222", scheduleBean, "", "", System.currentTimeMillis(), System.currentTimeMillis() + 24 * 60 * 60 * 1000);
```

### Modify a temporary password

![](https://images.tuyacn.com/fe-static/docs/img/052dd7af-76ed-4f32-9f0b-8ea0b578c548.png)

**Description**

```java
/**
 * @param availTimes    Specify a one-time password or a periodic password. Valid values: `0` that specifies a periodic password and `1` that specifies a one-time password.
 * @param passwordId    The ID of the temporary password on the service end.
 * @param sn            The serial number of the password in the lock.
 * @param name          The name of the temporary password.
 * @param scheduleBean  The period in which the password is valid. Ignore this parameter for a one-time password.
 * @param phone         The mobile number. This parameter can be set when you  subscribe to the SMS service.
 * @param countryCode   The country code. This parameter can be set when you subscribe to the SMS service
 * @param effectiveTime The 13-digit timestamp when the password takes effect.
 * @param invalidTime   The 13-digit timestamp when the password expires.
 */
public void modifyTempPassword(final int availTimes, int passwordId, int sn, final String name, final ScheduleBean scheduleBean, final String phone, final String countryCode, final long effectiveTime, final long invalidTime);
```

**Parameters**


| Parameter | Description | Required |
| ------------- | --------------------------------------------------------- | -------- |
| availTimes | Specify a one-time password or a periodic password. Valid values:<br>`0`: a periodic password<br> `1`: a one-time password | Yes |
| passwordId | The ID of the temporary password on the service end. | Yes |
| sn | The serial number of the password in the lock. | Yes |
| name | The name of the temporary password. | Yes |
| scheduleBean | The period in which the password is valid. Ignore this parameter for a one-time password. | No |
| phone | The mobile number. This parameter can be set when you subscribe to the SMS service. | No |
| countryCode | The country code. This parameter can be set when you subscribe to the SMS service. | No |
| effectiveTime | The 13-digit timestamp when the password takes effect. | Yes |
| invalidTime | The 13-digit timestamp when the password expires. | Yes |

**Sample code**

The following code block shows how to modify a one-time password:

```java
tuyaLockDevice.modifyTempPassword(1, 2202004, 2, "your_password_name", null, "", "", System.currentTimeMillis(), System.currentTimeMillis() + 24 * 60 * 60 * 1000);
```

The following code block shows how to modify a periodic password:

```java
ScheduleBean scheduleBean = new ScheduleBean();
scheduleBean.allDay = true; // The password is valid for the whole day. You do not need to specify the time when the password takes effect. 
scheduleBean.effectiveTime = 0;
scheduleBean.invalidTime = 0;
scheduleBean.dayOfWeeks.add(ScheduleBean.DayOfWeek.FRIDAY);
tuyaLockDevice.modifyTempPassword(0, 2202005, 3, "your_password_name", scheduleBean, "", "", System.currentTimeMillis(), System.currentTimeMillis() + 24 * 60 * 60 * 1000);
```

### Delete a temporary password

![](https://images.tuyacn.com/fe-static/docs/img/e1a05699-92f6-4407-917b-6299673ba3b8.png)

**Description**

```java
/**
 * @param passwordId The ID of the temporary password on the service end.
 * @param sn         The serial number of the password in the lock.
 */
public void deleteTempPassword(int passwordId, int sn);
```

**Parameters**

| Parameter | Description | Required |
| ------------- | --------------------------------------------------------- | -------- |
| passwordId | The ID of the temporary password on the service end. | Yes |
| sn | The serial number of the password in the lock. | Yes |

**Sample code**

```java
tuyaLockDevice.deleteTempPassword(2202004, 2);
```

## List of Bluetooth lock DPs

| DP name | DP code |
| ---- | ---- |
| Add an unlocking method | unlock_method_create |
| Delete an unlocking method | unlock_method_delete |
| Modify an unlocking method | unlock_method_modify |
| Disable an unlocking method | unlock_method_freeze |
| Enable an unlocking method | unlock_method_enable |
| Unlock a door with Bluetooth | bluetooth_unlock |
| Get feedback for Bluetooth-based unlocking | bluetooth_unlock_fb |
| Remaining battery level | residual_electricity |
| Battery status | battery_state |
| Child lock status | child_lock |
| Status of lifting up and double locking | anti_lock_outside |
| Unlock a door with fingerprints | unlock_fingerprint |
| Unlock a door with common passwords | unlock_password |
| Unlock a door with dynamic passwords | unlock_dynamic |
| Unlock a door with cards | unlock_card |
| Unlock a door with mechanical keys | unlock_key |
| Door opening and closing events | open_close |
| Unlock from the inside of the door | open_inside |
| Records of Bluetooth-based unlocking | unlock_ble |
| Door opened | door_opened |
| Alerts | alarm_lock |
| Duress alerts | hijack |
| Remote door knocking | doorbell |
| SMS notification | message |
| Doorbell ringtone | doorbell_song |
| Doorbell volume | doorbell_volume |
| Lock language switch | language |
| Manage welcome messages on the display screen | welcome_words |
| Key-pressing volume | key_tone |
| Local navigation volume | beep_volume |
| Double locking status | reverse_lock |
| Automatic locking switch | automatic_lock |
| Switch between single unlocking and combination unlocking | unlock_switch |
| Synchronize unlocking methods among members | synch_member |
| Delay setting of automatic locking | auto_lock_time |
| Timed automatic locking | auto_lock_timer |
| Number of times for fingerprint enrollment | finger_input_times |
| Unlock a door with face recognition | unlock_face |
| Open and closed status | closed_opened |
| Unlock a door with irises | unlock_eye |
| Unlock a door with palm prints | unlock_hand |
| Unlock a door with finger veins | unlock_finger_vein |
| Hardware clock RTC | rtc_lock |
| Report of countdown for automatic locking | auto_lock_countdown |
| Manual locking | manual_lock |
| Locking status | lock_motor_state |
| Rotation direction of the motor on a smart stick lock | lock_motor_direction |
| Disable users | unlock_user_freeze |
| Enable users | unlock_user_enable |
| Add a temporary password to a Bluetooth lock | temporary password_creat |
| Delete a temporary password from a Bluetooth lock | temporary password_delete |
| Modify a temporary password of a Bluetooth lock | temporary password_modify |
| Synchronize unlocking methods (large datasets) | synch_method |
| Unlock a door with temporary passwords | unlock_temporary |
| Motor torque | motor_torque |
| Records of combination unlocking | unlock_double |
| Switch of the arm away mode | arming_mode |
| Configure password-free remote unlocking | remote_no_pd_setkey |
| Password-free remote unlocking with keys | remote_no_dp_key |
| Remote unlocking with mobile phones | unlock_phone_remote |
| Remote unlocking with voice | unlock_voice_remote |
| Send the offline password T0 time | password_offline_time |
| Report the clearing of a single offline password | unlock_offline_clear_single |
| Report the clearing of offline passwords | unlock_offline_clear |
| Report offline password-based unlocking | unlock_offline_pd |