Tuya smart lock Android The SDK provides function packaging with smart door lock devices to speed up and simplify the development process of door lock application functions, including the following functions:

* Door lock user system (including lock user management, associated password, etc.)
* Door lock password unlocking (including dynamic password, temporary password management, etc.)
* Door lock usage records (including door lock unlock records, doorbell records, alarm records, etc.)

## Preparation

Tuya lock SDK is based on [Tuya Smart Home SDK](https://developer.tuya.com/en/docs/app-development/android-app-sdk/featureoverview?id=Ka69nt97vtsfu).

Before integrating Tuya Lock SDK, you need to do the following:

* Integrate TuyaHomeSdk (including the application for tuya App ID and App Secret, security image configuration related environment), see [Tuya Smart Home SDK](https://developer.tuya.com/en/docs/app-development/preparation/preparation?id=Ka69nt983bhh5).

* Activation of the door lock device

## Fast integration

### Add dependency

Add the door lock SDK dependency in the dependencies of `build.gradle` in the module layer

```groovy
dependencies {
    ...
   implementation 'com.tuya.smart:tuyasmart-lock-sdk:1.1.2'
}
```

### Permissions

In order to scan and connect Bluetooth devices, you need to add the following permissions to `AndroidManifest.xml`.

```xml
<!-- Required. Allows applications to connect to paired bluetooth devices.  -->
<uses-permission android:name="android.permission.BLUETOOTH" />
<!-- Required. Allows applications to discover and pair bluetooth devices.  -->
<uses-permission android:name="android.permission.BLUETOOTH_ADMIN" />
<!-- Required.  Allows an app to scan bluetooth device.  -->
<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
<!-- Required.  Allows an app to scan bluetooth device.  -->
<uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
<!--  Allows an app to use bluetooth low energy feature  -->
<uses-feature
	android:name="android.hardware.bluetooth_le"
	android:required="false" />
```

You can refer to the [Request App Permissions](https://developer.android.com/training/permissions/requesting?hl=en#make-the-request) on the Android website for dynamic permission acquisition.