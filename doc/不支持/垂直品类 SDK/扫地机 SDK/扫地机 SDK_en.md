Tuya Smart Sweeper SDK provides related interfaces for sweeping robots to help developers obtain functions such as sweeping maps, paths, and historical maps. Tuya Smart Sweeper SDK relies on Tuya Smart SDK, for more information about accessing to the Tuya Smart Sweeper SDK method, see [Tuya Smart Home SDK Integration](https://developer.tuya.com/en/docs/app-development/android-app-sdk/integration/integrated?id=Ka69nt96cw0uj). 

## Overview

At present, the sweeper SDK supports three types of gyro, vision, and laser sweeping robots. For more information, see [Terms](https://developer.tuya.com/en/docs/app-development/android-app-sdk/extension-sdk/sweeper-sdk/wordintroduction?id=Ka6o1ib4wjubv).

- The sweeper SDK provides real-time data channel capability and full data acquisition capability.

- The real-time data of the gyroscope and the visual sweeper are acquired by data flow, and the incremental data of the sweep is reported by the machine in real-time through the MQTT protocol.

- The real-time data of the laser sweeper is a full amount of data. Due to a large amount of data, it is not suitable for MQTT streaming service to transfer data. In fact, the machine uploads the full amount of data (maps and routes) to the OSS server, stores them in bin format files, and notifies the App of the file path on the OSS server through MQTT.

## Preparation for integration

Tuya Smart Sweeper Android SDK relies on the Tuya Smart Home Android SDK, and develop on this basis. Before starting to develop with the SDK, you need to register a developer account, create a product, etc. on the [Tuya Smart IoT Platform](https://iot.tuya.com/), and obtain a key to activate the SDK, see [Tuya Smart Home Android SDK Integration](https://developer.tuya.com/en/docs/app-development/android-app-sdk/integration/integrated?id=Ka69nt96cw0uj).

### Configure the root build.gradle

Add in repositories:

```groovy
maven {
  url "https://maven-other.tuya.com/repository/maven-releases/"
}
```

### Configure the build.gradle

Add the following codes to the build.gradle file.

```groovy
dependencies{
implementation 'com.tuya.smart:sweeper:2.1.0
}
```

### SDK initialization

```java
TuyaHomeSdk.init(appliction); // Home sdk init
TuyaOptimusSdk.init(appliction); //Sweeper SDK init
```

`ITuyaSweeperKitSdk`: provides the data capability entrance of the laser sweeper and the download function of voice packets.

Call method:

```java
ITuyaSweeperKitSdk iTuyaSweeperKitSdk = TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class);
```

## Version change
 
- The sweeper SDK 0.0.1-0.0.10 is separated from TuyaHomeSdk. Development requires the integration of the sweeper SDK and TuyaHomeSdk.
- Sweeper SDK 0.0.11 is fully integrated into TuyaHomeSdk, development only needs to integrate TuyaHomeSdk.
- In order to develop and open the sweeper function more flexibly, the sweeper SDK is from version 0.1.0 and TuyaHomeSdk is from version 3.15.0. The business interface is completely separated from TuyaHomeSdk. The `ITuyaSweeper` interface in TuyaHomeSdk can still be used. Only supports TuyaSweeperKitSDK. Development requires the integration of sweeper SDK and TuyaHomeSdk.

## Impact on three types of sweepers

 - Gyroscopes and visual sweepers use streaming service channels, and the merge and split of the sweeper SDK and TuyaHomeSdk will not affect them. The entrance is in TuyaHomeSdk.
 - The data channel function of the laser sweeper exists in both the sweeper SDK and TuyaHomeSdk.
 - Subsequent new functions only support the sweeper SDK. The first version after the split is 0.1.0, new features include a voice package download function, multi-map management.