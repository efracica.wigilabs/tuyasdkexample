A robot vacuum peer-to-peer (P2P) library provides P2P download channels to get map data from robot vacuums. This can reduce the cost of cloud storage.

:::important
The robot vacuum P2P feature is developed based on the P2P capabilities of the IPC vertical category. To integrate the robot vacuum P2P library, **you must also integrate the latest version of Tuya IPC SDK**.
:::

## Entry to the feature

```java
ITuyaSweeperKitSdk iTuyaSweeperKitSdk = TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class);

ITuyaSweeperP2P tuyaSweeperP2P = iTuyaSweeperKitSdk.getSweeperP2PInstance(devId);
```

## Connect to the robot vacuum P2P service

```java
/**
*
* @param callback The callback of a P2P connection.
*/
void connectDeviceByP2P( SweeperP2PCallback callback);
```

**Parameters**

| Parameter | Description |
| --- | --- |
| callback | The callback of P2P downloading. |


**Example**

```java
tuyaSweeperP2P.connectDeviceByP2P(new SweeperP2PCallback() {
    @Override
    public void onSuccess() {
            // The success callback.
            }

    @Override
    public void onFailure(int i) {
            // The failure callback.
            }
            });
```

## Enable P2P downloading

**API description**

```java
/**
*
* @param downloadType     The method to download data.
* @param callback         The callback that is executed when P2P downloading is enabled.
* @param callback2        The callback of P2P data.
*/
void startObserverSweeperDataByP2P(DownloadType downloadType, SweeperP2PCallback callback, SweeperP2PDataCallback callback2);
```

**Parameters**

| Parameter | Description |
| --- | --- |
| downloadType | The method to download data through P2P connections. Valid values:<ul><li> `DownloadType.P2PDownloadTypeOnce`: The data downloading is stopped after the existing device data is downloaded. New data will not be downloaded.</li><li> `DownloadType.P2PDownloadTypeStill`: Device data is continuously downloaded until the device automatically stops transferring data.</li></ul> |
| callback | The callback that is executed when P2P downloading is enabled. |
| callback2 | The callback that is executed when data is received through P2P connections. |

**Parameters of `SweeperP2PBean`**

| Parameter | Description |
| --- | --- |
| type | The data type. Valid values:<ul><li> `0`: map data</li><li> `1`: cleaning route data</li><li> `3`: navigation route data</li><li> `-1`: error data</li></ul> |
| data | The data downloaded through P2P connections. |

**Example**

```java
tuyaSweeperP2P.startObserverSweeperDataByP2P(DownloadType.P2PDownloadTypeOnce, new SweeperP2PCallback() {
    @Override
    public void onSuccess() {
            // The success callback.
            }

    @Override
    public void onFailure(int i) {
            // The failure callback.
            }
            }, new SweeperP2PDataCallback() {
    @Override
    public void receiveData(int type, @Nullable SweeperP2PBean sweeperP2PBean) {
            // The callback that is executed when data is received through P2P connections. `type` indicates the type of received data.
            }

    @Override
    public void onFailure(int i) {
            // The failure callback.
            }
            });
```

## Disable P2P downloading

```java
/**
*
* @param callback         The callback that is executed when P2P downloading is disabled.
*/
void stopObserverSweeperDataByP2P(SweeperP2PCallback callback);
```

**Parameters**

| Parameter | Description |
| --- | --- |
| callback | The callback that is executed when P2P downloading is disabled. |

**Example**

```java
tuyaSweeperP2P.stopObserverSweeperDataByP2P(new SweeperP2PCallback() {
    @Override
    public void onSuccess() {
            // The success callback.
            }

    @Override
    public void onFailure(int i) {
            // The success callback.
            }
            });
```

## Disable the P2P service

```java
/**
*
* @param callback         The callback that is executed when P2P downloading is disabled.
*/
void onDestroyP2P();
```

**Example**
```java
tuyaSweeperP2P.onDestroyP2P();
```

## Error codes

The following table lists the error codes of the robot vacuum P2P service.

| Error code | Description |
| --- | --- |
| -9000 | The data of `deviceBean` is missing. |
| -9001 | No dependency is configured for `ipcSdk`. |
| -9002 | A P2P connection is being created. |
| -9003 | The device goes offline. |
| -9004 | Timeout has occurred. |
| -9005 | No map files are found. |

For more information about error codes, see the [error codes of the IPC SDK](https://developer.tuya.com/en/docs/app-development/errorcode?id=Ka6nxw2k97l8a).

## FAQ

- **Problem**: The `SweeperP2PCallback` API method or the `SweeperP2PCallback` class is marked in red and cannot be found. The `arr` file of the dependency library is found. But the following error message is returned:

   ```java
   	// This class file was compiled with different version of Kotlin compiler and can't be decompiled.
   	// Current compiler ABI version is 1.1.16
   	// File ABI version is 1.5.1
   ```

- **Solution**: Try to update the Kotlin version of Android Studio at **Preferences** > **Languages & Framework** > **Kotlin**.