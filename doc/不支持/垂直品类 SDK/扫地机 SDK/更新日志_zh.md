## v3.0.2

- 发布日期：2022年07月29日
- 新增变更:
    * 修复一些已知问题

## v3.0.1

- 发布日期：2022年07月12日
- 新增变更:
    * 增加p2p状态查询接口

## v3.0.0

- 发布日期：2022年06月24日
- 新增变更:
    * P2P 数据通道增加下载过程中的异常回调
    * 云存储支持 Azure

## v2.2.1

- 发布日期：2021年11月11日
- 新增变更:

    * 支持扫地机通过P2P方式拉取数据


## v2.1.0

- 发布日期：2021年06月28日
- 新增变更:

    * 支持扫地机多地图命名
    * 查看历史地图，返回值带地图名称

## v0.1.0

- 新增变更:
   * Sweeper SDK 从 TuyaHomeSdk 拆分
   * 新增语音包下载功能
   * 激光扫地机新增多地图管理接口

## v0.0.10

 - 新增变更
   * 清空历史记录

## v0.0.9

 - 缺陷修复
  * 修复华为部分机型AP配网的问题

## v0.0.8

- 缺陷修复
  * 修复bug

## v0.0.7

- 缺陷修复
  * 清扫历史记录,支持共享成员查看

## v0.0.6

 - 新增变更
   * 清扫历史记录,会根据所属家庭的纬度区分

## v0.0.5

- 新增变更
   * 删除历史接口(deleteSweeperHistoryData)
   * 查询当前地图和路径path接口 (getSweeperCurrentPath)
   * getSweeperByteData 回调里面onSuccess的数据接口->(byte[])
   * startConnectSweeperDataChannel，startConnectSweeperByteDataChannel 回调里增加devId
   * getCloudFileUrl 返回的是一个完整url(包含https://)

## v0.0.4
- 新增变更
   * 数据通道直接返回byte[]

## v0.0.3

- 缺陷修复
   * 解决了onDpUpdate()重复回调的问题

## v0.0.2

- 缺陷修复
   * 历史记录数据接口返回值改变