Laser sweeper data is divided into real-time data and record data. Both types of data include map data and route data, which are stored on the Tuya IoT in the form of files.

- The maps and paths of real-time data are stored in different files, and the maps and paths of record data are stored in the same file. The map and path data are split and read according to the specified rules.

- It is recommended that all interfaces be called after the success of `initCloudConfig`, and the validity period of the configuration information will be updated within half an hour after` updateCloudConfig`.

## Sweeper data acquisition

### Data flow

![image.png](https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/goat/20210114/affd1ca64b3e4dff9e0bde5585c25698.png)

### Interface entrance

Different entrances, same functional interface

- Earlier version entrance: `ITuyaSweeper`

	```java
	ITuyaSweeper  iTuyaSweeper = TuyaHomeSdk.getSweeperInstance()
	```

- Later version entrance: `ITuyaSweeperKit`

	```java
	ITuyaSweeperKitSdk iTuyaSweeperKitSdk = TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class);

	ITuyaSweeperKit iTuyaSweeperKit = iTuyaSweeperKitSdk.getSweeperInstance()
	```

### Initialize cloud configuration

**Interface Description**

- Earlier version (version before 0.1.0): support
- Later version (version after 0.1.0): support

Get bucket information from file storage

```java
/**
 *
 * @param devId	 device id
 * @param callback
 */
void initCloudConfig(String devId, ITuyaCloudConfigCallback callback);
```

**Sample code**

```java
//Earlier version
TuyaHomeSdk.getSweeperInstance().initCloudConfig("xxx", new ITuyaCloudConfigCallback() {
	@Override
	public void onConfigSuccess(String bucket) {

	}

	@Override
	public void onConfigError(String errorCode, String errorMessage) {

	}
});

//Later version
TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class).getSweeperInstance().initCloudConfig("xxx", new ITuyaCloudConfigCallback() {
	@Override
	public void onConfigSuccess(String bucket) {

	}

	@Override
	public void onConfigError(String errorCode, String errorMessage) {

	}
});
```

### Update cloud configuration

**Interface Description**

- Earlier version (version before 0.1.0): support
- Later version (version after 0.1.0): support

Get the latest bucket information

Due to the timeliness of the obtained file address, when the file address becomes invalid, you need to call this interface to update the cloud configuration

```java
/**
 *
 * @param devId	device id
 * @param callback
 */
void updateCloudConfig(String devId, ITuyaCloudConfigCallback callback);
```

**Sample code**

```java
//Earlier version
TuyaHomeSdk.getSweeperInstance().updateCloudConfig(deviceId, new ITuyaCloudConfigCallback() {
	@Override
	public void onConfigSuccess(String bucket) {

	}

	@Override
	public void onConfigError(String errorCode, String errorMsg) {

	}
});
//Later version
TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class).getSweeperInstance().updateCloudConfig("", new ITuyaCloudConfigCallback() {
	@Override
	public void onConfigSuccess(String bucket) {

	}

	@Override
	public void onConfigError(String errorCode, String errorMessage) {

	}
});
```

### Register or close the real-time data channel (return file relative path)

**Interface Description**

- Earlier version (version before 0.1.0): support
- Later version (version after 0.1.0): support

When the cloud configuration is initialized successfully, you can open the real-time data channel to obtain real-time data

```java
void startConnectSweeperDataChannel(ITuyaSweeperDataListener listener);

void stopConnectSweeperDataChannel();
```

SweeperDataBean data analysis

| Fields | Type | Description |
| --- | --- | --- |
| mapType | int | 0 means map, 1 means path|
| mapPath | String | Route path or map path|

**Sample code**

```java
//Earlier version
TuyaHomeSdk.getSweeperInstance().startConnectSweeperDataChannel(new ITuyaSweeperDataListener() {
	@Override
	public void onSweeperDataReceived(SweeperDataBean sweeperDataBean) {

	}
});

//Later version
TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class).getSweeperInstance().startConnectSweeperDataChannel(new ITuyaSweeperDataListener() {
	@Override
	public void onSweeperDataReceived(SweeperDataBean bean) {

	}
});
```

### Register or close the real-time data channel (return byte array)

**Interface Description**

- Earlier version (version before 0.1.0): support
- Later version (version after 0.1.0): support

When the cloud configuration is initialized successfully, you can open the real-time data channel to obtain real-time data

```java
/**
 *
 * @param listener
 */
void startConnectSweeperByteDataChannel(ITuyaSweeperByteDataListener listener);

/**
 * Close the real-time data channel
 */
void stopConnectSweeperByteDataChannel();
```

SweeperByteData data analysis

| Fields | Type | Description |
| --- | --- | --- |
| type | int | 0 means map, 1 means path|
| data | byte[] | data content|
| devId |String | device id|

**Sample code**

```java
//Earlier version
TuyaHomeSdk.getSweeperInstance().startConnectSweeperByteDataChannel(new ITuyaSweeperByteDataListener() {
	@Override
	public void onSweeperByteData(SweeperByteData data) {

	}

	@Override
	public void onFailure(int code, String msg) {

	}
});
//Later version
TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class).getSweeperInstance().startConnectSweeperByteDataChannel(new ITuyaSweeperByteDataListener() {
	@Override
	public void onSweeperByteData(SweeperByteData data) {

	}

	@Override
	public void onFailure(int code, String msg) {

	}
});
```

### Get full file URL

**Interface Description**

- Earlier version (version before 0.1.0): support
- Later version (version after 0.1.0): support

  Get the complete path of the map file on the OSS server, you can download and analyze it yourself

```java
/**
 *
 * @param bucket File storage bucket
 * @param path   File relative path (startConnectSweeperDataChannel)
 */
String getCloudFileUrl(String bucket, String path);
```

**Sample code**

```java
//Earlier version
TuyaHomeSdk.getSweeperInstance().getCloudFileUrl("bucket","path");
//Later version
TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class).getSweeperInstance().getCloudFileUrl("bucket","path");

```

### Get data content of cloud storage

**Interface Description**

- Earlier version (version before 0.1.0): support
- Later version (version after 0.1.0): support

  When obtaining record data, you can directly call this interface to read the file content in the cloud

```java

/**
 *
 * @param bucket
 * @param path
 * @param listener
 */
void getSweeperByteData(String bucket, String path, ITuyaByteDataListener listener);
```

**Sample code**

```java
//Earlier version
TuyaHomeSdk.getSweeperInstance().getSweeperByteData("bucket", "path", new ITuyaByteDataListener() {
	@Override
	public void onSweeperByteData(byte[] data) {

	}

	@Override
	public void onFailure(int code, String msg) {

	}
});
//Later version
   TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class).getSweeperInstance().getSweeperByteData("bucket", "path", new ITuyaByteDataListener() {
	@Override
	public void onSweeperByteData(byte[] data) {

	}

	@Override
	public void onFailure(int code, String msg) {

	}
});
```

### Get current cleaning data

**Interface Description**

- Earlier version (version before 0.1.0): support
- Later version (version after 0.1.0): support

```java
/**
 * Get real-time map storage path and route storage path
 * @param devId	device id
 * @param callback
 */
void getSweeperCurrentPath(String devId,ITuyaResultCallback<SweeperPathBean> callback);
```

#### SweeperPathBean fields information

| Fields | Type | Description |
| --- | --- | --- |
| mapPath | String | map path|
| routePath | String | route path|

**Sample code**

```java
//Earlier version
TuyaHomeSdk.getSweeperInstance().getSweeperCurrentPath("devId", new ITuyaResultCallback<SweeperPathBean>() {
	@Override
	public void onSuccess(SweeperPathBean result) {

	}

	@Override
	public void onError(String errorCode, String errorMessage) {

	}
});
//Later version
TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class).getSweeperInstance().getSweeperCurrentPath("devId", new ITuyaResultCallback<SweeperPathBean>() {
	@Override
	public void onSuccess(SweeperPathBean result) {

	}

	@Override
	public void onError(String errorCode, String errorMessage) {

	}
});

```

### Get history sweep records

**Interface Description**

- Earlier version (version before 0.1.0): support
- Later version (version after 0.1.0): support

```java

/**
 *
 * @param devId	device id
 * @param limit	The number of data obtained at a time (it is recommended not to exceed 100)
 * @param offset   Get data offset (for paging)
 * @param callback
 */
void getSweeperHistoryData(String devId, int limit, int offset,ITuyaResultCallback<SweeperHistory> callback);

/**
 *
 * @param devId	 device id
 * @param limit	 The number of data obtained at a time (it is recommended not to exceed 100)
 * @param offset	Get data offset (for paging)
 * @param startTime Start timestamp
 * @param endTime   End timestamp
 * @param callback
 */
void getSweeperHistoryData(String devId, int limit, int offset, long startTime, long endTime,ITuyaResultCallback<SweeperHistory> callback);
```

#### SweeperHistoryFields information

| Fields | Type | Description |
| --- | --- | --- |
| datas | List<SweeperHistoryBean> | Historical data list|
| totalCount | int | total count |

#### SweeperHistoryBean fields information

| Fields | Type | Description |
| --- | --- | --- |
| id | String | map id|
| time | long | File upload timestamp |
| bucket | String | File storage bucket|
| file | String | file path |
| extend | String |Extended fields|

**extend**

extend is an extension fields can be transparently transmitted with the device (for example: {"map_id": 123, "layout_size": 4589, "route_size": 1024}

layout_size indicates the size of the map file, that is, the size of the map data in the file

route_size indicates the size of the path file, that is, the size of the path data in the file

When reading record data files, read map data and route data according to layout_size and route_size

**Sample code**

```java
//Earlier version
TuyaHomeSdk.getSweeperInstance().getSweeperHistoryData("devId", 10, 0, new ITuyaResultCallback<SweeperHistory>() {
	@Override
	public void onSuccess(SweeperHistory result) {

	}

	@Override
	public void onError(String errorCode, String errorMessage) {

	}
});
//Later version
TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class).getSweeperInstance().getSweeperHistoryData("devId", 10, 0, new ITuyaResultCallback<SweeperHistory>() {
	@Override
	public void onSuccess(SweeperHistory result) {

	}

	@Override
	public void onError(String errorCode, String errorMessage) {

	}
});
```

### Delete history sweep records

**Interface Description**

- Earlier version (version before 0.1.0): support
- Later version (version after 0.1.0): support

  Delete cloud log and data stored in OSS server

```java
 /**
 * @param devId	  device id
 * @param fileIdList History id collection
 * @param callback
 */
void deleteSweeperHistoryData(String devId, List<String> fileIdList, final ITuyaDelHistoryCallback callback);
```

**Sample code**

```java
List<String> list = new ArrayList<>();
list.add("10");
list.add("11");
//Earlier version		TuyaHomeSdk.getSweeperInstance().deleteSweeperHistoryData("devId", list, new ITuyaDelHistoryCallback() {
	@Override
	public void onSuccess() {

	}

	@Override
	public void onError(String errorCode, String errorMessage) {

	}
});
//Later version		TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class).getSweeperInstance().deleteSweeperHistoryData("devId", list, new ITuyaDelHistoryCallback() {
	@Override
	public void onSuccess() {

	}

	@Override
	public void onError(String errorCode, String errorMessage) {

	}
});

```

### Clear history sweep records

**Interface Description**

- Earlier version (version before 0.1.0): support
- Later version (version after 0.1.0): support

  Delete all records

```java
 /**
 * @param devId	 device id
 * @param callback
 */
void deleteAllHistoryData(String devId,final ITuyaDelHistoryCallback callback);
```

**Sample code**

```java
//Earlier version
TuyaHomeSdk.getSweeperInstance().deleteAllHistoryData("devId", new ITuyaDelHistoryCallback() {
	@Override
	public void onSuccess() {

	}

	@Override
	public void onError(String errorCode, String errorMessage) {

	}
});

//Later version
TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class).getSweeperInstance().deleteAllHistoryData("devId", new ITuyaDelHistoryCallback() {
	@Override
	public void onSuccess() {

	}

	@Override
	public void onError(String errorCode, String errorMessage) {

	}
});
```

### Get multi-floor map data

**Interface Description**

- Earlier version (Before 0.1.0): Not supported
- Later version (version after 0.1.0): support

```java
/**
 * Get sweeper records (multi-map)
 * @param devId	device id
 * @param limit	The number of data obtained at a time (it is recommended not to exceed 100)
 * @param offset   Get data offset (for paging)
 * @param callback
 */
void getSweeperMultiMapHistoryData(String devId, int limit, int offset,
								   ITuyaResultCallback<SweeperHistory> callback);

/**
 * Get sweeper records (multi-map)
 * @param devId	 device id
 * @param limit	 The number of data obtained at a time (it is recommended not to exceed 100)
 * @param offset	Get data offset (for paging)
 * @param startTime start timestamp
 * @param endTime   end timestamp
 * @param callback
 */
void getSweeperMultiMapHistoryData(String devId, int limit, int offset, long startTime, long endTime,
								   ITuyaResultCallback<SweeperHistory> callback);
```

**Sample code**

```java
TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class).getSweeperInstance().getSweeperMultiMapHistoryData("devId", 20, 0, new ITuyaResultCallback<SweeperHistory>() {
	@Override
	public void onSuccess(SweeperHistory result) {

	}

	@Override
	public void onError(String errorCode, String errorMessage) {

	}
});
```

## Update Multi-category Map File Name

For the case where the laser sweeper sweeps multiple maps in one sweep, it provides the protocol of update file name for multiple maps

```java
    /**
     * Update Multi-category Map File Name
     * @param devId device id
     * @param id map id
     * @param name map name
     * @param callback callback
     */
    void sweeperFileNameUpdateWithDevId(String devId, long id, String name,final ITuyaSweeperNameUpdateCallback callback);

```


**Sample code**


```java
TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class).getSweeperInstance().sweeperFileNameUpdateWithDevId("devId",id,"name", new ITuyaSweeperNameUpdateCallback() {
    @Override
    public void onNameUpdate(boolean result){
        
    }

    @Override
    public void void onFailure(String code, String msg){

    }
});

```

### Clear multi-floor map data

**Interface Description**

- Earlier version (Before 0.1.0): Not supported
- Later version (version after 0.1.0): support

```java
/**
 * Delete all records (applicable to multiple maps)
 * @param devId
 * @param callback
 */
void deleteAllMultiMapHistoryData(String devId, ITuyaDelHistoryCallback callback);
```

**Sample code**

```java
TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class).getSweeperInstance().deleteAllMultiMapHistoryData("devId", new ITuyaDelHistoryCallback() {
	@Override
	public void onSuccess() {

	}

	@Override
	public void onError(String errorCode, String errorMessage) {

	}
});
```