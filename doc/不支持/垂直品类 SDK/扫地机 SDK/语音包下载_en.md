
## Data flow
![download](images/mqtt_saodiji.png)

**Note**
- The development platform cannot find the configuration voice package entry. You need to submit the information to the project manager or business person responsible for your project.

## Function Introduction

The sweeper SDK provides functions such as downloading voice packets and monitoring progress.

Function entrance :

```java
ITuyaSweeperKitSdk iTuyaSweeperKitSdk = TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class);

ITuyaSweeperFileDownload iTuyaSweeperFileDownload = iTuyaSweeperKitSdk.newFileDownloadInstance(devId);

```

## Get Voice Package List

**Interface Description**

  - Old version (version before 0.1.0): Not supported
  - New version (version after 0.1.0): support

  ```java
  void getFileList(final ITuyaResultCallback<ArrayList<SweeperFileListInfoBean>> callback);
  
  ```
  
### SweeperFileListInfoBean Data structure information
  
|  Fields | Type | Description |
| --- | --- | --- |
| id | long | Voice file id|
| name | String | Voice file name|
| desc | String |Voice file description|
| auditionUrl | String | Test of the voice file url|
| officialUrl | String | Official voice file url|
| imgUrl | String | Voice file icon url |
| region | List<String> | region code |

**Sample Code**

```java
ITuyaSweeperKitSdk iTuyaSweeperKitSdk = TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class);

ITuyaSweeperFileDownload iTuyaSweeperFileDownload = iTuyaSweeperKitSdk.newFileDownloadInstance(devId);

iTuyaSweeperFileDownload.getFileList(new ITuyaResultCallback<ArrayList<SweeperFileListInfoBean>>() {
    @Override
    public void onSuccess(ArrayList<SweeperFileListInfoBean> result) {
        
    }

    @Override
    public void onError(String errorCode, String errorMessage) {

    }
});

```
---

## Registration and Unregistration Download Monitoring


**Interface Description**

  - Old version (version before 0.1.0): Not supported
  - New version (version after 0.1.0): support

```java
void registerDownloadListener(final ISweeperFileDownloadListener listener)

void unRegisterDownloadListener();

```

ISweeperFileDownloadListener 

```java

/**
 *
 * @param type file Type，Voice file is DOWNLOAD_VOICE
 * @param fileDownloadEnum Enumeration, including download completion and download failure
 */
void onResultStatus(String type, SweeperFileDownloadEnum fileDownloadEnum);

/**
 *
 * @param type
 * @param progress Download progress
 */
void onProgress(String type, int progress);

```

**Sample Code**

```java

ITuyaSweeperKitSdk iTuyaSweeperKitSdk = TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class);

ITuyaSweeperFileDownload iTuyaSweeperFileDownload = iTuyaSweeperKitSdk.newFileDownloadInstance(devId);


iTuyaSweeperFileDownload.registerDownloadListener(new ISweeperFileDownloadListener() {
    @Override
    public void onResultStatus(String type, SweeperFileDownloadEnum fileDownloadEnum) {
        
    }

    @Override
    public void onProgress(String type, int progress) {

    }
});
iTuyaSweeperFileDownload.unRegisterDownloadListener();

```

---

## Confirm Download Voice File


**Interface Description**

  - Old version (version before 0.1.0): Not supported
  - New version (version after 0.1.0): support

```java
/**
 * 
 * @param fileId   Voicie file id
 * @param callback
 */
void confirmDownload(long fileId, final ITuyaResultCallback<Integer> callback);

```

**Sample Code**

```java
ITuyaSweeperKitSdk iTuyaSweeperKitSdk = TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class);

ITuyaSweeperFileDownload iTuyaSweeperFileDownload = iTuyaSweeperKitSdk.newFileDownloadInstance(devId);

iTuyaSweeperFileDownload.confirmDownload(1000, new ITuyaResultCallback<Integer>() {
    @Override
    public void onSuccess(Integer result) {
        
    }

    @Override
    public void onError(String errorCode, String errorMessage) {

    }
});

```

---

## Get Voice Package Download Progress
**Interface Description**

  - Old version (version before 0.1.0): Not supported
  - New version (version after 0.1.0): support

```java
void queryProgress(final ITuyaResultCallback<SweeperProgressbean> callback);

```

SweeperProgressbean：

| Fields | Type | Description |
| --- | --- | --- |
| id | long | Voice file id |
| rate | int | Download progress |
| status | int | Status（0：Not downloaded 1：Downloading）|

**Sample Code**

```java

ITuyaSweeperKitSdk iTuyaSweeperKitSdk = TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class);

ITuyaSweeperFileDownload iTuyaSweeperFileDownload = iTuyaSweeperKitSdk.newFileDownloadInstance(devId);

iTuyaSweeperFileDownload.queryProgress(new ITuyaResultCallback<SweeperProgressbean>() {
    @Override
    public void onSuccess(SweeperProgressbean result) {
        
    }

    @Override
    public void onError(String errorCode, String errorMessage) {

    }
});

```

## Destroy

**Interface Description**

  - Old version (version before 0.1.0): Not supported
  - New version (version after 0.1.0): support

When exiting the download, destroy the download function

```java
 void onDestroy();

```
