
**注意**
- 开发平台找不到配置语音包入口，需要将资料提交给负责贵司项目的项目经理或商务。

扫地机SDK提供语音包下载，监听进度等功能。

功能入口 :

```java
ITuyaSweeperKitSdk iTuyaSweeperKitSdk = TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class);

ITuyaSweeperFileDownload iTuyaSweeperFileDownload = iTuyaSweeperKitSdk.newFileDownloadInstance(devId);

```

## 数据流程：
![download](images/mqtt_saodiji.png)

## 获取语音文件列表

**接口说明**

  - 老版本（0.1.0之前版本） ：不支持
  - 新版本（0.1.0以后版本） ：支持

  ```java
  void getFileList(final ITuyaResultCallback<ArrayList<SweeperFileListInfoBean>> callback);
  
  ```
  
### SweeperFileListInfoBean数据结构信息
  
|  字段 | 类型 | 描述 |
| --- | --- | --- |
| id | long | 语音文件 id|
| name | String | 语音文件名称|
| desc | String |语音文件描述|
| auditionUrl | String | 试听语音文件url|
| officialUrl | String | 正式语音文件url|
| imgUrl | String | 语音文件图标url |
| region | List<String> | 区域码 |

**代码示例**

```java
ITuyaSweeperKitSdk iTuyaSweeperKitSdk = TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class);

ITuyaSweeperFileDownload iTuyaSweeperFileDownload = iTuyaSweeperKitSdk.newFileDownloadInstance(devId);

iTuyaSweeperFileDownload.getFileList(new ITuyaResultCallback<ArrayList<SweeperFileListInfoBean>>() {
    @Override
    public void onSuccess(ArrayList<SweeperFileListInfoBean> result) {
        
    }

    @Override
    public void onError(String errorCode, String errorMessage) {

    }
});

```
---

## 注册和反注册下载进度监听


**接口说明**

  - 老版本（0.1.0之前版本） ：不支持
  - 新版本（0.1.0以后版本） ：支持

```java
void registerDownloadListener(final ISweeperFileDownloadListener listener)

void unRegisterDownloadListener();

```

ISweeperFileDownloadListener 

```java

/**
 *
 * @param type 文件类型，语音文件是DOWNLOAD_VOICE
 * @param fileDownloadEnum 枚举，包含下载完成、下载失败
 */
void onResultStatus(String type, SweeperFileDownloadEnum fileDownloadEnum);

/**
 *
 * @param type
 * @param progress 下载进度
 */
void onProgress(String type, int progress);

```

**代码示例**

```java

ITuyaSweeperKitSdk iTuyaSweeperKitSdk = TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class);

ITuyaSweeperFileDownload iTuyaSweeperFileDownload = iTuyaSweeperKitSdk.newFileDownloadInstance(devId);


iTuyaSweeperFileDownload.registerDownloadListener(new ISweeperFileDownloadListener() {
    @Override
    public void onResultStatus(String type, SweeperFileDownloadEnum fileDownloadEnum) {
        
    }

    @Override
    public void onProgress(String type, int progress) {

    }
});
iTuyaSweeperFileDownload.unRegisterDownloadListener();

```

---

##确认下载语音文件


**接口说明**

  - 老版本（0.1.0之前版本） ：不支持
  - 新版本（0.1.0以后版本） ：支持

```java
/**
 * 
 * @param fileId 语音文件id
 * @param callback
 */
void confirmDownload(long fileId, final ITuyaResultCallback<Integer> callback);

```

**代码示例**

```java

ITuyaSweeperKitSdk iTuyaSweeperKitSdk = TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class);

ITuyaSweeperFileDownload iTuyaSweeperFileDownload = iTuyaSweeperKitSdk.newFileDownloadInstance(devId);

iTuyaSweeperFileDownload.confirmDownload(1000, new ITuyaResultCallback<Integer>() {
    @Override
    public void onSuccess(Integer result) {
        
    }

    @Override
    public void onError(String errorCode, String errorMessage) {

    }
});

```

---

##获取语音文件下载进度
**接口说明**

  - 老版本（0.1.0之前版本） ：不支持
  - 新版本（0.1.0以后版本） ：支持

```java
void queryProgress(final ITuyaResultCallback<SweeperProgressbean> callback);

```

SweeperProgressbean结构如下：

| 字段 | 类型 | 描述 |
| --- | --- | --- |
| id | long | 语音文件 id |
| rate | int | 下载进度 |
| status | int | 状态（0：未下载 1：下载中）|

**代码示例**

```java

ITuyaSweeperKitSdk iTuyaSweeperKitSdk = TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class);

ITuyaSweeperFileDownload iTuyaSweeperFileDownload = iTuyaSweeperKitSdk.newFileDownloadInstance(devId);

iTuyaSweeperFileDownload.queryProgress(new ITuyaResultCallback<SweeperProgressbean>() {
    @Override
    public void onSuccess(SweeperProgressbean result) {
        
    }

    @Override
    public void onError(String errorCode, String errorMessage) {

    }
});

```

##销毁

**接口说明**

  - 老版本（0.1.0之前版本） ：不支持
  - 新版本（0.1.0以后版本） ：支持

退出下载时，要销毁下载功能

```java
 void onDestroy();

```

