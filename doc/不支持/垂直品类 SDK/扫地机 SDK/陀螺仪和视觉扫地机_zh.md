扫地机 SDK 提供 HTTP 接口请求和实时通道两种数据获取方式。获取、删除历史记录等数据需要请求 HTTP 接口，扫地机清扫中传递的数据可以通过注册实时通道监听获取。

## 通用接口

HTTPS 类接口采用 TuyaHomeSdk 的通过接口调用，主要传入接口名称，接口版本号，入参等。

**入口**：`TuyaHomeSdk.getRequestInstance()`

```java
public interface ITuyaSmartRequest {

	/**
     *
     * @param apiName  接口名称
     * @param version  接口版本号
     * @param postData 发送数据
     * @param callback 接口回调
     * @param object 接收回调数据的数据结构，例如相应的javabean
     */
    <T> void requestWithApiName(String apiName, String version, Map<String, Object> postData, Class<T> object, final ITuyaDataCallback<T> callback);

    <T> void requestWithApiNameWithoutSession(String apiName, String version, Map<String, Object> postData, Class<T> object, final ITuyaDataCallback<T> callback);

    void onDestroy();
}
```

### 最新一次清扫记录

请求参数：

* apiName：tuya.m.device.media.latest

* version：2.0

* postData：

    ```json
    {
    "devId" : "xxx",
    "start":"",
    "size" : 500
    }
    ```

**返回示例**

```json
 {
    "devId" : "xxx",
    "startRow" : "mtnva58ee6817b605ddcc6_35_1535629239586",
    "dataList" : [ "373702373700", "383702383802373901383901383800"],
    "subRecordId" : 35,
    "hasNext" : true,
    "startTime" : 1535629049,
    "endTime" : 1535629244,
    "status" : 2
}
```

### 获取历史记录列表

请求参数：

* apiName：tuya.m.sweeper.cleaning.history.get
* version：1.0
* postData：

    | 字段 | 类型 | 描述 |
    | --- | --- | --- |
    | devId | String | 设备ID |
    | offset | Integer | 偏移值 |
    | limit | Integer | 分页数量|
    | startTime	 | Long  | 开始时间 |
    | endTime	 | Long  | 结束时间 |

**返回示例**：

```json
{
    "recordId":"162992AAXKaZCdL2tDvVcWYecT9AA9630150",
    "gid":38498424,
    "dpId":15,
    "gmtCreate":1629929630203,
    "value":"20210826052504804100145",
    "uuid":"ecc8c633ef1d9ede"
}
```

| 字段 | 类型 | 描述 |
| --- | --- | --- |
| recordId | String | 地图的唯一id                   |
| gid | int | 家庭组id |
| dpId | int | 在涂鸦平台配置的清扫记录的dpId   |
| gmtCreate | long | 创建时间 |
| value | String | 地图信息, 解析subRecordId |
| uuid | String | 设备唯一id |

### 历史记录详情信息

请求参数：

* apiName：tuya.m.device.media.detail

* version：2.0

* postData：

```json
{
  "devId" : "xxx",
  "subRecordId" : 31,
  "start":"",
  "size" : 500
}
```

| 字段 | 类型 | 描述 |
|:-- |:-- |:-- |
| devId | String | 设备 id |
| subRecordId | Integer | 清扫记录 id,通过解析历史记录列表的value数据，根据数据长度使用不同的解析规则，规则见下表 |
| start | String | 第一次传空串，后面取下一页时填上一页返回值里的 startRow 值|
| size | Integer | 请求数据大小 |

**value解析协议**  

| 记录时间（长度12位） | 清扫时间（长度3位） | 清扫面积（长度3位） |  subRecordId（长度5位） | 举例 | 说明 |  
|:-- |:-- |:-- |  :-- |:-- |:-- |  
|有 | 有| 有 | 有 |20200319202500300200123 |1、取MCU上报的时间在面板中显示<br>2、点击对应记录可以显示地图|  
|没有 | 有 |有 | 有 |00300200123 |1、取DP点上报的云端时间戳时间在面板中显示<br>2、点击对应记录可以显示地图|  
|有 | 有 |有 | 没有 |202003192025003002 |1、取MCU上报的时间在面板中显示<br>2、点击对应记录不可以显示地图|  
|没有 | 有 |有 | 没有 |003002 |1、取DP点上报的云端时间戳时间在面板中显示<br>2、点击对应记录不可以显示地图|  

**返回示例**

```json
{
    "devId" : "xxx",
    "startRow" : "mtnva58ee6817b605ddcc6_31_1535622776561",
    "dataList" : ["3e3f02403e013e3f00", "3f3f024040013f3f00"],
    "subRecordId" : 31,
    "hasNext" : true,
    "startTime" : 1535621566,
    "endTime" : 1535623017,
    "status" : 2
  }
```

| 字段 | 类型 | 描述 |
| --- | --- | --- |
| startRow | String | 下次请求传入的 Start 值 |
| subRecordId | Integer | 清扫记录 id|
| startTime | String | 开始时间|
| endTime | String  | 结束时间 |
| hasNext | Boolean | 是否有下一页|


### 删除历史记录（最多100条）

请求参数：

* apiName：tuya.m.sweeper.cleaning.history.delete
* version：1.0
* postData：

    ```json
    {
    "devId" : "xxx",
    "uuid":"15607600058B81A6C4A0273FDD61091D0B02403848501,
    15607600058B81A6C4A0273FDD61091D0B0240384850"、
    }
    ```

* tip: uuid是清扫记录唯一标识,不是设备uuid, 是历史记录的recordId	

**返回示例**

```json
 {
     "result":true,
     "success":true,
     "status":"ok",
     "t":1557740732829
 }
```

## 实时数据通道

注册实时数据通道监听设备数据流上报，返回数据是 byte 数组形式，根据机器协议解析成可用数据，如x、y坐标、颜色值等。

### 通道流程

![扫地机增量数据获取](https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/goat/20201230/07c9f290165348c3bfe55f9ee52eb727.png)

**入口**：`TuyaHomeSdk.getTransferInstance();`

### 订阅地图流数据

```java
void registerTransferDataListener(ITuyaDataCallback<TransferDataBean> callback);
```

**代码示例**

```java
TuyaHomeSdk.getTransferInstance().registerTransferDataListener(new ITuyaDataCallback<TransferDataBean>() {
    @Override
    public void onSuccess(TransferDataBean result) {
        
    }

    @Override
    public void onError(String errorCode, String errorMessage) {

    }
});
```

 ***  TransferDataBean ***

| 字段 | 类型 | 描述 |  
| --- | --- | --- |  
| data |  byte[] | 0~3位数据为地图的subRecordId，4~12位数据非业务数据，不参与解析。注意从13位数据开始为地图实际数据，每3位数据为一个地图点，分别为地图的x坐标、y坐标和地图点的点类型 |  
| devId |  String | 设备id |  

### 取消订阅地图流数据

```java
void unRegisterTransferDataListener(ITuyaDataCallback<TransferDataBean> callback);
```

**代码示例**

```java
TuyaHomeSdk.getTransferInstance().unRegisterTransferDataListener(this);
```