The SDK provides the HTTPS interface request and real-time channel to obtain data. To acquire and delete historical data, you need to request the HTTPS interface. The data transferred during the sweeping of the sweeper can be obtained by monitoring the real-time channel registration.

## HTTPS interface

The HTTPS interface uses TuyaHomeSdk's call through the interface, mainly passing in the interface name, interface version number, and input parameters.

**Entrance**: `TuyaHomeSdk.getRequestInstance()`


```java
public interface ITuyaSmartRequest {

	/**
     *
     * @param apiName  API name
     * @param version  API version
     * @param postData params to post
     * @param callback 
     * @param object Data structure to receive callback data, such as the corresponding JavaBean     
     */
    <T> void requestWithApiName(String apiName, String version, Map<String, Object> postData, Class<T> object, final ITuyaDataCallback<T> callback);

    <T> void requestWithApiNameWithoutSession(String apiName, String version, Map<String, Object> postData, Class<T> object, final ITuyaDataCallback<T> callback);

    void onDestroy();
}

```

### Latest record

Request parameters:

* apiName: tuya.m.device.media.latest

* version: 2.0

* postData:

    ```json
    {
    "devId" : "xxx",
    "start":"",
    "size" : 500
    }
    ```

**Response example**

```json
 {
    "devId" : "xxx",
    "startRow" : "mtnva58ee6817b605ddcc6_35_1535629239586",
    "dataList" : [ "373702373700", "383702383802373901383901383800"],
    "subRecordId" : 35,
    "hasNext" : true,
    "startTime" : 1535629049,
    "endTime" : 1535629244,
    "status" : 2
}
```

### Get record list

Request parameters: 

* apiName: tuya.m.sweeper.cleaning.history.get

* version: 1.0

* postData:

    | Fields | Type | Description |
    | --- | --- | --- |
    | devId | String | Device ID. |
    | offset | int | Offset value. |
    | limit | int | Number of pages. |
    | startTime	 | Long  | Start time |
    | endTime	 | Long  | End time |

**Response example**: 

```json
{
    "recordId":"162992AAXKaZCdL2tDvVcWYecT9AA9630150",
    "gid":38498424,
    "dpId":15,
    "gmtCreate":1629929630203,
    "value":"20210826052504804100145",
    "uuid":"ecc8c633ef1d9ede"
}
```

| Fields | Type | Description |
| --- | --- | --- |
| recordId | String | Map Id              |
| gid | int | Home Id |
| dpId | int | Dp Id for cleaning records configured on the Tuya Iot platform |
| gmtCreate | long | Created time |
| value | String | Map information, parse subRecordId |
| uuid | String | Device Id |

### Get record information detail

Request parameters:

* apiName: tuya.m.device.media.detail

* version: 2.0

* postData:

    ```json
    {
    "devId" : "xxx",
    "subRecordId" : 31,
    "start":"",
    "size" : 500
    }
    ```

| Fields | Type | Description |
|:-- |:-- |:-- |
| devId | String | Device id |
| subRecordId | int | clean record id ,by parsing the value data in the history list,different parsing rules are used depending on the length of the data,the rules are shown in the table below|
| start | String | Pass the empty string for the first time, and fill the startRow value in the return value of the previous page when the next page is taken later|
| size | int | Request data size |


**value parsing protocol**  

| Recording time(The length of the 12) | Cleaning time(The length of the 3) | Clean the area (The length of the 3)|  subRecordId (The length of the 5)| example | instructions |  
|:-- |:-- |:-- |  :-- |:-- |:-- |  
|yes | yes| yes | yes |20200319202500300200123 |1、Take the time reported by MCU and display it in the panel<br>2、Click the corresponding record to display the map|  
|no | yes |yes | yes |00300200123 |1、Take the cloud timestamp time reported by DP point and display it in the panel<br>2、Click the corresponding record to display the map|  
|yes | yes |yes | no |202003192025003002 |1、Take the time reported by MCU and display it in the panel<br>2、The map cannot be displayed by clicking the corresponding record|  
|no | yes |yes | no |003002 |1、Take the cloud timestamp time reported by DP point and display it in the panel<br>2、The map cannot be displayed by clicking the corresponding record|  

**Response example**

```json
{
    "devId" : "xxx",
    "startRow" : "mtnva58ee6817b605ddcc6_31_1535622776561",
    "dataList" : ["3e3f02403e013e3f00", "3f3f024040013f3f00"],
    "subRecordId" : 31,
    "hasNext" : true,
    "startTime" : 1535621566,
    "endTime" : 1535623017,
    "status" : 2
  }
```

| Fields | Type | Description |
| --- | --- | --- |
| startRow | String | Start value passed in next request |
| subRecordId | int | clean record id |
| startTime | long | start time,timestamp|
| endTime | long  | end time,timestamp |
| hasNext | boolean | Is there a next page|



### Delete record(up to 100 records)
Request parameters:

* apiName: tuya.m.sweeper.cleaning.history.delete
* version: 1.0
* postData:

    ```json
    {
    "devId" : "xxx",
    "uuid":"15607600058B81A6C4A0273FDD61091D0B02403848501,
    15607600058B81A6C4A0273FDD61091D0B0240384850"
    }
    ```
  
* TIP: The UUID is the unique identifier of the cleaning record, not the device UUID, but the recordId of the historical record

**Response example**

```json
 {
       "result":true,
       "success":true,
       "status":"ok",
       "t":1557740732829
   }
```

## Data transfer subscribe

Register the real-time data channel to monitor the device data stream report, and the returned data is in the form of a byte array, which is parsed into available data according to the machine protocol, such as x, y coordinates, color value, etc.

### Data flow

![sweeper_incremental_en.png](https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/goat/20201230/9933138f65bc4fe6998755678a1a73ec.png)

**Entrance**: `TuyaHomeSdk.getTransferInstance();`

### Subscribe map stream data

```java
void registerTransferDataListener(ITuyaDataCallback<TransferDataBean> callback);
```

**Sample code**

```java
TuyaHomeSdk.getTransferInstance().registerTransferDataListener(new ITuyaDataCallback<TransferDataBean>() {
    @Override
    public void onSuccess(TransferDataBean result) {
        
    }

    @Override
    public void onError(String errorCode, String errorMessage) {

    }
});
```

 ***  TransferDataBean ***
 
| Fields | Type | Description |
| --- | --- | --- |  
| data |  byte[] | The 0-3 bits of data are the subrecordID of the map，4~12 bits of data are non-business data and will not be parsed. Note that starting from 13 bits of data is the actual data of the map, and every 3 bits of data is a map point, which are the x coordinate, y coordinate and point type of the map points respectively |  
| devId |  String | device id |  
  

### Unsubscribe map stream data

```java
void unRegisterTransferDataListener(ITuyaDataCallback<TransferDataBean> callback);
```

**Sample code**

```java
TuyaHomeSdk.getTransferInstance().unRegisterTransferDataListener(this);
```