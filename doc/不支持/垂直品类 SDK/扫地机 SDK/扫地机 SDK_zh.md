涂鸦智能扫地机 SDK 提供了扫地机器人相关接口，帮助开发者获取清扫地图、路径及历史地图等功能。涂鸦智能扫地机 SDK 依托于涂鸦全屋智能 SDK，更多详情，请参考 [涂鸦全屋智能 SDK 集成](https://developer.tuya.com/cn/docs/app-development/android-app-sdk/integration/integrated?id=Ka69nt96cw0uj)。

## 功能概述

目前扫地机SDK支持陀螺仪、视觉、激光三种类型扫地机器人，详情请参考 [名词解释](https://developer.tuya.com/cn/docs/app-development/android-app-sdk/extension-sdk/sweeper-sdk/wordintroduction?id=Ka6o1ib4wjubv)。

- 扫地机SDK提供实时数据的通道能力及全量数据获取能力。

- 陀螺仪和视觉扫地机实时数据采用数据流的方式获取，通过 MQTT 协议机器实时上报清扫的增量数据。

- 激光扫地机实时数据是全量数据，由于数据量大，不适用 MQTT 流服务传递数据。实际是激光扫地机把清扫的全量数据（地图和路线）上传到涂鸦 IoT 平台，以二进制文件存储，并通过 MQTT 通知 App 服务器上的文件存储路径。 

## 准备工作

涂鸦智能 Android 扫地机 SDK 依赖于涂鸦智能 Home SDK，基于此基础上进行拓展开发，在开始开发前，您需要在 [涂鸦智能 IoT 平台](https://iot.tuya.com/) 上注册账号、创建扫地机产品等，并获取到激活 SDK 的密钥，具体的操作流程请参考 [涂鸦全屋智能 SDK 集成](https://developer.tuya.com/cn/docs/app-development/android-app-sdk/integration/integrated?id=Ka69nt96cw0uj)。

## 快速集成

### 配置 build.gradle 

build.gradle `repositories` 增加以下配置：

```groovy
maven {
  url "https://maven-other.tuya.com/repository/maven-releases/"
}
```

### 模块 build.gradle 配置

build.gradle 文件里添加集成准备中下载的 `dependencies` 依赖库。

```groovy

dependencies{
implementation 'com.tuya.smart:sweeper:3.0.2'
}
```

### SDK 初始化

```java
TuyaHomeSdk.init(appliction); //home sdk init
TuyaOptimusSdk.init(appliction); // sweeper sdk init
```

`ITuyaSweeperKitSdk` 提供了激光扫地机数据能力入口和语音包下载功能，调用方式：

```java
ITuyaSweeperKitSdk iTuyaSweeperKitSdk = TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class);
```

## 版本变更
 
- 扫地机 SDK 0.0.1-0.0.10 与 TuyaHomeSdk 分离状态，开发需要集成扫地机 SDK 和 TuyaHomeSdk。
- 扫地机 SDK 0.0.11 完全合入 TuyaHomeSdk ，开发只需要集成 TuyaHomeSdk。
- 为了更灵活的开发和开放扫地机功能，扫地机 SDK 自0.1.0版本，TuyaHomeSdk 自3.15.0-beta3版本，业务接口与TuyaHomeSdk 完全分离，TuyaHomeSdk 中的 `ITuyaSweeper` 接口仍旧可以继续使用，后续扫地机新功能将只支持 TuyaSweeperKitSDK 开发需要集成扫地机 SDK 和 TuyaHomeSdk。

## 对三种类型扫地机影响

- 陀螺仪和视觉扫地机使用流服务通道，扫地机 SDK 和 TuyaHomeSdk 的合并与拆分均不对其产生影响，入口在 TuyaHomeSdk 中。
- 激光扫地机数据通道功能同时存在于 扫地机 SDK 和 TuyaHomeSdk。
- 后续新功能只支持扫地机 SDK。拆分后首个版本0.1.0，新功能包含语音包下载功能，多地图管理。