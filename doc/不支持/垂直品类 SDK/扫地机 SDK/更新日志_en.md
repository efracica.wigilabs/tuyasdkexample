## v 2.2.0

- Release date: 2021.11.10
- New updates
	* Support sweeper to pull data through P2P.


## v2.1.0

- Release date: 2021.06.28
- New updates
  * Support sweeper multi-map naming
  * View historical map, return the map name


## v0.1.0

- New updates
  * Sweeper SDK split from TuyaHomeSdk
  * Added voice package download function
  * Multi-map management interface added to laser sweeper


## v0.0.10

- New updates
  * Clear history


## v0.0.9

- Bugfix
  * Fix the problem of AP distribution network of some Huawei models
 

## v0.0.8

 - Bugfix
   * fix bug

## v0.0.7

- New updates
 * Sweep history records support shared members

## v0.0.6

- New updates
 * Cleaning history will be divided according to the family

## v0.0.5

- New updates
 * Delete sweeper history interface
 * Query the current map and path path interface(getSweeperCurrentPath)
 * getSweeperByteData return byte array
 * startConnectSweeperDataChannel，startConnectSweeperByteDataChannel add devId param in callback
 * getCloudFileUrl Returns a complete url

 
## v0.0.4

- New updates
 * The data channel directly returns byte []

## v0.0.3

- Bugfix
 * Solved the problem of onDpUpdate() repeated callback

## v0.0.2

- New updates
 * The return value of the historical data interface changes