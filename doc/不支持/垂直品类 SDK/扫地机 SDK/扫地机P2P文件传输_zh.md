扫地机 P2P（Peer-to-Peer）能力提供 P2P 下载通道来获取扫地机上的地图和清扫路线数据。由于数据直接来源于设备点对点的直传，所以无需将实时地图与数据上传至云存储服务器再做下载，从而达到数据传输速度更快，更节省云存储成本的目的。

:::important 扫地机 P2P 功能是基于垂直品类 IPC 的 P2P 能力开发，所以接入扫地机 P2P 库时，您需要同时接入涂鸦 [P2P SDK](https://developer.tuya.com/cn/docs/app-development/android-p2p?id=Kalemnh8lf8yi) 的最新版本。 :::



## 功能入口

```java
ITuyaSweeperKitSdk iTuyaSweeperKitSdk = TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class);

ITuyaSweeperP2P tuyaSweeperP2P = iTuyaSweeperKitSdk.getSweeperP2PInstance(devId);
```

## 连接 P2P 服务

**接口说明**

```java
/**
*
* @param callback  P2P 连接回调
*/
void connectDeviceByP2P( SweeperP2PCallback callback);
```

**参数说明**

| 参数     | 说明                  |
| -------- | --------------------- |
| callback | 连接 P2P 数据下载回调 |

**代码示例**

```java
tuyaSweeperP2P.connectDeviceByP2P(new SweeperP2PCallback() {
    @Override
    public void onSuccess() {
            //成功回调
            }

    @Override
    public void onFailure(int i) {
            //失败回调
            }
            });
```

## 开启 P2P 数据下载

**接口说明**

保证在connectDeviceByP2P回调成功后，开启文件传输。传输过程中发生通道异常断开时会有failure的错误回调，回调错误码可参考末尾错误码。发生错误回调后说明设备可能出现网络异常，可重新调用connectDeviceByP2P接口尝试重连设备。

```java
/**
*
* @param downloadType     下载数据方式
* @param callback         开启 P2P 数据下载回调
* @param callback2        P2P 数据回调
*/
void startObserverSweeperDataByP2P(DownloadType downloadType, SweeperP2PCallback callback, SweeperP2PDataCallback callback2);
```

**参数说明**

| 参数         | 说明                                                         |
| ------------ | ------------------------------------------------------------ |
| downloadType | P2P 数据交换方式： <ul><li> `DownloadType.P2PDownloadTypeOnce`：下载完当前设备的数据就会停止后续产生数据的下载 </li><li> `DownloadType.P2PDownloadTypeStill`：持续下载设备产生的数据，直到设备自动停止发送数据 </li></ul> |
| callback     | 开启 P2P 数据下载回调                                        |
| callback2    | P2P 数据接收回调                                             |

**`SweeperP2PBean` 说明**

| 参数 | 说明                                                         |
| ---- | ------------------------------------------------------------ |
| type | 数据类型： <ul><li> 0：地图数据 </li><li> 1：清扫路径数据 </li><li> 3：导航路径数据 </li><li> -1：错误数据 </li></ul> |
| data | P2P 数据                                                     |

**代码示例**

```java
tuyaSweeperP2P.startObserverSweeperDataByP2P(DownloadType.P2PDownloadTypeOnce, new SweeperP2PCallback() {
    @Override
    public void onSuccess() {
            //开启 P2P 下载成功回调
            }

    @Override
    public void onFailure(int i) {
            //开启 P2P 下载失败回调
            }
            }, new SweeperP2PDataCallback() {
    @Override
    public void receiveData(int type, @Nullable SweeperP2PBean sweeperP2PBean) {
            //接受 P2P 数据回调,type 表示接收到的数据类型
            }

    @Override
    public void onFailure(int i) {
            //接受 P2P 数据失败回调
            }
            });
```

## 关闭 P2P 数据下载

暂停p2p数据传输，不断开p2p通道，可重新使用startObserverSweeperDataByP2P接口继续下载。

```java
/**
*
* @param callback         关闭 P2P 数据下载回调
*/
void stopObserverSweeperDataByP2P(SweeperP2PCallback callback);
```

**参数说明**

| 参数     | 说明                  |
| -------- | --------------------- |
| callback | 关闭 P2P 数据下载回调 |

**代码示例**

```java
tuyaSweeperP2P.stopObserverSweeperDataByP2P(new SweeperP2PCallback() {
    @Override
    public void onSuccess() {
            //关闭 P2P 下载成功回调
            }

    @Override
    public void onFailure(int i) {
            //关闭 P2P 下载成功回调
            }
            });
```

## 查询P2P连接状态

```java

/**
* 检查P2P的连接状态
*/
boolan checkP2PActive();
```

**代码示例**

```java
tuyaSweeperP2P.checkP2PActive();
```


## 关闭 P2P 服务

停止下载，并销毁通道，释放占用的内存资源和线程资源。

```java
/**
*
* @param callback         关闭 P2P 数据下载回调
*/
void onDestroyP2P();
```

**代码示例**

```java
tuyaSweeperP2P.onDestroyP2P();
```

## 错误码

| 错误码 | 说明              |
| ------ | ----------------- |
| -9000  | `deviceBean` 丢失 |
| -9001  | 未集成 P2P SDK    |
| -9002  | 正在连接 P2P      |
| -9003  | 设备离线          |
| -9004  | 超时              |
| -9005  | 无地图文件        |
| -9006  | P2P连接断开        |

更多错误码见垂直品类 [IPC SDK 错误码](https://developer.tuya.com/cn/docs/app-development/errorcode?id=Ka6nxw2k97l8a)。

## 常见问题

- **问题现象**：`SweeperP2PCallback` 或 `SweeperP2PCallback` 类标红，提示找不到，找到依赖库的 `arr`，但提示类似于：

  ```java
  // This class file was compiled with different version of Kotlin compiler and can't be decompiled.
  // Current compiler ABI version is 1.1.16
  // File ABI version is 1.5.1
  ```

- **解决方法**：可以尝试更新 Android Studio 内的 Kotlin 版本。更新路径为 **Preferences** -> **Languages & Framework** -> **Kotlin**。
