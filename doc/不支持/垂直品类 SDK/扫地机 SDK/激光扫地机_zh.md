激光扫地机数据分为实时数据和历史记录数据。这两种数据都包含了地图数据和路径数据，以文件的形式存储在涂鸦 IoT 平台<!--OSS-->上。

- 实时数据的地图和路径是分别存储在不同文件内的，历史数据的地图和路径是存储在同一个文件内的，根据指定规则进行拆分读取地图和路径数据。注意2.2.1以后的版本将会下架OSS实时数据功能，请使用P2P获取实时数据。

- 建议所有接口在`initCloudConfig`成功之后调用，配置信息的有效期半个小时失败后`updateCloudConfig`。

## 获取扫地机数据

激光扫地机实时通道的流程如下图所示：

![image.png](https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/goat/20210114/d9965ab7ed70475f91bee21d9c1ec53a.png)

## 扫地机新旧版本入口

新旧版本入口不同，功能接口相同。

- 旧版本入口 `ITuyaSweeper`：

    ```java
    ITuyaSweeper  iTuyaSweeper = TuyaHomeSdk.getSweeperInstance()
    ``` 
    
- 新版本入口 `ITuyaSweeperKit`

    ```java
    ITuyaSweeperKitSdk iTuyaSweeperKitSdk = TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class);
    
    ITuyaSweeperKit iTuyaSweeperKit = iTuyaSweeperKitSdk.getSweeperInstance()
    ```
 
## 初始化云存储配置

- 旧版本（0.1.0之前版本） ：支持
- 新版本（0.1.0以后版本） ：支持
 
获取文件存储的Bucket信息。
 
```java
/**
*
* @param devId     设备ID
* @param callback  
*/
void initCloudConfig(String devId, ITuyaCloudConfigCallback callback);
```

**示例代码**

```java
//旧版本
TuyaHomeSdk.getSweeperInstance().initCloudConfig("xxx", new ITuyaCloudConfigCallback() {
    @Override
    public void onConfigSuccess(String bucket) {
        
    }

    @Override
    public void onConfigError(String errorCode, String errorMessage) {

    }
});

//新版本
TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class).getSweeperInstance().initCloudConfig("xxx", new ITuyaCloudConfigCallback() {
    @Override
    public void onConfigSuccess(String bucket) {
        
    }

    @Override
    public void onConfigError(String errorCode, String errorMessage) {

    }
});
```

## 更新云存储配置

- 旧版本（0.1.0之前版本） ：支持
- 新版本（0.1.0以后版本） ：支持

获取最新Bucket信息。

由于获取到的文件地址有时效性，当文件地址失效时，需要调用以下接口更新云配置。

```java
/**
 *
 * @param devId    设备ID
 * @param callback
 */
void updateCloudConfig(String devId, ITuyaCloudConfigCallback callback);
```

**示例代码**

```java 
//旧版本
TuyaHomeSdk.getSweeperInstance().updateCloudConfig(deviceId, new ITuyaCloudConfigCallback() {
    @Override
    public void onConfigSuccess(String bucket) {
        
    }

    @Override
    public void onConfigError(String errorCode, String errorMsg) {
        
    }
});
//新版本
TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class).getSweeperInstance().updateCloudConfig("", new ITuyaCloudConfigCallback() {
    @Override
    public void onConfigSuccess(String bucket) {
        
    }

    @Override
    public void onConfigError(String errorCode, String errorMessage) {

    }
});
```

## 注册或关闭实时数据通道(返回文件相对路径)

2.2.1以后的版本将会下架此功能，请使用P2P获取实时数据。


当云配置初始化成功后，即可开启实时数据通道来获取实时数据。

```java
/**
 * 开启扫地机数据通道，获取到的是url
 *
 * @param listener
 */
void startConnectSweeperDataChannel(ITuyaSweeperDataListener listener);

/**
 * 关闭实时数据通道
 */
void stopConnectSweeperDataChannel();
```
回调数据结构 SweeperDataBean

| 字段 | 类型 | 描述 |
| --- | --- | --- |
| mapType | int | 0表示地图，1表示路径|
| mapPath | String | 文件路径或者地图路径|

**示例代码**

```java
//旧版本
TuyaHomeSdk.getSweeperInstance().startConnectSweeperDataChannel(new ITuyaSweeperDataListener() {
    @Override
    public void onSweeperDataReceived(SweeperDataBean sweeperDataBean) {
               
    }
});

//新版本
TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class).getSweeperInstance().startConnectSweeperDataChannel(new ITuyaSweeperDataListener() {
    @Override
    public void onSweeperDataReceived(SweeperDataBean bean) {
        
    }
});
```

## 注册或关闭实时数据通道(返回byte数组)

2.2.1以后的版本将会下架此功能，请使用P2P获取实时数据。

当云配置初始化成功后，即可开启实时 数据通道来获取实时数据。

```java
/**
 * 开启实时数据通道，获取到的是byte[]
 *
 * @param listener
 */
void startConnectSweeperByteDataChannel(ITuyaSweeperByteDataListener listener);

/**
 * 关闭实时数据通道
 */
void stopConnectSweeperByteDataChannel();
```

回调数据结构 SweeperByteData

| 字段 | 类型 | 描述 |
| --- | --- | --- |
| type | int | 0表示地图，1表示路径|
| data | byte[] | 数据内容|
| devId |String | 设备ID|

**示例代码**

```java
//旧版本
TuyaHomeSdk.getSweeperInstance().startConnectSweeperByteDataChannel(new ITuyaSweeperByteDataListener() {
    @Override
    public void onSweeperByteData(SweeperByteData data) {
        
    }

    @Override
    public void onFailure(int code, String msg) {

    }
});
//新版本
TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class).getSweeperInstance().startConnectSweeperByteDataChannel(new ITuyaSweeperByteDataListener() {
    @Override
    public void onSweeperByteData(SweeperByteData data) {
        
    }

    @Override
    public void onFailure(int code, String msg) {

    }
});
```

## 获取完整文件地址

- 旧版本（0.1.0之前版本） ：支持
- 新版本（0.1.0以后版本） ：支持

获取地图文件在<!-- OSS -->服务器上完整的路径，可自行下载解析。
  
```java
/**
 *
 * @param bucket 文件存储的bucket
 * @param path   文件相对路径（startConnectSweeperDataChannel）
 */
String getCloudFileUrl(String bucket, String path);
```

**代码示例**

```java
//旧版本
TuyaHomeSdk.getSweeperInstance().getCloudFileUrl("bucket","path");
//新版本
TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class).getSweeperInstance().getCloudFileUrl("bucket","path");
```

## 获取数据内容

- 旧版本（0.1.0之前版本） ：支持
- 新版本（0.1.0以后版本） ：支持

获取历史数据时，可以直接调用该接口读取云端的文件内容。
  
```java

/**
 * 获取数据内容（byte[]）
 *
 * @param bucket
 * @param path
 * @param listener
 */
void getSweeperByteData(String bucket, String path, ITuyaByteDataListener listener);
```

**代码示例**

```java
//旧版本        
TuyaHomeSdk.getSweeperInstance().getSweeperByteData("bucket", "path", new ITuyaByteDataListener() {
    @Override
    public void onSweeperByteData(byte[] data) {
        
    }

    @Override
    public void onFailure(int code, String msg) {

    }
});
//新版本 
   TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class).getSweeperInstance().getSweeperByteData("bucket", "path", new ITuyaByteDataListener() {
    @Override
    public void onSweeperByteData(byte[] data) {
        
    }

    @Override
    public void onFailure(int code, String msg) {

    }
});
```

## 获取当前清扫数据

- 旧版本（0.1.0之前版本） ：支持
- 新版本（0.1.0以后版本） ：支持

    ```java
    /**
        * 获取实时的地图存储路径和路线存储路径
        * @param devId    设备 id
        * @param callback
        */
        void getSweeperCurrentPath(String devId,ITuyaResultCallback<SweeperPathBean> callback);
    ```

`SweeperPathBean`字段说明：

| 字段 | 类型 | 描述 |
| --- | --- | --- |
| mapPath | String | 地图 path|
| routePath | String | 路径 path|

**代码示例**

```java
//旧版本
TuyaHomeSdk.getSweeperInstance().getSweeperCurrentPath("devId", new ITuyaResultCallback<SweeperPathBean>() {
    @Override
    public void onSuccess(SweeperPathBean result) {
        
    }

    @Override
    public void onError(String errorCode, String errorMessage) {

    }
});
//新版本
TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class).getSweeperInstance().getSweeperCurrentPath("devId", new ITuyaResultCallback<SweeperPathBean>() {
    @Override
    public void onSuccess(SweeperPathBean result) {
        
    }

    @Override
    public void onError(String errorCode, String errorMessage) {

    }
});
```

## 获取历史清扫记录

- 旧版本（0.1.0之前版本） ：支持
- 新版本（0.1.0以后版本） ：支持

```java
/**
 *
 * @param devId    设备 id
 * @param limit    一次获取数据的数量(建议最大不要超过100)
 * @param offset   获取数据的偏移量(用于分页)
 * @param callback
 */
void getSweeperHistoryData(String devId, int limit, int offset,ITuyaResultCallback<SweeperHistory> callback);

/**
 *
 * @param devId     设备 id
 * @param limit     一次获取数据的数量(建议最大不要超过100)
 * @param offset    获取数据的偏移量(用于分页)
 * @param startTime 起始时间戳
 * @param endTime   结束时间戳
 * @param callback
 */
void getSweeperHistoryData(String devId, int limit, int offset, long startTime, long endTime,ITuyaResultCallback<SweeperHistory> callback);
```

### 回调数据结构 `SweeperHistory` 字段

| 字段 | 类型 | 描述 |
| --- | --- | --- |
| datas | List<SweeperHistoryBean> | 历史数据列表|
| totalCount | int | 数据总量 |

### `SweeperHistoryBean` 字段

| 字段 | 类型 | 描述 |
| --- | --- | --- |
| id | String | 地图 id|
| time | long | 文件上传时间戳 |
| bucket | String | 文件存储的 bucket|
| file | String | 文件路径 |
| extend | String |扩展字段|

>**说明**：extend是一个扩展字段可以和设备透传。例如：`{"map_id":123, "layout_size":4589, "route_size":1024}`。
>- layout_size表示地图文件数据大小。
>- route_size表示路径文件数据大小。

读取历史数据的文件时根据layout_size和route_size分别读取地图数据和路径数据。

**代码示例**

```java
//旧版本   
 TuyaHomeSdk.getSweeperInstance().getSweeperHistoryData("devId", 10, 0, new ITuyaResultCallback<SweeperHistory>() {
    @Override
    public void onSuccess(SweeperHistory result) {
        
    }

    @Override
    public void onError(String errorCode, String errorMessage) {

    }
});
//新版本 
   TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class).getSweeperInstance().getSweeperHistoryData("devId", 10, 0, new ITuyaResultCallback<SweeperHistory>() {
    @Override
    public void onSuccess(SweeperHistory result) {
        
    }

    @Override
    public void onError(String errorCode, String errorMessage) {

    }
});
```

## 删除历史清扫记录

- 旧版本（0.1.0之前版本）：支持
- 新版本（0.1.0以后版本）：支持

删除云端历史记录以及存储<!--在oss-->的数据。

```java
/**
* @param devId      设备 id
* @param fileIdList 历史记录 ID 集合
* @param callback  
*/
void deleteSweeperHistoryData(String devId, List<String> fileIdList, final ITuyaDelHistoryCallback callback);
```

**代码示例**

```java
List<String> list = new ArrayList<>();
list.add("10");
list.add("11");
//旧版本        
	TuyaHomeSdk.getSweeperInstance().deleteSweeperHistoryData("devId", list, new 				 	ITuyaDelHistoryCallback() {
    @Override
    public void onSuccess() {
        
    }

    @Override
    public void onError(String errorCode, String errorMessage) {

    }
});
//新版本        		
TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class).getSweeperInstance().deleteSweeperHistoryData("devId", list, new ITuyaDelHistoryCallback() {
    @Override
    public void onSuccess() {
        
    }

    @Override
    public void onError(String errorCode, String errorMessage) {

    }
});
```


## 获取多楼层地图数据

- 旧版本（0.1.0之前版本） ：不支持
- 新版本（0.1.0以后版本） ：支持

```java
/**
 * 获取扫地机历史记录(多地图)
 * @param devId    设备ID
 * @param limit    一次获取数据的数量(建议最大不要超过100)
 * @param offset   获取数据的偏移量(用于分页)
 * @param callback
 */
void getSweeperMultiMapHistoryData(String devId, int limit, int offset,
                                   ITuyaResultCallback<SweeperHistory> callback);
                                   
/**
 * 获取扫地机历史记录 （适用于多地图）
 * @param devId     设备ID
 * @param limit     一次获取数据的数量(建议最大不要超过100)
 * @param offset    获取数据的偏移量(用于分页)
 * @param startTime 起始时间戳
 * @param endTime   结束时间戳
 * @param callback
 */
void getSweeperMultiMapHistoryData(String devId, int limit, int offset, long startTime, long endTime,
                                   ITuyaResultCallback<SweeperHistory> callback);                                   
```

**代码示例**

```java
TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class).getSweeperInstance().getSweeperMultiMapHistoryData("devId", 20, 0, new ITuyaResultCallback<SweeperHistory>() {
    @Override
    public void onSuccess(SweeperHistory result) {
        
    }

    @Override
    public void onError(String errorCode, String errorMessage) {

    }
});
```

## 多楼层地图命名

针对激光型扫地机一次清扫记录有多个地图的情况，提供多楼层地图命名的接口。

```java
    /**
     * 多楼层地图命名
     * @param devId 设备id
     * @param id 地图id
     * @param name 地图名称
     * @param callback 命名回调
     */
    void sweeperFileNameUpdateWithDevId(String devId, long id, String name,final ITuyaSweeperNameUpdateCallback callback);
```

**代码示例**
```java
TuyaOptimusSdk.getManager(ITuyaSweeperKitSdk.class).getSweeperInstance().sweeperFileNameUpdateWithDevId("devId",id,"name", new ITuyaSweeperNameUpdateCallback() {
    @Override
    public void onNameUpdate(boolean result){
        
    }

    @Override
    public void void onFailure(String code, String msg){

    }
});

```

<!-- 
## 其他

[oss错误分析]( https://www.alibabacloud.com/help/zh/doc-detail/32005.html) -->