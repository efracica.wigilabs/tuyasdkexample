照明设备开发较为复杂，因为同时存在v1和v2新旧两种固件。即使使用了标准指令，您也需要开发两套控制逻辑。因此对照明设备功能进行封装，封装了灯具设备的开关、工作模式切换、亮度控制、冷暖控制、彩光控制和四种情景模式的控制。

## Demo App

请前往 GitHub 查看 [Lighting SDK Demo](https://github.com/tuya/tuya-lighting-android-sdk)。

参考[Home SDK Demo App](https://developer.tuya.com/cn/docs/app-development/android-app-sdk/featureoverview?id=Ka69nt97vtsfu)的简介，将 `BundleId`、 `AppKey`、`AppSecret`、`安全图片`配置正确。

Lighting SDK Demo App 在 Home SDK Demo App 的功能基础上扩展了接入照明设备相关功能的接口封装，加速开发过程。主要包括了以下功能：

- 获取当前设备是几路灯
- 获取灯所有功能点的值
- 开灯或关灯
- 切换工作模式
- 控制灯的亮度
- 控制灯的色温
- 切换场景模式
- 设置彩灯的颜色

## 依赖说明

```kotlin
implementation 'com.tuya.smart:tuyasmart:3.22.0'
// 控制SDK依赖
implementation 'com.tuya.smart:tuyasmart-centralcontrol:1.0.3'
```

需要注意的是，`tuyasmart-centralcontrol`使用了 Kotlin 编译，需要引入 Kotlin 库确保其正常使用。

> **说明**：如果您的项目中已引入 Kotlin，可忽略下面的配置。

**Kotlin 依赖接入**

1. 在根目录的`build.gradle`中引入Kotlin插件的依赖。

	```kotlin
	buildscript {
		ext.kotlin_version = '1.3.72'
		dependencies {
			...
			classpath "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version"
		}
	}
	```

2. 在app的`build.gradle`中引入Kotlin插件和Kotlin包。

	```kotlin
	apply plugin: 'kotlin-android'
	dependencies {
		implementation "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlin_version"
	}
	```