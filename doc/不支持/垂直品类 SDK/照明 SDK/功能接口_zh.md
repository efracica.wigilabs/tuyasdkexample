# 功能说明
## 设备初始化

首先，创建`ITuyaLightDevice`对象，灯相关的方法均封装在此方法中。

```kotlin
ITuyaLightDevice lightDevice = new TuyaLightDevice(String devId);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| devId | 设备 ID |

**示例代码**

```kotlin
// 创建lightDevice
ITuyaLightDevice lightDevice = new TuyaLightDevice("vdevo159793004250542");
```

该对象封装了灯的所有 DP，包括控制指令的下发和上报。

## 设备代理监听

**方法说明**

```kotlin
/**
 * 注册监听
 */
void registerLightListener(ILightListener listener);
```

其中，`ILightListener`回调如下：

```kotlin
public interface ILightListener {
    /**
     * 监听照明设备dp点变化
     *
     * @param dataPoint 该灯具所有dp点的状态
     */
    void onDpUpdate(LightDataPoint dataPoint);

    /**
     * 设备移除
     */
    void onRemoved();

    /**
     * 设备上下线
     */
    void onStatusChanged(boolean online);

    /**
     * 网络状态
     */
    void onNetworkStatusChanged(boolean status);

    /**
     * 设备信息更新例如name之类的
     */
    void onDevInfoUpdate();
}
```

**示例代码**

```kotlin
// 创建lightDevice
ITuyaLightDevice lightDevice = new TuyaLightDevice("vdevo159793004250542");

// 注册监听
lightDevice.registerLightListener(new ILightListener() {
    @Override
    public void onDpUpdate(LightDataPoint dataPoint) { // 返回LightDataPoint，包含灯所有功能点的值
        Log.i("test_light", "onDpUpdate:" + dataPoint);
    }

    @Override
    public void onRemoved() {
        Log.i("test_light", "onRemoved");
    }

    @Override
    public void onStatusChanged(boolean status) {
        Log.i("test_light", "onDpUpdate:" + status);
    }

    @Override
    public void onNetworkStatusChanged(boolean status) {
        Log.i("test_light", "onDpUpdate:" + status);
    }

    @Override
    public void onDevInfoUpdate() {
        Log.i("test_light", "onDevInfoUpdate:");
    }
});
```

**`TuyaSmartLightDataPointModel` 数据模型**

| 字段 | 类型 | 描述 |
| ---- | ---- | ---- |
| powerSwitch | boolea | 开关是否打开，YES代表开，NO代表关 |
| workMode | LightMode | 工作模式，MODE_WHITE为白光模式； MODE_COLOUR为彩光模式； MODE_SCENE为情景模式； |
| brightness | int | 亮度百分比，从1-100 |
| colorTemperature | int | 色温百分比，从0-100 |
| colourHSV | LightColourData | 颜色值，HSV色彩空间；H为色调，取值范围0-360；S为饱和度，取值范围0-100；V为明度，取值范围1-100 |
| scene | LightScene | 彩灯情景，* SCENE_GOODNIGHT为晚安情景； * SCENE_WORK为工作情景； * SCENE_READ为阅读情景； * SCENE_CASUAL为休闲情景； |

**参数说明**

值得说明的是`LightDataPoint`对象，该对象封装了当前设备所有功能点。当功能点发生变化时，将会回调。每次回调的都会是完整的对象。

以下是该对象参数的具体含义：

```kotlin
public class LightDataPoint {
    /**
     * 开关
     */
    public boolean powerSwitch;
    /**
     * 工作模式。
     * <p>
     * MODE_WHITE为白光模式；
     * MODE_COLOUR为彩光模式；
     * MODE_SCENE为情景模式；
     */
    public LightMode workMode;

    /**
     * 亮度百分比，从0到100
     */
    public int brightness;

    /**
     * 色温百分比，从0到100
     */
    public int colorTemperature;

    /**
     * 颜色值，HSV色彩空间.
     * <p>
     * 其中H为色调，取值范围0-360；
     * 其中S为饱和度，取值范围0-100；
     * 其中V为明度，取值范围0-100；
     */
    public LightColourData colorHSV;
    /**
     * 彩灯情景。
     *
     * SCENE_GOODNIGHT为晚安情景；
     * SCENE_WORK为工作情景；
     * SCENE_READ为阅读情景；
     * SCENE_CASUAL为休闲情景；
     */
    public LightScene scene;
}
```

**示例代码**

```kotlin
// 开灯
lightDevice.powerSwitch(true, new IResultCallback() {
    @Override
    public void onError(String code, String error) {
        Log.i("test_light", "powerSwitch onError:" + code + error);
    }

    @Override
    public void onSuccess() {
        Log.i("test_light", "powerSwitch onSuccess:");
    }
});
// 晚安场景
lightDevice.scene(LightScene.SCENE_GOODNIGHT, new IResultCallback() {
    @Override
    public void onError(String code, String error) {
        Log.i("test_light", "scene onError:" + code + error);
    }

    @Override
    public void onSuccess() {
        Log.i("test_light", "scene onSuccess:");
    }
});
// 设置颜色
lightDevice.colorHSV(100, 100, 100, new IResultCallback() {
    @Override
    public void onError(String code, String error) {
        Log.i("test_light", "colorHSV onError:" + code + error);
    }
    @Override
    public void onSuccess() {
        Log.i("test_light", "colorHSV onSuccess:");
    }
});
```

## 获取当前灯的类型

灯共分为一路灯（仅有白光）、二路灯（白光+冷暖控制）、三路灯（仅有彩光模式）、四路灯（白光+彩光）、五路灯（白光+彩光+冷暖）。

这5种灯具在功能定义上有所区别，在开发相应的UI和控制时有所区别。

该方法可获取当前灯的类型。

**接口说明**

```kotlin
/**
 * 获取当前是几路灯
 *
 * @return {@link LightType}
 */
LightType lightType();
```

其中LightType中定义的类型有：

```kotlin
/**
 * 白光灯，dpCode：bright_value
 */
TYPE_C,
/**
 * 白光+冷暖，dpCode：bright_value + temp_value
 */
TYPE_CW,
/**
 * RGB，dpCode：colour_data
 */
TYPE_RGB,
/**
 * 白光+RGB，dpCode：bright_value + colour_data
 */
TYPE_RGBC,
/**
 * 白光+冷暖+RGB，dpCode：bright_value + temp_value + colour_data
 */
TYPE_RGBCW
```

## 获取当前设备所有功能的值

打开一个设备面板时，需要获取所有功能点值来展示。可通过此接口获取上面提到的LightDataPoint对象。

```kotlin
/**
 * 获取灯所有功能点的值
 */
LightDataPoint getLightDataPoint();
```

## 开关

**接口说明**

```kotlin
/**
 * 开灯 or 关灯
 *
 * @param status         true or false
 * @param resultCallback callback
 */
void powerSwitch(boolean status, IResultCallback resultCallback);
```

**参数说明**

| 字段 | 含义 |
| ---- | ---- |
| status | true为开 |
| resultCallback | 仅表示此次发送指令成功or失败，真正控制成功需要识别ILightListener中的dp变化 |

## 切换工作模式

**接口说明**

```kotlin
/**
 * 切换工作模式
 *
 * @param mode           工作模式
 * @param resultCallback callback
 */
void workMode(LightMode mode, IResultCallback resultCallback);
```

**参数说明**

| 字段 | 含义 |
| ---- | ---- |
| mode | 工作模式，其值有MODE_WHITE（白光）, MODE_COLOUR（彩光）, MODE_SCENE（情景模式） |
| resultCallback | 仅表示此次发送指令成功or失败，真正控制成功需要识别ILightListener中的dp变化 |

**调用示例**

如切换到彩光模式：

```kotlin
lightDevice.workMode(LightMode.MODE_COLOUR, new IResultCallback() {
    @Override
    public void onError(String code, String error) {
        Log.i("test_light", "workMode onError:" + code + error);
    }

    @Override
    public void onSuccess() {
        Log.i("test_light", "workMode onSuccess");
    }
});
```

注意：部分灯具必须切换到对应的工作模式才可以控制，比如控制彩光，必须先切换到彩光模式才可以发颜色的值。

## 控制灯的亮度

**接口说明**

```kotlin
/**
 * 亮度控制。
 *
 * @param status         亮度的百分比，取值范围0-100
 * @param resultCallback callback
 */
void brightness(int status, IResultCallback resultCallback);
```

**参数说明**

| 字段 | 含义 |
| ---- | ---- |
| status | 亮度的百分比 |
| resultCallback | 仅表示此次发送指令成功or失败，真正控制成功需要识别ILightListener中的dp变化 |

## 控制灯的冷暖色温

**接口说明**

```kotlin
/**
 * 色温控制
 *
 * @param status         色温的百分比，取值范围0-100
 * @param resultCallback callback
 */
void colorTemperature(int status, IResultCallback resultCallback);
```

**参数说明**

| 字段 | 含义 |
| ---- | ---- |
| status | 色温的百分比 |
| resultCallback | 仅表示此次发送指令成功or失败，真正控制成功需要识别ILightListener中的dp变化 |

## 设置彩光的颜色

**接口说明**

```kotlin
/**
 * 设置彩灯的颜色
 *
 * @param hue            色调 （范围：0-360）
 * @param saturation     饱和度（范围：0-100）
 * @param value          明度（范围：0-100）
 * @param resultCallback callback
 */
void colorHSV(int hue, int saturation, int value, IResultCallback resultCallback);
```

## 切换场景模式

切换彩灯的情景模式，目前共有四种模式：

```kotlin
LightScene.SCENE_GOODNIGHT为晚安情景；
LightScene.SCENE_WORK为工作情景；
LightScene.SCENE_READ为阅读情景；
LightScene.SCENE_CASUAL为休闲情景；
```

**方法说明**

```kotlin
/**
 * @param lightScene     {@link LightScene}
 * @param resultCallback callback
 */
void scene(LightScene lightScene, IResultCallback resultCallback);
```