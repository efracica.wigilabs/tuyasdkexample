The lighting device development is complicated because there is both v1 and v2 hardware firmware available. Even if standard commands are used, two sets of control logic need to be developed.

Therefore, the functions of the lighting device are encapsulated, and the switches, working mode switching, brightness control, cooling and heating control, color light control, and the control of the four scene modes of the lighting equipment are encapsulated.

## Demo App

See [Lighting SDK Demo](https://github.com/tuya/tuya-lighting-android-sdk) in GitHub.

Refer to the introduction of [Home SDK Demo App](https://developer.tuya.com/cn/docs/app-development/android-app-sdk/featureoverview?id=Ka69nt97vtsfu), set `BundleId`, `AppKey`, `AppSecret`, `the security picture` correctly.

Lighting SDK Demo App expands the interface package for accessing lighting equipment related functions on the basis of the functions of Home SDK Demo App, speeding up the development process. Mainly includes the following functions:

- Query the color temperature and light colors of the light
- Get the value of all DPs of the light
- Turn the lights on or off
- Switch working mode
- Control the brightness of the light
- Control the color temperature of the light
- Switch scene mode
- Set the color of the light

## Dependency

```kotlin
implementation 'com.tuya.smart:tuyasmart:3.22.0'

// Control SDK dependencies
implementation 'com.tuya.smart:tuyasmart-centralcontrol:1.0.3'
```

Note that `tuyasmart-centralcontrol` uses Kotlin to compile, and the Kotlin library needs to be imported to guarantee its normal use.

>**Note**: If Kotlin has been introduced in the project, the following configuration can be ignored.

**Add Kotlin dependency**:

1. Introduce the Kotlin plugin dependencies in `build.gradle` in the root directory.

	```kotlin
	buildscript {
		ext.kotlin_version = '1.3.72'
		dependencies {
			...
			classpath "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version"
		}
	}
	```

2. Introduce the kotlin plugin and kotlin package in the app's `build.gradle` file:

	```kotlin
	apply plugin: 'kotlin-android'
	dependencies {
		implementation "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlin_version"
	}
	```