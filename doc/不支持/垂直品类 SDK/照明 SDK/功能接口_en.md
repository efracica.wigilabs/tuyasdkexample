# Function description
## Device initialization

First, create an `ITuyaLightDevice` object, and the lamp-related methods are encapsulated in this method.

```kotlin
ITuyaLightDevice lightDevice = new TuyaLightDevice(String devId);
```

**Parameter**

| Parameter | Description |
| ---- | ---- |
| devId | Device id |

**Sample code**

```kotlin
// init lightDevice
ITuyaLightDevice lightDevice = new TuyaLightDevice("vdevo159793004250542");
```

This object encapsulates all DP points of the lamp, including the issuance and reporting of control commands.

## Device agent monitoring

**Description**

```kotlin
/**
 * register Listener
 */
void registerLightListener(ILightListener listener);
```

Among them, the ILightListener callback is as follows:

```kotlin
public interface ILightListener {
    /**
     * Monitor DP point changes of lighting equipment
     *
     * @param dataPoint The state of all DP of the fixture
     */
    void onDpUpdate(LightDataPoint dataPoint);

    /**
     * remove device
     */
    void onRemoved();

    /**
     * device status
     */
    void onStatusChanged(boolean online);

    /**
     * network status
     */
    void onNetworkStatusChanged(boolean status);

    /**
     * Device information updates such as name and the like
     */
    void onDevInfoUpdate();
}
```

**Sample code**

```kotlin
// create lightDevice
ITuyaLightDevice lightDevice = new TuyaLightDevice("vdevo159793004250542");

// register listener
lightDevice.registerLightListener(new ILightListener() {
    @Override
    public void onDpUpdate(LightDataPoint dataPoint) { // return LightDataPoint，Contains the values of all function points of the lamp
        Log.i("test_light", "onDpUpdate:" + dataPoint);
    }

    @Override
    public void onRemoved() {
        Log.i("test_light", "onRemoved");
    }

    @Override
    public void onStatusChanged(boolean status) {
        Log.i("test_light", "onDpUpdate:" + status);
    }

    @Override
    public void onNetworkStatusChanged(boolean status) {
        Log.i("test_light", "onDpUpdate:" + status);
    }

    @Override
    public void onDevInfoUpdate() {
        Log.i("test_light", "onDevInfoUpdate:");
    }
});
```

**`TuyaSmartLightDataPointModel` Data model**

| Field | Types | description |
| ---- | ---- | ---- |
| powerSwitch | boolea | Whether the switch is on, YES means on, NO means off |
| workMode | LightMode | Working mode, MODE_WHITE is white light mode; MODE_COLOUR is color light mode; MODE_SCENE is scene mode; |
| brightness | int | Brightness percentage，从1-100 |
| colorTemperature | int | Color temperature percentage，从0-100 |
| colourHSV | LightColourData | Color value, HSV color space; H is hue, value range is 0-360; S is saturation, value range is 0-100; V is lightness, value range is 1-100 |
| scene | LightScene | Lantern scene, * SCENE_GOODNIGHT is good night scene; SCENE_WORK is work scene; SCENE_READ is reading scene; SCENE_CASUAL is leisure scene; |

**Parameter**

It is worth noting that the `LightDataPoint` object, which encapsulates all the function points of the current device. When the function point changes, it will be called back. Each callback will be a complete object.

The following is the specific meaning of the object parameters:

```kotlin
public class LightDataPoint {
	/**
	* switch
	*/
	public boolean powerSwitch;
	/**
	* Operating mode。
	* <p>
	* MODE_WHITE  White light mode;
	* MODE_COLOUR  color light mode;
	* MODE_SCENE Scene mode;
	*/
	public LightMode workMode;

	/**
	* Brightness percentage，0~100
	*/
	public int brightness;

	/**
	* Color temperature percentage，0~100
	*/
	public int colorTemperature;

	/**
	* Color value, HSV color space.
	* <p>
	* H，0-360;
	* S，0-100;
	* V，0-100;
	*/
	public LightColourData colorHSV;
	/**
	* Lantern scene
	*
	* SCENE_GOODNIGHT Good night scene;
	* SCENE_WORK Work situation;
	* SCENE_READ Reading situation;
	* SCENE_CASUAL Casual scene;
	*/
	public LightScene scene;
}
```

**Sample code**

```kotlin
// Turn on the lights
lightDevice.powerSwitch(true, new IResultCallback() {
	@Override
	public void onError(String code, String error) {
		Log.i("test_light", "powerSwitch onError:" + code + error);
	}

	@Override
	public void onSuccess() {
		Log.i("test_light", "powerSwitch onSuccess:");
	}
});
// Good night scene
lightDevice.scene(LightScene.SCENE_GOODNIGHT, new IResultCallback() {
	@Override
	public void onError(String code, String error) {
		Log.i("test_light", "scene onError:" + code + error);
	}

	@Override
	public void onSuccess() {
		Log.i("test_light", "scene onSuccess:");
	}
});
// Set color
lightDevice.colorHSV(100, 100, 100, new IResultCallback() {
	@Override
	public void onError(String code, String error) {
		Log.i("test_light", "colorHSV onError:" + code + error);
	}
	@Override
	public void onSuccess() {
		Log.i("test_light", "colorHSV onSuccess:");
	}
});
```

## Get the current ligthing type

The lights are divided into one road light (only white light), two road lights (white light + cooling and heating control), three road lights (only color light mode), four road lights (white light + color light), five lights (white light + color light + cold and warm).

These 5 kinds of lamps are different in the function definition, and they are different in the development of corresponding UI and control.

This method can get the current lamp type.

**Description**

```kotlin
/**
 * Get the current number of lights
 *
 * @return {@link LightType}
 */
LightType lightType();
```

Among them, the types defined in LightType are：

```kotlin
/**
 * White light，dpCode：bright_value
 */
TYPE_C,
/**
 * White light + cold and warm，dpCode：bright_value + temp_value
 */
TYPE_CW,
/**
 * RGB，dpCode：colour_data
 */
TYPE_RGB,
/**
 * White light+RGB，dpCode：bright_value + colour_data
 */
TYPE_RGBC,
/**
 * White light+cold and warm+RGB，dpCode：bright_value + temp_value + colour_data
 */
TYPE_RGBCW
```

## Get the values of all functions of the current device

When opening a device panel, you need to get all the function point values to display. The LightDataPoint object mentioned above can be obtained through this interface.

```kotlin
/**
 * Get the value of all function points of the lamp
 */
LightDataPoint getLightDataPoint();
```

## Switch

Control the light switch

**Description**

```kotlin
/**
 * Light on or off
 *
 * @param status         true or false
 * @param resultCallback callback
 */
void powerSwitch(boolean status, IResultCallback resultCallback);
```

**Parameter**

| Field | Description |
| ---- | ---- |
| status | true is on |
| resultCallback | It only means that the sending command is successful or failed. The real control success needs to identify the dp change in ILightListener |

## Switch working mode

Control the switching of working modes

**Description**

```kotlin
/**
 * Switch working mode
 *
 * @param mode           Operating mode
 * @param resultCallback callback
 */
void workMode(LightMode mode, IResultCallback resultCallback);
```

**Parameter**

| Field | Description |
| ---- | ---- |
| mode | Working mode, its values are MODE_WHITE (white light), MODE_COLOUR (color light), MODE_SCENE (scene mode) |
| resultCallback | It only means that the sending command is successful or failed. The real control success needs to identify the dp change in ILightListener |

**Example**

If switch to IPL mode:

```kotlin
lightDevice.workMode(LightMode.MODE_COLOUR, new IResultCallback() {
	@Override
	public void onError(String code, String error) {
		Log.i("test_light", "workMode onError:" + code + error);
	}

	@Override
	public void onSuccess() {
		Log.i("test_light", "workMode onSuccess");
	}
});
```

Note: Some lamps and lanterns must be switched to the corresponding working mode before they can be controlled.

## Control the brightness of the light

Control brightness

**Description**

```kotlin
/**
 * Brightness control.
 *
 * @param status         Percentage of brightness, value range 0-100
 * @param resultCallback callback
 */
void brightness(int status, IResultCallback resultCallback);
```

**Parameter**

| Field | Meaning |
| ---- | ---- |
| status | Percentage of brightness |
| resultCallback | It only means that the sending command is successful or failed. The real control success needs to identify the dp change in ILightListener |

## Control the color temperature of the light (warm and cold)

Control the heating and cooling value of the lamp.

**Description**

```kotlin
/**
 * Color temperature control
 *
 * @param status         Percentage of color temperature, value range 0-100
 * @param resultCallback callback
 */
void colorTemperature(int status, IResultCallback resultCallback);
```

**Parameter**

| Field | Description |
| ---- | ---- |
| status | Percentage of color temperature |
| resultCallback | It only means that the sending command is successful or failed. The real control success needs to identify the dp change in ILightListener |

## Set the color of the IPL

Control the color of colored lights

**Description**

```kotlin
/**
 * Set the color of the lantern
 *
 * @param hue            tone （0-360）
 * @param saturation     saturation（0-100）
 * @param value          Lightness（0-100）
 * @param resultCallback callback
 */
void colorHSV(int hue, int saturation, int value, IResultCallback resultCallback);
```

## Switch scene mode

Switch the scene mode of the colorful lights. There are currently four modes:

```kotlin
LightScene.SCENE_GOODNIGHT Good night scene;
LightScene.SCENE_WORK Work situation;
LightScene.SCENE_READ Reading situation;
LightScene.SCENE_CASUAL Casual scene;
```

**Description**

```kotlin
/**
 * @param lightScene     {@link LightScene}
 * @param resultCallback callback
 */
void scene(LightScene lightScene, IResultCallback resultCallback);
```