## 获取消息列表

**接口说明**

```java
void getMessageList(ITuyaDataCallback<List<MessageBean>> callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| callback | 回调，包括获取消息列表成功和失败 |

**MessageBean 数据模型**

| 字段 | 类型 | 描述 |
| ---- | ---- | ---- |
| dateTime | String | 日期和时间 |
| Icon | String | 消息图标 URL |
| msgTypeContent | String | 消息类型名称 |
| msgContent | String | 消息内容 |
| msgType | Integer | 消息类型 |
| msgSrcId | String | 设备 id（只有告警消息才有该字段） |
| id | String | 消息 id |

**示例代码**

```java
TuyaHomeSdk.getMessageInstance().getMessageList(new ITuyaDataCallback<List<MessageBean>>() {
    @Override
    public void onSuccess(List<MessageBean> result) {}
    @Override
    public void onError(String errorCode, String errorMessage) {}
});
```

## 获取分页的消息列表

**接口说明**

```java
void getMessageList(int offset, int limit, ITuyaDataCallback<MessageListBean> callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| offset | 已请求到的消息总数 |
| limit | 每页请求数据数 |
| callback | 回调，包括获取消息列表成功和失败 |

**示例代码**

```java
TuyaHomeSdk.getMessageInstance().getMessageList(offset, limit, new ITuyaDataCallback<MessageListBean>() {
            @Override
            public void onSuccess(MessageListBean result) {}
            @Override
            public void onError(String errorCode, String errorMessage) {}
});
```

## 根据消息类型分页获取消息列表

**接口说明**

```java
void getMessageListByMsgType(int offset, int limit, MessageType msgType, final ITuyaDataCallback<MessageListBean> callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| offset | 已请求到的消息总数 |
| limit | 每页请求数据数 |
| msgType | 消息类型（1 - 告警，2 - 家庭，3 - 通知) |
| callback | 回调，包括获取消息列表成功和失败 |

**示例代码**

```java
TuyaHomeSdk.getMessageInstance().getMessageListByMsgType(offset, limit, type, new ITuyaDataCallback<MessageListBean>() {
            @Override
            public void onSuccess(MessageListBean result) {}
            @Override
            public void onError(String errorCode, String errorMessage) {}
});
```

## 根据消息 msgSrcId 获取消息详情列表

根据消息 SrcId 获取消息列表， 目前只支持( MSG_REPORT 告警)类型的消息。

**接口说明**

```java
void getMessageListByMsgSrcId(int offset, int limit, MessageType msgType, String msgSrcId, ITuyaDataCallback<MessageListBean> callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| offset | 偏移从第 N 条数据开始获取 |
| limit | 每页的消息数量 |
| msgType | 消息类型  ( MSG_REPORT 告警) |
| msgSrcId | 消息组 id |
| callback | 回调，包括获取成功和失败 |

>**注意**
>1. 如果需要告警信息支持不同语言切换，需要做语言国际化。
>2. 已经收到的告警信息，不会随手机系统语言的改变而改变。

**示例代码**

```java
TuyaHomeSdk.getMessageInstance().getMessageListByMsgSrcId(offset, limit, MessageType.MSG_REPORT, msgSrcId, true, new ITuyaDataCallback<MessageListBean>() {
    @Override
    public void onSuccess(final MessageListBean result) {              
    }
    @Override
    public void onError(String errorCode, String errorMessage) {               
    }
});
```

## 批量删除消息

**接口说明**

```java
void deleteMessages(List<String> mIds, IBooleanCallback callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| mIds | 要删除的消息 id 组 |
| callback | 回调，包括删除成功和失败 |

**示例代码**

```java
TuyaHomeSdk.getMessageInstance().deleteMessages(mIds, new IBooleanCallback() {
    @Override
    public void onSuccess() {
    }
    @Override
    public void onError(String code, String error) {
    }
});
```

## 批量删除特定类型的消息

**接口说明**

```java
void deleteMessageWithType(int msgType, List<String> mIds, List<String> mSrcIds, final IBooleanCallback callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| msgType | 消息类型（1 - 告警，2 - 家庭，3 - 通知） |
| mIds | 要删除的消息 id 组 |
| mSrcIds | 消息类型 id 组，传 null或空表示不删除告警消息 |
| callback | 回调，包括删除成功和失败 |

> 注意：针对告警消息类型，当删除告警消息`列表`中的消息时，传参数mSrcIds；
  删除告警消息`详情列表`中的消息，传参数mIds，两者不可以同时传。
  
**示例代码**

```java
TuyaHomeSdk.getMessageInstance().deleteMessageWithType(msgType, mIds, mSrcIds, new IBooleanCallback() {
    @Override
    public void onSuccess() {
    }
    @Override
    public void onError(String code, String error) {
    }
});
```

## 检查新消息

> 注意：是否为新消息依赖于其它接口的请求情况，告警消息获取消息详情的时候会将消息标记为已读，其他类型的消息，在获取消息列表的时候会将消息标记为已读，只有未读的消息才是新消息，判断标识在`MessageBean`的`hasNotRead`字段。

**接口说明**

```java
void requestMessageNew(final ITuyaDataCallback<MessageHasNew> callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| callback | 获取最新消息类型 |

**MessageHasNew 数据模型**

| 字段 | 类型 | 描述 |
| ---- | ---- | ---- |
| alarm | boolean | 是否有告警消息 |
| family | boolean | 是否有家庭消息 |
| notification | boolean | 是否有通知消息 |

**示例代码**

```java
TuyaHomeSdk.getMessageInstance().requestMessageNew(new ITuyaDataCallback<MessageHasNew>() {
    @Override
    public void onSuccess(MessageHasNew result) {     
    }
    @Override
    public void onError(String errorCode, String errorMessage) {
    }
});
```