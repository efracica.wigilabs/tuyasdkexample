The message management module contains message center and push functions. Its messages have three categories: alarm, home, and notification, and each category supports turn on or turn off respectively.

| Class name | Description |
| ---- | ---- |
| ITuyaMessage | Get Message List, Batch Delete Message, Check New Message |
| ITuyaPush | Get Or Set Push Notification Status |