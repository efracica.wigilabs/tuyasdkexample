## Get push notification status 

The message push switch is the master switch. In the off state, no messages such as device alarms, home messages, and notification messages can be received.

**Declaration**

```java
void getPushStatus(ITuyaResultCallback<PushStatusBean> callback);
```

**Parameters**

| Parameters | Description |
| ---- | ---- |
| callback | callbacks, including success and failure to get total switch status |

**Example**

```java
 TuyaHomeSdk.getPushInstance().getPushStatus(new ITuyaResultCallback<PushStatusBean>() {
       @Override
       public void onSuccess(PushStatusBean result) {}
       @Override
       public void onError(String errorCode, String errorMessage) {}
 });
```

## Set push notification status

The message push switch is the master switch, and no messages such as device alarms, home messages, notification messages, etc. can be received in the off state.

**Declaration**

```java
void setPushStatus(boolean isOpen, ITuyaDataCallback<Boolean> callback);
```

**Parameters**

| Parameters | Description |
| ---- | ---- |
| isOpen | Whether to open |
| callback | Callbacks, including setting success and failure |

**Example**

```java
TuyaHomeSdk.getPushInstance().setPushStatus(open, new ITuyaDataCallback<Boolean>() {
      @Override
      public void onSuccess(Boolean result) {}
      @Override
      public void onError(String errorCode, String errorMessage) {}
});
```

## Get the switch status of the message according to the message type

Get the switch status of the current type of message according to the message type

**Declaration**

```java
void getPushStatusByType(PushType type, ITuyaDataCallback<Boolean> callback);
```

**Parameters**

| Parameters | Description |
| ---- | ---- |
| type | Message type(0-alarm message, 1-family message, 2-notification message, 4-marketing message) |
| callback | Callbacks, including success and failure |

**Example**

```java
TuyaHomeSdk.getPushInstance().getPushStatusByType(type, new ITuyaDataCallback<Boolean>() {
      @Override
      public void onSuccess(Boolean result) {}
      @Override
      public void onError(String errorCode, String errorMessage) {}
});
```

### Set the switch status of the message according to the message type

Set the switch status of the current type of message according to the message type.

**Declaration**

```java
void setPushStatusByType(PushType type, isOpen, ITuyaDataCallback<Boolean> callback);
```

**Parameter**

| Parameter | Description |
| ---- | ---- |
| type | Message type(0-alarm message, 1-family message, 2-notification message, 4-marketing message) |
| isOpen | Whether to open |
| callback | Callbacks, including success and failure |

**Example**

```java
TuyaHomeSdk.getPushInstance().setPushStatusByType(pushType, checked, new ITuyaDataCallback<Boolean>() {
      @Override
      public void onSuccess(Boolean result) {
      }

      @Override
      public void onError(String errorCode, String errorMessage) {
      }
});
```

### Get Do Not Disturb List

**Declaration**

```java
void getDNDList(ITuyaDataCallback<ArrayList<DeviceAlarmNotDisturbVO>> listener);
```

**Parameter**

| Parameters | Description |
| ---- | ---- |
| listener | Callbacks, including setting success and failure |

**Example**

```java
TuyaHomeSdk.getMessageInstance().getDNDList(new ITuyaDataCallback<DeviceAlarmNotDisturbVO>() {
      @Override
      public void onSuccess(DeviceAlarmNotDisturbVO result) {}
      @Override
      public void onError(String errorCode, String errorMessage) {}
});
```
### Get Device List

**Declaration**

```java
void getDNDDeviceList(ITuyaDataCallback<ArrayList<NodisturbDevicesBean>> listener);
```

**Parameter**

| Parameters | Description |
| ---- | ---- |
| listener | Callbacks, including setting success and failure |

**Example**

```java
TuyaHomeSdk.getMessageInstance().getDNDDeviceList(new ITuyaDataCallback<NodisturbDevicesBean>() {
      @Override
      public void onSuccess(NodisturbDevicesBean result) {}
      @Override
      public void onError(String errorCode, String errorMessage) {}
});
```
### Get Device Do Not Disturb Status.

**Declaration**

```java
void getDeviceDNDSetting(ITuyaDataCallback<Boolean> listener);
```

**Parameter**

| Parameters | Description |
| ---- | ---- |
| listener | Callbacks, including setting success and failure, open->true, close->false |

**Example**

```java
TuyaHomeSdk.getMessageInstance().getDeviceDNDSetting(new ITuyaDataCallback<Boolean>() {
      @Override
      public void onSuccess(Boolean result) {}
      @Override
      public void onError(String errorCode, String errorMessage) {}
});
```
### Set Device Do Not Disturb Status

**Declaration**

```java
void setDeviceDNDSetting(boolean open, ITuyaDataCallback<Boolean> listener);
```

**Parameter**

| Parameters | Description |
| ---- | ---- |
| open | open or close  |
| listener | Callbacks, including setting success and failure |

**Example**

```java
TuyaHomeSdk.getMessageInstance().setDeviceDNDSetting(open, new ITuyaDataCallback<Boolean>() {
      @Override
      public void onSuccess(Boolean result) {}
      @Override
      public void onError(String errorCode, String errorMessage) {}
});
```


### Add Device Do Not Disturb Time

**Declaration**

```java
void addDNDWithStartTime(String startTime, String endTime, String devIds, String loops, ITuyaDataCallback<Long> listener);
```

**Parameter**

| Parameters | Description |
| ---- | ---- |
| startTime | Start time |
| endTime | End time  |
| devIds | The device ID list |
| loops | Repeat days per week |
| listener | setDeviceDNDSetting |

**Example**

```java
TuyaHomeSdk.getMessageInstance().addDNDWithStartTime(startTime, endTime, devIds, loops, new ITuyaDataCallback<Long>() {
      @Override
      public void onSuccess(Long result) {}
      @Override
      public void onError(String errorCode, String errorMessage) {}
});
```


### Remove Do Not Disturb Time

**Declaration**

```java
void removeDNDWithTimerId(long id, ITuyaDataCallback<Boolean> listener);
```

**Parameter**

| Parameters | Description |
| ---- | ---- |
| id | time ID |
| listener | Callbacks, including setting success and failure |

**Example**

```java
TuyaHomeSdk.getMessageInstance().removeDNDWithTimerId(id, new ITuyaDataCallback<Boolean>() {
      @Override
      public void onSuccess(Boolean result) {}
      @Override
      public void onError(String errorCode, String errorMessage) {}
});
```

### Modify Device Do Not Disturb Time

**Declaration**

```java
void modifyDNDWithTimerId(long nodisturbAlarmId, String mStartTime, String mEndTime, String devIds, String loops, ITuyaDataCallback<Boolean> listener);
```

**Parameter**

| Parameters | Description |
| ---- | ---- |
| nodisturbAlarmId | time ID |
| mStartTime | start time |
| mEndTime | end time |
| loops | Repeat days per week |
| listener | Callbacks, including setting success and failure |

**Example**

```java
TuyaHomeSdk.getMessageInstance().modifyDNDWithTimerId(nodisturbAlarmId, mStartTime, mEndTime, devIds, loops, new ITuyaDataCallback<Boolean>() {
      @Override
      public void onSuccess(Boolean result) {}
      @Override
      public void onError(String errorCode, String errorMessage) {}
});
```