## 获取 App 消息推送的开启状态

消息推送开关为总开关，关闭状态下无法接收到 设备告警、家庭消息、通知消息 等任何消息。

***接口说明***

```java
void getPushStatus(ITuyaResultCallback<PushStatusBean> callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| callback | 回调，包括获取总开关状态成功和失败 |

**示例代码**

```java
 TuyaHomeSdk.getPushInstance().getPushStatus(new ITuyaResultCallback<PushStatusBean>() {
       @Override
       public void onSuccess(PushStatusBean result) {}
       @Override
       public void onError(String errorCode, String errorMessage) {}
 });
```

## 开启或者关闭App消息推送

消息推送开关为总开关，关闭状态下无法接收到 设备告警、家庭消息、通知消息 等任何消息

**接口说明**

```java
void setPushStatus(boolean isOpen, ITuyaDataCallback<Boolean> callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| isOpen | 是否开启 |
| callback | 回调，包括设置成功和失败 |

**示例代码**

```java
TuyaHomeSdk.getPushInstance().setPushStatus(open, new ITuyaDataCallback<Boolean>() {
      @Override
      public void onSuccess(Boolean result) {}
      @Override
      public void onError(String errorCode, String errorMessage) {}
});
```

## 根据消息类型获取消息的开关状态

根据消息类型获取当前类型消息的开关状态。

**接口说明**

```java
void getPushStatusByType(PushType type, ITuyaDataCallback<Boolean> callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| type | 消息类型（0-告警消息，1-家庭消息，2-通知消息，4-营销消息） |
| callback | 回调，包括获取成功和失败 |

**示例代码**

```java
TuyaHomeSdk.getPushInstance().getPushStatusByType(type, new ITuyaDataCallback<Boolean>() {
      @Override
      public void onSuccess(Boolean result) {}
      @Override
      public void onError(String errorCode, String errorMessage) {}
});
```

### 根据消息类型设置消息的开关状态

根据消息类型设置当前类型消息的开关状态。

**接口说明**

```java
void setPushStatusByType(PushType type, isOpen, ITuyaDataCallback<Boolean> callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| type | 消息类型（0-告警消息，1-家庭消息，2-通知消息，4-营销消息） |
| isOpen | 是否开启 |
| callback | 回调，包括设置成功和失败 |

**示例代码**

```java
TuyaHomeSdk.getPushInstance().setPushStatusByType(pushType, checked, new ITuyaDataCallback<Boolean>() {
      @Override
      public void onSuccess(Boolean result) {}
      @Override
      public void onError(String errorCode, String errorMessage) {}
});
```
### 获取已经设置的免打扰时间段列列表

**接口说明**

```java
void getDNDList(ITuyaDataCallback<ArrayList<DeviceAlarmNotDisturbVO>> listener);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| listener | 回调，包括设置成功和失败 |

**示例代码**

```java
TuyaHomeSdk.getMessageInstance().getDNDList(new ITuyaDataCallback<DeviceAlarmNotDisturbVO>() {
      @Override
      public void onSuccess(DeviceAlarmNotDisturbVO result) {}
      @Override
      public void onError(String errorCode, String errorMessage) {}
});
```
### 获取所有家庭下所有设备列表

**接口说明**

```java
void getDNDDeviceList(ITuyaDataCallback<ArrayList<NodisturbDevicesBean>> listener);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| listener | 回调，包括设置成功和失败 |

**示例代码**

```java
TuyaHomeSdk.getMessageInstance().getDNDDeviceList(new ITuyaDataCallback<NodisturbDevicesBean>() {
      @Override
      public void onSuccess(NodisturbDevicesBean result) {}
      @Override
      public void onError(String errorCode, String errorMessage) {}
});
```
### 获取设备消息免打扰开关状态

**接口说明**

```java
void getDeviceDNDSetting(ITuyaDataCallback<Boolean> listener);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| listener | 回调，返回成功包含开关状态, open->true, close->false |

**示例代码**

```java
TuyaHomeSdk.getMessageInstance().getDeviceDNDSetting(new ITuyaDataCallback<Boolean>() {
      @Override
      public void onSuccess(Boolean result) {}
      @Override
      public void onError(String errorCode, String errorMessage) {}
});
```
### 开启或者关闭设备消息免打扰开关

**接口说明**

```java
void setDeviceDNDSetting(boolean open, ITuyaDataCallback<Boolean> listener);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| open | 开启或关闭  |
| listener | 回调，包括设置成功和失败 |

**示例代码**

```java
TuyaHomeSdk.getMessageInstance().setDeviceDNDSetting(open, new ITuyaDataCallback<Boolean>() {
      @Override
      public void onSuccess(Boolean result) {}
      @Override
      public void onError(String errorCode, String errorMessage) {}
});
```


### 添加消息免打扰时段

**接口说明**

```java
void addDNDWithStartTime(String startTime, String endTime, String devIds, String loops, ITuyaDataCallback<Long> listener);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| startTime | 开始时间 |
| endTime | 结束时间  |
| devIds | 设备ID列表 |
| loops | 每周重复次数 |
| listener | 回调，包括设置成功和失败 |

**示例代码**

```java
TuyaHomeSdk.getMessageInstance().addDNDWithStartTime(startTime, endTime, devIds, loops, new ITuyaDataCallback<Long>() {
      @Override
      public void onSuccess(Long result) {}
      @Override
      public void onError(String errorCode, String errorMessage) {}
});
```


### 移除消息免打扰时间

**接口说明**

```java
void removeDNDWithTimerId(long id, ITuyaDataCallback<Boolean> listener);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| id | time ID |
| listener | 回调，包括设置成功和失败 |

**示例代码**

```java
TuyaHomeSdk.getMessageInstance().removeDNDWithTimerId(id, new ITuyaDataCallback<Boolean>() {
      @Override
      public void onSuccess(Boolean result) {}
      @Override
      public void onError(String errorCode, String errorMessage) {}
});
```

### 修改消息免打扰时间

**接口说明**

```java
void modifyDNDWithTimerId(long nodisturbAlarmId, String mStartTime, String mEndTime, String devIds, String loops, ITuyaDataCallback<Boolean> listener);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| nodisturbAlarmId | time ID |
| mStartTime | 开始时间 |
| mEndTime | 结束时间 |
| loops | 每周重复次数 |
| listener | 回调，包括设置成功和失败 |

**示例代码**

```java
TuyaHomeSdk.getMessageInstance().modifyDNDWithTimerId(nodisturbAlarmId, mStartTime, mEndTime, devIds, loops, new ITuyaDataCallback<Boolean>() {
      @Override
      public void onSuccess(Boolean result) {}
      @Override
      public void onError(String errorCode, String errorMessage) {}
});
```