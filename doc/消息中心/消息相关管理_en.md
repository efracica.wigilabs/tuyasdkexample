## Get message list

**Declaration**

```java
void getMessageList(ITuyaDataCallback<List<MessageBean>> callback);
```

**Parameters**

| Parameter | Description |
| ---- | ---- |
| callback | Callbacks, including success and failure of getting message list |

**MessageBean data model**

| Field | Type | Description |
| ---- | ---- | ---- |
| dateTime | String | Date and time |
| Icon | String | Message icon URL |
| msgTypeContent | String | Message type name |
| msgContent | String | Message content |
| msgType | Integer | Message type |
| msgSrcId | String | Device Id (this field is only available in alarm messages) |
| id | String | Message id |

**Example**

```java
TuyaHomeSdk.getMessageInstance().getMessageList(new ITuyaDataCallback<List<MessageBean>>() {
    @Override
    public void onSuccess(List<MessageBean> result) {}
    @Override
    public void onError(String errorCode, String errorMessage) {}
});
```

## Get a list of paged messages

**Declaration**

```java
void getMessageList(int offset, int limit, ITuyaDataCallback<MessageListBean> callback);
```

**Parameters**

| parameter | description |
| ---- | ---- |
| offset | Offset is obtained from the nth data |
| limit | Number of messages per page |
| callback | Callbacks, including success and failure |

**Example**

```java
TuyaHomeSdk.getMessageInstance().getMessageList(offset, limit, new ITuyaDataCallback<MessageListBean>() {
            @Override
            public void onSuccess(MessageListBean result) {}
            @Override
            public void onError(String errorCode, String errorMessage) {}
});
```

## Gets a list of messages by paging by message type

**Declaration**

```java
void getMessageListByMsgType(int offset, int limit, MessageType msgType, ITuyaDataCallback<MessageListBean> callback);
```

**Parameters**

| parameter | description |
| ---- | ---- |
| offset | Offset is obtained from the nth data |
| limit | Number of messages per page |
| msgType | Message type (MSG\_REPORT alert) (MSG\_FAMILY family) (MSG\_NOTIFY notification) |
| callback | Callbacks, including success and failure |

**Example**

```java
TuyaHomeSdk.getMessageInstance().getMessageListByMsgType(offset, limit, type, new ITuyaDataCallback<MessageListBean>() {
      @Override
      public void onSuccess(MessageListBean result) {}
      @Override
      public void onError(String errorCode, String errorMessage) {}
});
```

## Get a list of detail messages by paging by MsgSrcId

The message list is obtained according to the message SrcId. Currently, only messages of the type (MSG_REPORT alarm) are supported.

**Declaration**

```java
void getMessageListByMsgSrcId(int offset, int limit, MessageType msgType, String msgSrcId, ITuyaDataCallback<MessageListBean> callback);
```

**Parameters**

| parameter | description |
| ---- | ---- |
| offset | Offset is obtained from the nth data |
| limit | Number of messages per page |
| msgType | Message type (MSG_REPORT alert) |
| msgSrcId | Interest group id |
| callback | Callbacks, including success and failure |

>**Note**:
>1. If you need alarm information to support switching between different languages, you need to do language internationalization
>2. The alarm information that has been received will not change with the change of the phone system language

**Example**

```java
TuyaHomeSdk.getMessageInstance().getMessageListByMsgSrcId(offset, limit, MessageType.MSG_REPORT, msgSrcId, true, new ITuyaDataCallback<MessageListBean>() {
    @Override
    public void onSuccess(final MessageListBean result) {              
    }
    @Override
    public void onError(String errorCode, String errorMessage) {               
    }
});
```

## Delete message

### Delete one or more message

**Declaration**

```java
void deleteMessages(List<String> mIds, IBooleanCallback callback);
```

**Parameters**

| parameter | description |
| ---- | ---- |
| mIds | Device ids |
| contact | Callbacks, including delete success and failure |

**Example**

```java
TuyaHomeSdk.getMessageInstance().deleteMessages(mIds, new IBooleanCallback() {
    @Override
    public void onSuccess() {
    }
    @Override
    public void onError(String code, String error) {
    }
});
```

### Delete one or more specific types of message

**Declaration**

```java
void deleteMessageWithType(int msgType, List<String> mIds, List<String> mSrcIds, final IBooleanCallback callback);
```

**Parameters**

| Parameter | Description |
| ---- | ---- |
| msgType | Message type（1 - alarm，2 - family，3 - notifiction） |
| mIds | Delete messageId list |
| mSrcIds | Delete alarm messageId list，null or empty mean don't delete alarm message |
| callback | Callbacks, including delete success and failure |

> Note: For the alert message type, pass the parameter mSrcIds when deleting messages in the alert message `list`.
  When deleting messages in the alert message `detail list`, pass the parameter mIds, both cannot be passed at the same time.

**Example**

```java
TuyaHomeSdk.getMessageInstance().deleteMessageWithType(msgType, mIds, mSrcIds, new IBooleanCallback() {
    @Override
    public void onSuccess() {
    }
    @Override
    public void onError(String code, String error) {
    }
});
```

## Check new message

> Note: Whether it is a new message depends on the request situation of other interfaces. Alarm messages will mark the message as read when getting the message details, other types of messages will mark the message as read when getting the message list, only unread messages are new messages, and the judgment mark is in the `hasNotRead` field of the `MessageBean`.

**Declaration**

```java
void requestMessageNew(final ITuyaDataCallback<MessageHasNew> callback);
```

**Parameters**

| Parameter | Description |
| ---- | ---- |
| callback | Get the latest news type |

**MessageHasNew data model**

| field | type | description |
| ---- | ---- | ---- |
| alarm | boolean | Is there an alarm message |
| family | boolean | Is there an family message |
| notification | boolean | Is there an notification message |

**Example**

```java
TuyaHomeSdk.getMessageInstance().requestMessageNew(new ITuyaDataCallback<MessageHasNew>() {
    @Override
    public void onSuccess(MessageHasNew result) {     
    }
    @Override
    public void onError(String errorCode, String errorMessage) {
    }
});
```