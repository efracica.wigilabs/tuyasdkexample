# 原生控制面板

使用 Native 代码开发的原生控制面板，支持绝大多数涂鸦系智能产品以及产品功能点控制，简单接入即可快速集成完整的涂鸦生态产品能力，轻松体验不同智能产品。

## 功能优势

* 可以提前支持新产品，并且在单品类无法支持的情况下提供服务。
* 原生代码，接入就可以使用。
* 便于开发阶段调试。
* 部分面板支持通过 [自由配置面板](https://support.tuya.com/zh/help/_detail/Ka32mqup3uiiq) 配置展示。

<img src="https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/content-platform/hestia/162425148095e1b0a5b82.jpg" alt="light" style="zoom:33%;" />

<img src="https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/content-platform/hestia/1624251525e068068527a.jpg" alt="light" style="zoom:33%;" />


## 使用方法

**原生控制面板依赖**

```java
implementation 'com.tuya.smart:TuyaDefaultPanel:2.1.0'
```

**接口说明**

```java
void openDefaultPanel(Context context, String devId, MenuBean menuBean);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| context | 上下文，用于打开Activity |
| devId | 设备 ID |
| menuBean | 用于创建和显示自定义的 menu 及事件 |

> **说明**：menuBean参数用于创建和显示自定义的 menu 及事件，如不需要可以传null。

**MenuBean 数据模型**

| 字段 | 类型 | 描述 |
| ---- | ---- | ---- |
| menuRes | int | 自定义menu资源 ID|
| menuItemId | int | 自定义menu下的itemId |
| bundle | Bundle | 自定义传递数据Bundle，使用IDefaultPanelController.BUNDLE_KEY从Intent获取 |
| activityClassName | String | 自定义Activity的ClassName，如TestActivity.class.getName() |

**示例代码**

```java
MenuBean menuBean = new MenuBean();
menuBean.setMenuRes(R.menu.common_menu);
menuBean.setMenuItemId(R.id.check);
Bundle bundle = new Bundle();
bundle.putString("DEV_ID_KEY", devId);
menuBean.setBundle(bundle);
menuBean.setActivityClassName(TestActivity.class.getName());
IDefaultPanelController defaultPanelController = PluginManager.service(IDefaultPanelController.class);
defaultPanelController.usePanel(new HomeSDKPanel());
defaultPanelController.openDefaultPanel(context, devId, menuBean);
```

> **说明**：原生控制面板使用`startActivityForResult(Intent, IDefaultPanelController.REQUEST_CODE_DEFAULT_PANEL)`打开自定义`Activity`，在`onActivityResult`下判断`(requestCode == IDefaultPanelController.REQUEST_CODE_DEFAULT_PANEL && resultCode == RESULT_OK)`为`true`会关闭原生控制面板。


