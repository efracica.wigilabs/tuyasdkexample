# Native Control Panel

The native control panel supports most Tuya Smart products and functions. You can quickly integrate product capabilities powered by Tuya into devices with simple access and let users easily experience different smart devices.

## Advantages

* It supports new products in advance, and provides services when a single category cannot be supported, without submitting a new app version.
* It is native and easy to use.
* It is easy to debug in the development phase.
* Some panels supports configuration display through [DIY style UI template usage guide](https://support.tuya.com/en/help/_detail/Ka32mqup3uiiq).

<img src="https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/content-platform/hestia/162425148095e1b0a5b82.jpg" alt="light" style="zoom:33%;" />

<img src="https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/content-platform/hestia/1624251525e068068527a.jpg" alt="light" style="zoom:33%;" />

## Instructions

**dependencies**

```java
implementation 'com.tuya.smart:TuyaDefaultPanel:2.1.0'
```

**Interface description**

```java
void openDefaultPanel(Context context, String devId, MenuBean menuBean);
```

**Parameter description**

| Parameters | Description |
| ---- | ---- |
| context | Context for opening the Activity |
| devId | Device Id |
| menuBean | Used to create and display custom menus and events |

> **Note:** `menuBean` is used to create and display custom menus and events, you can set it null if you don't need it.

**MenuBean data model**

| Filed | Type | Description |
| ---- | ---- | ---- |
| menuRes | Int | Custom menu resource id |
| menuItemId | Int | Item id in custom menu |
| bundle | Bundle | Custom bundle can be obtained in custom Activity by IDefaultPanelController.BUNDLE_KEY |
| activityClassName | String | Custom activity ClassName，like TestActivity.class.getName() |

**Sample code**

```java
MenuBean menuBean = new MenuBean();
menuBean.setMenuRes(R.menu.common_menu);
menuBean.setMenuItemId(R.id.check);
Bundle bundle = new Bundle();
bundle.putString("DEV_ID_KEY", devId);
menuBean.setBundle(bundle);
menuBean.setActivityClassName(TestActivity.class.getName());
IDefaultPanelController defaultPanelController = PluginManager.service(IDefaultPanelController.class);
defaultPanelController.usePanel(new HomeSDKPanel());
defaultPanelController.openDefaultPanel(context, devId, menuBean);
```

> **Note:** The native control panel uses `startActivityForResult(Intent, IDefaultPanelController.REQUEST_CODE_DEFAULT_PANEL)` to enable the custom activity. If the `onActivityResult` shows that the `(requestCode == IDefaultPanelController.REQUEST_CODE_DEFAULT_PANEL && resultCode == RESULT_OK)` is `true`, the default panel will be closed.
