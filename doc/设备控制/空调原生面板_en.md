# Air Conditioning Control Panel

The air conditioning native control panel is a control panel developed by native code, which supports all kinds of air conditioning equipment.

## Advantages

* Lightweight, no React Native or other frameworks required.
* Support all types of devices. The new products can be supported in advance, without submitting a new app version.
* Easy for debugging in the developing stage.

<p> <img src="https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/content-platform/hestia/16242801354b5c562abe7.jpg" width = "250" / style='vertical-align:middle; display:inline;'>	<img src="https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/content-platform/hestia/162428015954fe813b404.jpg" width = "250" / style='vertical-align:middle; display:inline;'> </p>

## Instructions

**dependencies**

```java
implementation 'com.tuya.smart:TuyaAirConditionerPanel:1.0.1'
```

**Interface description**

```java
void openDefaultPanel(Context context, String devId, MenuBean menuBean);
```

**Parameter description**

| Parameters | Description |
| ---- | ---- |
| context | Context for opening the Activity |
| devId | Device Id |
| menuBean | Used to create and display custom menus and events |

> **Note:** `menuBean` is used to create and display custom menus and events, you can set it null if you don't need it.

**MenuBean data model**

| Filed | Type | Description |
| ---- | ---- | ---- |
| menuRes | Int | Custom menu resource id |
| menuItemId | Int | Item id in custom menu |
| bundle | Bundle | Custom bundle can be obtained in custom Activity by IDefaultPanelController.BUNDLE_KEY |
| activityClassName | String | Custom activity ClassName，like TestActivity.class.getName() |

**Sample code**

```java
MenuBean menuBean = new MenuBean();
menuBean.setMenuRes(R.menu.common_menu);
menuBean.setMenuItemId(R.id.check);
Bundle bundle = new Bundle();
bundle.putString("DEV_ID_KEY", devId);
menuBean.setBundle(bundle);
menuBean.setActivityClassName(TestActivity.class.getName());
IDefaultPanelController defaultPanelController = PluginManager.service(IDefaultPanelController.class);
defaultPanelController.usePanel(new HomeSDKPanel());
defaultPanelController.openDefaultPanel(context, devId, menuBean);
```

> **Note:** The native control panel uses `startActivityForResult(Intent, IDefaultPanelController.REQUEST_CODE_DEFAULT_PANEL)` to enable the custom activity. If the `onActivityResult` shows that the `(requestCode == IDefaultPanelController.REQUEST_CODE_DEFAULT_PANEL && resultCode == RESULT_OK)` is `true`, the default panel will be closed.


