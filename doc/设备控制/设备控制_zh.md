随着 IoT 设备的普及，如何安全、灵活地管理对设备的控制权限变得更加复杂。在以往简单的应用场景中，控制端 APP 仅仅需要对一个设备进行控制。但随着家庭拥有的物联网设备愈加丰富，控制端 APP 需要同时控制多个设备。另外，某些终端设备还需要提供给多人控制，例如家具式的智能排插能够支持被所有的家人打开或者关闭，因此就出现一个控制端 APP 能够控制多个设备端，或者多个用户能够相互控制多个设备的权限管理问题。由此出现了群组管理，智能场景等概念。
此章节介绍了关于设备控制的内容。


## 设备功能点

- [`DeviceBean`](https://developer.tuya.com/cn/docs/app-development/devicemanage?id=Ka6ki8r2rfiuu#title-0-%E5%8A%9F%E8%83%BD%E8%AF%B4%E6%98%8E) 类 `dps` 属性定义了设备的状态，称作数据点（DP，Data Point）或功能点。

- `dps` 数组里，每个 `key` 对应一个功能点的 `dpId`，`dpValue` 为该功能点的值。

	一款产品的功能点定义可以在 [涂鸦 IoT 平台](https://iot.tuya.com/pmg/list) 上查看。如下图：

	![image.png](https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/content-platform/hestia/1621330232a74e61398c8.png)

	更多详情，请参考 [功能定义](https://developer.tuya.com/cn/docs/iot/define-product-features?id=K97vug7wgxpoq)。

**指令格式**

发送控制指令按照以下格式：

```json
{
	"(dpId)":"(dpValue)"
}
```

**功能点示例**

假设您在涂鸦 IoT 平台上，查看到一款灯具产品的功能点有 101、102、103、104、105。其示例代码可能为：

```java
// 设置 dpId 为 101 的布尔型功能点示例，作用：开关打开
dps = {"101": true};

// 设置 dpId 为 102 的字符串型功能点示例，作用：设置 RGB 颜色为 ff5500
dps = {"102": "ff5500"};

// 设置 dpId 为 103 的枚举型功能点示例，作用：设置档位为 2 档
dps = {"103": "2"};

// 设置 dpId 为 104 的数值型功能点示例，作用：设置温度为 20°
dps = {"104": 20};

// 设置 dpId 为 105 的透传型（byte 数组）功能点示例，作用：透传红外数据为 1122
dps = {"105": "1122"};

// 多个功能合并发送
dps = {"101": true, "102": "ff5500"};

mDevice.publishDps(dps, new IResultCallback() {
		@Override
		public void onError(String code, String error) {
		// 错误码 11001 有下面几种原因：
		//1：数据类型发送格式错误，例如，String 类型格式发成 Boolean 类型数据。
		//2：不能下发只读类型 DP 数据，参考 SchemaBean getMode，"ro" 是只读类型。
		//3：Raw 格式数据发送的不是 16 进制字符串。
		}
		@Override
		public void onSuccess() {
		}
	});
```

> **注意**：发送控制命令时，请注意数据类型。例如：
>- 功能点的数据类型是数值型（ value ）时，则发送的应该是 `{"104": 25}` 而不是  `{"104": "25"}`。
>- 透传类型传输的 Byte 数组是 16 进制字符串格式，并且必须是偶数位，则发送的应该是 `{"105": "0110"}` 而不是 `{"105": "110"}`。

## 控制设备

首先需要[初始化设备控制](https://developer.tuya.com/cn/docs/app-development/devicemanage?id=Ka6ki8r2rfiuu#title-3-%E5%88%9D%E5%A7%8B%E5%8C%96%E8%AE%BE%E5%A4%87%E6%8E%A7%E5%88%B6%E7%B1%BB)，设备控制接口向设备发送功能点，改变设备状态或功能，来达到设备控制的目的。

设备控制支持局域网控制、云端控制、自动方式这三种控制通道。如果可以连接到局域网，建议优先通过局域网控制；否则，请通过云端控制。

* 局域网控制

	```java
	ITuyaDevice.publishDps(dps, TYDevicePublishModeEnum.TYDevicePublishModeLocal, callback);
	```

* 云端控制

	```java
	ITuyaDevice.publishDps(dps, TYDevicePublishModeEnum.TYDevicePublishModeInternet, callback);
	```

* 自动控制

	```java
	ITuyaDevice.publishDps(dps, TYDevicePublishModeEnum.TYDevicePublishModeAuto, callback);
	```

	或者

	```java
	ITuyaDevice.publishDps(dps, callback);
	```

	>**说明**：设备控制推荐使用 `ITuyaDevice.publishDps(dps, callback)` 调用方式。

* 指定通道控制

  ```java
  ITuyaDevice.publishDps(dps, orders, callback);
  ```

**参数说明**

| 参数| 说明|
| ---- | --- |
| dps | 设备功能点，全称为 data points，通过 JSON 字符串的格式表示，详情请请参考 [设备功能点](#DeviceFunction) 章节 |
| publishModeEnum | 设备控制方式 |
| callback | 返回控制指令是否成功的回调 |
| orders | 通道顺序，可以参考 CommunicationEnum枚举类，例如"[3 , 1]" 指定的是 优先蓝牙控制，蓝牙不在线走云端控制 |

**Java 示例**

假设开启一个灯具采用 101 设备功能点，则开灯的代码如下：

```java
mDevice.publishDps("{\"101\": true}", new IResultCallback() {
	@Override
	public void onError(String code, String error) {
		Toast.makeText(mContext, "开灯失败", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onSuccess() {
		Toast.makeText(mContext, "开灯成功", Toast.LENGTH_SHORT).show();
	}
});
```

> **注意**：
>
> * 指令下发成功并不是指设备真正操作成功，只是意味着成功发送指令。操作成功会有 DP 数据信息通过 `IDevListener onDpUpdate` 接口返回。
>
> * 指令字符串通过 `Map<String,Object>` 数据格式转成 JSON 字符串,其中 `String` 和 `Object` 是 `dpId` 和 `dpValue` 的键值对。
>
> * 指令字符串可以一次发送多个 DP 数据。

<a id="DeviceFunction"></a>

