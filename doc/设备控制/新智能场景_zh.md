智能场景分为 **一键执行场景** 和 **自动化场景** ，本文分别简称为 **场景** 和 **自动化**。

* 场景是用户添加动作，手动触发。
* 自动化是由用户设定条件，当条件触发后自动执行设定的动作。

## 功能概述

涂鸦 IoT 支持用户根据实际生活场景，通过设置气象或设备条件，当条件满足时，让一个或多个设备执行相应的任务。

| 场景管理 | 说明 |
| ---- | ---- |
|`TuyaHomeSdk.getSceneServiceInstance()`| 提供场景里条件、任务、设备、场景操作、日志、执行 |

在使用智能场景相关的接口之前，需要首先了解场景条件和场景任务这两个概念。

## 场景条件

场景条件对应 `SceneCondition` 类，涂鸦 IoT 支持以下条件类型：

- 气象条件：包括温度、湿度、天气、PM2.5、空气质量、日落日出，用户选择气象条件时，可以选择当前城市。
- 设备条件：指用户可预先选择一个设备的功能状态，当该设备达到该状态时，会触发当前场景里的任务，但同一设备不能同时作为条件和任务，避免操作冲突。
- 定时条件：指可以按照指定的时间去执行预定的任务。

`SceneCondition` 的主要属性定义如下：

| 字段           | 类型               | 描述                                                         |
| -------------- | ------------------ | ------------------------------------------------------------ |
| defaultIconUrl | String             | 条件默认图标                                                 |
| id             | String             | 条件id                                                       |
| entitySubIds   | String             | 除设备之外的其它条件标识，如：<br />温度：“**temp**”<br />湿度：“**humidity**”<br />天气：“**condition**”<br />PM2.5：“**pm25**”<br />空气质量：“**aqi**”<br />日出日落：“**sunsetrise**”<br />风速：“**windSpeed**”<br /> |
| entityType     | Int                | 条件类型，如：<br />一键执行：99<br />设备：1<br />家人回家：11<br />位置变化：10 <br />定时：6 |
| entityId       | String             | 条件为设备类型时，设备id                                     |
| expr           | List<Object>       | 条件dp表达式，如：<br />[["$temp"**,**"<"**,**-40]]          |
| iconUrl        | String             | 条件图标                                                     |
| entityName     | String             | 条件名称                                                     |
| condType       | Integer            | 条件匹配规则，如：<br />1：表达式匹配 <br />2：简单匹配 为了raw类型而加，raw类型condType为2 |
| exprDisplay    | String             | 副标题名称，如：**"温度 : 小于-40°F"**                       |
| productId      | String             | 条件为设备类型时，设备产品id                                 |
| productPic     | String             | 条件为设备类型时，设备产品图片                               |
| devDelMark     | boolean            | 条件为设备类型时，设备是否被移除                             |
| deleteDevIcon  | String             | 条件为设备类型时，设备被删除时的图标                         |
| extraInfo      | ConditionExtraInfo | 条件拓展属性                                                 |

`ConditionExtraInfo`主要属性定义如下

| 字段           | 类型               | 描述                                 |
| -------------- | ------------------ | ------------------------------------ |
| tempUnit       | String             | 当前温度单位                         |
| cityName       | String             | 城市名称                             |
| delayTime      | String             | 红外设备预置“持续时间”条件设定数值   |
| percent        | Map<String,String> | “0%-100%”条件类型dp点及对应百分值    |
| percent1       | Map<String,String> | “1%-100%”条件类型dp点及对应百分值    |
| members        | String             | 门锁条件家庭成员                     |
| timeWindow     | long               | 红外设备自定义“持续时间”条件设定数值 |
| calType        | String             | 红外设备自定义“持续时间”条件类型标记 |
| maxSeconds     | long               | 红外设备自定义“持续时间”条件最大值   |
| center         | Map<String,Double> | 地理围栏中心坐标                     |
| radius         | int                | 地理围栏半径                         |
| geotitle       | String             | 地理围栏名称                         |
| windSpeedUnit  | String             | 风速单位                             |
| originTempUnit | String             | 原始温度单位                         |
| dpScale        | int                | 华氏度与摄氏度转换系数               |

## 场景任务

场景任务是指当该场景满足已经设定的气象或设备条件时，让一个或多个设备执行某种操作，对应 `SceneAction` 类。或者关闭、开启一个自动化。

`SceneAction`的主要属性定义如下：

|字段|类型| 描述 |
| ---- | ---- | ---- |
| id | Sting | 动作 ID
| actionExecutor | String | 动作类型。枚举如下：<br>ruleTrigger：触发场景<br>ruleEnable：启用场景<br>ruleDisable：禁用场景<br>appPushTrigger：推送消息<br>mobileVoiceSend：电话服务<br>smsSend：短信服务<br>deviceGroupDpIssue：执行群组<br>irIssue：执行红外设备<br>dpIssue：执行普通设备<br>delay：延时<br>irIssueVii：执行红外设备（执行参数为真实红外控制码）<br>toggle：执行切换开关动作<br>dpStep：执行步进动作
| entityId | String | 设备 ID
| entityName | String | 设备名称
| actionDisplayNew | Map&lt;String, List&lt;String&gt;&gt; | 动作展示信息
| executorProperty | Map&lt;String, Object&gt; | 动作执行信息
| extraProperty | Map&lt;String, Object&gt; | 动作额外信息

**提供CovertCompatUtil工具类实现新旧版本sdk中数据模型的互相转换**



## 场景条件管理

### <span id="getLocalCityAll">查询城市列表</span>

**接口说明**

用于在创建天气条件时，选择城市。
注： 目前城市列表暂时仅支持中国。

```kotlin
fun getLocalCityAll(countryCode: String, callback: IResultCallback<List<LocationCity>?>?)
```

**参数说明**

| 参数        | 说明                            |
| ----------- | ------------------------------- |
| countryCode | 国家码，例如中国大陆地区为 `cn` |
| callback    | 回调                            |

**示例代码**

```kotlin

      TuyaHomeSdk.getSceneServiceInstance().conditionService().getLocalCityAll(
            "cn",  //中国大陆地区
            object : IResultCallback<List<LocationCity?>?>() {
                override fun onSuccess(result: List<LocationCity?>?) {
                  
                }
                override fun onError(errorCode: String?, errorMessage: String?) {
                  
                }
            })

```

其中, `LocationCity` 类主要属性定义如下:

| 字段     | 类型   | 描述     |
| -------- | ------ | -------- |
| area     | Sting  | 区域名称 |
| province | String | 省份名称 |
| city     | String | 城市名称 |
| cityId   | long   | cityId   |
| pinyin   | String | 城市拼音 |

### <span id="getLocalByCoordinate">根据经纬度查询城市信息</span>

**接口说明**

根据经纬度获取城市信息， 用于展示已有的天气条件。

```kotlin
fun getLocalByCoordinate(lon: String, lat: String, callback: IResultCallback<LocationCity?>?)
```

**参数说明**

| 参数     | 说明 |
| -------- | ---- |
| lon      | 经度 |
| lat      | 纬度 |
| callback | 回调 |

其中，`LocationCity`的主要属性同【查询城市列表】

**示例代码**

```kotlin

        TuyaHomeSdk.getSceneServiceInstance().conditionService().getLocalByCoordinate(
            longitude.toString(),  //经度
            latitude.toString(),  //纬度
            object : IResultCallback<LocationCity?>() {
                override fun onSuccess(result: LocationCity?) {
                  
                }
                override fun onError(errorCode: String?, errorMessage: String?) {
                  
                }
            })
```

### <span id="getLocalByCityId">根据城市id查询城市信息</span>

**接口说明**

根据城市 id 获取城市信息， 用于展示已有的天气条件。城市 id 可以在获取城市列表接口中获取。

```kotlin
fun getLocalByCityId(cityId: Long, callback: IResultCallback<LocationCity?>?)
```

**参数说明**

| 参数     | 说明    |
| -------- | ------- |
| cityId   | 城市 ID |
| callback | 回调    |

其中，`LocationCity`的主要属性同【查询城市列表】

**示例代码**

```kotlin
        TuyaHomeSdk.getSceneServiceInstance().conditionService().getLocalByCityId(
            cityId,  //城市ID
            object : IResultCallback<LocationCity?>() {
                override fun onSuccess(result: LocationCity?) {
                  
                }
                override fun onError(errorCode: String?, errorMessage: String?) {
                  
                }
            })
```

### <span id="getConditionAll">查询条件列表</span>

**接口说明**

获取条件列表，如温度、湿度、天气、PM2.5、日落日出等，注意：设备也可作为条件。
条件中的温度分为摄氏度和华氏度，根据需求传入需要的数据。

```kotlin
fun getConditionAll(
        relationId: Long,
        showFahrenheit: Boolean,
        windSpeedUnit: String? = "",
        callback: IResultCallback<ConditionItemList?>?
    )
```

**参数说明**

| 参数           | 说明                                    |
| -------------- | --------------------------------------- |
| relationId     | 家庭id                                  |
| showFahrenheit | true：使用华氏单位，false：使用摄氏单位 |
| windSpeedUnit  | 风速单位                                |
| callback       | 回调                                    |

**示例代码**

```kotlin
        TuyaHomeSdk.getSceneServiceInstance().conditionService().getConditionAll(
            homeId,  // 家庭id
            true, "", object : IResultCallback<ConditionItemList?>() {
                override fun onSuccess(result: ConditionItemList?) {
                  
                }
                override fun onError(errorCode: String?, errorMessage: String?) {
                  
                }
            })
```

其中， `ConditionItemList` 主要属性定义如下

| 字段     | 类型                      | 描述                 |
| -------- | ------------------------- | -------------------- |
| envConds | List<ConditionItemDetail> | 气象变化条件列表     |
| devConds | List<ConditionItemDetail> | 创建场景条件类型列表 |

其中，`ConditionItemDetail`主要属性定义如下

| 字段             | 类型                   | 描述                                                         |
| ---------------- | ---------------------- | ------------------------------------------------------------ |
| entitySubId      | String                 | dpId或条件类型标记("temp"，"humidity"，"condition"，"pm25"，"aqi"，"windSpeed"，"sunsetrise") |
| operators        | String                 | "<","=",">"表达式数组Json信息                                |
| id               | long                   | 条件Id                                                       |
| entityId         | String                 | 设备id或围栏id或条件类型标记("temp"，"humidity"，"condition"，"pm25"，"aqi"，"windSpeed"，"sunsetrise") |
| entityType       | int                    | 条件类型                                                     |
| entityName       | String                 | 条件名称                                                     |
| valueRangeJson   | List<List<Object>>     | dpId和dp值                                                   |
| mcGroups         | List<NewMCGroup>       | 多控组信息对象                                               |
| condCalExtraInfo | ConditionExtraInfo     | 条件拓展属性                                                 |
| property         | ConditionOuterProperty | 条件属性                                                     |

`ConditionExtraInfo`主要属性定义如下

| 字段           | 类型               | 描述                                 |
| -------------- | ------------------ | ------------------------------------ |
| tempUnit       | String             | 当前温度单位                         |
| cityName       | String             | 城市名称                             |
| delayTime      | String             | 红外设备预置“持续时间”条件设定数值   |
| percent        | Map<String,String> | “0%-100%”条件类型dp点及对应百分值    |
| percent1       | Map<String,String> | “1%-100%”条件类型dp点及对应百分值    |
| members        | String             | 门锁条件家庭成员                     |
| timeWindow     | long               | 红外设备自定义“持续时间”条件设定数值 |
| calType        | String             | 红外设备自定义“持续时间”条件类型标记 |
| maxSeconds     | long               | 红外设备自定义“持续时间”条件最大值   |
| center         | Map<String,Double> | 地理围栏中心坐标                     |
| radius         | int                | 地理围栏半径                         |
| geotitle       | String             | 地理围栏名称                         |
| windSpeedUnit  | String             | 风速单位                             |
| originTempUnit | String             | 原始温度单位                         |
| dpScale        | int                | 华氏度与摄氏度转换系数               |

`ConditionOuterProperty`主要属性定义如下

| 字段     | 类型                   | 描述            |
| -------- | ---------------------- | --------------- |
| id       | String                 | dpId      |
| property | ConditionInnerProperty | 属性详情 |

`ConditionInnerProperty`主要属性定义如下

| 字段  | 类型   | 描述                                                         |
| ----- | ------ | ------------------------------------------------------------ |
| unit  | String | 单位                                                         |
| min   | int    | 最小值                                                       |
| max   | int    | 最大值                                                       |
| step  | int    | 步长:当最大值为100步长为10，则可取值为min，min+10，min+20... |
| type  | String | 类型                                                         |
| scale | int    | 小数点转换系数                                               |

Property 是涂鸦智能中一种常用的数据结构，可以用来控制设备和其他功能。目前提供五种 Property: 数值型，枚举型，布尔型和用于定时的类型(与条件中的数值型，枚举型，布尔型相对应), 每种 Property 提供不同的访问接口。详见[规则](#rules)。

## 场景设备管理

### <span id="getConditionDeviceDpAll">查询条件设备的DP列表</span>

**接口说明**

选择场景条件时，选择了设备，需要根据选择设备的 `deviceId` 获取设备 dp 列表，进而选择某一个dp功能点，即指定该设备执行该dp功能作为该场景的执行条件。

```kotlin
fun getConditionDeviceDpAll(deviceId: String, callback: IResultCallback<List<ConditionItemDetail>?>?)
```

**参数说明**

| 参数     | 说明   |
| -------- | ------ |
| deviceId | 设备id |
| callback | 回调   |

其中，`ConditionItemDetail`的主要属性同【查询条件列表】

**示例代码**

```kotlin
        TuyaHomeSdk.getSceneServiceInstance().deviceService().getConditionDeviceDpAll(
            "devId", // 设备id
            object : IResultCallback<List<ConditionItemDetail>?>() {
                override fun onError(errorCode: String?, errorMessage: String?) {
                }

                override fun onSuccess(result: List<ConditionItemDetail>?) {
                }
            })
```

### <span id="getActionDeviceDpAll">查询动作设备的DP列表</span>

**接口说明**

选择场景动作时，选择了设备，需要根据选择设备的 `deviceId` 获取设备 dp 列表，进而选择某一个dp功能点，即指定该设备执行该dp功能作为该场景的执行动作。

```kotlin
fun getActionDeviceDpAll(deviceId: String, callback: IResultCallback<List<ActionDeviceDataPointList>?>?)
```

**参数说明**

| 参数     | 说明   |
| -------- | ------ |
| deviceId | 设备id |
| callback | 回调   |

其中 `ActionDeviceDataPointList` 主要属性定义如下:

| 字段         | 类型                              | 描述           |
| ------------ | --------------------------------- | -------------- |
| id           | long                              | 设备dpId       |
| functionType | Int                               | 设备功能类型   |
| productId    | String                            | 设备产品id     |
| functionName | String                            | 设备功能名称   |
| dataPoints   | List<ActionDeviceDataPointDetail> | 设备功能dp列表 |

其中 `ActionDeviceDataPointDetail` 主要属性定义如下:

| 字段               | 类型           | 描述                 |
| ------------------ | -------------- | -------------------- |
| dpName             | String         | 设备dp名称           |
| dpId               | int            | 设备dpId             |
| editable           | boolean        | 功能dp是否可被修改   |
| valueType          | String         | dp灯类类型           |
| dpProperty         | String         | dp值类型             |
| valueRangeJson     | String         | dp取值内容           |
| defaultValue       | Object         | dp默认值             |
| stepHighDpProperty | StepDpProperty | 固定调高的步进可取值 |
| stepLowDpProperty  | StepDpProperty | 固定调低的步进可取值 |

**示例代码**

```kotlin
        TuyaHomeSdk.getSceneServiceInstance().deviceService().getActionDeviceDpAll(
            "devId", // 设备id
            object : IResultCallback<List<ActionDeviceDataPointList>?>() {
                override fun onError(errorCode: String?, errorMessage: String?) {
                    
                }

                override fun onSuccess(result: List<ActionDeviceDataPointList>?) {
                }

            })
```

### <span id="getActionGroupDeviceDpAll">查询动作设备群组的DP列表</span>

**接口说明**

选择场景动作时，选择了设备，需要根据选择设备的 deviceId 获取设备 dp 列表，进而选择某一个dp功能点，即指定该设备执行该dp功能作为该场景的执行动作。

```kotlin
fun getActionGroupDeviceDpAll(groupDeviceId: String, callback: IResultCallback<List<ActionDeviceDataPointList>?>?)
```

**参数说明**

| 参数          | 说明       |
| ------------- | ---------- |
| groupDeviceId | 设备群组id |
| callback      | 回调       |

其中，`ActionDeviceDataPointList`的主要属性同【查询动作设备的DP列表】

**示例代码**

```kotlin
        TuyaHomeSdk.getSceneServiceInstance().deviceService().getActionGroupDeviceDpAll(
            groupDeviceId, // 设备群组id
            object : IResultCallback<List<ActionDeviceDataPointList>?>() {
                override fun onError(errorCode: String?, errorMessage: String?) {
                    TODO("Not yet implemented")
                }

                override fun onSuccess(result: List<ActionDeviceDataPointList>?) {
                    TODO("Not yet implemented")
                }
            })
```

### <span id="getConditionDeviceAll">查询条件设备列表</span>

**接口说明**

查询支持设备条件的设备列表。

```kotlin
fun getConditionDeviceAll(
        relationId: Long,
        callback: IResultCallback<List<DeviceBean?>?>?
    )
```

**参数说明**

| 参数       | 说明    |
| ---------- | ------- |
| relationId | 家庭 id |
| callback   | 回调    |

**示例代码**

```kotlin
        TuyaHomeSdk.getSceneServiceInstance().deviceService().getConditionDeviceAll(
            homeId,  // 家庭id
            object : IResultCallback<List<DeviceBean?>?>() {
                override fun onSuccess(deviceBeans: List<DeviceBean?>?) {
                  
                }
                override fun onError(errorCode: String?, errorMessage: String?) {
                  
                }
            })
```

### <span id="getConditionDeviceIdAll">查询条件设备ID列表</span>

**接口说明**

查询支持设备条件的设备Id列表。

```kotlin
fun getConditionDeviceIdAll(
        relationId: Long,
        callback: IResultCallback<List<String>?>?
    )
```

**参数说明**

| 参数       | 说明    |
| ---------- | ------- |
| relationId | 家庭 id |
| callback   | 回调    |

**示例代码**

```kotlin
  TuyaHomeSdk.getSceneServiceInstance().deviceService().getConditionDeviceIdAll(
            homeId,  // 家庭id
            object : IResultCallback<List<String?>?>() {
                override fun onSuccess(deviceIds: List<String?>?) {
                  
                }
                override fun onError(errorCode: String?, errorMessage: String?) {
                  
                }
            })
```

### <span id="getActionDeviceAll">查询动作设备列表</span>

**接口说明**

获取支持场景动作的设备列表，包含普通设备、群组设备、红外设备， 用于选择添加到要执行的动作中。

```kotlin
fun getActionDeviceAll(relationId: Long, callback: IResultCallback<ActionDeviceGroup?>?)
```

**参数说明**

| 参数       | 说明    |
| ---------- | ------- |
| relationId | 家庭 id |
| callback   | 回调    |

其中 `ActionDeviceGroup` 主要属性定义如下:

| 字段    | 类型                             | 描述         |
| ------- | -------------------------------- | ------------ |
| devices | List&lt;DeviceBean&gt;           | 普通设备列表 |
| groups   | List<GroupBean&gt;               | 群组设备列表 |
| exts    | Map<String, Map<String, String>> | 附加信息     |

**示例代码**

```kotlin
        TuyaHomeSdk.getSceneServiceInstance().deviceService().getActionDeviceAll(homeId, object : IResultCallback<ActionDeviceGroup>() {
            override fun onError(errorCode: String?, errorMessage: String?) {
            }

            override fun onSuccess(result: ActionDeviceGroup) {
            }
        })
```

### <span id="getActionDeviceIdAll">查询动作设备ID列表</span>

**接口说明**

获取支持场景动作的设备ID列表。

```kotlin
fun getActionDeviceIdAll(relationId: Long, callback: IResultCallback<ActionDeviceGroupId?>?)
```

**参数说明**

| 参数       | 说明    |
| ---------- | ------- |
| relationId | 家庭 id |
| callback   | 回调    |

其中 `ActionDeviceGroupId` 主要属性定义如下:

| 字段      | 类型                             | 描述           |
| --------- | -------------------------------- | -------------- |
| deviceIds | List&lt;String&gt;               | 普通设备ID列表 |
| groupIds  | List<Long&gt;                    | 群组设备ID列表 |
| exts      | Map<String, Map<String, String>> | 附加信息       |

**示例代码**

```kotlin
     TuyaHomeSdk.getSceneServiceInstance().deviceService().getActionDeviceIdAll(homeId, object : IResultCallback<ActionDeviceGroupId>() {
            override fun onError(errorCode: String?, errorMessage: String?) {
                
            }

            override fun onSuccess(result: ActionDeviceGroupId) {
            }

        });
```

## 场景管理

TuyaHomeSdk 提供了单个场景的创建、修改、执行、删除4种操作，除了创建其他操作需要使用场景id进行初始化，场景id可以从[获取场景列表接口](#GetSceneList)的接口中拿到

### 删除场景

**接口说明**

用于删除场景。

```kotlin
fun deleteSceneWithHomeId(relationId: Long, sceneId: String, callback: IResultCallback<Boolean>?)
```

**参数说明**

| 参数       | 说明    |
| ---------- | ------- |
| relationId | 家庭 ID |
| sceneId    | 场景 ID |
| callback   | 回调    |

**示例代码**

```kotlin
        val sceneId: String = sceneBean.getId()

        TuyaHomeSdk.getSceneServiceInstance().baseService().deleteSceneWithHomeId(
            relationId,
            sceneId,
            object : IResultCallback<Boolean?>() {
                override fun onSuccess(result: Boolean?) {
                    Log.d(TAG, "Delete Scene Success")
                }

                override fun onError(errorCode: String?, errorMessage: String?) {
                  
                }
            })
```

### <span id="GetSceneList">查询简易场景列表</span>

**接口说明**

获取场景列表数据。场景和自动化一起返回，通过条件 `conditions` 字段是否为空或者仅有一个条件且类型是一键执行来区分场景和自动化。

```kotlin
fun getSimpleSceneAll(relationId: Long, callback: IResultCallback<List<NormalScene>?>?)
```

**参数说明**

| 参数       | 说明    |
| ---------- | ------- |
| relationId | 家庭 ID |
| callback   | 回调    |

**示例代码**

```kotlin

        TuyaHomeSdk.getSceneServiceInstance().baseService().getSimpleSceneAll(
            homeId,
            object : IResultCallback<List<NormalScene?>?>() {
                override fun onSuccess(result: List<NormalScene?>?) {
                  
                }
                override fun onError(errorCode: String?, errorMessage: String?) {
                  
                }
            })
```

其中，`NormalScene`的主要属性如下

| 字段          | 类型                 | 描述                             |
| ------------- | -------------------- | -------------------------------- |
| id            | String               | 场景id                           |
| coverIcon     | String               | 一键执行的图标                   |
| name          | String               | 场景名称                         |
| conditions    | List<SceneCondition> | 条件列表                         |
| displayColor  | String               | 场景背景颜色                     |
| actions       | List<SceneAction>    | 动作列表                         |
| enabled       | boolean              | 自动化是否启用                   |
| stickyOnTop   | boolean              | 是否在首页显示                   |
| newLocalScene | boolean              | 是否是同网关下的一键执行         |
| localLinkage  | boolean              | 是否是同网关下的自动化           |
| arrowIconUrl  | String               | 场景箭头icon                     |
| preConditions | List<PreCondition>   | 场景生效时间段                   |
| fullData      | boolean              | 场景是否是全量数据               |
| outOfWork     | int                  | 场景状态：0-正常；1-失效；2-异常 |

`PreCondition`的主要属性如下

| 字段     | 类型             | 描述                |
| -------- | ---------------- | ------------------- |
| id       | String           | 条件id              |
| condType | String           | 固定值“"timeCheck"” |
| expr     | PreConditionExpr | 生效时间段详情      |

`PreConditionExpr`的主要属性如下

| 字段         | 类型   | 描述                                                         |
| ------------ | ------ | ------------------------------------------------------------ |
| loops        | String | 生效时间周期：例如"1111111"为每天,"1000001"为周末,“0111110”为周一到周五 |
| start        | String | 自定义生效开始时间：例如"00:00"                              |
| end          | String | 自定义生效结束时间：例如"23:59"                              |
| timeInterval | String | 生效时间段：例如“custom”自定义时间；“daytime”白天；“night”夜间；“allDay”全天 |
| cityId       | String | 城市ID                                                       |
| timeZoneId   | String | 时区ID                                                       |
| cityName     | String | 城市名称                                                     |

### <span id="getSceneDetail">根据 ID 查询场景详情</span>

**接口说明**

获取场景详情。

```kotlin
fun getSceneDetail(relationId: Long, sceneId: String, callback: IResultCallback<NormalScene?>?)
```

**参数说明**

| 参数       | 说明    |
| ---------- | ------- |
| relationId | 家庭 ID |
| sceneId    | 场景 ID |
| callback   | 回调    |

其中，`NormalScene`的主要属性同【查询简易场景列表】

**示例代码**

```kotlin
        TuyaHomeSdk.getSceneServiceInstance().baseService().getSceneDetail(
            homeId,
            sceneId,
            object : IResultCallback<NormalScene?>() {
                override fun onSuccess(result: NormalScene?) {
                  
                }
                override fun onError(errorCode: String?, errorMessage: String?) {
                  
                }
            })
```

### 添加场景

**接口说明**

添加场景需要传入家庭的 Id，场景名称，是否显示在首页，背景图片的 url，条件列表，任务列表（至少一个任务），前置条件列表（生效时间段，可不传，默认全天生效)，满足任一条件还是满足所有条件时执行。也可以只设置名称和任务，背景图片，不设置条件，但是需要手动执行。

```kotlin
fun saveScene(relationId: Long, sceneData: NormalScene, callback: IResultCallback<NormalScene?>?)
```
**参数说明**

|参数|说明|
| ---- | ---- |
| relationId | 家庭 Id |
| sceneData | 场景数据 |
| callback   | 回调    |

其中，`NormalScene`的主要属性同【查询简易场景列表】

**示例代码**

```kotlin
        TuyaHomeSdk.getSceneServiceInstance().baseService().saveScene(
            homeId,
            sceneData,
            object : IResultCallback<NormalScene?>() {
                override fun onSuccess(result: NormalScene?) {
                  
                }
                override fun onError(errorCode: String?, errorMessage: String?) {
                  
                }
            })
```

###  修改场景

**接口说明**

用于修改场景， 成功后会返回新的场景数据。
注：该接口只能用于修改场景，请勿传入新建的 NormalScene 对象。

```kotlin
fun modifyScene(sceneId: String, sceneData: NormalScene, callback: IResultCallback<NormalScene?>?)
```

**参数说明**

|参数|说明|
| ---- | ---- |
| sceneId | 场景 ID |
| sceneData | 修改后的场景数据 |
| callback | 回调 |

其中，`NormalScene`的主要属性同【场景列表】

**示例代码**

```kotlin
        sceneBean.setName("New name") //更改场景名称

        sceneBean.setConditions(Collections.singletonList(condition)) //更改场景条件

        sceneBean.setActions(tasks) //更改场景动作


        val sceneId: String = sceneBean.getId() //获取场景 ID 以初始化

        TuyaHomeSdk.getSceneServiceInstance().baseService().modifyScene(
            sceneId,
            sceneBean,
            object : IResultCallback<NormalScene?>() {
                override fun onSuccess(result: NormalScene?) {
                  
                }
                override fun onError(errorCode: String?, errorMessage: String?) {
                  
                }
            })
```

###  对场景或自动化进行排序

**接口说明**

用于排序场景

```kotlin
fun sortSceneList(relationId: Long, sceneIds: List<String>, callback: IResultCallback<Boolean>?)
```

**参数说明**

| 参数       | 说明         |
| ---------- | ------------ |
| relationId | 家庭 ID      |
| sceneIds   | 场景 ID 列表 |
| callback   | 回调         |

**示例代码**

```kotlin
        TuyaHomeSdk.getSceneServiceInstance().baseService().sortSceneList(
            homeId,
            sceneIds,
            object : IResultCallback<Boolean?>() {
                override fun onSuccess(result: Boolean?) {
                  
                }
                override fun onError(errorCode: String?, errorMessage: String?) {
                  
                }
            })
```

### 开启或关闭自动化场景

**接口说明**

用于开启或关闭自动化场景

```kotlin
fun enableAutomation(sceneId: String, callback: IResultCallback<Boolean>?)

fun disableAutomation(sceneId: String, callback: IResultCallback<Boolean>?)
```

**参数说明**

| 参数     | 说明    |
| -------- | ------- |
| sceneId  | 场景 ID |
| callback | 回调    |

**示例代码**

```kotlin
        val sceneId: String = sceneBean.getId()

        TuyaHomeSdk.getSceneServiceInstance().baseService().enableAutomation(
            sceneId,
            object : IResultCallback() {
                override fun onSuccess() {
                    Log.d(TAG, "enable Scene Success")
                }

                override fun onError(errorCode: String?, errorMessage: String?) {
                  
                }
            })

        TuyaHomeSdk.getSceneServiceInstance().baseService().disableAutomation(
            sceneId,
            object : IResultCallback() {
                override fun onSuccess() {
                    Log.d(TAG, "disable Scene Success")
                }

                override fun onError(errorCode: String?, errorMessage: String?) {
                  
                }
            })
```

### 定时启用自动化

**接口说明**

用于定时开启自动化场景

```kotlin
fun enableAutomationWithTime(sceneId: String, time: Int, callback: IResultCallback<Boolean>?)
```

**参数说明**

| 参数     | 说明       |
| -------- | ---------- |
| sceneId  | 场景 ID    |
| time     | 启用的时间 |
| callback | 回调       |

**示例代码**

```kotlin
        val sceneId: String = sceneBean.getId()

        TuyaHomeSdk.getSceneServiceInstance().baseService().enableAutomationWithTime(
            sceneId,
            time,
            object : IResultCallback() {
                override fun onSuccess() {
                    Log.d(TAG, "enable Scene Success")
                }

                override fun onError(errorCode: String?, errorMessage: String?) {
                  
                }
            })
```

### 获取场景数量限制

**接口说明**

用于获取场景数量限制

```kotlin
fun getCountLimit(relationId: Long, callback: IResultCallback<CountLimit?>?)
```

**参数说明**

| 参数       | 说明   |
| ---------- | ------ |
| relationId | 家庭ID |
| callback   | 回调   |

其中，`CountLimit`的主要属性如下

| 字段            | 类型 | 描述                   |
| --------------- | ---- | ---------------------- |
| automationLimit | int  | 可创建的自动化数量     |
| automationCount | int  | 已创建的自动化数量     |
| sceneLimit      | int  | 可创建的一键执行的数量 |
| sceneCount      | int  | 已创建的一键执行的数量 |

**示例代码**

```kotlin
        val sceneId = sceneBean.getId();

        TuyaHomeSdk.getSceneServiceInstance().baseService().getCountLimit(
            relationId,
            object : IResultCallback<CountLimit>() {
                override fun onError(errorCode: String?, errorMessage: String?) {
                }

                override fun onSuccess(result: CountLimit) {
                }

            });
```

### 执行场景

**接口说明**

用于执行手动场景。

>**注意**：具体设备执行成功与否，需要通过TuyaHomeSdk.newDeviceInstance(devId).registerDevListener() 监听设备的  DP  点变化。

```kotlin
fun executeScene(sceneData: NormalScene, callback: IResultCallback? = null)
```

**参数说明**

|参数|说明|
| ---- | ---- |
|sceneData|场景数据|
| callback | 回调 |

**示例代码**

```kotlin
        val sceneId: String = sceneBean.getId()
        TuyaHomeSdk.getSceneServiceInstance().executeService().executeScene(
            sceneData,
            object : IResultCallback() {
                override fun onSuccess() {
                    Log.d(TAG, "Excute Scene Success")
                }

                override fun onError(errorCode: String?, errorMessage: String?) {
                  
                }
            })
```

### 注册场景信息变更监听

**接口说明**

场景添加、编辑、删除、开启和关闭操作的监听

```kotlin
fun registerDeviceMqttListener(callback: SceneChangeCallback)
```

**参数说明**

|参数|说明|
| ---- | ---- |
| callback |场景状态监听 |

`SceneChangeCallback` 接口如下：

```kotlin
interface SceneChangeCallback {
    /**
     * 停用自动化
     */
    fun onDisableScene(sceneId: String)

    /**
     * 启用自动化
     */
    fun onEnableScene(sceneId: String)

    /**
     * 移除场景
     */
    fun onDeleteScene(sceneId: String)

    /**
     * 创建场景
     */
    fun onAddScene(sceneId: String)

    /**
     * 更新场景
     */
    fun onUpdateScene(sceneId: String)
}
```

**示例代码**

```kotlin
        TuyaHomeSdk.getSceneServiceInstance().executeService().registerDeviceMqttListener(object : SceneChangeCallback {
            override fun onDisableScene(sceneId: String) {
              
            }
            override fun onEnableScene(sceneId: String) {
              
            }
            override fun onDeleteScene(sceneId: String) {
              
            }
            override fun onAddScene(sceneId: String) {
              
            }
            override fun onUpdateScene(sceneId: String) {
              
            }
        })
```

### 注销场景信息变更监听

**接口说明**

当不需要场景信息变更监听时，注销场景监听器

```kotlin
fun unRegisterDeviceMqttListener()
```

**示例代码**

```kotlin
TuyaHomeSdk.getSceneServiceInstance().executeService().unRegisterDeviceMqttListener()
```

## 场景日志

### 查询场景执行日志

**接口说明**

进入日志界面获取日志列表

```kotlin
fun getExecuteLogAll(
        relationId: Long,
        startTime: Long,
        endTime: Long,
        size: Int,
        lastId: String?,
        lastRecordTime: Long?,
        callback: IResultCallback<ExecuteLogList?>?
    )
```

**参数说明**

| 参数           | 说明                                                         |
| -------------- | ------------------------------------------------------------ |
| relationId     | 家庭 Id                                                      |
| startTime      | 日志记录的开始时间                                           |
| endTime        | 日志记录的截止时间                                           |
| size           | 分页获取的数据条数                                           |
| lastId         | 上一次获取日志的最后一条数据的eventId（用于分页加载更多数据） |
| lastRecordTime | 上一次获取日志的最后一条数据的execTime（用于分页加载更多数据） |
| callback       | 回调                                                         |

其中，`ExecuteLogList`的主要属性如下

| 字段       | 类型                 | 描述         |
| ---------- | -------------------- | ------------ |
| totalCount | int                  | 日志条数     |
| datas      | List<ExecuteLogItem> | 日志详情列表 |

`ExecuteLogItem`的主要属性如下

| 字段          | 类型   | 描述         |
| ------------- | ------ | ------------ |
| eventId       | String | 日志id       |
| ruleId        | String | 场景id       |
| ruleName      | String | 场景名称     |
| uid           | String | 用户id       |
| execResult    | int    | 执行结果码   |
| execResultMsg | String | 执行结果信息 |
| failureCode   | int    | 错误码       |
| failureCause  | String | 错误信息     |
| execTime      | long   | 执行时间     |
| sceneType     | int    | 场景类型     |
| execMessage   | String | 执行信息     |

**代码示例**

```kotlin
        TuyaHomeSdk.getSceneServiceInstance().logService().getExecuteLogAll(
            homeId,
            startTime,
            endTime,
            size,
            lastId,
            lastRecordTime,
            object : IResultCallback<ExecuteLogList?>() {
                override fun onSuccess(result: ExecuteLogList?) {
                  
                }
                override fun onError(errorCode: String?, errorMessage: String?) {
                  
                }
            })
```

### 查询日志详情

**接口说明**

根据日志id获取单条日志详情

```kotlin
fun getExecuteLogDetail(
        relationId: Long,
        eventId: String?,
        startTime: Long,
        endTime: Long,
        returnType: Long,
        callback: IResultCallback<List<ExecuteLogDetail>?>?
    )
```

**参数说明**

| 参数       | 说明                               |
| ---------- | ---------------------------------- |
| relationId | 家庭 Id                            |
| eventId    | 日志id                             |
| startTime  | 日志记录的开始时间                 |
| endTime    | 日志记录的截止时间                 |
| returnType | 日志类型（0-全部日志；1-失败日志） |
| callback   | 回调                               |

其中，`ExecuteLogDetail`的主要属性如下

| 字段             | 类型            | 描述           |
| ---------------- | --------------- | -------------- |
| actionId         | String          | 执行的动作id   |
| actionEntityId   | String          | 执行的设备id   |
| actionEntityName | String          | 执行的设备名称 |
| executeTime      | String          | 执行时间       |
| errorCode        | String          | 错误码         |
| execStatus       | int             | 执行状态       |
| errorMsg         | String          | 错误信息       |
| actionEntityUrl  | String          | 执行的设备icon |
| actionExecutor   | String          | 执行的设备dp   |
| detail           | List<LogDetail> | 日志详情       |

其中，`LogDetail`的主要属性如下

| 字段       | 类型   | 描述         |
| ---------- | ------ | ------------ |
| detailId   | String | 日志详情id   |
| detailName | String | 日志详情名称 |
| status     | int    | 执行状态     |
| code       | String | 错误码       |
| msg        | String | 错误信息     |
| icon       | String | 图标         |

**代码示例**

```kotlin
        TuyaHomeSdk.getSceneServiceInstance().logService().getExecuteLogDetail(
            homeId,
            eventId,
            startTime,
            endTime,
            type,
            object : IResultCallback<List<ExecuteLogDetail?>?>() {
                override fun onSuccess(result: List<ExecuteLogDetail?>?) {
                  
                }
                override fun onError(errorCode: String?, errorMessage: String?) {
                  
                }
            })
```

### 查询设备日志列表

**接口说明**

提供给面板调用

```kotlin
fun getDeviceLogAll(
        relationId: Long,
        deviceId: String,
        startTime: Long,
        endTime: Long,
        size: Int,
        lastId: String?,
        lastRecordTime: Long?,
        callback: IResultCallback<ExecuteLogList?>?
    )
```

**参数说明**

| 参数           | 说明                                                         |
| -------------- | ------------------------------------------------------------ |
| relationId     | 家庭Id                                                       |
| deviceId       | 设备Id                                                       |
| startTime      | 日志记录的开始时间                                           |
| endTime        | 日志记录的截止时间                                           |
| size           | 分页获取的数据条数                                           |
| lastId         | 上一次获取日志的最后一条数据的eventId（用于分页加载更多数据） |
| lastRecordTime | 上一次获取日志的最后一条数据的execTime（用于分页加载更多数据） |
| callback       | 回调                                                         |

其中，`ExecuteLogList`的主要属性同【查询场景执行日志】

**代码示例**

```kotlin
        TuyaHomeSdk.getSceneServiceInstance().logService().getDeviceLogAll(
            homeId,
            devId,
            startTime,
            endTime,
            size,
            lastId,
            lastRecordTime,
            object : IResultCallback<ExecuteLogList?>() {
                override fun onSuccess(result: ExecuteLogList?) {
                  
                }
                override fun onError(errorCode: String?, errorMessage: String?) {
                  
                }
            })
```
## 创建示例
### 创建场景条件对象
场景条件类是 `SceneCondition`，重点关注 entityId 、 entitySubIds 、 entityType 、 expr这四个属性。

| 场景条件类型 | 说明 |
| --- | --- |
| 气象变化 | <ul><li> entityId 属性为城市 ID </li><li> entitySubIds 属性是对应气象类型枚举，比如 "humidity" </li><li> entityType 属性为 3 </li><li> expr 属性内层为一个list，比如`["$humidity", "==", "comfort"]`</li></ul>  |
| 定时 | <ul><li> entityId 属性为固定字符串 "timer" </li><li> entitySubIds 属性为固定字符串 "timer" </li><li> entityType 属性为 6</li><li> expr 属性内层是一个map，比如 `{timeZoneId = "Asia/Shanghai",loops = "0000000",time = "08:00",date = "20180308"}`</li></ul>  |
| 设备 | <ul><li> entityId 属性为设备 ID </li><li> entitySubIds 属性为 DP 功能点 ID </li><li> entityType 属性为 1 </li><li> expr 属性内层为一个list，比如`["$dp1", "==", true]`</li></ul>  |

### 组装场景条件对象 expr 表达式
`SceneCondition` 的 expr 属性描述了条件的表达式，如 **温度低于 15℃** 这样的一个条件，就可以用 expr 来描述。

expr 是一个数组（请注意，最外层一定是数组），数组中的每一个对象描述了一个条件，如 `["$temp","<",15]` 这个条件数组就描述了温度低于 15℃ 这个条件。

- 气象条件 expr 示例：

    - 温度 [["$temp","<",15]]
    - 湿度 [["$humidity","==","comfort"]]
    - 天气 [["$condition","==","snowy"]]
    - PM2.5 [["$pm25","==","fine"]]
    - 空气质量 [["$aqi","==","fine"]]
    - 日出日落 [["$sunsetrise","==","sunrise"]]

- 定时条件 expr 示例:

  定时条件使用一个map表示，例如 {timeZoneId = "Asia/Shanghai",loops = "0000000",time = "08:00",date = "20180308"}。其中 loops 中的每一位分别表示周日到周六的每一天，1 表示生效，0 表示不生效。注意这个表示定时的map也需要使用数组包起来，因为 expr 是个数组。

- 设备条件 expr 示例：

  设备条件使用一个数组表示选定的条件值。选择的条件组装的 expr 可以表示为 [["$dp1","==",true]] ,这里可以表示一个“电灯开”的条件。其中 `dp1` 是 dp+具体设备的DP功能点 ID。

### 设置场景动作
场景动作类是 `SceneAction`，重点关注三个属性 entityId 、 actionExecutor 、 executorProperty，这三个属性描述了哪个对象要做动作，做什么类型的动作，具体做什么动作。

| 场景动作类型 | 说明 |
| --- | --- |
| 设备 | <ul><li> entityId 属性对应设备的 devId </li><li>  actionExecutor 是 dpIssue </li><li> executorProperty 是一个map，比如 {"1":true} ,表示 DP  “1”，执行的动作是 true，这个可以表示灯打开等布尔类型的 DP。DP 的 ID 和 DP 可取的值可以通过“查询任务设备的 DP 列表”接口查询到。 </li></ul>  |
| 群组 | <ul><li> entityId 属性对应群组的 groupId </li><li> actionExecutor 是 deviceGroupDpIssue </li><li> 其他的属性和设备动作相同 </li></ul>  |
| 触发场景 | <ul><li> entityId 是场景 sceneId </li><li> actionExecutor 是 ruleTrigger </li><li> executorProperty 不需要 </li></ul>  |
| 启动自动化 | <ul><li> entityId 是自动化 sceneId </li><li> actionExecutor 是 ruleEnable </li><li> executorProperty 不需要 </li></ul>  |
| 禁用自动化 | <ul><li> entityId 是自动化 sceneId </li><li> actionExecutor 是 ruleDisable </li><li> executorProperty 不需要 </li></ul>  |
| 延时动作 | <ul><li> entityId 是 delay </li><li> actionExecutor 是 delay </li><li> executorProperty 是延时的时间，用一个map表示，形式和 key 为 `{"minutes":"1","seconds":"30"}` ,表示 1 分 30 秒。目前最大支持 5 小时，即 300 分钟。 </li></ul> |

**代码示例**

```kotlin
/**
 * 创建以定时作为条件,设备作为执行动作的场景
 */

var deviceId: String //设备的ID
var dpId: Int  //设备DP功能点的ID

val normalScene = NormalScene().apply {
    conditions = listOf(SceneCondition().apply {
        entityId = "timer"
        entitySubIds = "timer"
        entityType = 3
        expr = listOf(
            mutableMapOf(
                "timeZoneId" to TimeZone.getDefault().id,
                "loops" to "0000000",
                "time" to "08:00",
                "date" to "20180308"
            )
        )
    })
    actions = listOf(SceneAction().apply {
        entityId = deviceId
        actionExecutor = "dpIssue"
        executorProperty = mapOf("\$dp${dpId}" to true)
    })
}
```