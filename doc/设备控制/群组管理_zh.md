
## 功能概述

涂鸦云支持群组管理体系：可以创建群组，修改群组名称，管理群组设备，通过群组管理多个设备，解散群组。
涂鸦智能提供一些设备群组控制的接口。

## 群组创建

### 创建 Wi-Fi 群组

#### 群组列表获取

查询可以创组建群组的设备列表

```java
TuyaHomeSdk.newHomeInstance(homeId).queryDeviceListToAddGroup(groupId, productId, 
        new ITuyaResultCallback<List<GroupDeviceBean>>() {
               @Override
               public void onSuccess(List<GroupDeviceBean> arrayList) {
               }
        
               @Override
               public void onError(String errorCode, String errorMsg) {
               }
        });
```
**参数说明**

| 参数     | 说明 |
| ---- | ---- |
| homeId      | 家庭 id |
| groupId     | 群组未创建，入参 groupId 传-1；已有群组，请传实际群组 ID |
| productId   | 选择创建群组的设备的 pid |

#### 创建群组

创建一个群组

```java
TuyaHomeSdk.newHomeInstance(mHomeId).createGroup(productId, name, selectedDeviceIds, 
        new ITuyaResultCallback<Long>() {
            @Override
            public void onSuccess(Long groupId) {
                    //返回groupId
            }
        
            @Override
            public void onError(String errorCode, String errorMsg) {
            }
        });
```
**参数说明**

| 参数     | 说明 |
| ---- | ---- |
| homeId          | 家庭 id |
| productId       | 选择创建群组的设备的 pid |
| name            | 选择创建群组的名称 |
| selectedDeviceIds  | 选择的设备的 deviceId 列表 |


#### 更新保存群组

更新保存群组到云端

```java
TuyaHomeSdk.newGroupInstance(groupId).updateDeviceList(deviceIds, 
        new IResultCallback() {
        
            @Override
            public void onError(String s, String s1) {
            
            }
            
            @Override
            public void onSuccess() {
            
            }
        });
```
**参数说明**

| 参数     | 说明 |
| ---- | ---- |
| groupId         | 群组 id |
| deviceIds       | 新增或者删除选择后的设备 id 列表 |

### 创建 ZigBee 群组

支持 ZigBee 子设备、智能网关 pro 子设备、Sub-G 子设备等复用 ZigBee 网络协议的设备组建群组

#### 群组列表获取

获取可创建群组设备列表

```java
TuyaHomeSdk.newHomeInstance(homeId).queryZigbeeDeviceListToAddGroup(groupId, productId, meshId, 
          new ITuyaResultCallback<List<GroupDeviceBean>>() {
                @Override
                public void onSuccess(List<GroupDeviceBean> arrayList) {
                }
    
                @Override
                public void onError(String errorCode, String errorMsg) {
                }
          });
```
**参数说明**

| 参数     | 说明 |
| ---- | ---- |
| homeId         | 家庭 id |
| groupId        | 群组未创建，入参 groupId 传-1；已有群组，请传实际群组 ID |
| productId      | 选择创建群组的设备的 pid |
| meshId         | 选择创建群组的设备的网关 id，deviceBean.getMeshId() |

#### 创建群组

创建一个空群组

```java
TuyaHomeSdk.newHomeInstance(homeId).createZigbeeGroup(productId, meshId, name, 
        new ITuyaResultCallback<CloudZigbeeGroupCreateBean>() {
        
            @Override
            public void onSuccess(CloudZigbeeGroupCreateBean cloudZigbeeGroupCreateBean) {
                //输出结果
                long mGroupId = cloudZigbeeGroupCreateBean.getGroupId();
                String mGId = cloudZigbeeGroupCreateBean.getLocalId();
            }
        
            @Override
            public void onError(String errorCode, String errorMsg) {
                
            }
        });
```
**参数说明**

| 参数     | 说明 |
| ---- | ---- |
| homeId | 家庭 id |
| productId | 选择创建群组的设备的 pid |
| meshId | 选择创建群组的设备的网关 id（可使用 deviceBean.getMeshId() 获取）|
| name  | 选择创建群组的名称 |

#### 新增设备到群组

添加新设备到群组，主要跟固件交互，写入群组设备到网关

```java
TuyaHomeSdk.newZigbeeGroupInstance().addDeviceToGroup(meshId, selectedDeviceIds, gid, 
        new ITuyaResultCallback<ZigbeeGroupCreateResultBean>() {
       
            @Override
            public void onSuccess(ZigbeeGroupCreateResultBean zigbeeGroupCreateResultBean) {
                if (zigbeeGroupCreateResultBean != null) {
                    //新增设备成功的列表
                    if (zigbeeGroupCreateResultBean.getSuccess() != null && zigbeeGroupCreateResultBean.getSuccess().size() > 0) {
                        List<String> mAddSuccessDeviceIds = new ArrayList<>();
                        mAddSuccessDeviceIds.addAll(zigbeeGroupCreateResultBean.getSuccess());
                    }
                    //新增设备失败的列表
                    if (zigbeeGroupCreateResultBean.getFailure() != null && zigbeeGroupCreateResultBean.getFailure().size() > 0) {
                        List<String>mAddFailDeviceIds = new ArrayList<>();
                        mAddFailDeviceIds.addAll(zigbeeGroupCreateResultBean.getFailure());
                    }
                }
            }
            
            @Override
            public void onError(String errorCode, String errorMsg) {
                
            }
        });
```
**参数说明**

| 参数     | 说明 |
| ---- | ---- |
| meshId             | 选择创建群组的设备的网关 id（可使用 deviceBean.getMeshId() 获取）|
| selectedDeviceIds  | 选择新增设备的 deviceId 列表 |
| gid                | 群组的 localId 可以通过创建空群组时获得，若已有群组可通过 groupBean.getLocalId() 获取 |

#### 删除群组已有设备

删除网关中存储的群组中已有设备

```java
TuyaHomeSdk.newZigbeeGroupInstance().delDeviceToGroup(meshId, selectedDeviceIds, gid, 
        new ITuyaResultCallback<ZigbeeGroupCreateResultBean>() {
        
            @Override
            public void onSuccess(ZigbeeGroupCreateResultBean zigbeeGroupCreateResultBean) {
                if (zigbeeGroupCreateResultBean != null) {
                    //删除设备成功的列表
                    if (zigbeeGroupCreateResultBean.getSuccess() != null && zigbeeGroupCreateResultBean.getSuccess().size() > 0) {
                        List<String> mDelSuccessDeviceIds = new ArrayList<>();
                        mDelSuccessDeviceIds.addAll(zigbeeGroupCreateResultBean.getSuccess());
                    }
                    //删除设备失败的列表
                    if (zigbeeGroupCreateResultBean.getFailure() != null && zigbeeGroupCreateResultBean.getFailure().size() > 0) {
                        List<String> mDelFailDeviceIds = new ArrayList<>();
                        mDelFailDeviceIds.addAll(zigbeeGroupCreateResultBean.getFailure());
                    }
                }
            }
            
            @Override
            public void onError(String errorCode, String errorMsg) {
                
            }
        }); 
```
**参数说明**

| 参数     | 说明 |
| ---- | ---- |
| meshId             | 选择创建群组的设备的网关 id（可使用 deviceBean.getMeshId() 获取）|
| selectedDeviceIds  | 选择删除设备的 deviceId 列表 |
| gid                | 群组的 localId 可以通过创建空群组时获得，若已有群组可通过 groupBean.getLocalId() 获取 |

#### 更新保存群组

将跟网关固件群组设备增删后的结果同步更新保存到云端

```java
TuyaHomeSdk.newZigbeeGroupInstance(groupId).updateGroupDeviceList(homeId, selectedDeviceIds, 
        new IResultCallback() {
        
            @Override
            public void onError(String s, String s1) {
                
            }
            
            @Override
            public void onSuccess() {
                
            }
        });
```
**参数说明**

| 参数     | 说明 |
| ---- | ---- |
| groupId            | 群组 id |
| homeId             | 家庭 id |
| selectedDeviceIds  | 新增或者删除成功后的总设备 id 列表 |

## 群组控制

### 群组初始化

群组实例初始化

```java
ITuyaGroup mITuyaGroup= TuyaHomeSdk.newGroupInstance(groupId);
```
**参数说明**

| 参数     | 说明 |
| ---- | ---- |
| groupId         | 群组 id |

### 修改名称

修改某个群组名称

```java
TuyaHomeSdk.newGroupInstance(groupId).renameGroup(titleName, 
            new IResultCallback() {
            
            @Override
            public void onError(String s, String s1) {
            
            }
            
            @Override
            public void onSuccess() {
                
            }
        });
```
**参数说明**

| 参数     | 说明 |
| ---- | ---- |
| groupId         | 群组 id |
| titleName       | 群组名称 |

### 解散群组

解散一个群组

```java
TuyaHomeSdk.newGroupInstance(groupId).dismissGroup(new IResultCallback() {
    
            @Override
            public void onError(String s, String s1) {
            
            }
            
            @Override
            public void onSuccess() {
            
            }
        });
```
**参数说明**

| 参数     | 说明 |
| ---- | ---- |
| groupId         | 待解散的群组 id |


### 控制发送

```java
mTuyaGroup.publishDps(String command, IResultCallback listener);
```
**参数说明**

| 参数     | 说明 |
| ---- | ---- |
| command         | 控制命令 |

**代码范例**

```java
//群组开灯代码片段
LampBean bean = new LampBean();
bean.setOpen(true);
HashMap<String, Object> hashMap = new HashMap<>();
hashMap.put(STHEME_LAMP_DPID_1, bean.isOpen());
mTuyaGroup.publishDps(JSONObject.toJSONString(hashMap),callback)；
```

### 控制回调

```java
//注册群组回调事件
mITuyaGroup.registerGroupListener(new IGroupListener() {
        
            @Override
            public void onDpUpdate(long l, String s) {
            
            }
            
            @Override
            public void onGroupInfoUpdate(long l) {
            
            }
            
            @Override
            public void onGroupRemoved(long l) {
            
            }
        });

//注销群组回调事件
mITuyaGroup.unRegisterGroupListener();
```

**注意事项**

群组的发送命令返回结果，是指发送给云端成功，并不是指实际控制设备成功。 

### 查询群组

获取群组数据，需要初始化 Home（调用 getHomeDetail() 或者 getHomeLocalCache() ）之后,才能取到数据

```java
//获取制定群组数据
TuyaHomeSdk.getDataInstance().getGroupBean(long groupId);

//获取群组下设备列表
TuyaHomeSdk.getDataInstance().getGroupDeviceList(long groupId);
```

#### 数据销毁

```java
//群组数据销毁，建议退出群组控制页面的时候调用。
mITuyaGroup.onDestroy();
```