
## 概述
 
 多控关联是指设备 dp 与设备 dp 之间建立关联，生成一个多控组，当控制多控组某个建立 dp 关联的设备，组内其他设备关联的 dp 点状态同步。
 
 例如：三个二路 zigbee 子设备开关，每个开关的第一个 dp 点与另外两个开关的第一个 dp 点建立多控组，当控制其中一个开关的第一个 dp 状态为关闭状态，另外两个开关的第一个 dp 同步关闭。
 
 **目前支持 zigbee 子设备、mesh 子设备类型的开关**
 
 **支持跨 pid**
 
 **注:** 目前限制 dpCode 是 switch\_数字 、sub\_switch\_数字类型的 dp
 
 
## 功能入口

```java
ITuyaDeviceMultiControl iTuyaDeviceMultiControl = TuyaHomeSdk.getDeviceMultiControlInstance();

```

## 业务流程图
![multi_control](https://images.tuyacn.com/fe-static/docs/img/720bb10b-f0a5-4fc3-9a2d-68350b33396d.png)
 
## 获取设备 dp 信息
 
 **接口说明**
 
```java
void getDeviceDpInfoList(String devId, ITuyaDataCallback<ArrayList<DeviceDpInfoBean>> callback);

```

**参数说明**

| 参数 | 说明 |
| --- | --- |
| devId | 设备 id |
| callback | 获取结果的回调 |


**示例代码** 

```java
ITuyaDeviceMultiControl iTuyaDeviceMultiControl = TuyaHomeSdk.getDeviceMultiControlInstance();
iTuyaDeviceMultiControl.getDeviceDpInfoList(mDevId, new ITuyaDataCallback<ArrayList<DeviceDpInfoBean>>() {
    @Override
    public void onSuccess(ArrayList<DeviceDpInfoBean> result) {
        
    }

    @Override
    public void onError(String errorCode, String errorMessage) {
        ToastUtil.shortToast(mContext,errorMessage);
    }
});

```


## 查询设备下某个 dp 关联信息

**接口说明**

我们称查询当前的设备 dp 关联的多控和自动化为主设备，返回的数据关联的其他设备为附属设备

```java
void queryLinkInfoByDp(String devId, String dpId, ITuyaDataCallback<MultiControlLinkBean> callback);

```

**参数说明**

| 参数 | 说明 |
| --- | --- |
| devId | 设备 id |
| dpId | 设备 dp 点 id|
| callback | 获取结果的回调 |


**示例代码**

```java

iTuyaDeviceMultiControl.queryLinkInfoByDp(mDevId, dpId, new ITuyaDataCallback<MultiControlLinkBean>() {
    @Override
    public void onSuccess(MultiControlLinkBean result) {
        
    }

    @Override
    public void onError(String errorCode, String errorMessage) {
        ToastUtil.shortToast(mContext,errorMessage);
    }
});

```

**MultiControlLinkBean 字段信息**

| 字段 | 类型 | 描述 |
| --- | --- | --- |
| multiGroup | MultiControlLinkBean.MultiGroupBean | 已关联多控组数据结构|
| parentRules | List<MultiControlLinkBean.ParentRulesBean> | 已关联的场景自动化数据结构

**MultiControlLinkBean.MultiGroupBean 字段信息**

| 字段 | 类型 | 描述 |
| --- | --- | --- |
| uid | String | 用户 id |
| groupName | String | 多控组名称 |
| groupType | int | 多控组类型 |
| multiRuleId | String | |
| id | int | |
| ownerId | String | 家庭 id |
| enabled | boolean | 是否开启多控组 |
| status | int | |
| groupDetail | List<GroupDetailBean> | 多控组信息|


**GroupDetailBean 字段信息** 

| 字段 | 类型 | 描述 |
| --- | --- | --- |
| devId | String | 附属设备 id |
| dpName | String | 已关联的附属设备的 dp 名称|
| multiControlId | int | 多控组 id|
| dpId | int | 已关联的附属设备的 dp id |
| devName | String | 已关联的附属设备的名称 |
| enabled | boolean | 该已关联的附属设备是否可以通过多控功能控制 |
| status | int | |
| datapoints | List<MultiControlDataPointsBean> | dp 点信息|


**MultiControlDataPointsBean 字段信息** 

| 字段 | 类型 | 描述 |
| --- | --- | --- |
| code | String | dp 点标准名称（dpCode）|
| dpId | int | dp id |
| name | String | dp 名称|


**ParentRulesBean 字段信息**

| 字段 | 类型 | 描述 |
| --- | --- | --- |
| id | String | 自动化 id |
| name | String| 自动化名称 |


## 获取支持多控的设备列表

**接口说明**

```java
void getMultiControlDeviceList(long mHomeId,  ITuyaDataCallback<ArrayList<MultiControlDevInfoBean>> callback);

```

**参数说明**

| 参数 | 说明 |
| --- | --- |
| mHomeId | 家庭 id |
| callback | 获取结果的回调 |

**示例代码**

```java

iTuyaDeviceMultiControl.getMultiControlDeviceList(Constant.HOME_ID, new ITuyaDataCallback<ArrayList<MultiControlDevInfoBean>>() {
    @Override
    public void onSuccess(ArrayList<MultiControlDevInfoBean> result) {
        iMultiControlDeviceView.setData(result);
    }

    @Override
    public void onError(String errorCode, String errorMessage) {
        ToastUtil.shortToast(mContext,errorMessage);
    }
});

```

**MultiControlDevInfoBean 字段说明**

| 字段 | 类型 | 描述 |
| --- | --- | --- |
| productId | String | 产品 id|
| devId | String | 设备 id|
| iconUrl | String | 设备图标 |
| name | String | 设备名称|
| roomName | String | 房间名称 |
| inRule | boolean | 是否已经关联到主设备 |
| datapoints | List<MultiControlDataPointsBean> | dp 点信息|





## 添加、更新、删除多控组

**接口说明**

 - 提供两个重载方法，可以通过 json 格式数据更新多控组，也可以使用 MultiControlBean。通过此接口可以实现为主设备添加其他设备进入多控组，可以更新多控组名称，更新多控组的设备，删除多控组的设备
 - 添加一个新的多控组，不需要传多控组id
 - 添加一个新的多控组是把附属设备的 dp 和 主设备 dp 关联，所以 GroupDetail 中必须包含主设备的设备信息
 - groupType 必须传 1
 - 删除一个多控组需要 GroupDetail 传空即可

```java
void saveDeviceMultiControl(long mHomeId, MultiControlBean multiControlBean, ITuyaResultCallback<MultiControlBean> callback);

void saveDeviceMultiControl(long mHomeId, String json, ITuyaResultCallback<MultiControlBean> callback);

```

**参数说明**

| 参数 | 说明 |
| --- | --- |
| mHomeId | 家庭 id |
| MultiControlBean | 多控组的数据结构 |
| json | 多控组的数据结构|
| callback | 获取结果的回调 |

json 完整数据结构如下：

```json
{
    "groupName":"多控组1",
    "groupType":1,
    "groupDetail":[{"devId":"adadwfw3e234ferf41","dpId":2, "id":22, "enable":true}],
    "id":22         //多控组Id
}          

```

**示例代码**

```java

iTuyaDeviceMultiControl.saveDeviceMultiControl(Constant.HOME_ID, multiControlBean, new ITuyaResultCallback<MultiControlBean>() {
    @Override
    public void onSuccess(MultiControlBean result) {
        ToastUtil.shortToast(mContext,"success");
    }

    @Override
    public void onError(String errorCode, String errorMessage) {
        ToastUtil.shortToast(mContext,errorMessage);
    }
});

```

**MultiControlBean 字段说明**

| 字段 | 类型 | 是否可选|描述 |
| --- | --- | --- | --- |
| groupName| String| 是|多控组名称|
| groupType | int |否| 多控组类型，固定传1|
| groupDetail| List<GroupDetailBean>| 是 | 多控组设备信息|
| id |int |是| 多控组 id|


## 启用或禁用多控组

**接口说明**

```java
void enableMultiControl(long multiControlId, ITuyaResultCallback<Boolean> callback);

void disableMultiControl(long multiControlId, ITuyaResultCallback<Boolean> callback);
```

**参数说明**

| 参数 | 说明 |
| --- | --- |
| multiControlId | 多控组 id |
| callback | 获取结果的回调 |

**示例代码**

```java
iTuyaDeviceMultiControl.enableMultiControl(id, new ITuyaResultCallback<Boolean>() {
    @Override
    public void onSuccess(Boolean result) {
        ToastUtil.shortToast(mContext,"success");
    }

    @Override
    public void onError(String errorCode, String errorMessage) {
        ToastUtil.shortToast(mContext,errorMessage);
    }
});

iTuyaDeviceMultiControl.disableMultiControl( id, new ITuyaResultCallback<Boolean>() {
    @Override
    public void onSuccess(Boolean result) {
        ToastUtil.shortToast(mContext,"success");
    }

    @Override
    public void onError(String errorCode, String errorMessage) {
        ToastUtil.shortToast(mContext,errorMessage);
    }
});

```

## 查询附属设备关联详情
**接口说明**

```java
void getDeviceDpLinkRelation(String devId, ITuyaDataCallback<DeviceMultiControlRelationBean> callback);

```

**参数说明**

| 参数 | 说明 |
| --- | --- |
| devId | 设备 id |
| callback | 获取结果的回调 |

**示例代码**

```java

iTuyaDeviceMultiControl.getDeviceDpLinkRelation(devId, new ITuyaDataCallback<DeviceMultiControlRelationBean>() {
    @Override
    public void onSuccess(DeviceMultiControlRelationBean result) {
        L.d("MultiControlDeviceListPresenter",result.toString());
    }

    @Override
    public void onError(String errorCode, String errorMessage) {
        ToastUtil.shortToast(mContext,errorMessage);
    }
});

```
**DeviceMultiControlRelationBean 字段信息**


| 字段 | 类型 | 描述 |
| --- | --- | --- |
| datapoints | List<MultiControlDataPointsBean> | dp 点信息|
| mcGroups | List<McGroupsBean> | 已关联的多控组信息|
| parentRules |List<ParentRulesBean> | 已关联的场景自动化信息|
