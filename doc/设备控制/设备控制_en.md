With the popularity of IoT devices, how to safely and flexibly manage the control permissions of the devices has become more complicated. In the past simple application scenarios, the control APP only needs to control one device. However, as the Internet of Things devices owned by the family become more abundant, the control APP needs to control multiple devices at the same time. In addition, some terminal devices also need to be controlled by multiple people. For example, smart power strips can be turned on or off by all family members. Therefore, permission management problems arise. One APP can control multiple devices, or multiple users can control multiple devices with each other. As a result, concepts such as group management and smart scenes emerged.
This chapter introduces the content about device control.

## Device function point description

The DPs attribute of the [DeviceBean](https://developer.tuya.com/en/docs/app-development/devicemanage?id=Ka6ki8r2rfiuu#title-0-DeviceBean%20data%20model) class defines the device The state of the device is called a data point (DP point) or function point. Each `key` in the `dps` dictionary corresponds to the `dpId` of a function point, and `dpValue` is the value of the function point. For the definition of the respective product function points, please refer to the product functions of [Tuya Developer Platform](https://iot.tuya.com/index).

For function points, please refer to [Function Point Related Concepts](https://developer.tuya.com/en/docs/iot/configure-in-platform/function-definition/custom-functions?id=K937y38137c64)

**Command format**

Send control commands in the following format:
{"(dpId)":"(dpValue)"}

**Function point example**

The development platform can see an interface like a product

![image.png](https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/goat/20201223/649a26378ee44f83a73e64b5d7a52196.png)

According to the function point definition of the product in the background, the sample code is as follows:

```java
//Example of boolean function point with dpId set to 101 Function: switch on
dps = {"101": true};

//Example of string type function points with dpId set to 102 Function: Set RGB color to ff5500
dps = {"102": "ff5500"};

//Example of enumerated function points with dpId set to 103 Function: Set the gear to 2 gears
dps = {"103": "2"};

//Example of numerical function points with dpId set to 104 Use: set temperature to 20°
dps = {"104": 20};

//Set the transparent transmission type (byte array) function point example with dpId as 105 Function: transparent transmission infrared data is 1122
dps = {"105": "1122"};

//Multiple functions are combined and sent
dps = {"101": true, "102": "ff5500"};

mDevice.publishDps(dps, new IResultCallback() {
        @Override
        public void onError(String code, String error) {
        //Error code 11001
        //There are several situations:
        //1. The type is incorrect, for example, string format is sent as boolean data.
        //2. The read-only type of DP data cannot be sent. Refer to SchemaBean getMode "ro" is a read-only type.
        //3. The data format sent in raw format is not a hexadecimal string.
        }
        @Override
        public void onSuccess() {
        }
    });
```

> **Important**
>
> * The sending of control commands requires special attention to the data type.
>
> For example, if the data type of the function point is value (value), the control command should be `{"104": 25}` instead of `{"104": "25"}`
>
> * The byte array transmitted by the transparent transmission type is in hexadecimal string format and must be an even number
> For example, the correct format is: `{"105": "0110"}` instead of `{"105": "110"}`.

## Device control

Firstly, [Initialize device control](https://developer.tuya.com/en/docs/app-development/devicemanage?id=Ka6ki8r2rfiuu#title-4-Initialize%20device%20control) .The device control interface function is to send function points to the device to change the device state or function.

**Interface Description**

Device control supports three-channel control, LAN control, cloud control, and automatic mode (if the LAN is online, use the LAN control first, and the LAN is offline, use the cloud control)

* LAN mode

  ```java
  ITuyaDevice.publishDps(dps, TYDevicePublishModeEnum.TYDevicePublishModeLocal, callback);
  ```

* Cloud control

  ```java
  ITuyaDevice.publishDps(dps, TYDevicePublishModeEnum.TYDevicePublishModeInternet, callback);
  ```

* Automatic control

  ```java
  ITuyaDevice.publishDps(dps, TYDevicePublishModeEnum.TYDevicePublishModeAuto, callback);
  ```

  or

  ```java
  ITuyaDevice.publishDps(dps, callback);
  ```

  > **Note**: Recommend to use `ITuyaDevice.publishDps(dps, callback)`
	
* Control the device with a specific channel

  ```java
  ITuyaDevice.publishDps(dps, orders, callback);
  ```

**Parameter Description**

| Parameters | Description |
| ---- | --- |
| dps | data points, device function points, the format is json string|
| publishModeEnum | Device control mode |
| callback |Callback of whether the control command is sent successfully or not|
| orders | For the channel order, please refer to the CommunicationEnum enumeration class. For example, "[3, 1]" specifies priority Bluetooth control, and Bluetooth does not go online to cloud control. |


**Sample Code**

Assuming that the function point of the device for turning on the light is 101, the control code for turning on the light is as follows:
```java
mDevice.publishDps("{\"101\": true}", new IResultCallback() {
    @Override
    public void onError(String code, String error) {
        Toast.makeText(mContext, "Failed to turn on the light", Toast.LENGTH_SHORT).show();
    }
To
    @Override
    public void onSuccess() {
        Toast.makeText(mContext, "Turn on the light successfully", Toast.LENGTH_SHORT).show();
    }
});
```


> **Important**
>
> * Successful command delivery does not mean that the device is actually successfully operated, but only means that the command is successfully sent. If the operation is successful, the DP data information will be reported and returned through the `IDevListener onDpUpdate` interface.
>
> * The command string is converted into a json string in the data format of `Map<String,Object>` (dpId and dpValue key-value pair).
>
> * Command can send multiple DP data at once.

<a id="DeviceFunctionPoint"></a>


