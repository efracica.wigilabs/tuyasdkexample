涂鸦智能提供了基本的定时能力，支持设备定时（包括 WiFi 设备，蓝牙 Mesh 子设备，Zigbee 子设备）和群组定时。并封装了针对设备 dp 点的定时器信息的增删改查接口。APP 通过定时接口设置好定时器信息后，硬件模块会自动根据定时要求进行预订的操作。每个定时任务下可以包含多个定时器。如下图所示：
![timer](https://images.tuyacn.com/fe-static/docs/img/ec9dd5a2-8e7f-4a68-b008-f6bd47abae74.jpg)

| 类                                                           | 说明       |
| ------------------------------------------------------------ | ---------- |
| ITuyaCommonTimer                                             | 定时封装类 |
| 以下多个接口用到了 taskName 这个参数，具体可描述为一个分组，一个分组可以有多个定时器。每个定时属于或不属于一个分组，分组目前仅用于展示。例如一个开关可能有多个 dp 点，可以根据每个 dp 点设置一个定时分组，每个分组可以添加多个定时器，用于控制这个 dp 点各个时段的开启或关闭。 |            |

## 版本说明

从3.18.0版本新增一套定时接口，解决老接口存在的问题，(见[旧版接口](https://github.com/TuyaInc/tuyasmart_home_android_sdk_doc/blob/3.14.5/zh-hans/resource/Timer.md))
新版定时都在`TuyaHomeSdk.getTimerInstance()`中，

旧版定时接口在`TuyaHomeSdk.getTimerManagerInstance()` 中。

与旧版接口相比，新版接口有以下几点更新：

- 新增或更新定时器接口可以设置定时器开关状态
- 提供批量修改定时器状态接口
- 修复旧版关闭定时分组失效问题
- 提供分组定时删除功能


**升级建议**

旧版接口不再维护，建议升级到新版接口。升级需要将替换整套定时接口，使用旧版接口设置定时仍可以通过新版获取定时器列表获取。



## 增加定时任务


**接口说明**

每个设备或群组定时添加上限为30个

为 device 或 group 新增一个定时 timer 到指定的 task 下

```java
void addTimer(TuyaTimerBuilder builder, final IResultCallback callback);

```

**参数说明**

TuyaTimerBuilder 及 IResultCallback 说明

| 参数                | 说明                                                         |
| ------------------- | ------------------------------------------------------------ |
| taskName            | 定时分组名称                                                 |
| devId               | 设备 id 或群组 id                                            |
| loops               | 一周7天定时执行次数，"0000000", 每一位 0:关闭,1:开启, 从左至右依次表示: 周日 周一 周二 周三 周四 周五 周六 。"0000000" 表示只执行一次，"1111111" 表示每天执行。 |
| actions             | dps任务 json格式: {/"dps/":{},/"time/":””}，其中 raw 类型的 dp 点需要格式转换：`new String(Base64.encodeBase64(HexUtil.hexStringToBytes(dps)))` |
| status              | 初始化定时器开关状态 0：关闭，1：开启                        |
| appPush             | 是否支持定时执行结果推送                                     |
| aliasName           | 设置定时备注名                                               |
| TimerDeviceTypeEnum | 枚举，定时类型：设备定时或群组定时                           |
| callback            | 回调，返回成功或失败的结果，不能为 null                      |


**示例代码**

```java
TuyaTimerBuilder builder = new TuyaTimerBuilder.Builder()
        .taskName(mTaskName)
        .devId("efw9990wedsew")
        .deviceType(TimerDeviceTypeEnum.DEVICE)
        .actions(dps)
        .loops("1100011")
        .aliasName("Test")
        .status(1)
        .appPush(true)
        .build();
TuyaHomeSdk.getTimerInstance().addTimer(builder, new IResultCallback() {
    @Override
    public void onSuccess() {
            }

    @Override
    public void onError(String errorCode, String errorMsg) {
        
    }
});
```


## 批量修改普通定时状态或删除定时器

**接口说明**

```java
void updateTimerStatus(String devId, TimerDeviceTypeEnum deviceTimerTypeEnum, List<String> ids, TimerUpdateEnum timerUpdateEnum, final IResultCallback callback);
```

**参数说明**

| 参数                | 说明                                               |
| ------------------- | -------------------------------------------------- |
| devId               | 设备 id 或群组 id                                  |
| TimerDeviceTypeEnum | 枚举，定时类型：设备定时或群组定时                 |
| ids                 | 定时器 id 列表                                     |
| TimerUpdateEnum     | 枚举，操作类型：开启定时器、关闭定时器、删除定时器 |
| callback            | 回调，主要包括发送成功或失败的回调，不能为 null    |


**示例代码**

```java
List<String> list = new ArrayList<>();
list.add("111");
TuyaHomeSdk.getTimerInstance().updateTimerStatus(mDevId, TimerDeviceTypeEnum.DEVICE, list, TimerUpdateEnum.DELETE, new IResultCallback() {
    @Override
    public void onError(String code, String error) {
        
    }

    @Override
    public void onSuccess() {
        
    }
});
```



## 更新定时


**接口说明**

更新 device 或群组下指定 task 下的指定 timer 信息

```java
void updateTimer(TuyaTimerBuilder builder, final IResultCallback callback);

```

**参数说明**


TuyaTimerBuilder 及 IResultCallback 说明

| 参数                | 说明                                                         |
| ------------------- | ------------------------------------------------------------ |
| taskName            | 定时分组名称                                                 |
| devId               | 设备 id 或群组 id                                            |
| loops               | 一周7天定时执行次数，"0000000", 每一位 0:关闭,1:开启, 从左至右依次表示: 周日 周一 周二 周三 周四 周五 周六 。"0000000" 表示只执行一次，"1111111" 表示每天执行。 |
| actions             | dps任务 json格式: {/"dps/":{},/"time/":""}，其中 raw 类型的 dp 点需要格式转换：`new String(Base64.encodeBase64(HexUtil.hexStringToBytes(dps)))` |
| status              | 定时开关状态 0：关闭，1：开启                                |
| appPush             | 是否支持定时执行结果推送                                     |
| aliasName           | 设置定时备注名                                               |
| timerDeviceTypeEnum | 枚举，定时类型：设备定时或群组定时                           |
| callback            | 回调，返回成功或失败的结果，不能为 null                      |

**示例代码**

```java
TuyaTimerBuilder builder = new TuyaTimerBuilder.Builder()
    .timerId("1204923")
    .devId(groupId+"")
    .deviceType(TimerDeviceTypeEnum.GROUP)
    .actions(dps)
    .loops("0000000")
    .aliasName("test")
    .status(1)
    .appPush(true)
    .build();
TuyaHomeSdk.getTimerInstance().updateTimer(builder, new IResultCallback() {
    @Override
    public void onSuccess() {
        
    }

    @Override
    public void onError(String errorCode, String errorMsg) {
        
    }
});
```

## 获取定时任务下所有定时

**接口说明**

获取 device 或群组下指定的 task 下的定时 timer

```java
void getTimerList(String taskName, String devId, TimerDeviceTypeEnum deviceTimerTypeEnum, final ITuyaDataCallback<TimerTask> callback);
```

**参数说明**

| 参数                | 说明                                            |
| ------------------- | ----------------------------------------------- |
| taskname            | 定时分组，可选 为空表示获取普通定时器列表       |
| devId               | 设备 id 或群组 id                               |
| TimerDeviceTypeEnum | 枚举，定时类型：设备定时或群组定时              |
| callback            | 回调，主要包括发送成功或失败的回调，不能为 null |

回调参数 TimerTask 说明

| 参数             | 说明                 |
| ---------------- | -------------------- |
| TimerTaskStatus  | 定时器开关状态及名称 |
| ArrayList<Timer> | 单个定时器列表       |
| category         | 定时分组信息         |



**示例代码**

```java
TuyaHomeSdk.getTimerInstance().getTimerList(null, mGwId, TimerDeviceTypeEnum.DEVICE, new ITuyaDataCallback<TimerTask>() {
    @Override
    public void onSuccess(TimerTask timerTask){

    }

    @Override
    public void onError(String errorCode, String errorMessage){
        
    }
});
```



## 获取所有定时任务

**接口说明**

获取 device 或群组下所有task的定时timer

```java
void getAllTimerList(String devId, TimerDeviceTypeEnum deviceTimerTypeEnum, final ITuyaDataCallback<List<TimerTask>> callback);
```

**参数说明**

| 参数                | 说明                                            |
| ------------------- | ----------------------------------------------- |
| devId               | 设备 id 或群组 id                               |
| TimerDeviceTypeEnum | 枚举，定时类型：设备定时或群组定时              |
| callback            | 回调，主要包括发送成功或失败的回调，不能为 null |

回调参数 TimerTask 说明

| 参数             | 说明                 |
| ---------------- | -------------------- |
| TimerTaskStatus  | 定时器开关状态及名称 |
| ArrayList<Timer> | 单个定时器列表       |
| category         | 定时分组信息         |



**示例代码**

```java
TuyaHomeSdk.getTimerInstance().getAllTimerList(mGwId, TimerDeviceTypeEnum.DEVICE, new ITuyaDataCallback<List<TimerTask>>() {
    @Override
    public void onSuccess(List<TimerTask> timerTaskList){

    }

    @Override
    public void onError(String errorCode, String errorMessage){
        
    }
});
```



## 修改分类下所有定时任务状态或删除定时器

**接口说明**

```java
void updateCategoryTimerStatus(String taskName, String devId, TimerDeviceTypeEnum deviceTimerTypeEnum, TimerUpdateEnum timerUpdateEnum, IResultCallback callback);
```

**参数说明**

| 参数                | 说明                                               |
| ------------------- | -------------------------------------------------- |
| taskname            | 定时分组                                           |
| devId               | 设备 id 或群组 id                                  |
| TimerDeviceTypeEnum | 枚举，定时类型：设备定时或群组定时                 |
| TimerUpdateEnum     | 枚举，操作类型：开启定时器、关闭定时器、删除定时器 |
| callback            | 回调，主要包括发送成功或失败的回调，不能为 null    |

**示例代码**

```java
TuyaHomeSdk.getTimerInstance().updateCategoryTimerStatus("test" ,"90sdjoi3dedj33", TimerDeviceTypeEnum.DEVICE, TimerUpdateEnum.CLOSE,new IResultCallback() {
    @Override
    public void onSuccess() {
        
    }

    @Override
    public void onError(String errorCode, String errorMsg) {
        
    }
});
```