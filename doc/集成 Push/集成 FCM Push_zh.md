
对于国外的用户，需要集成 Google 的 FCM 推送服务。先参照官方文档 [将 Firebase 添加到您的 Android 项目](https://firebase.google.com/docs/android/setup?hl=zh-cn),之后配置客户端，参考 [Android 客户端配置 Firebase](https://firebase.google.com/docs/cloud-messaging/android/client?hl=zh-cn)。

## 配置 FCM 信息到 Tuya IOT 平台

请将参照文档 [**FCM注册流程**](https://docs.tuya.com/docDetail?code=K8uhkijtdvosi) 将 Sender ID、Server Key、google-services.json 文件配置到 iot 平台的“ App 工作台”-“APP SDK”  对应应用的配置的 "Push（谷歌）"中。

> [warning] 注意事项
>
> 请复制内容填写到对应的输入框中，不要配置错误，或者将 Sender ID、Server Key 配置反了

![](https://images.tuyacn.com/fe-static/docs/img/d5671e58-47c2-4f97-9651-67f950ac4843.png)

## 将注册令牌注册到涂鸦云

在继承`FirebaseInstanceIdService`类的`onTokenRefresh`方法监控注册令牌的生成，并将令牌注册到涂鸦云。

```java
		TuyaHomeSdk.getPushInstance().registerDevice(String token, String pushProvider, new IResultCallback() {
    @Override
    public void onError(String code, String error) {
    }

    @Override
    public void onSuccess() {

    }
});
```

**参数说明**

| 参数     | 说明                       |
| ---- | ---- |
| token    | fcm 生成的 token           |
| pushProvider | 注册 push 的类别  fcm 填 "fcm" |

## 接收处理消息

在继承 FirebaseMessagingService  的类中的 onMessageReceived 方法中接收处理消息

```java
public class MyFcmListenerService extends FirebaseMessagingService {
    public static final String TAG = "MyFcmListenerService";
    public static HashMap<String, Long> pushTimeMap = new HashMap<>();

    @Override
    public void onMessageReceived(RemoteMessage message) {
        Log.d(TAG, "FCM message received" + message.getData().toString());
  
    }
}
```

> message.getData() 中的内容就是收到的推送信息

## 用户解绑

在用户退出登录等需要解除应用和用户关系的操作下调用 FCM 的移除注册令牌的方法

```java
FirebaseInstanceId.getInstance().deleteInstanceId();
```

## 发送 Push

### 新增运营 Push

涂鸦IoT平台 - 运营 - App消息 - 新增消息

![](https://images.tuyacn.com/fe-static/docs/img/2d1f2abd-9d37-4ac5-8cdd-e5e3462c1916.png)

#### 设备消息 Push

涂鸦IoT平台 - 产品 - 消息推送 - 设备信息推送

更多详细操作，请参考[推送设备信息](https://developer.tuya.com/cn/docs/iot/configure-in-platform/advanced-features/alarm?id=K93ixsmlff32o).

![](https://images.tuyacn.com/fe-static/docs/img/69b4c06c-8a17-4440-91a3-a2d984fd1278.png)

## 注意事项

* 使用 FCM ,需要手机有安装 Google 服务，并且可以正常连接 Google。
