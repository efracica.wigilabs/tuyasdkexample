
## 配置信息

申请华为方式可以参考文档 [**申请华为推送**](https://developer.tuya.com/cn/docs/iot/app-development/oem-app/message-push/huawei-push?id=K96glwztgpizb)

将申请到的 app key 等信息复制到 “APP 工作台”-“APP SDK”-对应应用的配置中，

![image-20201126110659686](https://images.tuyacn.com/fe-static/docs/img/8ec0d615-4130-49de-82e5-41dec176bf75.png)

> **请勿将信息配置错误：**
>
> * 确认华为后台包名与 SDK 应用包名一致
> * 确认在华为后台已经正确配置了 SHA256 证书指纹

![image-20201126111027643](https://images.tuyacn.com/fe-static/docs/img/b40e67b8-cba0-4d4e-bb98-1067e5b2bfa3.png)

## 集成华为 SDK

涂鸦使用华为推送的 token 方式进行推送

华为推送的 SDK 接入、初始化请按照 [华为开发文档](https://developer.huawei.com/consumer/cn/doc/development/HMSCore-Guides/android-dev-process-0000001050263396) 进行接入；完成 “开发准备" 和 ""基础能力" 部分

## 创建推送使用的 channel

参考如下代码，在 Android O 以上创建涂鸦使用的推送 channel

```java
// channel id
public static final String[] channelIds = {
  "tuya_common",
  "tuya_shortbell",
  "tuya_longbell",
  "tuya_doorbell"
};
// channel name
public static final String[] channelNames = {
  context.getString(R.string.push_channel_common),
  context.getString(R.string.push_channel_shortbell),
  context.getString(R.string.push_channel_longbell),
  context.getString(R.string.push_channel_doorbell)
};
// 通知渠道的自定义声音文件
public static final String[] channelSounds = {
  "android.resource://" + context.getPackageName() + "/" + R.raw.tuya_common,
  "android.resource://" + context.getPackageName() + "/" + R.raw.tuya_shortbell,
  "android.resource://" + context.getPackageName() + "/" + R.raw.tuya_longbell,
  "android.resource://" + context.getPackageName() + "/" + R.raw.tuya_doorbell
};

```

```java
for (int i = 0; i < TuyaPushChnnels.channelIds.length; i++) {
  createNotificationChannel(channelIds[i], channelNames[i], importance, soundPath);
}
```

```java
@TargetApi(Build.VERSION_CODES.O)
private static void createNotificationChannel(String channelId, String channelName, int importance, String soundPath) {
    NotificationChannel channel = new NotificationChannel(channelId, channelName, importance);
    channel.setSound(Uri.parse(soundPath), Notification.AUDIO_ATTRIBUTES_DEFAULT);
    channel.setVibrationPattern(new long[]{
            300, 1000, 300, 1000
    });
    channel.canBypassDnd();
    channel.setBypassDnd(true);
    channel.setLockscreenVisibility(VISIBILITY_SECRET);
    NotificationManager notificationManager = (NotificationManager) MicroContext.getApplication().getSystemService(
            NOTIFICATION_SERVICE);
    notificationManager.createNotificationChannel(channel);
}
```



## 注册 pushToken 到涂鸦云

在完成接入和初始化工作后，便可以获取 [华为推送的 token](https://developer.huawei.com/consumer/cn/doc/development/HMSCore-Guides/android-client-dev-0000001050042041#ZH-CN_TOPIC_0000001050042041__section876955375919)，将 token 注册到涂鸦云

```java
public static final String PUSH_PROVIDER_HUAWEI = "huawei";
String pushtoken = HmsInstanceId.getInstance(activity).getToken(appId, "HCM");
if (!TextUtils.isEmpty(pushtoken)) {
    Log.i(TAG, "get token:" + pushtoken);
    TuyaHomeSdk.getPushInstance().registerDevice(pushtoken, PUSH_PROVIDER_HUAWEI, new IResultCallback() {
        @Override
        public void onError(String code, String error) {
            Log.e(TAG, "registerDevice error: " + code + "   " + error);
        }

        @Override
        public void onSuccess() {
            Log.i(TAG, "register push token success");
        }
    });
}
```

在应用的启动页的 activity（例如 splash activity）配置：

```xml
<intent-filter>
  <action android:name="android.intent.action.VIEW" />
  <category android:name="android.intent.category.DEFAULT" />
  <category android:name="android.intent.category.BROWSABLE" />
  <data
        android:host="包名"
        android:path="/deeplink"
        android:scheme="@string/tuya_jump_scheme" />
</intent-filter>
```

* 包名：填写你应用的包名

* tuya_jump_scheme：填写渠道标识符的内容

![image-20201126113336549](https://images.tuyacn.com/fe-static/docs/img/d1286039-dfce-407c-a908-719cd73df9f6.png)

完成后，在设备等触发消息推送后，通知栏就会有相应的推送显示。

## 解析自定义数据

当点击通知栏的通知后，华为自定义数据在启动页（上面配置了 intent-filter）的 activity 的 intent 中可以获取自定义的数据，获取数据的key值为约定好的"hw_link"

```java
Intent intent = getIntent();
if (intent != null) {
    String message = intent.getStringExtra("hw_link");
    if (!TextUtils.isEmpty(pushLink)) {
       String decodeMessage = new String(Base64.decodeBase64(message.getBytes()));
       // TODO
    }
}
```



## 常见问题

1. 接收不到推送

  * 确认华为后台 sha256 证书是否配置正确，是否与调试的应用的证书一致
  * 使用华为后台发送消息看是否能收到
  * 其他集成情况请参照华为的帮助文档

  
