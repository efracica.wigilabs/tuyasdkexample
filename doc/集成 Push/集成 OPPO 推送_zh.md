
## 配置信息

申请 OPPO 方式可以参考文档 [**申请 OPPO 推送**](https://developer.tuya.com/cn/docs/iot/app-development/oem-app/message-push/oppo-push?id=K96glwu3xlpaf)

将申请到的 app key 等信息复制到 “APP 工作台”-“APP SDK”-对应应用的配置中，

![image-20201126113552817](https://images.tuyacn.com/fe-static/docs/img/ef9fa7a4-7496-456f-a231-5245824daf72.png)

> **请勿将信息配置错误：**
>
> * 确认 OPPO 后台包名与 SDK 应用包名一致
> * 确认在 OPPO 后台 通知栏推送处于 "已开通"状态

![image-20201126114351242](https://images.tuyacn.com/fe-static/docs/img/6385c300-8bb7-43d6-b67a-39b762e8062f.png)

## 集成 OPPO SDK

涂鸦使用OPPO推送的 regId 方式进行推送

OPPO推送的 SDK 接入、初始化请按照 [OPPO开发文档](https://open.oppomobile.com/wiki/doc#id=10704) 进行接入；完成 “SDK 集成” 

## 创建推送使用的 channel

参考如下代码，在 Android O 以上创建涂鸦使用的推送 channel

```java
// channel id
public static final String[] channelIds = {
  "tuya_common",
  "tuya_shortbell",
  "tuya_longbell",
  "tuya_doorbell"
};
// channel name
public static final String[] channelNames = {
  context.getString(R.string.push_channel_common),
  context.getString(R.string.push_channel_shortbell),
  context.getString(R.string.push_channel_longbell),
  context.getString(R.string.push_channel_doorbell)
};
// 通知渠道的自定义声音文件
public static final String[] channelSounds = {
  "android.resource://" + context.getPackageName() + "/" + R.raw.tuya_common,
  "android.resource://" + context.getPackageName() + "/" + R.raw.tuya_shortbell,
  "android.resource://" + context.getPackageName() + "/" + R.raw.tuya_longbell,
  "android.resource://" + context.getPackageName() + "/" + R.raw.tuya_doorbell
};

```

```java
for (int i = 0; i < TuyaPushChnnels.channelIds.length; i++) {
  createNotificationChannel(channelIds[i], channelNames[i], importance, soundPath);
}
```

```java
@TargetApi(Build.VERSION_CODES.O)
private static void createNotificationChannel(String channelId, String channelName, int importance, String soundPath) {
    NotificationChannel channel = new NotificationChannel(channelId, channelName, importance);
    channel.setSound(Uri.parse(soundPath), Notification.AUDIO_ATTRIBUTES_DEFAULT);
    channel.setVibrationPattern(new long[]{
            300, 1000, 300, 1000
    });
    channel.canBypassDnd();
    channel.setBypassDnd(true);
    channel.setLockscreenVisibility(VISIBILITY_SECRET);
    NotificationManager notificationManager = (NotificationManager) MicroContext.getApplication().getSystemService(
            NOTIFICATION_SERVICE);
    notificationManager.createNotificationChannel(channel);
}
```



## 注册 registerID 到涂鸦云

OPPO 推送使用 registerID 

```java
HeytapPushManager.register(context, appKey, appSecret, mPushCallback);

// 在 mPushCallBack onRegister 成功后将 registerID 注册到 涂鸦云
  
/**
 * 注册的结果,如果注册成功,registerID就是客户端的唯一身份标识
 * @param code
 * @param registerID
 */
@Override
public void onRegister(int code, String registerID) {
    if (code == 0) {
        L.d(TAG, "register success : registerId:" + registerID);
        registerAilasToTuya(registerID);
    } else {
        L.d(TAG, "register fail : code= " + code + ",  msg= " + registerID);
    }
}
```

```java
public static final String PUSH_PROVIDER_OPPO = "oppo";

private void registerAilasToTuya(String aliasId) {
    TuyaHomeSdk.getPushInstance().registerDevice(aliasId, PUSH_PROVIDER_OPPO, new IResultCallback() {
        @Override
        public void onError(String s, String s1) {
            L.e(TAG, "register device failed" + s + "   " + s1);
        }

        @Override
        public void onSuccess() {
            L.d(TAG, "---- register device to tuya success --");
        }
    });
}
```

在应用的启动页的 activity（例如 splash activity）配置：

```xml
<intent-filter>
  <action android:name="android.intent.action.VIEW" />
  <category android:name="android.intent.category.DEFAULT" />
  <category android:name="android.intent.category.BROWSABLE" />
  <data
        android:host="包名"
        android:path="/deeplink"
        android:scheme="@string/tuya_jump_scheme" />
</intent-filter>
```

* 包名：填写你应用的包名

* tuya_jump_scheme：填写渠道标识符的内容

![image-20201126113336549](https://images.tuyacn.com/fe-static/docs/img/d1286039-dfce-407c-a908-719cd73df9f6.png)

完成后，在设备等触发消息推送后，通知栏就会有相应的推送显示。


>* **OPPO 手机的通知栏权限默认是关闭的，需要进入手机设置中，将通知栏权限手动打开**

## 解析自定义数据

当点击通知栏的通知后，OPPO 通道自定义数据在启动页（上面配置了 intent-filter）的 activity 的 intent 中可以获取自定义的数据，获取数据的key值为约定好的"custom"

```java
Intent intent = getIntent();
if (intent != null) {
    String message = intent.getStringExtra("custom");
    if (!TextUtils.isEmpty(message)) {
       // TODO
       Log.d(TAG, "custom message: " + message);
    }
}
```

