
## 配置信息

申请小米方式可以参考文档 [**申请小米推送**](https://developer.tuya.com/cn/docs/iot/app-development/oem-app/message-push/xiaomi-push?id=K96glwif365hf)

将申请到的 app key 等信息复制到 “APP 工作台”-“APP SDK”-对应应用的配置中，

![image-20201126100257643](https://images.tuyacn.com/fe-static/docs/img/eea584f1-b509-4ef9-ac76-bd80e2447526.png)

> **请勿将信息配置错误**：
>
> * 确认小米后台包名与 SDK 应用包名一致
> * 确认在小米后台 “通知类别”中要创建 
> 	* tuya_common
> 	* tuya_doorbell
> 	* tuya_longbell
> 	* tuya_shortbell 通道

![image-20201126100635310](https://images.tuyacn.com/fe-static/docs/img/52f516f0-28a0-4186-a0f1-afe1e2465486.png)

## 集成小米 SDK

涂鸦使用小米推送的 regId 方式进行推送

小米推送的 SDK 接入、初始化请按照 [小米开发文档](https://dev.mi.com/console/doc/detail?pId=41#_1_3) 进行接入；完成 “SDK 接入指南部分” 

## 创建推送使用的 channel

参考如下代码，在 Android O 以上创建涂鸦使用的推送 channel

```java
// channel id
public static final String[] channelIds = {
  "tuya_common",
  "tuya_shortbell",
  "tuya_longbell",
  "tuya_doorbell"
};
// channel name
public static final String[] channelNames = {
  context.getString(R.string.push_channel_common),
  context.getString(R.string.push_channel_shortbell),
  context.getString(R.string.push_channel_longbell),
  context.getString(R.string.push_channel_doorbell)
};
// 通知渠道的自定义声音文件
public static final String[] channelSounds = {
  "android.resource://" + context.getPackageName() + "/" + R.raw.tuya_common,
  "android.resource://" + context.getPackageName() + "/" + R.raw.tuya_shortbell,
  "android.resource://" + context.getPackageName() + "/" + R.raw.tuya_longbell,
  "android.resource://" + context.getPackageName() + "/" + R.raw.tuya_doorbell
};

```

```java
for (int i = 0; i < TuyaPushChnnels.channelIds.length; i++) {
  createNotificationChannel(channelIds[i], channelNames[i], importance, soundPath);
}
```

```java
@TargetApi(Build.VERSION_CODES.O)
private static void createNotificationChannel(String channelId, String channelName, int importance, String soundPath) {
    NotificationChannel channel = new NotificationChannel(channelId, channelName, importance);
    channel.setSound(Uri.parse(soundPath), Notification.AUDIO_ATTRIBUTES_DEFAULT);
    channel.setVibrationPattern(new long[]{
            300, 1000, 300, 1000
    });
    channel.canBypassDnd();
    channel.setBypassDnd(true);
    channel.setLockscreenVisibility(VISIBILITY_SECRET);
    NotificationManager notificationManager = (NotificationManager) MicroContext.getApplication().getSystemService(
            NOTIFICATION_SERVICE);
    notificationManager.createNotificationChannel(channel);
}
```



## 注册 regId 到涂鸦云

在 ``` extends PushMessageReceiver ``` 的类中获取

```java
public static final String PUSH_PROVIDER_MI = "mi";

@Override
public void onCommandResult(Context context, MiPushCommandMessage message) {
    L.i(TAG, "onCommandResult: " + message.toString());

    String command = message.getCommand();
    List<String> arguments = message.getCommandArguments();
    String cmdArg1 = ((arguments != null && arguments.size() > 0) ? arguments.get(0) : null);
    if (MiPushClient.COMMAND_REGISTER.equals(command)) {
        if (message.getResultCode() == ErrorCode.SUCCESS) {
            mRegId = cmdArg1;
            Log.i(TAG, "mi push regid: " + mRegId);
            if (!TextUtils.isEmpty(mRegId)) {
                // 注册 regId 到涂鸦云，涂鸦云使用注册的 regId 进行推送
                TuyaHomeSdk.getPushInstance().registerDevice(mRegId, PUSH_PROVIDER_MI, new IResultCallback() {
                    @Override
                    public void onError(String s, String s1) {
                        Log.e(TAG, "register device error: " + s + "  " + s1);
                    }

                    @Override
                    public void onSuccess() {
                        Log.d(TAG, "register device success");
                    }
                });
            } else {
                Log.e(TAG, "mRegId is empty!");
            }

        }
    } else{
        // TODO
    }
}
```

完成后，在设备等触发消息推送后，通知栏就会有相应的推送显示。

## 解析自定义数据

当点击通知栏的通知后，在小米 ```extends PushMessageReceiver``` 的 `onNotificationMessageClicked` 中获取自定义消息，获取数据的key值为约定好的"mi_link"

```java
@Override
public void onNotificationMessageClicked(Context context, MiPushMessage message) {
  Log.i(TAG, "onNotificationMessageClicked: " + message.toString());
  String message = message.getExtra().get("mi_link");
  if (!TextUtils.isEmpty(message)) {
    // TODO
     Log.d(TAG, "mi_link: " + message);
  }
} 
```

