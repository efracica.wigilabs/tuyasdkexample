
The domestic Push function is developed based on Umeng's push. Please refer to the [Umeng Document](https://developer.umeng.com/docs/66632/detail/66744)to integrate Umeng into the project. Tuya Cloud supports Umeng third-party channel. If Xiaomi, Huawei and Meizu channels are required, a user can apply to each platform for initialization according to Umeng Document.

### Configuration app information

Copy the information such as “app key” to the “APP Workbench” - “APP SDK” - the configuration of the corresponding application.


>   **Do not configure the information incorrectly:**
>
>   - **Confirm that the Friend League background package name is the same as the sdk application package name**
>   - **Umeng Message Secret and App Master Secret don't be reversed**
>   - **You Union background "Enable server IP address" needs to be manually closed**
<img src="https://images.tuyacn.com/fe-static/docs/img/cdd9ee40-dfbb-4a42-835e-15b2c6f7a7bf.png" style="zoom: 33%;" />

### Set up a user alias

After Umeng has been ensured to integrate into the project, the user id is set through the Umeng SDK, and the message is pushed to the user according to the user id.

```java
/**
@param aliasId Alias		
1.It can be the unique id that your app automatically generates for each user.

* 2. It can also be the user id obtained from the third party platform when the user logs in with a third-party platform. Refer to the Umeng Document for details.

* 3.You can also use the methods in the tuya SDK to get a unique id. PhoneUtil.getDeviceID(context),PhoneUtilis located in com.tuya.smart.android.common.utils.

* @param ALIAS_TYPE		fill in"TUYA_SMART"

*/
mPushAgent.setAlias("zhangsan@sina.com", "TUYA_SMART", new UTrack.ICallBack() {
     @Override
     public void onMessage(boolean isSuccess, String message) {

     }
});
```
### Push Tuya Cloud Registration

Register aliasId to the Tuya Cloud
```java
/**
* @param aliasId   user alias, the alias as the user alias used in registering Umeng in Step 2
* @param pushProvider   registered push category   It is filled in"umeng”

*/
TuyaHomeSdk.getPushInstance().registerDevice(String aliasId, String pushProvider, new IResultCallback() {
     @Override
     public void onError(String code, String error) {

     }
     @Override
     public void onSuccess() {

     }
});
```

### Third-party channel setting

If users use the Umeng third-party channel, the pop-up activity must be named SpecialPushActivity.   Take Xiaomi as an example, SpecialPushActivity inherits from UmengNotifyClickActivity, and the complete package name path is com.activity.SpecialPushActivity.

**Push message reception**

For how to use custom messages, please refer to the Umeng documentation.
```java
UmengMessageHandler messageHandler = new UmengMessageHandler(){
    @Override
    public void dealWithCustomMessage(final Context context, final UMessage msg) {
        new Handler(getMainLooper()).post(new Runnable() {

            @Override
            public void run() { 
                Toast.makeText(context, msg.custom, Toast.LENGTH_LONG).show();
            }
        });
    }
};

mPushAgent.setMessageHandler(messageHandler);
```
**Description**

- msg.custom contains the received push information.
- The specific protocol format of msg.custom: custom=tuya://message?a=view&ct="title"&cc="content"&p={}&link=tuyaSmart%3A%2F%2Fbrowser%3Furl%3Dhttp%253A%252F%252Fwww.baidu.com;
- The data is parsed with Uri uri = Uri.parse(message)  to obtain data.

### User unbind

**Description**

When the user needs to unburden the application and user relationship, such as logging out, user can invoke the removal alias method by Umeng.
```java
/**
* @param aliasId Alias		user-generated unique id
* @param ALIAS_TYPE		fill in"TUYA_SMART"
*/
mPushAgent.deleteAlias(aliasId, "TUYA_SMART", new UTrack.ICallBack() {
    @Override
    public void onMessage(boolean isSuccess, String message) {
    }

});
```
### Send push

### Add Notification push

Tuya IoT Platform -> Operation -> App Message ->New Notification

![](https://images.tuyacn.com/fe-static/docs/img/96b52466-1436-406a-a836-8f27247d3f75.png)

### Add device notification

Tuya IoT Platform -> Product -> Notifications -> Create device message notification

![](https://images.tuyacn.com/fe-static/docs/img/9e37be2e-9c88-4272-94e5-6265baaf18b8.png)

For more information, please check [Device Notification](https://developer.tuya.com/en/docs/iot/configure-in-platform/advanced-features/alarm?id=K93ixsmlff32o).


