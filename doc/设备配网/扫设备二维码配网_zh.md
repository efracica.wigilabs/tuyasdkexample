该功能只适用于已连接互联网的设备。

## 配网流程

<img src="https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/goat/20201228/5cac5a133f8a458a922412076a209808.png" width="500">

## 查询设备 UUID

```java
Map<String, Object> postData = new HashMap<>();
//二维码扫码得到的URL
postData.put("code", url);

TuyaHomeSdk.getRequestInstance().requestWithApiNameWithoutSession("tuya.m.qrcode.parse", "4.0", postData, String.class, new ITuyaDataCallback<String>() {
	@Override
	public void onSuccess(String result) {
	  //从result中得到uuid
	  Log.i("TAG" , result);
	}

	@Override
	public void onError(String errorCode, String errorMessage) {
		Log.i("TAG" , errorCode);
	}
});
```

## 初始化配网参数

```java
TuyaQRCodeActivatorBuilder builder = new TuyaQRCodeActivatorBuilder()
		.setUuid(uuid)
		.setHomeId(homeId)
		.setContext(mActivity)
		.setTimeOut(timeout)
		.setListener(new ITuyaSmartActivatorListener() {

				@Override
				public void onError(String errorCode, String errorMsg) {

				}

				@Override
				public void onActiveSuccess(DeviceBean devResp) {

				}

				@Override
				public void onStep(String step, Object data) {

				}
			}
		));
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| uuid | 设备 UUID，可通过扫设备二维码获取 |
| homeId | 家庭 ID，详情参考 [家庭管理](https://developer.tuya.com/cn/docs/app-development/homemanage?id=Ka6kjkgere4ae) 章节 |
| timeout | 配网的超时时间设置，默认是 100s，单位: 秒 |

## 配网方法调用

```java
ITuyaActivator mTuyaActivator = TuyaHomeSdk.getActivatorInstance().newQRCodeDevActivator(builder);
//开始配网
mTuyaActivator.start();
//停止配网
mTuyaActivator.stop();
//销毁
mTuyaActivator.onDestory();
```