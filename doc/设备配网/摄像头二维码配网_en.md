## Camera scan code network configuration

### Description

Use the camera device to scan the QR code of the app to transfer the Configuration information to active and bind the camera device.

![image.png](https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/goat/20201228/1c089f2df8cc4749afc34c46dd4d29bd.png)

### Initialize parameters

```java
TuyaCameraActivatorBuilder builder = new TuyaCameraActivatorBuilder()
		.setContext(context)
		.setSsid(ssid)
		.setPassword(password)
		.setToken(token)
		.setTimeOut(timeout)
		.setListener(new ITuyaSmartCameraActivatorListener() {
			@Override
			public void onQRCodeSuccess(String qrcodeUrl) {
				//Return URL link to generate QR code
			}

			@Override
			public void onError(String errorCode, String errorMsg) {
				//Network configuration failed
			}

			@Override
			public void onActiveSuccess(DeviceBean devResp) {
			//Network configuration succeed
			}
		}));
```

**Parameters**

| Parameters | Description                                                  |
| ---------- | ------------------------------------------------------------ |
| token      | Activation key required for Configuration                    |
| context    | context                                                      |
| ssid       | WiFi ssid                                                    |
| password   | WiFi password                                                |
| timeout    | Configuration timeout, default setting is 100s, unit is second |

**Get Network Configuration Token**

Before the Wi-Fi network configuration, the SDK needs to obtain the network configuration Token from the Tuya IoT.

The term of validity of Token is 10 minutes, and the Token becomes invalid once the network configuration succeeds.

A new Token has to be obtained if you have to reconfigure the network.

```java
TuyaHomeSdk.getActivatorInstance().getActivatorToken(homeId,
		new ITuyaActivatorGetToken() {

			@Override
			public void onSuccess(String token) {

			}

			@Override
			public void onFailure(String s, String s1) {

			}
		});
```

**Parameters**

| Parameters | Description                                                  |
| ---------- | ------------------------------------------------------------ |
| homeId     | Family ID, please refer to the family management section for details |

### Configuration Method Invocation

* Instance class

  ```java
  ITuyaCameraDevActivator mTuyaActivator = TuyaHomeSdk.getActivatorInstance().newCameraDevActivator(builder);
  ```

* Get QR code URL link

  ```java
  mTuyaActivator.createQRCode(); //Return via onQRCodeSuccess callback
  ```

* Generate QR code based on URL

  Examples: Need to implement zxing (implementation `com.google.zxing:core:3.2.1`)

  ```java
  public static Bitmap createQRCode(String url, int widthAndHeight)
  			throws WriterException {
  		Hashtable hints = new Hashtable();
  		hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
  		hints.put(EncodeHintType.MARGIN,0);
  		BitMatrix matrix = new MultiFormatWriter().encode(url,
  				BarcodeFormat.QR_CODE, widthAndHeight, widthAndHeight, hints);
  
  		int width = matrix.getWidth();
  		int height = matrix.getHeight();
  		int[] pixels = new int[width * height];
  
  		for (int y = 0; y < height; y++) {
  			for (int x = 0; x < width; x++) {
  				if (matrix.get(x, y)) {
  					pixels[y * width + x] = BLACK;
  				}
  			}
  		}
  		Bitmap bitmap = Bitmap.createBitmap(width, height,
  				Bitmap.Config.ARGB_8888);
  		bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
  		return bitmap;
  	}
  ```

* Start configuration

  ```java
  mTuyaActivator.start();
  ```

* Stop configuration

  ```java
  mTuyaActivator.stop();
  ```

* Exit the page to destroy some cache data and monitoring data

  ```java
  mTuyaActivator.onDestory();
  ```

###