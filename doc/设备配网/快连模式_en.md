## Process of pairing in Wi-Fi EZ mode

<!--```sequence

Title: Wi-Fi EZ Mode

participant App
participant SDK
participant Device
participant Service

Note over App: Connect Wi-Fi of router
Note over Device: Switch to EZ mode

App->SDK: Request token
SDK->Service: Request token
Service->SDK: Return token
SDK->App: Return token

App->SDK: Start pairing (send pairing data)
Note over SDK: Broadcast pairing data
Device->Device: Get pairing data

Device->Service: Activate device
Service->Device: Pairing succeeds

Device->SDK: Pairing succeeds
SDK->App: Pairing succeeds
```-->
![image.png](https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/content-platform/hestia/1625643965a0a632fbe62.png)

### Initialize parameters

```java
ActivatorBuilder builder = new ActivatorBuilder()
		.setSsid(ssid)
		.setContext(context)
		.setPassword(password)
		.setActivatorModel(ActivatorModelEnum.TY_EZ)
		.setTimeOut(timeout)
		.setToken(token)
		.setListener(new ITuyaSmartActivatorListener() {

				@Override
				public void onError(String errorCode, String errorMsg) {

				}

				@Override
				public void onActiveSuccess(DeviceBean devResp) {
					//If multiple devices are activated at the same time, they will be called back multiple times
				}

				@Override
				public void onStep(String step, Object data) {

				}
			}
		));
```

**Parameters**

| Parameters     | Description                                                  |
| -------------- | ------------------------------------------------------------ |
| token          | Token required for pairing                    |
| context        | context                                                      |
| ssid           | Wi-Fi SSID                                                    |
| password       | Wi-Fi password                                                |
| activatorModel | Pairing in EZ mode: ActivatorModelEnum.TY_EZ        |
| timeout        | Pairing timeout. Unit: seconds. Default value: 100. |

**Get a pairing token**

Before pairing in Wi-Fi EZ mode, the SDK needs to get the token from the Tuya IoT Cloud.

The token is valid for 10 minutes, and it becomes invalid immediately after pairing succeeds.

A new token has to be obtained if the device needs to be paired again.

```java
TuyaHomeSdk.getActivatorInstance().getActivatorToken(homeId,
		new ITuyaActivatorGetToken() {

			@Override
			public void onSuccess(String token) {

			}

			@Override
			public void onFailure(String s, String s1) {

			}
		});
```

**Parameters**

| Parameter | Description                                                  |
| ---------- | ------------------------------------------------------------ |
| homeId     | Home ID. For more information, see the home management section. |

### Call the pairing method

```java
ITuyaActivator mTuyaActivator = TuyaHomeSdk.getActivatorInstance().newMultiActivator(builder);
// Start pairing
mTuyaActivator.start();
// Stop pairing
mTuyaActivator.stop();
// Exit the page to destroy some cache data and monitoring data.
mTuyaActivator.onDestroy();
```

###