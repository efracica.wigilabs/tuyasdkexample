## Process of pairing in AP mode
<!--```sequence

Title: AP Mode

participant App
participant SDK
participant Device
participant Service

Note over Device: Switch to AP mode
App->SDK: Request token
SDK->Service: Request token
Service->SDK: Return token
SDK->App: Return token

Note over App: Connect hotspot device

App->SDK: Start pairing (send pairing data)
SDK->Device: Send pairing data
Note over Device: Device automatically disables AP

Note over Device: Device connected to Wi-Fi of router

Device->Service: Activate device
Service->Device: Pairing succeeds

Device->SDK: Pairing succeeds
SDK->App: Pairing succeeds

```-->

![](https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/content-platform/hestia/16256481033eff2a0b493.png)

### Initialize parameters

```java
ActivatorBuilder builder = new ActivatorBuilder()
		.setContext(context)
		.setSsid(ssid)
		.setPassword(password)
		.setActivatorModel(ActivatorModelEnum.TY_AP)
		.setTimeOut(timeout)
		.setToken(token)
		.setListener(new ITuyaSmartActivatorListener() {

				@Override
				public void onError(String errorCode, String errorMsg) {

				}

				@Override
				public void onActiveSuccess(DeviceBean devResp) {

				}

				@Override
				public void onStep(String step, Object data) {

				}
			}
		));
```

**Parameters**

| Parameters     | Description                                                  |
| -------------- | ------------------------------------------------------------ |
| token          | Activation key required for Configuration                    |
| context        | context                                                      |
| ssid           | Wi-Fi ssid                                                   |
| password       | Wi-Fi password                                               |
| activatorModel | Configuration Mode, AP Mode: ActivatorModelEnum.TY_AP        |
| timeout        | Configuration timeout, default setting is 100s, unit is second |

**Get Network Configuration Token**

Before the Wi-Fi network configuration, the SDK needs to obtain the network configuration Token from the Tuya IoT.

The term of validity of Token is 10 minutes, and the Token becomes invalid once the network configuration succeeds.

A new Token has to be obtained if you have to reconfigure the network.

```java
TuyaHomeSdk.getActivatorInstance().getActivatorToken(homeId,
	new ITuyaActivatorGetToken() {

		@Override
		public void onSuccess(String token) {

		}

		@Override
		public void onFailure(String s, String s1) {

		}
	});
```

**Parameters**

| Parameters | Description                                                  |
| ---------- | ------------------------------------------------------------ |
| homeId     | Family ID, please refer to the family management section for details |

### Configuration method invocation

```java
ITuyaActivator mTuyaActivator = TuyaHomeSdk.getActivatorInstance().newActivator(builder);
//Start configuration
mTuyaActivator.start();
//Stop configuration
mTuyaActivator.stop();
//Exit the page to destroy some cache data and monitoring data.
mTuyaActivator.onDestroy();
```

###