子设备配网需要网关设备云端在线的情况下才能发起，且子设备处于配网状态。本文以 Zigbee 网关子设备为例介绍配网业务流程。

## 配网流程

<img src="https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/goat/20201228/3f832d7b19d24173aca6fbbe97e28b41.png" width="500">

## 初始化配网参数

**接口说明**

```java
TuyaGwSubDevActivatorBuilder builder = new TuyaGwSubDevActivatorBuilder()
		.setDevId(mDevId)
		.setTimeOut(timeout)
		.setListener(new ITuyaSmartActivatorListener() {

				@Override
				public void onError(String errorCode, String errorMsg) {

				}

				@Override
				public void onActiveSuccess(DeviceBean devResp) {

				}

				@Override
				public void onStep(String step, Object data) {

				}
			}
		));
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| mDevId | 网关 ID |
| timeout | 配网的超时时间设置，默认是 100s，单位是秒 |

## 配网方法调用

```java
ITuyaActivator mTuyaGWSubActivator = TuyaHomeSdk.getActivatorInstance().newGwSubDevActivator(builder);
//开始配网
mTuyaGWSubActivator.start();
//停止配网
mTuyaGWSubActivator.stop();
//销毁
mTuyaGWSubActivator.onDestory();
```