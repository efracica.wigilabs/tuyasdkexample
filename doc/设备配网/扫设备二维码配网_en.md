## Scan the QR code of the device configuration

### Description

This feature is only available for devices connected to the Internet

![image.png](https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/goat/20201228/e71c74e25f204ff29701913925e86af4.png)

### Get device uuid

```java
Map<String, Object> postData = new HashMap<>();
//get url by scanning the QR code
postData.put("code", url);

TuyaHomeSdk.getRequestInstance().requestWithApiNameWithoutSession("tuya.m.qrcode.parse", "4.0", postData, String.class, new ITuyaDataCallback<String>() {
    @Override
    public void onSuccess(String result) {
      //get uuid from result
      Log.i("TAG" , result);
    }

    @Override
    public void onError(String errorCode, String errorMessage) {
        Log.i("TAG" , errorCode);
    }
});
```

### Initialize parameters

```java
TuyaQRCodeActivatorBuilder builder = new TuyaQRCodeActivatorBuilder()
        .setUuid(uuid)
        .setHomeId(homeId)
        .setContext(mActivity)
        .setTimeOut(timeout)
        .setListener(new ITuyaSmartActivatorListener() {

                @Override
                public void onError(String errorCode, String errorMsg) {

                }

                @Override
                public void onActiveSuccess(DeviceBean devResp) {

                }

                @Override
                public void onStep(String step, Object data) {

                }
            }
        ));
```

**Parameters**

| Parameters | Description                                                  |
| ---------- | ------------------------------------------------------------ |
| uuid       | Device Uuid                                                  |
| homeId     | Family ID, please refer to the family management section for details |
| timeout    | Setting the time-out period for network configuration, The default is 100s |

### Configuration method invocation

```java
ITuyaActivator mTuyaActivator = TuyaHomeSdk.getActivatorInstance().newQRCodeDevActivator(builder);
// Start network configuration
mTuyaActivator.start();
// Stop network configuration
mTuyaActivator.stop();
// Destroy
mTuyaActivator.onDestory();
```

###