Wi-Fi 快连配网又称 **快连模式**（Easy-Connect）、SmartConfig 或 EZ 配网。用户将手机连接到路由器后，利用路由器广播报文与设备进行通信配对。对用户操作简单，但对手机和路由器有兼容性要求，成功率低于 [热点配网](https://developer.tuya.com/cn/docs/app-development/hotspot-mode?id=Kaixk6wxla1oy)。

## 配网流程

![image.png](https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/goat/20201228/b421cbb4cc8546c4992f235e8cc679a0.png)

## 初始化配网参数

**接口说明**

```java
ActivatorBuilder builder = new ActivatorBuilder()
		.setSsid(ssid)
		.setContext(context)
		.setPassword(password)
		.setActivatorModel(ActivatorModelEnum.TY_EZ)
		.setTimeOut(timeout)
		.setToken(token)
		.setListener(new ITuyaSmartActivatorListener() {

				@Override
				public void onError(String errorCode, String errorMsg) {

				}

				@Override
				public void onActiveSuccess(DeviceBean devResp) {
					//多个设备同时配网，将多次回调
				}

				@Override
				public void onStep(String step, Object data) {

				}
			}
		));
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| token | 配网所需要的激活 Token |
| context | 需要传入 `activity` 的 `context` |
| ssid | 配网之后，设备工作 Wi-Fi 的名称 |
| password | 配网之后，设备工作 Wi-Fi 的密码 |
| activatorModel | 配网模式，快连模式：`ActivatorModelEnum.TY_EZ` |
| timeout | 配网的超时时间设置，默认是 100s，单位是秒 |

<a id="getActivatorToken"></a>

## **获取配网 Token**

开始配网之前，SDK 需要在联网状态下从云端获取配网 Token，Token 的有效期为 10 分钟。且配置成功后就会失效，再次配网需要重新获取。

**接口说明**

```java
TuyaHomeSdk.getActivatorInstance().getActivatorToken(homeId,
		new ITuyaActivatorGetToken() {

			@Override
			public void onSuccess(String token) {

			}

			@Override
			public void onFailure(String s, String s1) {

			}
		});
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| homeId | 家庭 ID，详情参考 [家庭管理](https://developer.tuya.com/cn/docs/app-development/homemanage?id=Ka6kjkgere4ae) 章节 |

## 配网方法调用

```java
ITuyaActivator mTuyaActivator = TuyaHomeSdk.getActivatorInstance().newMultiActivator(builder);
//开始配网
mTuyaActivator.start();
//停止配网
mTuyaActivator.stop();
//退出页面销毁一些缓存和监听
mTuyaActivator.onDestroy();
```