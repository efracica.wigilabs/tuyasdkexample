## Sub-device configuration

### Description

The sub-device configuration network can only be initiated when the gateway device cloud is online, and the sub-device is in the network configuration state. The following uses the Zigbee gateway sub-device as an example to introduce the configuration network business process

![image.png](https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/goat/20201228/f93b7264564b48d4a745f38d5e43f72d.png)

### Initialize parameters

```java
TuyaGwSubDevActivatorBuilder builder = new TuyaGwSubDevActivatorBuilder()
		.setDevId(mDevId)
		.setTimeOut(timeout)
		.setListener(new ITuyaSmartActivatorListener() {

				@Override
				public void onError(String errorCode, String errorMsg) {

				}

				@Override
				public void onActiveSuccess(DeviceBean devResp) {

				}

				@Override
				public void onStep(String step, Object data) {

				}
			}
		));
```

**Parameters**

| Parameters | Description                                           |
| ---------- | ----------------------------------------------------- |
| mDevId     | Setting the gateway ID                                |
| timeout    | Setting the time-out period for network configuration |

### Configuration method invocation

```java
ITuyaActivator mTuyaGWActivator = TuyaHomeSdk.getActivatorInstance(). newGwSubDevActivator(builder);
// Start network configuration
mTuyaGWActivator.start();
// Stop network configuration
mTuyaGWActivator.stop();
// Destroy
mTuyaGWActivator.onDestory();
```

###