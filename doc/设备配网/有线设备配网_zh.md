有线设备是指通过有线网络连接路由器，配网过程不用输入路由器的热点名称和密码。本文以 Zigbee 有线网关介绍有线配网业务流程。

## 配网流程

<img src="https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/goat/20201228/92f7bdbe31ba4fffa8eb62a0d5131b88.png" width="500">

## 发现设备

SDK 提供发现待配网有线设备的功能。查询设备前，手机需与设备接入同一网络，然后注册查询有线设备的通知。待 SDK 收到有线设备的广播，即会通过通知转发设备信息。

**接口说明**

```java
ITuyaGwSearcher mTuyaGwSearcher = TuyaHomeSdk.getActivatorInstance().newTuyaGwActivator().newSearcher();
		mTuyaGwSearcher.registerGwSearchListener(new IGwSearchListener() {
			@Override
			public void onDevFind(HgwBean hgwBean) {

			}
		});
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| hgwBean | 发现的网关数据实体 |

## 初始化配网参数

### 使用 SDK 的发现设备功能

**接口说明**

```java
ITuyaActivator mITuyaActivator = TuyaHomeSdk.getActivatorInstance().newGwActivator(
		new TuyaGwActivatorBuilder()
			.setToken(token)
			.setTimeOut(timeout)
			.setContext(context)
			.setHgwBean(hgwBean)
			.setListener(new ITuyaSmartActivatorListener() {

					@Override
					public void onError(String errorCode, String errorMsg) {

					}

					@Override
					public void onActiveSuccess(DeviceBean devResp) {

					}

					@Override
					public void onStep(String step, Object data) {

					}
			}
		));
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| token | 配网所需要的激活 Token |
| context | 需要传入 `activity` 的 `context` |
| timeout | 配网的超时时间设置，默认是 100s，单位是秒 |
| hgwBean | 监听发现的网关数据实体 |

### 不使用 SDK 的发现设备功能

**接口说明**

```java
ITuyaActivator mITuyaActivator = TuyaHomeSdk.getActivatorInstance().newGwActivator(
		new TuyaGwActivatorBuilder()
			.setToken(token)
			.setTimeOut(timeout)
			.setContext(context)
			.setListener(new ITuyaSmartActivatorListener() {

					@Override
					public void onError(String errorCode, String errorMsg) {

					}

					@Override
					public void onActiveSuccess(DeviceBean devResp) {

					}

					@Override
					public void onStep(String step, Object data) {

					}
			}
		));
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| token | 配网所需要的激活 Token |
| context | 需要传入 `activity` 的 `context` |
| timeout | 配网的超时时间设置，默认是100s ，单位是秒 |

## **获取配网 Token**

开始配网之前，SDK 需要在联网状态下从云端获取配网 Token，Token 的有效期为 10 分钟。且配置成功后就会失效，再次配网需要重新获取。

```java
TuyaHomeSdk.getActivatorInstance().getActivatorToken(homeId,
		new ITuyaActivatorGetToken() {

			@Override
			public void onSuccess(String token) {

			}

			@Override
			public void onFailure(String s, String s1) {

			}
		});
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| homeId | 家庭 ID，详情参考 [家庭管理](https://developer.tuya.com/cn/docs/app-development/homemanage?id=Ka6kjkgere4ae) 章节 |

## 配网方法调用

```java
ITuyaActivator mITuyaActivator = TuyaHomeSdk.getActivatorInstance().newGwActivator(builder);
//开始配网
mITuyaActivator.start()
//停止配网
mITuyaActivator.stop()
//退出页面清理
mITuyaActivator.onDestroy()
```