Tuya Smart Life App SDK empowers smart devices to be paired through routers or gateways. The typical pairing mode is Wi-Fi Easy Connect (EZ). In this mode, the SDK gets the pairing token from the cloud and enables the app to broadcast the pairing information. The router ID, password, and pairing token are published. Then, the smart device receives the pairing information and starts the activation process. The activated device is connected to both the app and the cloud. Now, you have kicked off your journey with smart control in the cloud.

:::info
Before you get started on the development of device pairing, you must get to know the basic logic of the Smart Life App SDK. Basic operations such as login and home creation are finished based on the SDK.
:::

## Features

Device pairing supports the following features:

- Wi-Fi device pairing, including the following modes:
   - Wi-Fi EZ
   - Access point (AP), also known as the hotspot mode
- Smart camera pairing in QR code mode
- Device pairing in wired mode
- Sub-device pairing
- Device paring in QR code mode
- Lightning search and pairing
- NB-IoT device pairing
- Bluetooth device pairing

    :::info
    For more information about Bluetooth device pairing, see [Control Bluetooth Devices with Smart Life App SDK for Android](https://developer.tuya.com/en/docs/app-development/ble?id=Ka6km4855a1pa).
    :::

## Terms and definitions

The following table lists specific terms and definitions that are used in this topic. For more information, see [Glossary](https://developer.tuya.com/en/docs/iot/terms?id=K914joq6tegj4).

| Term | Definition |
| ---- | ---- |
| Wi-Fi device | A smart device that uses a Tuya Wi-Fi module to connect to a router and interact with apps and the cloud. |
| Wi-Fi Easy Connect | Also known as the EZ mode. This pairing mode is implemented through the following steps:<ol><li>The app encapsulates the pairing data into the specified section of an IEEE 802.11 packet and sends the packet to the network.</li><li> The Wi-Fi module of a smart device runs in promiscuous mode and listens for and captures all packets over the network.</li><li> The Wi-Fi mode parses the packets that carry the pairing data from the app into the data format specified by the protocol.</li></ol> |
| AP pairing | Also known as the hotspot mode or access point (AP) pairing. A mobile phone acts as a station (STA) and connects to the hotspot of a smart device. Then, both devices are paired to establish a socket connection between them and exchange data through the specified ports. |
| Smart camera pairing in QR code mode | A smart camera scans the QR code on the app to get the pairing data. |
| Wired device | A device that connects to a router over a wired network. For example, a wired device can be a Zigbee wired gateway or a wired camera. |
| Sub-device | A device that interacts with apps and the cloud through a gateway. For example, a Zigbee sub-device is such a device. |
| Zigbee | Zigbee is a short-range, simple, low-power, low-data-rate, cost-efficient, and two-way wireless communication technology. <br> It applies to short-range, low-power, and low-rate data transfers between various electronic devices, periodic data transfers, intermittent data transfers, and data transfers at a long interval. |
| Zigbee gateway | The device that integrates the coordinator with Wi-Fi features on a Zigbee network. The gateway is responsible to formulate the Zigbee network and store data. |
| Zigbee sub-device | A router or a terminal on a Zigbee network. Each Zigbee sub-device is responsible to forward data, or responds to terminal control commands. |