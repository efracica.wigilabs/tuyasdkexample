## Wired network configuration

### Description

A wired device refers to the connection of the router through a wired network, without entering the hotspot name and password of the router during the network configuration. The Zigbee cable gateway is used to introduce the cable configuration network business process.

![image.png](https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/goat/20201228/48362e2a3c8f448cac09f9a90209f2a8.png)

### Discovery device

Tuya SDK provides the function of discovering wired devices. You should register the notification of the wired device to get device information. Before obtaining the device, the phone must be connected to the same network as the device.

```java
ITuyaGwSearcher mTuyaGwSearcher = TuyaHomeSdk.getActivatorInstance().newTuyaGwActivator().newSearcher();
		mTuyaGwSearcher.registerGwSearchListener(new IGwSearchListener() {
			@Override
			public void onDevFind(HgwBean hgwBean) {

			}
		});
```

**Parameters**

| Parameters | Description                    |
| ---------- | ------------------------------ |
| hgwBean    | gateway data entity discovered |

### Initialize parameters

* Used device discovery function

```java
ITuyaActivator mITuyaActivator = TuyaHomeSdk.getActivatorInstance().newGwActivator(
		new TuyaGwActivatorBuilder()
			.setToken(token)
			.setTimeOut(timeout)
			.setContext(context)
			.setHgwBean(hgwBean)
			.setListener(new ITuyaSmartActivatorListener() {

					@Override
					public void onError(String errorCode, String errorMsg) {

					}

					@Override
					public void onActiveSuccess(DeviceBean devResp) {

					}

					@Override
					public void onStep(String step, Object data) {

					}
			}
		));
```

**Parameters**

| Parameters | Description                                                  |
| ---------- | ------------------------------------------------------------ |
| token      | Activation key required for Configuration                    |
| timeout    | Configuration timeout, the default setting is 100s, the unit is second |
| context    | context                                                      |
| hgwBean    | the discovered gateway data entity                           |

* Unused device discovery function

```java
ITuyaActivator mITuyaActivator = TuyaHomeSdk.getActivatorInstance().newGwActivator(
		new TuyaGwActivatorBuilder()
			.setToken(token)
			.setTimeOut(timeout)
			.setContext(context)
			.setListener(new ITuyaSmartActivatorListener() {

					@Override
					public void onError(String errorCode, String errorMsg) {

					}

					@Override
					public void onActiveSuccess(DeviceBean devResp) {

					}

					@Override
					public void onStep(String step, Object data) {

					}
			}
		));
```

**Parameters**

| Parameters | Description                                                  |
| ---------- | ------------------------------------------------------------ |
| token      | Activation key required for Configuration                    |
| timeout    | Configuration timeout, the default setting is 100s, the unit is second |
| context    | context                                                      |

**Get network configuration token**

Before the Wi-Fi network configuration, the SDK needs to obtain the network configuration Token from the Tuya IoT.

The term of validity of Token is 10 minutes, and the Token becomes invalid once the network configuration succeeds.
A new Token has to be obtained if you have to reconfigure the network.

```java
TuyaHomeSdk.getActivatorInstance().getActivatorToken(homeId,
		new ITuyaActivatorGetToken() {

			@Override
			public void onSuccess(String token) {

			}

			@Override
			public void onFailure(String s, String s1) {

			}
		});
```

**Parameters**

| Parameters | Description                                                  |
| ---------- | ------------------------------------------------------------ |
| homeId     | Family ID, please refer to the family management section for details |

### Configuration method invocation

```java
ITuyaActivator mITuyaActivator = TuyaHomeSdk.getActivatorInstance().newGwActivator(builder);
//Start the network pairing
mITuyaActivator.start()
//Stop the network pairing
mITuyaActivator.stop()
//What to do if the user leaves the page.
mITuyaActivator.onDestroy()
```

###