涂鸦智能提供邮箱密码登录体系。

## 邮箱密码注册

邮箱注册分两个接口，先获取验证码，然后使用验证码、密码注册。

**接口说明**

邮箱注册获取验证码

>**说明**：该接口在 3.26.5 版本之前的 SDK 中会有所变化，如果您使用的是旧版本 SDK，详情请参考 [3.26.5 升级说明](https://developer.tuya.com/cn/docs/app-development/android-update?id=Kalelc613v4mw)。

```java
TuyaHomeSdk.getUserInstance().sendVerifyCodeWithUserName(String userName, String region, String countryCode, int type, IResultCallback callback);
```

**参数说明**

| 参数        | 说明                          |
| ----------- | ----------------------------- |
| userName    | 邮箱账号                      |
| region      | 区域，默认填写：""  即可      |
| countryCode | 国家区号：如 "86"             |
| type        | 发送验证码类型： 1:注册验证码 |
| callback    | 回调                          |

**接口说明**

邮箱密码注册

```java
TuyaHomeSdk.getUserInstance().registerAccountWithEmail(final String countryCode, final String email, final String passwd, final String code, final IRegisterCallback callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| countryCode | 国家区号,例如：86 |
| email | email |
| passwd | 密码 |
| code | 验证码 |
| callback | 回调 |

**代码范例**

```java
//注册获取邮箱验证码
TuyaHomeSdk.getUserInstance().sendVerifyCodeWithUserName("123456@123.com", "", "86", 1, new IResultCallback() {
            @Override
            public void onError(String code, String error) {
                Toast.makeText(mContext, "code: " + code + "error:" + error, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess() {
                Toast.makeText(mContext, "获取验证码成功", Toast.LENGTH_SHORT).show();
            }
        });


//邮箱密码注册
TuyaHomeSdk.getUserInstance().registerAccountWithEmail("86", "123456@123.com","123456","5723", new IRegisterCallback() {
    @Override
    public void onSuccess(User user) {
        Toast.makeText(mContext, "注册成功", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(String code, String error) {
        Toast.makeText(mContext, "code: " + code + "error:" + error, Toast.LENGTH_SHORT).show();
    }
});
```

> **注意**:
>
>  目前，账户一旦注册到一个国家或地区，数据无法迁移到其他国家或地区。

## 邮箱密码登录

**接口说明**

用户邮箱密码登录

```java
TuyaHomeSdk.getUserInstance().loginWithEmail(String countryCode, String email, String passwd, final ILoginCallback callback);
```

| 参数 | 说明 |
| ---- | ---- |
| countryCode | 国家区号,例如：86 |
| email | 邮箱 |
| passwd | 登录密码 |
| callback | 回调 |

**代码范例**

```java
//邮箱密码登录
TuyaHomeSdk.getUserInstance().loginWithEmail("86", "123456@123.com", "123123", new ILoginCallback() {
    @Override
    public void onSuccess(User user) {
        Toast.makeText(mContext, "登录成功，用户名：").show();
    }

    @Override
    public void onError(String code, String error) {
        Toast.makeText(mContext, "code: " + code + "error:" + error, Toast.LENGTH_SHORT).show();
    }
});
```

## 邮箱重置密码

用户邮箱重置密码功能分为两个接口，发送验证码接口和重置密码接口。

**接口说明**

邮箱找回密码，获取验证码

>**说明**：该接口在 3.26.5 版本之前的 SDK 中会有所变化，如果您使用的是旧版本 SDK，详情请参考 [3.26.5 升级说明](https://developer.tuya.com/cn/docs/app-development/android-update?id=Kalelc613v4mw)。

```java
TuyaHomeSdk.getUserInstance().sendVerifyCodeWithUserName(String userName, String region, String countryCode, int type, IResultCallback callback);
```

**参数说明**

| 参数        | 说明                              |
| ----------- | --------------------------------- |
| userName    | 邮箱账号                          |
| region      | 区域，默认填写：""  即可          |
| countryCode | 国家区号：如 "86"                 |
| type        | 发送验证码类型： 3:重置密码验证码 |
| callback    | 回调                              |

**接口说明**

邮箱重置密码

```java
TuyaHomeSdk.getUserInstance().resetEmailPassword(String countryCode, final String email, final String emailCode, final String passwd, final IResetPasswordCallback callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| countryCode | 国家区号,例如：86 |
| email | 邮箱 |
| emailCode | 验证码 |
| passwd | 新密码 |
| callback | 回调 |

**示例代码**

```java
//获取邮箱验证码
TuyaHomeSdk.getUserInstance().sendVerifyCodeWithUserName("123456@123.com", "", "86", 3, new IResultCallback() {
            @Override
            public void onError(String code, String error) {
                Toast.makeText(mContext, "code: " + code + "error:" + error, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess() {
                Toast.makeText(mContext, "获取验证码成功", Toast.LENGTH_SHORT).show();
            }
        });
//重置密码
TuyaHomeSdk.getUserInstance().resetEmailPassword("86", "123456@123.com", "123123"，"a12345", new IResetPasswordCallback() {
    @Override
    public void onSuccess() {
        Toast.makeText(mContext, "找回密码成功", Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onError(String code, String error) {
        Toast.makeText(mContext, "code: " + code + "error:" + error, Toast.LENGTH_SHORT).show();
    }
});
```

>**说明**：
>
>- 当密码重置后，如果有多台设备同时登录同一个账号，那么其他设备会触发 session 失效的回调。请自行实现回调后的动作，如跳转到登录页面等。
>
>- 更多请参考 《 Session 过期的处理 》章节

## 验证码验证功能

验证码验证接口

**接口说明**

验证验证码，用于注册、登录、重设密码 时验证的校验

```java
TuyaHomeSdk.getUserInstance().checkCodeWithUserName(String userName, String region, String countryCode, String code, int type, IResultCallback callback)
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| userName | 用户名 |
| region | 区域，默认填写：""  即可 |
| countryCode | 国家码 |
| code | 验证码 |
| type | 类型: <br>1: 注册时验证码验证用<br>2: 验证码登录时用<br>3: 重置密码时用 |
| callback | 回调 |

## 邮箱验证码登录

邮箱验证码登录功能，需要先调用验证码发送接口，发送验证码。再调用邮箱验证码验证接口，将收到的验证码填入对应的参数中。

获取邮箱验证码

>**说明**：该接口在 3.26.5 版本之前的 SDK 中会有所变化，如果您使用的是旧版本 SDK，详情请参考 [3.26.5 升级说明](https://developer.tuya.com/cn/docs/app-development/android-update?id=Kalelc613v4mw)。

**接口说明**

获取邮箱验证码。
```java
TuyaHomeSdk.getUserInstance().sendVerifyCodeWithUserName(String userName, String region, String countryCode, int type, IResultCallback callback);
```

**参数说明**

| 参数        | 说明                           |
| ----------- | ------------------------------ |
| userName    | 邮箱                           |
| region      | 区域，默认填写:""即可          |
| countryCode | 区号：如“86”                   |
| type        | 发送验证码类型：2:是登录验证码 |
| callback    | 回调                           |

邮箱验证码登录

**接口说明**

邮箱验证码登录

```java
TuyaHomeSdk.getUserInstance().loginWithEmailCode(String countryCode, String email, String code, ILoginCallback callback);
```

**参数说明**

| 参数        | 说明         |
| ----------- | ------------ |
| countryCode | 区号：如“86” |
| email       | 邮箱         |
| code        | 验证码       |
| callback    | 登录回调接口 |

**示例代码**

```java
TuyaHomeSdk.getUserInstance().sendVerifyCodeWithUserName("123456@123.com", "", "86", 2, new IResultCallback() {
                @Override
                public void onError(String s, String s1) {
                    	Toast.makeText(mContext, "code: " + code + "error:" + error, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess() {
                    	Toast.makeText(mContext, "获取验证码成功", Toast.LENGTH_SHORT).show();
                }
            });


 TuyaHomeSdk.getUserInstance().loginWithEmailCode("86", "123456@123.com", "5723", new ILoginCallback() {
                @Override
                public void onError(String code, String error) {
                    	Toast.makeText(mContext, error, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(User user) {
                    	Toast.makeText(mContext, "登录成功，用户名：" +TuyaHomeSdk.getUserInstance().getUser().getUsername(), Toast.LENGTH_SHORT).show();

                }
            });
```
