涂鸦智能提供移动端密码登录体系。

>**注意**：出于对用户信息的隐私安全考虑，涂鸦对手机号验证码的机制做了优化和账号限制。若您想继续使用，请联系您的涂鸦客户经理或 [提交工单](https://service.console.tuya.com/)开通该服务。

## 手机号密码注册

手机密码注册，包括获取验证码接口和注册账号接口。

### 获取手机验证码

>**说明**：该接口在 3.25.0 版本之前的 SDK 中会有所变化，如果您使用的是旧版本 SDK，详情请参考 [3.25.0 升级说明](https://developer.tuya.com/cn/docs/app-development/android-app-sdk/change-log/migrating-to-3250?id=Kahyk9w272wi3)。

**接口说明**


```java
TuyaHomeSdk.getUserInstance().sendVerifyCodeWithUserName(String userName, String region, String countryCode, int type, IResultCallback callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| userName | 手机号码|
| region | 区域，默认填写：""  即可 |
| countryCode | 手机区号：如 "86" |
| type | 发送验证码类型：1:注册验证码 |
| callback | 回调 |

### 手机号密码注册

**接口说明**

手机密码注册。

```java
TuyaHomeSdk.getUserInstance().registerAccountWithPhone(final String countryCode, final String phoneNumber, final String passwd, final String code, final IRegisterCallback callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| countryCode | 手机区号：如 "86" |
| phoneNumber | 电话号码 |
| passwd | 密码 |
| code | 验证码 |
| callback | 回调 |

**示例代码**

```java
//获取手机验证码
TuyaHomeSdk.getUserInstance().sendVerifyCodeWithUserName("13666666666", "", "86", 1, new IResultCallback() {
			@Override
			public void onError(String code, String error) {
				Toast.makeText(mContext, "code: " + code + "error:" + error, Toast.LENGTH_SHORT).show();
			}

			@Override
			public void onSuccess() {
				Toast.makeText(mContext, "获取验证码成功", Toast.LENGTH_SHORT).show();
			}
		});
//注册手机密码账户
TuyaHomeSdk.getUserInstance().registerAccountWithPhone("86","13666666666","123456","124332", new IRegisterCallback() {
	@Override
	public void onSuccess(User user) {
		Toast.makeText(mContext, "注册成功", Toast.LENGTH_SHORT).show();
	}
	@Override
	public void onError(String code, String error) {
		Toast.makeText(mContext, "code: " + code + "error:" + error, Toast.LENGTH_SHORT).show();
	}
});
```

## 手机号密码登录

**接口说明**

使用手机号码和密码登录。

```java
TuyaHomeSdk.getUserInstance().loginWithPhonePassword(String countryCode, String phone, String passwd, final ILoginCallback callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| countryCode | 手机区号：如 "86" |
| phone | 手机号码 |
| passwd | 登陆密码 |
| callback | 登陆回调接口 |

**示例代码**

```java
//手机密码登录
TuyaHomeSdk.getUserInstance().loginWithPhonePassword("86", "13666666666", "123456", new ILoginCallback() {
	@Override
	public void onSuccess(User user) {
		Toast.makeText(mContext, "登录成功，用户名：" +TuyaHomeSdk.getUserInstance().getUser().getUsername(), Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onError(String code, String error) {
		Toast.makeText(mContext, "code: " + code + "error:" + error, Toast.LENGTH_SHORT).show();
	}
});
```

## 手机号验证码登录

手机验证码登录功能，需要先调用验证码发送接口，发送验证码。再调用手机验证码验证接口。将收到的验证码填入对应的参数中。

### 获取手机验证码

>**说明**：该接口在 3.25.0 版本之前的 SDK 中会有所变化，如果您使用的是旧版本 SDK，详情请参考 [3.25.0 升级说明](https://developer.tuya.com/cn/docs/app-development/android-app-sdk/change-log/migrating-to-3250?id=Kahyk9w272wi3)。

**接口说明**

获取手机验证码。[注意白名单](#用户获取白名单列表)

```java
TuyaHomeSdk.getUserInstance().sendVerifyCodeWithUserName(String userName, String region, String countryCode, int type, IResultCallback callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| userName | 手机号码|
| region | 区域，默认填写：""  即可 |
| countryCode | 手机区号：如 "86" |
| type | 发送验证码类型：2:登录验证码|
| callback | 回调 |

### 手机验证码登录

**接口说明**

手机验证码登录

```java
TuyaHomeSdk.getUserInstance().loginWithPhone(String countryCode, String phone, String code, final ILoginCallback callback)
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| countryCode | 手机区号：如 "86" |
| phone | 电话号码 |
| code | 验证码 |
| callback | 登陆回调接口 |

**示例代码**

```java
//获取手机验证码
TuyaHomeSdk.getUserInstance().sendVerifyCodeWithUserName("13666666666", "", "86", 2, new IResultCallback() {
			@Override
			public void onError(String code, String error) {
				Toast.makeText(mContext, "code: " + code + "error:" + error, Toast.LENGTH_SHORT).show();
			}

			@Override
			public void onSuccess() {
				Toast.makeText(mContext, "获取验证码成功", Toast.LENGTH_SHORT).show();
			}
		});
//手机验证码登录
TuyaHomeSdk.getUserInstance().loginWithPhone("86", "13355555555", "123456", new ILoginCallback() {
	@Override
	public void onSuccess(User user) {
		Toast.makeText(mContext, "登录成功，用户名：" +TuyaHomeSdk.getUserInstance().getUser().getUsername(), Toast.LENGTH_SHORT).show();
	}
	@Override
	public void onError(String code, String error) {
		Toast.makeText(mContext, error, Toast.LENGTH_SHORT).show();
	}
});
```

## 手机重置密码

手机重置密码功能，包含两个接口：发送验证码接口和重置密码接口。

### 获取手机验证码

>**说明**：该接口在 3.25.0 版本之前的 SDK 中会有所变化，如果您使用的是旧版本 SDK，详情请参考 [3.25.0 升级说明](https://developer.tuya.com/cn/docs/app-development/android-app-sdk/change-log/migrating-to-3250?id=Kahyk9w272wi3)。

**接口说明**

获取手机验证码。[注意白名单](#用户获取白名单列表)

```java
TuyaHomeSdk.getUserInstance().sendVerifyCodeWithUserName(String userName, String region, String countryCode, int type, IResultCallback callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| userName | 手机号码|
| region | 区域，默认填写：""  即可 |
| countryCode | 手机区号：如 "86" |
| type | 发送验证码类型：3:重置密码验证码|
| callback | 回调 |

### 重置密码

**接口说明**

重置密码。

```java
TuyaHomeSdk.getUserInstance().resetPhonePassword(final String countryCode, final String phone, final String code, final String newPasswd, final IResetPasswordCallback callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| countryCode | 手机区号：如 "86" |
| phone | 手机号码 |
| code | 验证码 |
| newPasswd | 新密码 |
| callback | 回调 |

**示例代码**

```java
//手机获取验证码
TuyaHomeSdk.getUserInstance().sendVerifyCodeWithUserName("13666666666", "", "86", 3, new IResultCallback() {
			@Override
			public void onError(String code, String error) {
				Toast.makeText(mContext, "code: " + code + "error:" + error, Toast.LENGTH_SHORT).show();
			}

			@Override
			public void onSuccess() {
				Toast.makeText(mContext, "获取验证码成功", Toast.LENGTH_SHORT).show();
			}
		});
//重置手机密码
TuyaHomeSdk.getUserInstance().resetPhonePassword("86", "13555555555", "123456", "123123", new IResetPasswordCallback(){
	@Override
	public void onSuccess() {
		Toast.makeText(mContext, "找回密码成功", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onError(String code, String error) {
		Toast.makeText(mContext, "code: " + code + "error:" + error, Toast.LENGTH_SHORT).show();
	}
});
```

>**说明**
>- 当密码重置后，如果有多台设备同时登录同一个账号，那么其他设备会触发 session 失效的回调。请自行实现回调后的动作，如跳转到登录页面等。
>- 更多请参考 《 Session 过期的处理 》章节。

## 验证码验证功能

验证码验证接口

**接口说明**

验证验证码，用于注册、登录、重设密码 时验证的校验

```java
TuyaHomeSdk.getUserInstance().checkCodeWithUserName(String userName, String region, String countryCode, String code, int type, IResultCallback callback)
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| userName | 用户名 |
| region | 区域，默认填写：""  即可 |
| countryCode | 国家码 |
| code | 验证码 |
| type | 类型: <br>1: 注册时验证码验证用<br>2: 验证码登录时用<br>3: 重置密码时用<br>8: 验证码注销时用 |
| callback | 回调 |

<a id="WhiteList"></a>

## 用户获取白名单列表

用户获取白名单列表，只有在白名单的地区才可以发送手机号验证码来注册账号。

**接口说明**

```java
TuyaHomeSdk.getUserInstance().getWhiteListWhoCanSendMobileCodeSuccess(IWhiteListCallback callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| callback | 回调 |

**示例代码**

```java
TuyaHomeSdk.getUserInstance().getWhiteListWhoCanSendMobileCodeSuccess(new IWhiteListCallback() {
			@Override
			public void onSuccess(WhiteList whiteList) {
				Toast.makeText(mContext, "获取白名单成功:" + whiteList.getCountryCodes(), Toast.LENGTH_SHORT).show();
			}

			@Override
			public void onError(String code, String error) {
				Toast.makeText(mContext, "code: " + code + "error:" + error, Toast.LENGTH_SHORT).show();
			}
		});
```