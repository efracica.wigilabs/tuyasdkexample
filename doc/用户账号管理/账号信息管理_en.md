The IoT platform supports the management of user account information, including account binding, modifying user information, session expiration processing, account cancellation, etc.

## Account Binding

There are two interfaces for account mailbox association. First, get the verification code, and then use the verification code to bind the mailbox.

### Get the verification code

**Declaration**

Get the verification code of the bound email

``` java
void sendBindVerifyCodeWithEmail(@NonNull String countryCode, @NonNull String email, @NonNull IResultCallback callback);
```

**Parameters**

| Param | Description   |
| ----------- | ----------------- |
| countryCode | Country code, for example: 86 |
| email | email |
| callback | Callback |

### Associate with the Email address

**Declaration**

Bind Email

``` java
void bindEmail(@NonNull String countryCode, @NonNull String email, @NonNull String code, @NonNull String sId, @NonNull IResultCallback callback);
```

**Parameters**

| Param | Description  |
| ----------- | ----------------- |
| countryCode | Country code, for example: 86 |
| email | email |
| code  | Verification Code   |
| sId   | User sessionId |
| callback | Callback  |

**Example**

``` java
        TuyaHomeSdk.getUserInstance().sendBindVerifyCodeWithEmail("86","123456@123.com", new IResultCallback(){

            @Override
            public void onError(String code, String error) {

            }

            @Override
            public void onSuccess() {
                Toast.makeText(mContext, "sendBindVerifyCodeWithEmail success", Toast.LENGTH_SHORT).show();
            }
        });

        TuyaHomeSdk.getUserInstance().bindEmail("86", "123456@123.com","123456", TuyaHomeSdk.getUserInstance().getUser().getSid(), new IResultCallback() {
            @Override
            public void onError(String code, String error) {
                Toast.makeText(mContext, "bind fail", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess() {
                Toast.makeText(mContext, "bind success", Toast.LENGTH_SHORT).show();
            }
        });
```

&&

## Modify User Infomation

### Change Nickname

**Declaration**

Change nickname

```java
void updateNickName (String name, final IReNickNameCallback callback);
```
**Parameters**

| Parameters | Description |
| ---- | ---- |
| name | Nickname |
| callback | callback |

**Example**

```java
TuyaHomeSdk.getUserInstance().updateNickName (nickName,
    new IReNickNameCallback () {
        @Override
        public void onSuccess () {
        }
        @Override
        public void onError (String code, String error) {

        }
});
```
### Update User Time Zone

**Declaration**

Used to update the user time zone.

```java
void updateTimeZone(String timezoneId, IResultCallback callback);
```
**Parameters**

| Parameters | Description |
| ---- | ---- |
| timezoneId | timezone id |
| callback | callback |

**Example**

```java
TuyaHomeSdk.getUserInstance().updateTimeZone (
    timezoneId,
    new IResultCallback () {
        @Override
        public void onSuccess () {
        }

        @Override
        public void onError (String code, String error) {

        }
});
```
### Upload User Avatar
**Declaration**

Used to upload user-defined avatars.

```java
void uploadUserAvatar(File file, IBooleanCallback callback)
```
**Parameters**

| Parameters | Description |
| ---- | ---- |
| file | User avatar image file |
| callback | callback |

** Code Example **

```java
TuyaHomeSdk.getUserInstance().uploadUserAvatar (
    file,
    new IBooleanCallback () {
        @Override
        public void onSuccess () {
        }

        @Override
        public void onError (String code, String error) {

        }
});
```
### Set the Temperature Unit
**Declaration**

Set whether the temperature unit is Celsius or Fahrenheit

* TempUnitEnum.Celsius: Celsius
* TempUnitEnum.Fahrenheit: Hua degree

```java
void setTempUnit (TempUnitEnum unit, IResultCallback callback);
```

| Parameters | Description |
| ---- | ---- |
| unit | unit |
| callback | callback |

### Update User Targeting

If necessary, the positioning information can be reported through the following interfaces:

```java
TuyaSdk.setLatAndLong (lat, lon);
```

### Synchronize User information

When the user information changes, such as modifying the user's avatar, nickname, etc., you need to call the synchronous user information interface to keep the user information up to date. If multiple devices log in at the same time, one device modifies the user information. Another device also needs to synchronize user information. You can call the synchronization interface when viewing user information to synchronize the latest user information.

**Description**

Used to synchronize user information

```java
void updateUserInfo(IResultCallback callback);
```

**Parameters**

| **Parameters** | **Description** |
| ---- | ---- |
| callback   | callback    |

**Example**

```java
TuyaHomeSdk.getUserInstance().updateUserInfo(new IResultCallback() {
    @Override
    public void onError(String code, String error) {

    }

    @Override
    public void onSuccess() {
        User user = TuyaHomeSdk.getUserInstance().getUser();
    }
});
```

&&

## Account Logout

Account logout is divided into: anonymous logout and other account logout.

### Anonymous User Logout

**Declaration**

 Users who log in anonymously can log out through this interface. Anonymous accounts will be logged out immediately.

 ```java
void touristLogOut(final ILogoutCallback callback)
 ```

 **Parameters**

| Params | Type        | Description |
| ---- | ---- | ---- |
| success | ILogoutCallback | Callback    |

 **Example**

```java
TuyaHomeSdk.getUserInstance().touristLogOut(new ILogoutCallback() {
    @Override
    public void onSuccess() {
        
    }
    @Override
    public void onError(String code, String error) {

    }
});
```

### Other Account Logout

#### Java sample

```java
TuyaHomeSdk.getUserInstance().logout(new ILogoutCallback() {
  @Override
  public void onSuccess() {
    
  }

  @Override
  public void onError(String errorCode, String errorMsg) {
  }
});
```

## Account Cancellation

After calling the logout account interface, the account will be permanently deactivated after one week and all information under your account will be deleted. Log back in before that, and your deactivation request will be canceled.

```java
void cancelAccount (IResultCallback callback);
```
**Example**

```java
TuyaHomeSdk.getUserInstance().cancelAccount (new IResultCallback () {
    @Override
    public void onError (String code, String error) {

    }
    @Override
    public void onSuccess () {

    }
});

```

## Login session expired

If you have not logged in to your account for a long time, the session expiration error will be returned when you access the server interface. You have to monitor the notification of the`setOnNeedLoginListener` and log in to the account again after the login page is displayed.

**Declaration**

```java
TuyaHomeSdk.setOnNeedLoginListener(INeedLoginListener needLoginListener);
```
**Example**

```java
TuyaHomeSdk.setOnNeedLoginListener(new INeedLoginListener() {
  @Override
  public void onNeedLogin(Context context) {

  }
});
```

>**Notes**:
>* Once such a callback occurs, please go to the login page and let the user log in again.
>* Recommended for use in Application

## User Information Acquisition by Cloud Development

For more information, see [Login-free token of Cloud Development](https://developer.tuya.com/cn/docs/iot/open-api/api-reference/api-list/oauth-management?id=K95ztzpoll7v5#title-15-%E5%85%8D%E7%99%BB%E5%BD%95%E4%BB%A4%E7%89%8C).

**Description**

The user obtains the ticket field through the third-party interface and calls the interface to obtain user information.

```java
TuyaHomeSdk.getUserInstance().loginWithTicket(String ticket, ILoginCallback callback);
```

**Parameters**

| Parameter | Description |
| ---- | ---- |
| ticket | Third-party APP uses "ticket" to log in to Tuya SDK |

**Example**

```objc
//ticket login
TuyaHomeSdk.getUserInstance().loginWithTicket("ticket", new ILoginCallback() {
	@Override
	public void onSuccess(User user) {
		Toast.makeText(mContext, "Successfully logged in：" , Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onError(String code, String error) {
		Toast.makeText(mContext, "code: " + code + "error:" + error, Toast.LENGTH_SHORT).show();
	}
});
```