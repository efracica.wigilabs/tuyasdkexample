
Tuya IoT Platform supports multi-sign-in and various kinds of user systems such as mobile phones, email, and UID. The mobile phone supports verification code login and password login. The registration and login of each user system will be separately described later.

The `countryCode` parameter (country code) needs to be provided in the registration and login method for region selection of Tuya IoT Platform. Data of all available regions are independent. The Chinese Mainland account `(country code: 86)` cannot be used in the `USA (country code: 1)`. The Chinese Mainland account does not exist in the USA region.

For details about the available region, see [Tuya Cloud Platform Overview](https://developer.tuya.com/en/docs/iot/introduction-of-tuya/tuya-smart-cloud-platform-overview?id=K914joiyhhf7r#title-7-Availability%20zone).

## User Data Model

### User

| Parameters | Description                              |
| ---- | ---- |
| nickName  | Nickname                                 |
| phoneCode | Country code                             |
| mobile | Mobile phone number                      |
| username  | User name                                |
| email  | Email                                    |
| uid    | Unique user identifier                   |
| sid    | Login generates the unique identification id |

| Parameters    | Description                                              |
| ---- | ---- |
| headPic       | Url of the User Account Picture                          |
| nickName      | Nickname                                                 |
| username      | User name<br>If the main account is a mobile phone number, username is the mobile phone number<br>If the primary account is a mailbox, username is the mailbox |
| mobile        | Phone number                                             |
| email         | email                                                    |
| phoneCode     | Country code<br/>For example:<br/>86: China<br/>1: United States |
| Domain.regionCode | The country area where the current account is located. AY: China, AZ: United States, EU: Europe |
| timezoneId    | User time zone information, for example: Asia/Shanghai   |
| tempUnit      | Temperature unit. 1: °C, 2: °F                           |
| snsNickname   | Nickname of the third-party account                      |
| regFrom       | Types of account registration<br/>0: email<br/>1: phone<br/>2: other<br/>3: qq<br/>4: weibo<br/>5: facebook<br/>6: twitter<br/>7: weixin<br>9: uid<br/>10: google |