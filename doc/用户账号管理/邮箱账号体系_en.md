Tuya Smart provides a login system for email registration.

## Account registration

There are two interfaces for email registration. First, get the verification code, and then use the verification code and password to register.

### Get verification code

>**Important**:
>
>This interface has differences in SDKs prior to version 3.26.5, if you are using an older version of the SDK, please refer to [Migrating to 3.26.5](https://developer.tuya.com/en/docs/app-development/android-update?id=Kalelc613v4mw).

```java
TuyaHomeSdk.getUserInstance().sendVerifyCodeWithUserName(String userName, String region, String countryCode, int type, IResultCallback callback);
```

**Parameters**

| Parameter | Description |
| ---- | ---- |
| userName | email address |
| region | Region, fill in by default: "" is fine |
| countryCode | Country code: such as "86" |
| type | Type of verification code sent: 1: Register verification code |
| callback | callback |

### Registration

```java
void registerAccountWithEmail (final String countryCode, final String email, final String passwd, final String code, final IRegisterCallback callback);
```

**Parameters**

| Parameters | Description |
| ---- | ---- |
| countryCode | country code, for example: 86 |
| email | email |
| passwd | password |
| code | Verification Code |
| callback | callback |

**Example**

```java
// Sign up for email verification code
TuyaHomeSdk.getUserInstance().sendVerifyCodeWithUserName("123456@123.com", "", "86", 1, new IResultCallback() {
			@Override
			public void onError(String code, String error) {
				Toast.makeText(mContext, "code: " + code + "error:" + error, Toast.LENGTH_SHORT).show();
			}

			@Override
			public void onSuccess() {
				Toast.makeText(mContext, "Get the verification code successfully", Toast.LENGTH_SHORT).show();
			}
		});
// Email password registration
TuyaHomeSdk.getUserInstance(). registerAccountWithEmail ("86", "123456@123.com", "123456", "5723", new IRegisterCallback () {
    @Override
    public void onSuccess (User user) {
        Toast.makeText (mContext, "Registration succeeded", Toast.LENGTH_SHORT) .show ();
    }

    @Override
    public void onError (String code, String error) {
        Toast.makeText (mContext, "code:" + code + "error:" + error, Toast.LENGTH_SHORT) .show ();
    }
});
```

> **Note**:
>
> Once the account is registered in one country, data cannot currently be migrated to other countries.

## Login with Email

### User email password login

```java
TuyaHomeSdk.getUserInstance().loginWithEmail(String countryCode, String email, String passwd, final ILoginCallback callback);
```

**Parameters**

|Parameters | Description |
| ---- | ---- |
| countryCode | country code, for example: 86 |
| email | email |
| passwd | Login password |
| callback | callback |

**Example**

```java
// mail password login
TuyaHomeSdk.getUserInstance().loginWithEmail("86", "123456@123.com", "123123", new ILoginCallback () {
    @Override
    public void onSuccess (User user) {
        Toast.makeText (mContext, "Login succeeded, username:"). Show ();
    }

    @Override
    public void onError (String code, String error) {
        Toast.makeText (mContext, "code:" + code + "error:" + error, Toast.LENGTH_SHORT) .show ();
    }
});
```

## User email reset password

The user mailbox reset password function is divided into two interfaces, a verification code interface, and a password reset interface.

### Get verification code

>**Important**:
>
>- This interface has differences in SDKs prior to version 3.26.5, if you are using an older version of the SDK, please refer to [Migrating to 3.26.5](https://developer.tuya.com/en/docs/app-development/android-update?id=Kalelc613v4mw).

```java
TuyaHomeSdk.getUserInstance().sendVerifyCodeWithUserName(String userName, String region, String countryCode, int type, IResultCallback callback);
```

**Parameters**

| Parameter | Description |
| ---- | ---- |
| userName | email address |
| region | Region, fill in by default: "" is fine |
| countryCode | Country code: such as "86" |
| type | Type of verification code sent:  3: Reset password verification code |
| callback | callback |

### Email reset password

```java
TuyaHomeSdk.getUserInstance().resetEmailPassword (String countryCode, final String email, final String emailCode, final String passwd, final IResetPasswordCallback callback);
```

**Parameters**

| Parameters | Description |
| ---- | ---- |
| countryCode | country code, for example: 86 |
| email | email |
| emailCode | verification code |
| passwd | New password |
| callback | callback |

**Example**

```java
// Get email verification code
TuyaHomeSdk.getUserInstance().getEmailValidateCode("86", "123456@123.com", new IValidateCallback () {
    @Override
    public void onSuccess () {
        Toast.makeText (mContext, "Success in obtaining verification code", Toast.LENGTH_SHORT) .show ();
    }
    @Override
    public void onError (String code, String error) {
        Toast.makeText (mContext, "code:" + code + "error:" + error, Toast.LENGTH_SHORT) .show ();
    }
});
//reset Password
TuyaHomeSdk.getUserInstance().sendVerifyCodeWithUserName("123456@123.com", "", "86", 3, new IResultCallback() {
			@Override
			public void onError(String code, String error) {
				Toast.makeText(mContext, "code: " + code + "error:" + error, Toast.LENGTH_SHORT).show();
			}

			@Override
			public void onSuccess() {
				Toast.makeText(mContext, "Get the verification code successfully", Toast.LENGTH_SHORT).show();
			}
		});
```

>**Note**:
>- After the password is reset, if multiple devices log in to the same account at the same time, other devices will trigger a callback for session failure. Please implement the actions after the callback, such as jumping to the login page.
>- For more information, please refer to the chapter "Handling of Expired Session".

## Verification of verification code

**Description**

Verification verification code, used for verification verification during registration, login, and password reset

```java
TuyaHomeSdk.getUserInstance().checkCodeWithUserName(String userName, String region, String countryCode, String code, int type, IResultCallback callback)
```

**Parameters**

| Parameters | Description |
| ---- | ---- |
| userName | User name |
| region | region，Fill in the default: "" |
| countryCode | Country code |
| code | Verification code |
| type | Type: <br/>1: For verification code verification during registration <br/>2: Use the verification code to log in <br/>3: Used when resetting the password<br/>8: Used when canceling the account |

## Email verification code login

The verification code login function of the email needs to call the verification code sending interface first to send the verification code. Then call the email verification code verification interface. Fill the received verification code into the corresponding parameters.

**Description**
 
Get email verification code

**Important**:

>-  This interface has differences in SDKs prior to version 3.26.5, if you are using an older version of the SDK, please refer to [Migrating to 3.26.5](https://developer.tuya.com/en/docs/app-development/android-update?id=Kalelc613v4mw).
  
```java
TuyaHomeSdk.getUserInstance().sendVerifyCodeWithUserName(String userName, String region, String countryCode, int type, IResultCallback callback);
```

**Parameters**

| Parameters  | Description                                               |
| ----------- | --------------------------------------------------------- |
| userName    | email address                                             |
| region      | Region, fill in by default: "" is fine                    |
| countryCode | Country code: such as "86"                                |
| type        | Type of verification code sent:2: Verification code login |
| callback    | callback                                                  |

**Description**

Email verification code login

```java
TuyaHomeSdk.getUserInstance().loginWithEmailCode(String countryCode, String email, String code, ILoginCallback callback);
```

**Parameters**

| Parameters  | Description                |
| ----------- | -------------------------- |
| countryCode | Country code: such as "86" |
| email       | email address              |
| code        | verification code          |
| callback    | callback                   |

**Example**

```java
TuyaHomeSdk.getUserInstance().sendVerifyCodeWithUserName("123456@123.com", "", "86", 2, new IResultCallback() {
                @Override
                public void onError(String s, String s1) {
                    	Toast.makeText(mContext, "code: " + code + "error:" + error, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess() {
										Toast.makeText(mContext, "Get the verification code successfully", Toast.LENGTH_SHORT).show();
                }
            });


 TuyaHomeSdk.getUserInstance().loginWithEmailCode("86", "123456@123.com", "5723", new ILoginCallback() {
                @Override
                public void onError(String code, String error) {
                    	Toast.makeText(mContext, error, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(User user) {
                    	        Toast.makeText (mContext, "Login succeeded, username:"). Show ();
                }
            });
```