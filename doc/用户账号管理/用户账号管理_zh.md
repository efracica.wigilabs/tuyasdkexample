涂鸦 IoT App SDK 支持手机号码、邮箱、App UID 等多种用户账号类型。

## 账号类型

用户账号支撑的具体能力包括：

- 手机号码支持验证码登录和密码登录两种方式。
- 邮箱同样支持验证码登录和密码登录两种方式。
- App UID 适用于您已经有自己的账号体系的场景。
- 第三方平台登录支持常见的平台账号授权登录，例如 Facebook、Twitter等。
- 匿名登录提供了快捷登录方式，匿名用户也可以通过丰富用户信息转换为正式账号。

## 功能说明

- 在账号注册登录方法中，您需要提供 `countryCode` 参数，即国家区号，用于就近选择涂鸦 IoT 平台的可用区。各个可用区的数据是相互独立的，因此在 **中国大陆（86）** 注册的账号，在 **美国（1）** 无法使用，会报错用户不存在。可用区相关概念请参考 [涂鸦云平台](https://developer.tuya.com/cn/docs/iot/introduction-of-tuya/tuya-smart-cloud-platform-overview?id=K914joiyhhf7r#title-7-%E5%8F%AF%E7%94%A8%E5%8C%BA) 可用区章节。

- 在该模块中，您将频繁地调用对象 `User`。它是一个单例，存储了当前用户的所有信息及相关的登录注册方法。其数据模型如下表所示：

	| 字段 | 说明 |
	| ---- | ---- |
	| headPic | 用户头像链接 |
	| nickName | 用户昵称 |
	| username | 用户名<ul><li>如果注册账号时使用的是手机号码，username 则为手机号</li><li> 如果注册账号时使用的是邮箱，username 则为邮箱</li></ul>|
	| mobile | 手机号码 |
	| email | 邮箱地址 |
	| phoneCode | 国家码。取值示例：<ul><li> 86：中国 </li><li> 1：美国 </li></ul> |
	| Domain.regionCode | 当前账号所在的国家区域。取值示例：<ul><li> AY：中国 </li><li> AZ：美国 </li><li> EU：欧洲  </li></ul> |
	| timezoneId | 用户时区信息。取值示例：`Asia/Shanghai` |
	| tempUnit | 温度单位。取值：<ul><li> 1：表示摄氏度（°C） </li><li>  2：表示华氏度（°F） </li></ul> |
	| snsNickname | 第三方平台账号的昵称 |
	| regFrom | 账号注册的类型。取值：<ul><li> 0：邮箱 </li><li> 1：手机 </li><li> 2：注册（其它） </li><li> 3：qq </li><li> 5：facebook </li><li> 6：twitter </li><li> 7：weixin </li><li> 9：uid </li><li> 10：google </li></ul> |