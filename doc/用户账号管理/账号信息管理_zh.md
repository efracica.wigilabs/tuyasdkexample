涂鸦 IoT 平台支持对用户账号信息的管理，包括账号绑定、修改用户信息、会话过期处理、账号注销等。

## 账号绑定

账号邮箱绑定分两个接口，先获取验证码， 然后使用验证码进行邮箱绑定操作。

### 获取验证码

**接口说明**

邮箱绑定获取验证码。

``` java
void sendBindVerifyCodeWithEmail(@NonNull String countryCode, @NonNull String email, @NonNull IResultCallback callback);
```

**参数说明**

| 参数        | 说明              |
| ----------- | ----------------- |
| countryCode | 国家区号,例如：86 |
| email       | email             |
| callback    | 回调              |

### 绑定账号邮箱

**接口说明**

邮箱绑定。

``` java
void bindEmail(@NonNull String countryCode, @NonNull String email, @NonNull String code, @NonNull String sId, @NonNull IResultCallback callback);
```

**参数说明**

| 参数        | 说明              |
| ----------- | ----------------- |
| countryCode | 国家区号,例如：86 |
| email       | email             |
| code        | 验证码            |
| sId         | 用户sessionId , 从用户数据模型中获取(User.sid)    |
| callback    | 回调              |

**代码范例**

``` java
        // 绑定邮箱获取验证码
        TuyaHomeSdk.getUserInstance().sendBindVerifyCodeWithEmail("86","123456@123.com", new IResultCallback(){

            @Override
            public void onError(String code, String error) {

            }

            @Override
            public void onSuccess() {
                Toast.makeText(mContext, "验证码发送成功", Toast.LENGTH_SHORT).show();
            }
        });

        // 绑定邮箱
        TuyaHomeSdk.getUserInstance().bindEmail("86", "123456@123.com","123456", TuyaHomeSdk.getUserInstance().getUser().getSid(), new IResultCallback() {
            @Override
            public void onError(String code, String error) {

            }

            @Override
            public void onSuccess() {
                Toast.makeText(mContext, "绑定成功成功", Toast.LENGTH_SHORT).show();
            }
        });
```

## 修改用户信息


### 修改用户头像

**接口说明**

用于上传用户自定义的头像。

```java
void uploadUserAvatar(File file, IBooleanCallback callback)
```
**参数说明**

| 参数     | 说明             |
| -------- | ---------------- |
| file     | 用户头像图片文件 |
| callback | 回调             |

**示例代码**

```java
TuyaHomeSdk.getUserInstance().uploadUserAvatar(
    file, 
    new IBooleanCallback() {
        @Override
        public void onSuccess() {
        }

        @Override
        public void onError(String code, String error) {

        }
});
```

### 设置用户温度单位
**接口说明**

设置温度单位是摄氏度还是华氏度

```java
void setTempUnit(TempUnitEnum unit, IResultCallback callback);
```

| 参数     | 说明                                                         |
| -------- | ------------------------------------------------------------ |
| unit     | 文档的单位。<br>TempUnitEnum.Celsius：摄氏度<br>TempUnitEnum.Fahrenheit：华摄度 |
| callback | 回调                                                         |

### 修改昵称

**接口说明**

修改昵称

```java
void updateNickName(String name, final IReNickNameCallback callback);
```
**参数说明**

| 参数     | 说明 |
| -------- | ---- |
| name     | 昵称 |
| callback | 回调 |

**示例代码**

```java
TuyaHomeSdk.getUserInstance().updateNickName(nickName, 
    new IReNickNameCallback() {
        @Override
        public void onSuccess() {
        }
        @Override
        public void onError(String code, String error) {

        }
});
```
### 更新用户时区

**接口说明**

用于更新用户时区。

```java
void updateTimeZone(String timezoneId, IResultCallback callback);
```
**参数说明**

| 参数       | 说明    |
| ---------- | ------- |
| timezoneId | 时区 id |
| callback   | 回调    |

**示例代码**

```java
TuyaHomeSdk.getUserInstance().updateTimeZone(
    timezoneId, 
    new IResultCallback() {
        @Override
        public void onSuccess() {
        }

        @Override
        public void onError(String code, String error) {

        }
});
```


### 更新用户定位

如果有需要的话，定位信息可以通过以下接口上报：

```java
TuyaSdk.setLatAndLong(lat,lon);
```

### 同步用户信息

当用户信息发生了变更，如修改了用户头像、昵称等，需要调用同步用户信息接口，保持用户信息是最新的状态。如果多台设备同时登录，一台设备修改了用户信息。另外一台设备也需要同步用户信息，可以在查看用户信息时调用同步接口，同步最新的用户信息。

**接口说明**

用于同步用户信息

```java
void updateUserInfo(IResultCallback callback);
```

**参数说明**

| 参数     | 说明 |
| -------- | ---- |
| callback | 回调 |

**示例代码**

```java
TuyaHomeSdk.getUserInstance().updateUserInfo(new IResultCallback() {
    @Override
    public void onError(String code, String error) {
        
    }

    @Override
    public void onSuccess() {
        User user = TuyaHomeSdk.getUserInstance().getUser();
    }
});
```

## 账号退出登录

账号退出登录分为：匿名退出登录和其它账号方式退出登录。

### 匿名退出登录

**接口说明**

 匿名登录的用户可以通过这个接口退出登录，匿名账号会立即注销，其他账号有7天的窗口期

 ```java
void touristLogOut(final ILogoutCallback callback)
 ```

 **参数说明**

| 参数    | 类型            | 说明 |
| :------ | :-------------- | :--- |
| success | ILogoutCallback | 回调 |

 **实例代码**

```java
TuyaHomeSdk.getUserInstance().touristLogOut(new ILogoutCallback() {
    @Override
    public void onSuccess() {

    }
    @Override
    public void onError(String code, String error) {

    }
});
```

### 其它账号退出登录

#### Java 示例

```java
TuyaHomeSdk.getUserInstance().logout(new ILogoutCallback() {
  @Override
  public void onSuccess() {
    //退出登录成功
  }

  @Override
  public void onError(String errorCode, String errorMsg) {
  }
});
```

## 账号注销

**接口说明**

调用注销账户接口后，账号在一周后才会永久停用并删除你账户下的所有信息。在此之前重新登录，则你的停用请求将被取消。

```java
void cancelAccount(IResultCallback callback);    
```
**示例代码**

```java
TuyaHomeSdk.getUserInstance().cancelAccount(new IResultCallback() {
    @Override
    public void onError(String code, String error) {

    }
    @Override
    public void onSuccess() {

    }
});
```

## 登录会话过期

会话（Session）由于可能存在一些异常或者在一段时间不操作（45 天）会失效掉，修改密码，注销账户等会也走这个流程，这时候需要退出应用，重新登录获取 Session。

**接口说明**

```java
TuyaHomeSdk.setOnNeedLoginListener(INeedLoginListener needLoginListener);
```
**实现回调**

```java
needLoginListener.onNeedLogin(Context context);
```
**代码示例**

```java
TuyaHomeSdk.setOnNeedLoginListener(new INeedLoginListener() {
  @Override
  public void onNeedLogin(Context context) {

  }
});
```

**注意事项**

* 建议在 Application 中注册该监听。
* 一旦出现此类回调，请跳转到登录页面，让用户重新登录。

## 通过云开发获取用户信息

更多详情，请参考 [云开发用户及相关配置](https://developer.tuya.com/cn/docs/iot/open-api/api-reference/api-list/oauth-management?id=K95ztzpoll7v5#title-15-%E5%85%8D%E7%99%BB%E5%BD%95%E4%BB%A4%E7%89%8C)。

**接口描述**

用户通过三方接口获取到 `ticket` 字段，调用该接口获取用户信息。

```java
TuyaHomeSdk.getUserInstance().loginWithTicket(String ticket, ILoginCallback callback);
```

| 参数   | 说明 |
| ------ | ------ |
| ticket | 第三方 App 使用“临时ticket”登录涂鸦SDK。 |

**示例代码**

```java
//获取用户信息
TuyaHomeSdk.getUserInstance().loginWithTicket("ticket", new ILoginCallback() {
	@Override
	public void onSuccess(User user) {
		Toast.makeText(mContext, "获取用户信息成功：" , Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onError(String code, String error) {
		Toast.makeText(mContext, "code: " + code + "error:" + error, Toast.LENGTH_SHORT).show();
	}
});
```