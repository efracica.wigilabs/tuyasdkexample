IPC SDK is based on Tuya Smart Home SDK, please refer to [Preparation for Integration](https://developer.tuya.com/en/docs/app-development/android-app-sdk/integration/integrated?id=Ka69nt96cw0uj) and complete the integration of the Home SDK.

## Fast integration

### Configure the build.gradle

* Add the following codes to the project build.gradle file.

    ```groovy
    allprojects {
        repositories {
            ...
            maven {
                url "https://maven-other.tuya.com/repository/maven-releases/"
            }
        ...
        }
    }
    ```

* Add the following codes to the app build.gradle file.

    ```groovy
    defaultConfig {
        ndk {
            abiFilters "armeabi-v7a","arm64-v8a"
        }
    }
	packagingOptions {
        pickFirst 'lib/*/libc++_shared.so'
        pickFirst 'lib/*/libyuv.so'
        pickFirst 'lib/*/libopenh264.so'
    }
    dependencies {
        ...
        implementation 'com.tuya.smart:tuyasmart-ipcsdk:1.0.0-cube'
        ...
    }
    ```

>**Note**:
> 1. Tuya IPC SDK solely supports the platform of armeabi-v7a,arm64-v8a.
> 2. By default, SDK no longer supports P2P 1.0 devices (p2ptype = 1). If you need to use them, please add dependencies: implementation 'com.tuya.smart:tuyasmart-ipc-camera-v1:3.20.0'.

### Set the AndroidManifest.xml

Configure corresponding permissions in the AndroidManifest.xml file.

```xml
<!-- sdcard -->
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
<!-- Network -->
<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="android.permission.CHANGE_NETWORK_STATE" />
<uses-permission android:name="android.permission.CHANGE_WIFI_STATE" />
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
<uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
<uses-permission android:name="android.permission.RECORD_AUDIO" />
<uses-permission android:name="android.permission.MODIFY_AUDIO_SETTINGS" />
```

### Proguard-rules

Arrange aliasing configuration in corresponding proguard-rules.pro files.

```bash
-keep class com.tuyasmart.**{*;}
-dontwarn com.tuyasmart.**
```

> **Note**: The P2P 1.0 devices (p2ptype = 1) aresupported. However, you need to add the following config rules:
>```
> -keep class com.tutk.**{*;}
> -dontwarn com.tutk.**
>```

## Demo app

[Home SDK Demo App](https://developer.tuya.com/en/docs/app-development/android-app-sdk/demo?id=Ka69nt6t3uryg) contains the control panel module of the IPC device.

- Live video panel: The p2p connection process of IPC device, live video, memory card recording, screenshots, intercom, and other functions.
- Playback panel: The acquisition and playback of video clips in the device memory card, the use of timeline components.
- Cloud storage panel: Cloud storage service activation, status judgment, cloud storage video, and cloud storage event acquisition and playback.
- Message panel: Acquisition and display of the detect message.
- Setting panel: Use of IPC device standard DP.