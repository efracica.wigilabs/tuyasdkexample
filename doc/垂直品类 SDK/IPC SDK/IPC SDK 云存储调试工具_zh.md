云存储调试工具是一个能方便查看云存储数据的工具。以往开发者在对接 IPC SDK 时，如果云存储出现错误数据，排查问题时往往需要耗费大量时间写测试代码抓取数据。只要接入本工具，云存储相关的图片数据、视频数据抓取或验证就极为方便。

## 应用场景

- 场景一：云存储视频播放时，请求到云存储时间列表后，无法正常播放视频

    可在调试工具中验证云存储视频是否能正常播放。

    - 如果可以，首先确认云存储接口调用流程是否正确。例如，初始化接口是否调用等，具体可参考Demo代码。
    - 如果不可以，在确保网络正常的情况下，可与设备端联调视频原始数据是否有问题。

- 场景二：侦测事件的图片无法显示

    可在调试工具中验证对应侦测事件图片能否显示。这种情况的原因比较多，大概有下面几个方面：

    - 可能是网络波动，导致图片文件下载失败。
    - 图片的加密密钥错误。可下载原始图片数据与嵌入式工程师联合排查。
    - 图片原始数据有问题，解密失败或者解密后不是有效的图片数据。可下载原始图片数据与嵌入式工程师联合排查。
    - 设备未成功上传加密图片。可验证原始图片数据能否下载成功。

- 场景三：有云存储事件，但是没有对应的视频片段

    - 大概率是设备端在上传视频的过程中，由于网络等原因，导致上传失败。
    - 这种情况可能导致还有一种现象是，事件对应的视频片段特别短，正常来讲一个视频片段最少有 10 秒以上的长度。

    以上两种情况都可以利用调试工具查看事件对应视频片段。

## 快速集成

### 第一步：集成 IPC SDK

本工具依赖 IPC SDK，IPC SDK 接入请参考 [快速集成安卓版 IPC SDK](https://developer.tuya.com/cn/docs/app-development/preparation?id=Ka8j28bt6i7eo)。

### 第二步：配置 build.gradle 文件

在组件的 `build.gradle` 文件里，添加依赖库。

```groovy
dependencies {
    ...
        implementation 'com.tuya.smart:tuyasmart-ipc-camera-cloudtool:3.34.5'

    ...
}
```

将以下代码添加到项目 `build.gradle` 文件中。

```groovy
repositories {
   ...
   maven {
      url "https://maven-other.tuya.com/repository/maven-releases/"
   }
   ...
}
```

### 工具使用

以下示例代码可以跳转到云存储调试工具页面：

```
//需要传入当前家庭id
Intent intent = new Intent(this, CloudToolHomeActivity.class);
intent.putExtra("extra_current_home_id", HomeModel.getCurrentHome(this));
startActivity(intent);
```

## 主要功能

### 侦测事件图片的加密文件下载及查看

<video src="https://images.tuyacn.com/content-platform/hestia/16442985102f38fbbd03b.mp4" width="250px" height="400px" controls="controls"><video>

### 侦测事件对应的视频片段下载

<video src="https://images.tuyacn.com/content-platform/hestia/1631691491baf2fce4429.mp4" width="250px" height="400px" controls="controls"><video>

### 选定时间段的视频片段下载

<video src="https://images.tuyacn.com/content-platform/hestia/164429865809ba62fad60.mp4" width="250px" height="400px" controls="controls"><video>

### 图片解密正确性验证

<img alt="cloud_image_verify_zh.png" src="https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/content-platform/hestia/16316720706109914e330.png" width="250">