When the APP is activated, the device is successfully bound to the home and is online. When someone presses the doorbell, the IPC SDK will receive the doorbell call event. You can answer, hang up, reject, etc. later. The entire process relies on the MQTT protocol. In order for developers to access the doorbell service more conveniently and reduce learning costs, the doorbell service is encapsulated.

In addition, if you want to display a video stream during a doorbell call, you need to access [Live Video](https://developer.tuya.com/en/docs/app-development/livestream?id=Ka6nuvynnjk7h).

> **Explanation**: `doorbell` type devices are not supported. Because its answering and hanging up are not realized through MQTT protocol, but DP point.

## Integrated push notification

When the application exits, MQTT will also be disconnected. At this time, the doorbell call notification will rely on message push. For user experience, developers need to access and push SDK separately. Push access reference: [Integrated Push](https://developer.tuya.com/en/docs/app-development/push?id=Ka6ki8ipunvb8). When the doorbell is pressed, the app will receive a push message. The user can click the push message to proceed to the next step.

## Doorbell call notification

> The premise is that the APP process is active.

### Doorbell call management class acquisition

**Example**

```java
 ITuyaIPCDoorBellManager doorBellInstance = TuyaIPCSdk.getDoorbell().getIPCDoorBellManagerInstance();
```

### Doorbell call model
The doorbell call is encapsulated by the `TYDoorBellCallModel` model class, which is used to save the relevant information of the doorbell call and maintain the current state of the doorbell call. The unique identifier is `messageId` (ie the message id of the doorbell call), a doorbell call corresponds to a `TYDoorBellCallModel` model

| Parameter | Description |
| --------- | ---------- |
| type      | Doorbell call type, etype: doorbell, ac_doorbell, custom |
| devId     | device id |
| messageId | The unique identifier of the doorbell call message |
| time      | The time point when the doorbell call was triggered, Unix timestamp, in seconds |
| answeredBySelf | Has been answered by yourself |
| answeredByOther | Has it been answered by someone else |
| canceled  | Has it been canceled |

### Doorbell call model acquisition

The doorbell call status is maintained by the `TYDoorBellCallModel` model class. If the caller wants to know the status of a doorbell call, it can return the corresponding `TYDoorBellCallModel` instance to obtain it through this interface.

**Declaration**

```java
TYDoorBellCallModel getCallModelByMessageId(String messageId);
```

**Parameter**

| Parameter | Description |
| :------------------- | :----------------- |
| messageId | The unique identifier of the doorbell call message |

### Register doorbell call monitor
Register the doorbell call listener and pass in the listener subclass.

> Precondition for monitoring to take effect: APP process is in active state. When the app process is killed, the monitoring is invalid.

**Declaration**

```java
void addObserver(TuyaSmartDoorBellObserver observer);
```
TuyaSmartDoorBellObserver
**Parameter**

| Parameter | Description |
| :------------------- | :----------------- |
| observer | Doorbell call monitor |

**TuyaSmartDoorBellObserver**

The doorbell call monitor includes the callback of the entire life cycle of the doorbell call, as shown in the following table:

| Interface name | Parameter list | Return value | Description |
| :------------------- | :----------------- | :----------------- | :----------------- |
| doorBellCallDidReceivedFromDevice | TYDoorBellCallModel, DeviceBean | void |Doorbell call event received from the device|
| doorBellCallDidHangUp | TYDoorBellCallModel |void |The device ends the doorbell call |
| doorBellCallDidAnsweredByOther | TYDoorBellCallModel| void | The doorbell call has been answered by someone else. It is up to the business layer to handle whether it needs to be automatically cancelled or it can still be answered. If it cannot be answered, you need to call the [reject doorbell](#Reject_doorbell) method to end the call; if you can still answer, if someone else has already answered, you can only hear the sound of the device, and you can't start the intercom. |
| doorBellCallDidCanceled | TYDoorBellCallModel, boolean| void | The doorbell call is cancelled, the isTimeOut parameter indicates whether it is automatically cancelled after timeout or the device actively cancels |

**Example**

```java
private TYDoorBellCallModel mCallModel;

private final TuyaSmartDoorBellObserver observer = new TuyaSmartDoorBellObserver() {
    @Override
    public void doorBellCallDidReceivedFromDevice(TYDoorBellCallModel callModel, DeviceBean deviceModel) {
        mCallModel = callModel;
    }

    @Override
    public void doorBellCallDidAnsweredByOther(TYDoorBellCallModel callModel) {
    }

    @Override
    public void doorBellCallDidCanceled(TYDoorBellCallModel callModel, boolean isTimeOut) {
    }

    @Override
    public void doorBellCallDidHangUp(TYDoorBellCallModel callModel) {
    }
};
```
When you receive the doorbell call event `doorBellCallDidReceivedFromDevice` of the device, you need to save the `TYDoorBellCallModel` object or the unique identifier of the doorbell call `messageId` as a parameter for subsequent methods such as answering, hanging up, and rejecting.

### Remove doorbell call monitor
After the doorbell call monitor is not in use, it needs to be removed at an appropriate time.

**Declaration**

```java
void removeObserver(TuyaSmartDoorBellObserver observer);
```
**Example**

Remove the specified listener and call the method:

```java
if (doorBellInstance != null) {
    doorBellInstance.removeObserver(observer);
}
```

### Set to ignore subsequent calls from the same device

When receiving a doorbell call from a device, set whether to automatically ignore subsequent calls from this device.

**Declaration**

```java
void setIgnoreWhenCalling(boolean ignoreWhenCalling);
```

**Parameter**

|       Parameter      |     Description    |
| :------------------- | :----------------- |
| ignoreWhenCalling | true: If a device is receiving a doorbell call, it will automatically ignore subsequent calls from this device; false: Do not ignore subsequent calls from the same device. The default value is true.|

### Set doorbell call timeout time

Set the doorbell call timeout time. If the doorbell call is not processed within the specified time, it will call back `TuyaSmartDoorBellObserver#doorBellCallDidCanceled` to notify and end the doorbell call

**Declaration**

```java
void setDoorbellRingTimeOut(int doorbellRingTimeOut);
```

**Parameter**

| Parameter            |   Description      |
| :------------------- | :----------------- |
| doorbellRingTimeOut | Doorbell call timeout time, in seconds, the default value is 25s|

### Answer the doorbell

After receiving the doorbell call, you can answer the doorbell, but the call may not be answered successfully because it has been answered, cancelled, or other reasons. If the answering fails, the failure reason will be notified in the form of the return value error code. [Click to view error code](#error_code)

**Declaration**

```java
int answerDoorBellCall(String messageId);
```

**Parameter**

| Parameter | Description |
| ---- | --- |
| messageId | The unique identifier of the doorbell call message |

**Example**

```java
if (doorBellInstance != null && mCallModel != null) {
    doorBellInstance.answerDoorBellCall(mCallModel.getMessageId());
}
```

### <span id="Reject_doorbell">Reject the doorbell<span/>
After receiving the doorbell call, you can directly refuse to answer the doorbell.

**Declaration**

```java
void refuseDoorBellCall(String messageId);
```

**Parameter**

| Parameter | Description |
| ---- | --- |
| messageId | The unique identifier of the doorbell call message |

**Example**

```java
if (doorBellInstance != null && mCallModel != null) {
    doorBellInstance.refuseDoorBellCall(mCallModel.getMessageId());
}
```
### Hang up the doorbell

After the doorbell call is answered, you can hang up the doorbell. If the hangup fails, the reason for the failure will be returned as the return value. [Click to view error code](#error_code)

**Declaration**

```java
int hangupDoorBellCall(String messageId);
```

**Parameter**

| Parameter | Description |
| ---- | --- |
| messageId | The unique identifier of the doorbell call message |

**Example**

```java
if (doorBellInstance != null && mCallModel != null) {
    doorBellInstance.hangupDoorBellCall(mCallModel.getMessageId());
}
```

### Doorbell call processing from other sources

In addition to notifying the client through MQTT protocol messages, doorbell calls may also be pushed, so this interface is provided. The interface can generate a doorbell call model based on external input parameters to manage subsequent doorbell answering, hanging up, and rejection operations.

**Declaration**

```java
void generateCallModel(String deviceId, String messageType, String msgId, long startTime);
```

**Parameter**

| Parameter | Description |
| ---- | --- |
| deviceId | Device id |
| messageType | Message type |
| msgId | Message id |
| startTime | Doorbell call start time |

**Example**

```java
TuyaIPCSdk.getDoorbell().getIPCDoorBellManagerInstance().generateCallModel(deviceId, messageType, msgId, startTime);
```

### <span id="error_code">Error code<span/>

Doorbell call related error codes

| Error code | Description |
| :------------------- | :----------------- |
| 0 | The operation was successful without errors |
| -1 | The call processing failed, the reasons may be: 1. The call event has ended (hung up or rejected) 2. Same as error code -1006 |
| -1001 | When calling the answering call interface, if it has been answered by another user, this error code is returned |
| -1002 | When calling the answering call interface, if the device has cancelled the call, this error code is returned |
| -1003 | When calling the answering call interface, if it has timed out, this error code is returned |
| -1004 | When calling the answering call interface, if the answer has already been answered, this error code is returned |
| -1005 | If the doorbell call is not answered when calling the hang-up call interface, this error code is returned |
| -1006 | This type of equipment is not supported, such as `doorbell` type equipment |