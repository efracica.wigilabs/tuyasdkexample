## January 26, 2022

- Updated SDK (version 3.34.5)
- Screenshot and screen recording function optimization
- Improved Live Video stability.

## November 24, 2021

- Update SDK (3.32.5).
- Support video playback download and delete.

## September 23, 2021

- Update SDK (3.31.5).
- Added PTZ service interface.
- P2P capabilities are independent and open, and the interface is simplified.
- Added cloud storage debugging tools.
- Added IPC SDK Automated testing tools.

## August 14, 2021

- Update SDK (3.28.5).
- Cloud storage and message center interface optimization.

## June 17, 2021

- Update SDK (3.27.5).
- Improved Live Video stability.

## May 17, 2021

- Added an interface for doorbell ringing(3.26.5).
- Added an interface for P2P capability.

## April 12, 2021

- Update SDK (3.25.0).
- Improved Live Video stability.

## March 2, 2021

- Update SDK (3.24.0).
- Support sub-device preview.
- You can specify the priority connection method when connecting to the P2P channel.
- Add delete cloud video method.
- Add get the playback speed supported by the device.
- Add `ITuyaIPCDoorbell` interface, used to wake up the doorbell.

## December 30, 2020

- Update SDK (3.22.0).
- Add `ITuyaIPCCore` interface, provide auxiliary methods.
- Sound playback provides switching between the speaker and earpiece playback modes.
- Add cloud storage download and cancel methods.
- Supports direct download of encrypted pictures.

## November 10, 2020

- Update SDK (3.20.0).
- `ICameraP2P` is abandoned, `ITuyaSmartCameraP2P` is recommended to simplify the video live broadcast process.
- Add EncryptImage doc.
- Improved Live Video stability.

## May 9, 2020

- Update SDK(base 3.17.0r139),fix audio problems (switch definition sound off) and .so crash, improve stability.
- Add reporting callback of all DP point operations of the device.
- Update Demo.

## March 31, 2020

Update SDK, modify messageCenter Video player bug on armabi.so.

## March 4, 2020

Update SDK(base 3.15.0r135),add message center cloud strage video.

## November 15, 2019

- Update SDK (base 3.13.0r129), ffmpeg 4.1.4.
- Update SDK Demo.

## October 8, 2019

Update SDK (base 3.12.6r125).

## August 23, 2019

Support P2P 3.0.

## August 1, 2019

Add cloud storage module.

## July 13, 2019

- New SDK code API has changed.
- To be compatible with the old version of SDK, use tuyaCamera: 3.11.0r119h2.
- Suggestions for old API to upgrade New API.

## June 11, 2019

Support arm64.