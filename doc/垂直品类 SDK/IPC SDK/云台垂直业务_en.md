Pan-tilt-zoom (PTZ) is one of the important data points (DPs) of IP cameras (IPCs) and is widely used in IPC scenarios. The integration of PTZ is lacking in the business encapsulation as vertical resources. To speed up the integration of PTZ, Tuya provides the encapsulation of PTZ features.

## PTZ management class

The following PTZ features are supported:

- PTZ control
- Focal length adjustment
- Site favorites
- Auto-patrol mode
- Motion tracking
- Preset points

All these PTZ features are managed by the PTZ management class `ITuyaIPCPTZ`. To implement diverse PTZ DPs, required API methods are encapsulated in `ITuyaIPCPTZ`.

### Get the PTZ management class

**Example**

```java
ITuyaIPCPTZ tuyaIPCPTZ = TuyaIPCSdk.getPTZInstance(mDevId)
```

### Register a PTZ listener

**API description**

```java
void addPTZListener(@NonNull ITuyaIPCPTZListener listener);
```

**Request parameter**

| Parameter | Description |
| :---- | :---- |
| listener | The PTZ listener. |

**`ITuyaIPCPTZListener`**

The PTZ listener is registered to listen for PTZ DP updates. The listener is described in the following table.

| API name | Parameter | Return value | Description |
| :---- | :---- | :---- | :---- |
| onPTZDeviceDpUpdate | dpCode | void | The callback of PTZ DP updates. You call the API method [getCurrentValue](#dp_data_get) to query current DP data by `dpCode`. |

**Example**

```java
if (tuyaIPCPTZ != null) {
    tuyaIPCPTZ.addPTZListener(new ITuyaIPCPTZListener() {
        @Override
        public void onPTZDeviceDpUpdate(String dpCode) {

        }
    });
}
```

### Remove a PTZ listener

Removes a PTZ listener. We recommend that you remove the listener when the page is destroyed. Otherwise, a memory leak might occur.

**API description**

```java
void removePTZListener(@NonNull ITuyaIPCPTZListener listener);
```

**Request parameter**

| Parameter | Description |
| :---- | :---- |
| listener | The PTZ listener to be removed. |

**Example**

```java
if (tuyaIPCPTZ != null) {
    tuyaIPCPTZ.removePTZListener(listener);
}
```

<a id="ptz_dp_kit"></a>

## PTZ DP encapsulation

PTZ DPs are further encapsulated based on the API methods for [device management](https://developer.tuya.com/en/docs/app-development/devicemanage?id=Ka6ki8r2rfiuu) and [device control](https://developer.tuya.com/en/docs/app-development/andoird_device_control?id=Kaixh4pfm8f0y) to implement different PTZ features. For more information about the concept of DPs, see [Device Control (Android)](https://developer.tuya.com/en/docs/app-development/andoird_device_control?id=Kaixh4pfm8f0y).

### Query DP status

Checks whether a specific IPC DP is supported and whether the UI of the DP is displayed.

**API description**

```java
boolean querySupportByDPCode(String dpCode);
```

**Request parameter**

| Parameter | Description |
| :---- | :---- |
| dpCode | The identifier of the DP. |

**Example**

```java
if (tuyaIPCPTZ != null) {
    tuyaIPCPTZ.querySupportByDPCode("motion_tracking");
}
```

<a id="dp_data_get"></a>

### Query DP data

Returns current DP data including the return value object. For example, to return a Boolean value, the `tClass` parameter is set to `Boolean.class`. The return value is of the `Boolean` object type. The same rules apply to other types of data. If the DP is not supported, a null value is returned.

**API description**

```java
<T> T getCurrentValue(String dpCode, @NonNull Class<T> tClass);
```

**Request parameter**

| Parameter | Description |
| :---- | :---- |
| dpCode | The identifier of the DP. |
| tClass | The DP value object. Example: `Boolean.class`, `Integer.class`, `String.class`, and `JSONBean.class`. |

**Example**

```java
if (tuyaIPCPTZ != null) {
    tuyaIPCPTZ.getCurrentValue("motion_tracking", Boolean.class);
}
```

<a id="dp_get_device"></a>

### Query data from an IPC

Returns current DP data that is not actively reported. The listener `ITuyaIPCPTZListener` returns the query result.

**API description**

```java
void getCurrentValueFromDevice(String dpCode);
```

**Request parameter**

| Parameter | Description |
| :---- | :---- |
| dpCode | The identifier of the DP. |

**Example**

```java
if (tuyaIPCPTZ != null) {
    tuyaIPCPTZ.getCurrentValueFromDevice("cruise_status");
}
```

### Query DP properties

Returns DP properties. For example, the API method can be used to return properties of Enum DPs. If the DP is not supported, a null value is returned.

**API description**

```java
<T> T getSchemaProperty(String dpCode, @NonNull Class<T> tClass);
```

**Request parameter**

| Parameter | Description |
| :---- | :---- |
| dpCode | The identifier of the DP. |
| tClass | The DP property object. Example: `com.tuya.smart.android.device.bean.EnumSchemaExBean.class`, `com.tuya.smart.android.device.bean.ValueSchemaBean.class`, and `String.class`. |

**Example**

```java
if (tuyaIPCPTZ != null) {
    tuyaIPCPTZ.getSchemaProperty("ptz_control", EnumSchemaBean.class);
}
```

### Send DPs

Sends DPs to an IPC to change the device status or features and control the device.

**API description**

```java
void publishDps(@NonNull String dpCode, @NonNull Object value, IResultCallback callback);
```

**Request parameter**

| Parameter | Description |
| :---- | :---- |
| dpCode | The identifier of the DP. |
| value | The object of the new DP value. |
| callback | The callback. |

**IResultCallback**

| API name | Parameter | Return value | Description |
| :---- | :---- | :---- | :---- |
| onSuccess | void | void | The success callback. |
| onError | String, String | void | The error code and error message. |

The `onSuccess()` callback can be executed to process the logic of a successful call. For example, in the callback, the [getCurrentValue](#dp_data_get) API method can be used to get the latest DP data and update the UI.

**Example**

```java
if (tuyaIPCPTZ != null) {
    tuyaIPCPTZ.publishDps("ptz_stop", true, new IResultCallback() {
        @Override
        public void onError(String code, String error) {

        }

        @Override
        public void onSuccess() {

        }
    });
}
```

### Report DP data

The listener `ITuyaIPCPTZListener` notifies DP changes when the DP that can be reported is changed. DP changes include but are not limited to:

1. An IPC is controlled by a client other than the client that receives the DP changes.
2. DPs are sent.
3. Query data from an IPC

## PTZ control

| DP name | Identifier | Data transfer type | Data type | DP property | Remarks |
| --- | --- |--- |--- |--- |--- |
| PTZ Direction | ptz_control | Send and report (rw) | Enum | Enum values: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 | `direction`: the direction. Valid values:<ul><li>`0`: up</li><li>`1`: upper right</li><li>`2`: right</li><li>`3`: lower right</li><li>`4`: down</li><li>`5`: lower left</li><li>`6`: left</li><li>`7`: upper left</li><li>`8`: rotate to the limit</li><li>`9`: calibrating the PTZ, please wait.</li></ul> |
| PTZ Stop | ptz_stop | Send and report (rw) | Bool | - | <ul><li>`true`: stop</li><li>`false`: not stop</li></ul><br> |

PTZ control is implemented with these DPs. For more information, see [PTZ DP encapsulation](#ptz_dp_kit).

## Zoom control

| DP name | Identifier | Data transfer type | Data type | DP property | Remarks |
| --- | --- | --- | --- | --- | --- |
| Zoom Control | zoom_control | Send and report (rw) | Enum | Enum values: `0` and `1` | Mechanical zoom. Valid values:<ul><li>`0`: zoom in</li><li>`1`: zoom out</li></ul> |
| Stop Zoom | zoom_stop | Send and report (rw) | Bool | `true`: stop zoom |

Zoom control is implemented with these DPs. For more information, see [PTZ DP encapsulation](#ptz_dp_kit).

## Site favorites

| DP name | Identifier | Data transfer type | Data type |
| --- | --- | --- | --- |
| Site Favorites | memory_point_set | Send and report (rw) | String |

Site favorites are implemented with these DPs.

### Get a list of site favorites

**API description**

```java
void requestCollectionPointList(ITuyaResultCallback<List<CollectionPointBean>> callback);
```

**Request parameter**

| Parameter | Description |
| :--- | :--- |
| callback | <ul><li>The success callback<br></li><li>The failure callback. The error code and error message are returned.</li></ul> |

**CollectionPointBean**

| Parameter | Description |
| --- | --- |
| id | The ID of a site favorite. |
| devId | The device ID. |
| pos | The location information of a preset point. |
| name | The name of a preset point. |
| pic | The address of an image. |
| mpId | The unique ID of a device. The device generates the value. |
| encryption | The map that stores the image decryption key. The value is a JSON string. |

For more information about displaying encrypted images of site favorites, see [`DecryptImageView`](https://developer.tuya.com/en/docs/app-development/encryptimage?id=Ka6nxw2hetr2y#title-1-DecryptImageView).

**Example**

```java
if (tuyaIPCPTZ != null) {
    tuyaIPCPTZ.requestCollectionPointList(new ITuyaResultCallback<List<CollectionPointBean>>() {
        @Override
        public void onSuccess(List<CollectionPointBean> result) {

        }

        @Override
        public void onError(String errorCode, String errorMessage) {

        }
    });
}
```

### Add a site favorite

Adds a site favorite. This API method is not recommended for panoramic auto-patrol and site favorite auto-patrol. Otherwise, the failure `callback` will be executed and the error code [`-2001`](#ptz_error_code) will be returned.

**API description**

```java
void addCollectionPoint(@NonNull String name, IResultCallback callback);
```

**Request parameter**

| Parameter | Description |
| :--- | :--- |
| name | The custom name of the site favorite. |
| callback | The callback. |

**Example**

```java
if (tuyaIPCPTZ != null) {
    tuyaIPCPTZ.addCollectionPoint("name", new IResultCallback() {
        @Override
        public void onError(String code, String error) {

        }

        @Override
        public void onSuccess() {

        }
    });
}
```

### Modify a site favorite

**API description**

```java
void modifyCollectionPoint(@NonNull CollectionPointBean pointBean, @NonNull String name, IResultCallback callback);
```

**Request parameter**

| Parameter | Description |
| :--- | :--- |
| pointBean | The bean of the site favorite to be modified. |
| name | The new name. |
| callback | The callback. |

**Example**

```java
if (tuyaIPCPTZ != null) {
    tuyaIPCPTZ.modifyCollectionPoint(collectionPointBean, "NewTitle", new IResultCallback() {
        @Override
        public void onError(String code, String error) {
        }

        @Override
        public void onSuccess() {
        }
    });
}
```

### Delete site favorites

**API description**

```java
void deleteCollectionPoints(@NonNull List<CollectionPointBean> points, IResultCallback callback);
```

**Request parameter**

| Parameter | Description |
| :--- | :--- |
| points | The collection of site favorites to be deleted. |
| callback | The callback. |

**Example**

```java
if (tuyaIPCPTZ != null) {
    tuyaIPCPTZ.deleteCollectionPoints(points, new IResultCallback() {
        @Override
        public void onError(String code, String error) {

        }

        @Override
        public void onSuccess() {

        }
    });
}
```

### Preview a site favorite

**API description**

```java
void viewCollectionPoint(@NonNull CollectionPointBean collectionPointBean, IResultCallback callback);
```

**Request parameter**

| Parameter | Description |
| :--- | :--- |
| collectionPointBean | The site favorite to be previewed. |
| callback | The callback. |

**Example**

```java
if (tuyaIPCPTZ != null) {
    tuyaIPCPTZ.viewCollectionPoint(collectionPointBean, new IResultCallback() {
        @Override
        public void onError(String code, String error) {

        }

        @Override
        public void onSuccess() {

        }
    });
}
```

## Auto-patrol mode

| DP name | Identifier | Data transfer type | Data type | DP property | The remarks. |
| --- | --- | --- | --- | --- | --- |
| Auto-patrol Switch | cruise_switch | Send and report (rw) | Bool | - | - |
| Auto-patrol Mode | cruise_mode | Send and report (rw) | Enum | Enum values: `0` and `1` | `0`: panoramic auto-patrol<br>`1`: site favorite auto-patrol |
| Auto-patrol Status | cruise_status | Send and report (rw) | Enum | Enum values: `0`, `1`, and `2` | `0`: panoramic auto-patrol<br>`1`: site favorite auto-patrol<br>`2`: non-auto-patrol mode |
| Auto-patrol Schedule Setting | cruise_time_mode | Send and report (rw) | Enum | Enum values: `0` and `1` | `0`: all-day auto-patrol<br>`1`: custom auto-patrol |
| Auto-patrol Schedule | cruise_time | Send and report (rw) | String | Data format:<br>t_start: start time<br>t_end: end time<br>Multiple time values are separated with semicolons (`;`). |

Auto-patrol is implemented with these DPs. Individual API methods are available to implement the `Auto-patrol Mode` and `Auto-patrol Schedule Setting` DPs. For more information about other DPs, see [PTZ DP encapsulation](#ptz_dp_kit).

:::important
The `Auto-patrol Status` DP (`cruise_status`) is not actively reported. To get the DP data, call the API method [getCurrentValueFromDevice](#dp_get_device). Other PTZ DPs are standard DPs. You can call the API method [getCurrentValue](#dp_data_get) to get the current values of these standard DPs.
:::

### Set the auto-patrol mode

Sets the auto-patrol mode. The DP identifier is `cruise_mode`. During the setting of site favorite auto-patrol, if the number of site favorites is less than`2`, the error code [-2002](#ptz_error_code) will be returned.

**API description**

```java
void setCruiseMode(String mode, IResultCallback callback);
```

**Request parameter**

| Parameter | Description |
| :--- | :--- |
| mode | The auto-patrol mode. Valid values:<br>`0`: panoramic auto-patrol<br>`1`: site favorite auto-patrol |
| callback | The callback. |

**Example**

```java
if (tuyaIPCPTZ != null) {
    tuyaIPCPTZ.setCruiseMode("0", new IResultCallback() {
        @Override
        public void onError(String code, String error) {
        }

        @Override
        public void onSuccess() {

        }
    });
}
```

### Set the auto-patrol schedule

Sets the auto-patrol mode. The DPs `cruise_time_mode` and `cruise_time` are used. To set an auto-patrol schedule, use `cruise_time_mode` to enable the auto-patrol schedule and then use `cruise_time` to set the scheduled time.

**API description**

```java
void setCruiseTiming(@NonNull String startTime, @NonNull String endTime, IResultCallback callback);
```

**Request parameter**

| Parameter | Description |
| :--- | :--- |
| startTime | The start time. Format: `00:00`. |
| endTime | The end time. Format: `00:00`. |
| callback | The callback. |

**Example**

```java
if (tuyaIPCPTZ != null) {
    tuyaIPCPTZ.setCruiseTiming("08:00", "10:00", new IResultCallback() {
        @Override
        public void onError(String code, String error) {

        }

        @Override
        public void onSuccess() {

        }
    });
}
```

## Motion tracking

| DP name | Identifier | Data transfer type | Data type |
| --- | --- | --- | --- |
| Motion Tracking Switch | motion_tracking | Send and report (rw) | Boolean |

Motion Tracking Switch is implemented with these DPs. For more information, see [PTZ DP encapsulation](#ptz_dp_kit).

## Preset points

| DP name | Identifier | Data transfer type | Data type | DP property | The remarks. |
| --- | --- | --- | --- | --- | --- |
| Set Preset Point | ipc_preset_set | Send and report (rw) | Enum | Enum values: `1`, `2`, `3`, and `4` | Preset points `1`, `2`, `3`, and `4` |

Preset points are implemented with these DPs. For more information, see [PTZ DP encapsulation](#ptz_dp_kit).

<a id="ptz_error_code"></a>

## Error codes

The following table shows the error codes of PTZ features.

| Error codes | Description |
| :---- | :---- |
| -2001 | Site favorites cannot be added during panoramic auto-patrol and site favorite auto-patrol. |
| -2002 | The number of site favorites is less than 2 so that site favorite auto-patrol cannot be enabled. |