设备在触发侦测报警时，通常会上传一张实时视频的截图到涂鸦云端，通过 IPC SDK 获取到的告警消息或者云存储事件中，都会携带一张加密后的视频截图，需要使用加密图片组件来展示加密图片。

## 初始化

图片解密组件 DecryptImageView 是基于 `Fresco` 开发的图片加载组件，在加载图片之前，你必须初始化 `Fresco` 类。

> 如果其他 SDK 也使用到 Fresco，请将涂鸦的初始化放到最后，确保不被覆盖，否则加密图片无法正常显示.

**示例代码**

```java
public class MyApplication extends Application {
	@Override
	public void onCreate() {
		super.onCreate();
        FrescoManager.initFresco(this);
	}
}
```

## DecryptImageView

**接口说明**

显示加密图片

```java
public void setImageURI(String url, byte[] key);
```

**参数说明**

| 参数 | 说明      |
| --- | ---- |
| url | 加密的图片地址 |
| key | 密钥      |

**示例代码**

```java
String encroption = "xxx";
DecryptImageView bg = view.findViewById(R.id.decrypt_image_view);
bg.setImageURI(imgUrl, encroption.getBytes());
```

## 报警消息

报警消息中，图片 url 的值由两部分组成，图片地址和加密密钥，以 “{path}@{key}” 的格式拼接。展示图片时，需要将这个字符串拆开。如果图片 url 字符串的值没有 “@{key}” 的后缀，则表示这是一张未加密的普通的图片。

**示例代码**

```kotlin
val img: SimpleDraweeView = findViewById(R.id.img);
if (mUriString.contains("@")) {
    val index = mUriString.lastIndexOf("@")
    try {
        val decryption = mUriString.substring(index + 1)
        val imageUrl = mUriString.substring(0, index)
        val builder = ImageRequestBuilder.newBuilderWithSource(Uri.parse(imageUrl))
                .setRotationOptions(RotationOptions.autoRotateAtRenderTime())
                .disableDiskCache()
        val imageRequest = DecryptImageRequest(builder, "AES", "AES/CBC/PKCS5Padding",
                decryption.toByteArray())
        controller = Fresco.newDraweeControllerBuilder().setImageRequest(imageRequest)
                .build()
    } catch (e: Exception) {
        e.printStackTrace()
    }
} else {
    try {
        uri = Uri.parse(mUriString)
    } catch (e: Exception) {
        e.printStackTrace()
    }
    val builder = Fresco.newDraweeControllerBuilder().setUri(uri)
    controller = builder.build()
}
img.controller = controller
```

## 云存储事件

云存储事件中，事件截图 TimeRangeBean.snapshotUrl 的值是一个完整的加密过的图片地址，密钥使用云存储视频播放的统一密钥。

**示例代码**

```java
String mEncryptKey = "";

@Override
public void getCloudSecret(String encryKey) {
    //获取云存储视频播放统一密钥
    mEncryptKey = encryKey;
}

@Override
public void getMotionDetectionByTimeSlice(List<TimeRangeBean> list) {
    //获取指定时间相应的移动侦测数据
    TimeRangeBean timeRangeBean = list.get(0);
    if (timeRangeBean.getV() == 2 && !TextUtils.isEmpty(mEncryptKey)) {
      mSnapshot.setImageURI(timeRangeBean.getSnapshotUrl(), mEncryptKey.getBytes());
    } else {
      mSnapshot.setImageURI(timeRangeBean.getSnapshotUrl());
    }
}
```

## 加密图片下载

**接口说明**

```java
void downloadEncryptedImg(String url, String key, ITuyaResultCallback<Bitmap> callback);
```

**参数说明**

| 参数 | 说明          |
| :--- | :----------- |
| url | 加密图片的地址 |
| key | 图片加密的密钥          |
| callback | 下载回调，下载成功返回 bitmap |

**示例代码**

```java
ITuyaIPCTool tool = TuyaIPCSdk.getTool();
if (tool != null) {
    tool.downloadEncryptedImg(url, key, new ITuyaResultCallback<Bitmap>() {
        @Override
        public void onSuccess(Bitmap result) {

        }

        @Override
        public void onError(String errorCode, String errorMessage) {

        }
    });
}
```