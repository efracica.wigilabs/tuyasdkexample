
## Introduction

**IPC SDK** is based on the **Tuya IoT App SDK** and encapsulates the functions of smart cameras. The entire SDK architecture is divided into three levels: Tuya Smart SDK, IPC network communication layer, and camera business layer.

	- **Tuya IoT App SDK**  provides a home-based package for hardware devices and Tuya cloud communication.
	- IPC network communication layer provides P2P network channel implementation.
	- The camera business layer provides audio and video communication and display, equipment function points, alarm messages, cloud storage video management and other business functions.

## Architecture diagram

![image.png](https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/content-platform/hestia/163782410059520ca586c.png)

IPC SDK relies on the communication layer include P2P SDK, Audio-engine, etc., and the functional components provided by the IPC SDK include live video, playback, cloud storage, etc., which are automatically imported when the IPC SDK is imported.
