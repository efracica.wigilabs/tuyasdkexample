
在获取到设备列表后，就可以根据设备 id 来判断是否是智能摄像机设备，如果是智能摄像机设备，则可以根据 DeviceBean 中的信息来创建摄像机对象。

### 判断是否是智能摄像机

**接口说明**

判断设备是否是智能摄像机设备。

```java
boolean isIPCDevice(String devId);
```

**示例代码**

```java
ITuyaIPCCore cameraInstance = TuyaIPCSdk.getCameraInstance();
if (cameraInstance != null) {
    cameraInstance.isIPCDevice(devId)
}
```

### P2P 类型

涂鸦智能摄像机支持三种 p2p 通道实现方案，IPC SDK 会根据 p2p 类型来初始化不同的摄像机具体实现的对象，通过下面的方式获取设备的 p2p 类型。

**接口说明**

获取设备的 p2p 类型。

```java
int getP2PType(String devId);
```

**示例代码**

```java
ITuyaIPCCore cameraInstance = TuyaIPCSdk.getCameraInstance();
if (cameraInstance != null) {
    cameraInstance.getP2PType(devId)
}
```
