Cloud storage debugging tool helps you easily check cloud storage data. In the earlier use of the IPC SDK, to troubleshoot cloud storage errors, a great deal of time was spent on coding for data capture. To simplify the troubleshooting process, you can integrate the cloud storage debugging tool so that data of cloud-stored images and videos can be captured or tested within a few steps.

## Scenarios

- Scenario 1: After cloud-stored videos are requested and displayed, the videos failed to be played back as expected.

    Solution: Use the debugging tool to test whether cloud-stored videos can be played back as expected.

    - If they can, check whether cloud storage API methods are correctly called. For example, the initialization method must be called. For more information, see the sample code.
    - If they cannot, in normal networking conditions, run joint debugging tasks on the IPC to test raw video data.

- Scenario 2: The images of detection events cannot be displayed.

    Solution: Use the debugging tool to test whether the images of detection events can be displayed. This problem might arise due to the following causes:

    - Network fluctuation causes the failure to download images.
    - The image encryption key is incorrect. You can download raw image data and join smart product development engineers in a debugging task on the raw data.
    - Raw image data has errors and failed in decryption, or causes invalid image data after decryption. You can download raw image data and join smart product development engineers in a debugging task on the raw data.
    - The IPC failed to upload encrypted images. Check whether raw image data can be downloaded.

- Scenario 3: A cloud storage event occurs, but it does not generate video clips. Possible causes:

    - Most probably, the IPC failed to upload videos due to network fluctuation.
    - The duration of an event video clip is too short to be generated. In normal cases, the duration is at least 10 seconds.

    The debugging tool can be used to troubleshoot the problems caused by these two factors.

## Fast integration

### Step 1: Integrate the IPC SDK

The debugging tool depends on the IPC SDK. For more information, see [Fast Integration with IPC SDK for Android](https://developer.tuya.com/en/docs/app-development/preparation?id=Ka8j28bt6i7eo).

### Step 2: Configure build.gradle

Add the following dependency to the module file `build.gradle`:

```groovy
dependencies {
    ...
        implementation 'com.tuya.smart:tuyasmart-ipc-camera-cloudtool:3.34.5'

    ...
}
```

Add the following code block to the project file `build.gradle`.

```groovy
repositories {
   ...
   maven {
      url "https://maven-other.tuya.com/repository/maven-releases/"
   }
   ...
}
```

## Features

### Download encrypted or decrypted images of detection events

```
// The current home ID.
Intent intent = new Intent(this, CloudToolHomeActivity.class);
intent.putExtra("extra_current_home_id", HomeModel.getCurrentHome(this));
startActivity(intent);
```

## Features

### Download encrypted images of detection events

<video src="https://images.tuyacn.com/content-platform/hestia/16442985936ae27bc66c3.mp4" width="250px" height="400px" controls="controls"><video>

### Download video clips of detection events

<video src="https://images.tuyacn.com/content-platform/hestia/16316917302f6c171fbf6.mp4" width="250px" height="400px" controls="controls"><video>

### Download video clips within specific periods

<video src="https://images.tuyacn.com/content-platform/hestia/164429871414928e9d121.mp4" width="250px" height="400px" controls="controls"><video>

### Test image decryption

<img alt="Test image decryption" src="https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/content-platform/hestia/16327993240b66a787d58.png" width="250">