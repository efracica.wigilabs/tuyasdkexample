IPC SDK 依赖于涂鸦智能 Home SDK，基于此基础上进行拓展开发，准备工作请参考 [涂鸦全屋智能 SDK 接入文档](https://developer.tuya.com/cn/docs/app-development/android-app-sdk/integration/integrated?id=Ka69nt96cw0uj)，并完成对 Home SDK 的集成。

## 快速集成

### build.gradle 配置

* 开发项目目录下的 build.gradle 配置如下：

	```groovy
	allprojects {
	    repositories {
	        ...
	        maven {
	            url "https://maven-other.tuya.com/repository/maven-releases/"
	        }
		...
	    }
	}
	```

* `app` 目录下的 build.gradle 配置：

	```groovy
	defaultConfig {
		ndk {
		   abiFilters "armeabi-v7a","arm64-v8a"
		}
	}
	packagingOptions {
        pickFirst 'lib/*/libc++_shared.so'
        pickFirst 'lib/*/libyuv.so'
        pickFirst 'lib/*/libopenh264.so'
    }
	dependencies {
		...
		implementation 'com.tuya.smart:tuyasmart-ipcsdk:1.0.0-cube'
		...
	}
	```

	>**注意**：
	> - 涂鸦智能摄像机sdk默认支持 armeabi-v7a , arm64-v8a。
	> - SDK 默认不再支持 p2p 1.0 的设备（p2pType =1），如果需要使用，请添加依赖 `implementation 'com.tuya.smart:tuyasmart-ipc-camera-v1:3.20.0'`。

### AndroidManifest.xml 设置

在 AndroidManifest.xml 文件里配置相应的权限。

```xml
<!-- sdcard -->
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
<!-- 网络 -->
<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="android.permission.CHANGE_NETWORK_STATE" />
<uses-permission android:name="android.permission.CHANGE_WIFI_STATE" />
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
<uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
<uses-permission android:name="android.permission.RECORD_AUDIO" />
<uses-permission android:name="android.permission.MODIFY_AUDIO_SETTINGS" />
```

### 混淆配置

在 proguard-rules.pro 文件配置相应混淆配置：

```bash
-keep class com.tuyasmart.**{*;}
-dontwarn com.tuyasmart.**
```

> **说明**：p2p 1.0 的设备（p2pType =1），还需要引入以下混淆配置：
>```
> -keep class com.tutk.**{*;}
> -dontwarn com.tutk.**
>```

## Demo App

[Home SDK Demo App](https://developer.tuya.com/cn/docs/app-development/android-app-sdk/demo?id=Ka69nt6t3uryg) 中包含了 IPC 设备的控制面板模块。

- 直播页面：IPC 设备的 P2P 连接流程，实时视频播放，视频录制、截图、对讲等功能。
- 回放页面：设备存储卡中视频片段的获取与播放，时间轴组件的使用。
- 云存储页面：云存储服务的开通，状态判断，云存储视频和云存储事件的获取与播放。
- 消息页面：告警事件的获取与展示。
- 设置页面：IPC 设备标准 DP 功能点的使用。