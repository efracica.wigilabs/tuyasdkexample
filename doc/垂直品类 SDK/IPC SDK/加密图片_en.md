When the device triggers a detection alarm, it usually uploads a screenshot of a real-time video to Tuya Cloud. The alarm message or cloud storage event obtained through the IPC SDK will carry an encrypted video screenshot, Need to use the Encrypted Image component to display encrypted images.

## Init

The image decryption component DecryptImageView is an image loading component developed based on Fresco, Fresco needs to be initialized.

> If Fresco is also used in other SDKs, please put the initialisation at the end to make sure it is not overwritten, otherwise the encrypted images cannot be displayed normally.

**Example**

```java
public class MyApplication extends Application {
	@Override
	public void onCreate() {
		super.onCreate();
        FrescoManager.initFresco(this);
	}
}
```

## DecryptImageView

**Declaration**

Display encrypted image

```java
public void setImageURI(String url, byte[] key);
```

**Parameters**

| Parameter | Description      |
| --- | ---- |
| url | Image url path |
| key | Encryption key      |

**Example**

```java
String encroption = "xxx";
DecryptImageView bg = view.findViewById(R.id.decrypt_image_view);
bg.setImageURI(imgUrl, encroption.getBytes());
```

## Alarm message

In the alarm message, the value of the picture consists of two parts, the picture URL path and the encryption key, which are spliced in the format of "{path}@{key}". When displaying the picture, you need to split this string with "@". If the value of the picture attachment string does not have the suffix "@{key}", it means that the picture is not encrypted.

**Example**

```kotlin
val img: SimpleDraweeView = findViewById(R.id.img);
if (mUriString.contains("@")) {
    val index = mUriString.lastIndexOf("@")
    try {
        val decryption = mUriString.substring(index + 1)
        val imageUrl = mUriString.substring(0, index)
        val builder = ImageRequestBuilder.newBuilderWithSource(Uri.parse(imageUrl))
                .setRotationOptions(RotationOptions.autoRotateAtRenderTime())
                .disableDiskCache()
        val imageRequest = DecryptImageRequest(builder, "AES", "AES/CBC/PKCS5Padding",
                decryption.toByteArray())
        controller = Fresco.newDraweeControllerBuilder().setImageRequest(imageRequest)
                .build()
    } catch (e: Exception) {
        e.printStackTrace()
    }
} else {
    try {
        uri = Uri.parse(mUriString)
    } catch (e: Exception) {
        e.printStackTrace()
    }
    val builder = Fresco.newDraweeControllerBuilder().setUri(uri)
    controller = builder.build()
}
img.controller = controller
```

## Cloud storage event

In the cloud storage event, the value of the event screenshot (TimeRangeBean.snapshotUrl) is just A complete encrypted picture path, and the key uses the unified key for cloud storage video playback.

**Example**

```java
String mEncryptKey = "";

@Override
public void getCloudSecret(String encryKey) {
    //get the unified key for cloud storage video playback
    mEncryptKey = encryKey;
}

@Override
public void getMotionDetectionByTimeSlice(List<TimeRangeBean> list) {
    //get the corresponding motion detection data according to the beginning and end of the time segment
    TimeRangeBean timeRangeBean = list.get(0);
    if (timeRangeBean.getV() == 2 && !TextUtils.isEmpty(mEncryptKey)) {
      mSnapshot.setImageURI(timeRangeBean.getSnapshotUrl(), mEncryptKey.getBytes());
    } else {
      mSnapshot.setImageURI(timeRangeBean.getSnapshotUrl());
    }
}
```


## Download encrypted image

**Declaration**

```java
void downloadEncryptedImg(String url, String key, ITuyaResultCallback<Bitmap> callback);
```

**Parameters**

| Parameter | Description |
| :--- | :----------- |
| url | Image URL path |
| key | Encryption key |
| callback | callback，return bitmap if success |

**Example**

```java
ITuyaIPCTool tool = TuyaIPCSdk.getTool();
if (tool != null) {
    tool.downloadEncryptedImg(url, key, new ITuyaResultCallback<Bitmap>() {
        @Override
        public void onSuccess(Bitmap result) {

        }

        @Override
        public void onError(String errorCode, String errorMessage) {

        }
    });
}
```