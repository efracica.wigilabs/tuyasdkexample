
After obtaining the device list, you can determine whether it is a smart camera device according to the type of device. If it is a smart camera device, you can create a camera object based on the information in DeviceBean.

### Determine if it is a smart camera

**Declaration**

Determine if it is a smart camera.

```java
boolean isIPCDevice(String devId);
```

**Example**

```java
ITuyaIPCCore cameraInstance = TuyaIPCSdk.getCameraInstance();
if (cameraInstance != null) {
    cameraInstance.isIPCDevice(devId)
}
```

### P2p type

The Tuya smart camera supports three p2p channel implementation solutions. The IPC SDK will initialize different camera objects according to the p2p type. Obtain the p2p type of the device as follows:

**Declaration**

get p2p type of device.

```java
int getP2PType(String devId);
```

**Example**

```java
ITuyaIPCCore cameraInstance = TuyaIPCSdk.getCameraInstance();
if (cameraInstance != null) {
    cameraInstance.getP2PType(devId)
}
```
