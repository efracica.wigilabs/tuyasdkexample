## 2022 年 1 月 26 日

- 更新 SDK（版本3.34.5）
- 截图、录屏功能优化
- 音视频功能优化，提升稳定性

## 2021 年 11 月 24 日

- 更新 SDK（版本3.32.5）
- 支持视频回放下载、删除

## 2021 年 9 月 23 日

- 更新 SDK（版本3.31.5）
- 新增云台机垂直业务封装
- P2P 能力独立开放，接口简化
- 新增云存储调试工具
- 新增 IPC SDK 自动化测试工具

## 2021 年 8 月 14 日

- 更新 SDK（版本3.28.5）
- 云存储、消息中心接口优化

## 2021 年 6 月 17 日

- 更新 SDK（版本3.27.5）
- 音视频功能优化，提升稳定性

## 2021 年 5 月 17 日

- 新增门铃呼叫业务接口（版本3.26.5）
- 新增 P2P 能力接口

## 2021 年 4 月 12 日

- 更新 SDK（版本3.25.0）
- 音视频稳定性提升

## 2021 年 3 月 2 日
- 更新 sdk（版本3.24.0）
- 支持子设备预览
- 连接 P2P 通道可以指定优先连接方式
- 新增删除云存储视频方法
- 新增获取设备支持的播放倍速列表方法
- 新增 `ITuyaIPCDoorbell` 接口，提供唤醒门铃方法

## 2020 年 12 月 30 日
- 更新 sdk（版本3.22.0）
- 新增 `ITuyaIPCCore` 接口，提供辅助方法
- 声音播放支持提供切换扬声器与听筒播放模式
- 新增云存储下载和取消下载方法
- 新增下载加密图片方法

## 2020 年 11 月 10 日

- 更新 sdk（版本 3.20.0）
- 废弃 `ICameraP2P`，推荐使用 `ITuyaSmartCameraP2P`，简化视频直播流程
- 新增图片解密相关文档
- 音视频稳定性提升

## 2020 年 5 月 9 日

- 更新 sdk（版本 3.17.0r139）
    - 升级依赖的 homeSDK ：implementation 'com.tuya.smart:tuyasmart:3.17.0-beta1'
    - 删除paho mqtt 库的依赖： 
        - ~~implementation 'org.eclipse.paho:org.eclipse.paho.client.mqttv3:1.2.0'~~
    - 获取 camera 配置信息接口 requestCameraInfo 更新
      -  注意：p2pType =2 时参数p2pId，initString ；p2pType =4 时参数 p2pId，token的获取方式
    - 修复音频问题（切换清晰度声音关闭），so崩溃问题，提升sdk稳定性；
    - ITuyaCameraDevice 增加设备所有dp点操作的上报回调 setRegisterDevListener
- 更新优化 demo
    - 混淆修改 #mqtt
        - 移除 
            - ~~-keep class org.eclipse.paho.client.mqttv3.** { *; }~~
            - ~~-dontwarn org.eclipse.paho.client.mqttv3.**~~
        - 新增 
            - -keep class com.tuya.smart.mqttclient.mqttv3.** { *; }
            - -dontwarn com.tuya.smart.mqttclient.mqttv3.**
## 2020 年 3 月 31 日

更新底层库，修复armabi消息中心视频播放问题

## 2020 年 3 月 4 日

更新sdk（版本 3.15.0r135），消息中心多媒体预览（云存储视频播放）

## 2019 年 11 月 15 日

- 更新sdk(版本 3.13.0r129)，对应的ffmpeg是4.1.4版本
- 更新sdk demo

## 2019 年 10 月 8 日

更新sdk（版本 3.12.6r125）

## 2019 年 8 月 1 日

支持云存储功能 （版本 3.11.1r119）

## 2019 年 7 月 13 日

新的sdk代码重构，接口方法有变更，为了兼容老版本sdk请使用tuyaCamera:3.11.0r119h2。建议老用户向上升级

## 2019 年 6 月 11 日

支持arm64