APP启动状态时，设备成功绑定到家庭且处在在线状态下，有人按门铃，IPC SDK 将收到门铃呼叫事件，后续可进行接听、挂断、拒绝等操作。整个过程都依赖MQTT协议，为了开发者更方便地接入门铃业务，减少学习成本，所以对门铃业务做了封装。

另外，如果想要实现门铃呼叫时显示视频流，需要另外接入[视频直播](https://developer.tuya.com/cn/docs/app-development/livestream?id=Ka6nuvynnjk7h)。

> **说明**：不支持`doorbell`类型设备。因为其接听、挂断不是通过MQTT协议来实现的，而是DP点。

## 集成推送通知

当应用退出时，MQTT也随之断开，此时门铃呼叫通知将依赖消息推送。为了用户体验，开发者需要另外接入推送SDK。推送接入参考：[集成推送](https://developer.tuya.com/cn/docs/app-development/push?id=Ka6ki8ipunvb8)。当按下门铃时，app 端将会收到推送消息。用户可点击推送消息进行下一步操作。

## 门铃呼叫通知

> **前提**:  APP 进程处于 active 状态

### 门铃呼叫管理类获取

**示例代码**

```java
 ITuyaIPCDoorBellManager doorBellInstance = TuyaIPCSdk.getDoorbell().getIPCDoorBellManagerInstance();
```

### 门铃呼叫模型

门铃呼叫用 `TYDoorBellCallModel` 模型类封装，用来保存门铃呼叫相关信息以及维护门铃呼叫的当前状态。唯一标识为 `messageId` （即门铃呼叫的消息id），一次门铃呼叫对应一个 `TYDoorBellCallModel` 模型

| 参数      | 描述       |
|     ---------     | ---------- |
| type              | 门铃呼叫类型，etype：doorbell, ac_doorbell, 自定义 |
| devId             | 设备 id    |
| messageId         | 门铃呼叫消息的唯一标识    |
| time              | 门铃呼叫触发的时间点，Unix 时间戳，单位秒   |
| answeredBySelf    | 是否已被自己接听   |
| answeredByOther   | 是否已被其他人接听   |
| canceled          | 是否已经被取消   |

### 门铃呼叫模型获取

门铃呼叫状态使用`TYDoorBellCallModel`模型类来维护，如果调用方想知道某个门铃呼叫的状态，可通过此接口返回对应的`TYDoorBellCallModel`实例来获取。

**接口说明**

```java
TYDoorBellCallModel getCallModelByMessageId(String messageId);
```

**参数说明**

| 参数                 | 说明               |
| :------------------- | :----------------- |
| messageId | 门铃呼叫消息的唯一标识 |

### 注册门铃呼叫监听

注册门铃呼叫监听者，传入监听者子类。

> **监听生效前提**: APP 进程处于 active 状态。当app进程被杀死的时候，该监听无效。

**接口说明**

```java
void addObserver(TuyaSmartDoorBellObserver observer);
```

**参数说明**

| 参数                 | 说明               |
| :------------------- | :----------------- |
| observer | 门铃呼叫监听者 |

**TuyaSmartDoorBellObserver**

门铃呼叫监听者，包含门铃呼叫整个生命周期的回调，详见下表：

| 接口名 |	参数列表 |	返回值 |	描述 |
| :------------------- | :----------------- | :----------------- | :----------------- |
| doorBellCallDidReceivedFromDevice | TYDoorBellCallModel, DeviceBean | void |收到设备的门铃呼叫事件|
| doorBellCallDidHangUp | TYDoorBellCallModel |void |设备端挂断了门铃呼叫 |
| doorBellCallDidAnsweredByOther | TYDoorBellCallModel| void | 门铃呼叫已经被其他人接听，由业务层去处理是否需要自动取消或者仍然可以接听。如果不能接听，需要调用[拒绝门铃](#拒绝门铃)方法以结束呼叫；如果仍然可以接听，在已被其他人接听的情况下，再次接听，只能听见设备的声音，无法开启对讲。 |
| doorBellCallDidCanceled | TYDoorBellCallModel, boolean| void | 门铃呼叫被取消，isTimeOut 参数表明是超时自动取消还是设备端主动取消 |

**示例代码**

```java
private TYDoorBellCallModel mCallModel;

private final TuyaSmartDoorBellObserver observer = new TuyaSmartDoorBellObserver() {
    @Override
    public void doorBellCallDidReceivedFromDevice(TYDoorBellCallModel callModel, DeviceBean deviceModel) {
        mCallModel = callModel;
    }

    @Override
    public void doorBellCallDidAnsweredByOther(TYDoorBellCallModel callModel) {
    }

    @Override
    public void doorBellCallDidCanceled(TYDoorBellCallModel callModel, boolean isTimeOut) {
    }

    @Override
    public void doorBellCallDidHangUp(TYDoorBellCallModel callModel) {
    }
};
```
收到设备的门铃呼叫事件`doorBellCallDidReceivedFromDevice`时，需要将`TYDoorBellCallModel`对象或者其中门铃呼叫唯一标识符`messageId`保存起来，作为后续接听、挂断、拒绝等方法的传参

### 移除门铃呼叫监听
门铃呼叫监听不使用后，需要在合适的时机移除。

**接口说明**

```java
void removeObserver(TuyaSmartDoorBellObserver observer);
```

**示例代码**

移除指定的监听者，调用方法：

```java
if (doorBellInstance != null) {
    doorBellInstance.removeObserver(observer);
}
```

### 设置忽略同设备的后续呼叫

正在接听某个设备的门铃呼叫时，设置是否自动忽略掉此设备的后续呼叫。

**接口说明**

```java
void setIgnoreWhenCalling(boolean ignoreWhenCalling);
```

**参数说明**

| 参数                 | 说明               |
| :------------------- | :----------------- |
| ignoreWhenCalling | true：如果某个设备正在接听一个门铃呼叫时，自动忽略掉此设备的后续呼叫；false：不忽略同设备后续呼叫。默认值为true。|

### 设置门铃呼叫超时时间

设置门铃呼叫超时时间，在规定时间内门铃呼叫未处理时，会回调`TuyaSmartDoorBellObserver#doorBellCallDidCanceled`进行通知，并结束此次门铃呼叫

**接口说明**

```java
void setDoorbellRingTimeOut(int doorbellRingTimeOut);
```

**参数说明**

| 参数                 | 说明               |
| :------------------- | :----------------- |
| doorbellRingTimeOut | 门铃呼叫超时时间，单位秒，默认值为25s |

### 接听门铃

收到门铃呼叫之后，可接听门铃，但是呼叫可能因为已经被接听、取消或者别的原因而导致无法成功接听，如果接听失败，失败原因会以返回值错误码的形式告知。[点击查看错误码](#错误码) 

**接口说明**

```java
int answerDoorBellCall(String messageId);
```

**参数说明**

| 参数 | 说明 |
| ---- | --- |
| messageId | 门铃呼叫消息的唯一标识 |

**示例代码**

```java
if (doorBellInstance != null && mCallModel != null) {
    doorBellInstance.answerDoorBellCall(mCallModel.getMessageId());
}
```

### <span id="拒绝门铃">拒绝门铃<span/>
收到门铃呼叫之后，可直接拒绝接听门铃。

**接口说明**

```java
void refuseDoorBellCall(String messageId);
```

**参数说明**

| 参数 | 说明 |
| ---- | --- |
| messageId | 门铃呼叫消息的唯一标识 |

**示例代码**

```java
    if (doorBellInstance != null && mCallModel != null) {
        doorBellInstance.refuseDoorBellCall(mCallModel.getMessageId());
    }
```
### 挂断门铃

门铃呼叫接听后，可挂断接听。挂断接听可能由于呼叫未被接听等原因为失败，失败原因会以返回值错误码的形式告知。[点击查看错误码](#错误码) 

**接口说明**

```java
int hangupDoorBellCall(String messageId);
```

**参数说明**

| 参数 | 说明 |
| ---- | --- |
| messageId | 门铃呼叫消息的唯一标识 |

**示例代码**

```java
if (doorBellInstance != null && mCallModel != null) {
    doorBellInstance.hangupDoorBellCall(mCallModel.getMessageId());
}
```
### 其他来源门铃呼叫处理

门铃呼叫除了通过MQTT协议消息通知客户端，还可能通过推送，所以提供此接口。接口可以根据外部传入参数来生成门铃呼叫模型，达到管理后续门铃接听、挂断、拒绝的操作。

**接口说明**

```java
void generateCallModel(String deviceId, String messageType, String msgId, long startTime);
```

**参数说明**

| 参数 | 说明 |
| ---- | --- |
| deviceId | 设备 ID |
| messageType | 消息类型 |
| msgId | 消息 ID |
| startTime | 门铃呼叫开始时间 |

**示例代码**

```java
TuyaIPCSdk.getDoorbell().getIPCDoorBellManagerInstance().generateCallModel(deviceId, messageType, msgId, startTime);
```

### <span id="错误码">错误码<span/>

门铃呼叫相关错误码

| 错误码                | 说明               |
| :------------------- | :----------------- |
| 0 | 操作成功，无错误 |
| -1 | 呼叫处理失败，原因可能为：1.呼叫事件已结束（已挂断或已拒绝）2.同错误码-1006 |
| -1001   | 调用接听呼叫接口时，如果已经被其他用户接听，返回此错误码    |
| -1002   | 调用接听呼叫接口时，如果设备端已经取消掉了此次呼叫，返回此错误码    |
| -1003   | 调用接听呼叫接口时，如果已经超时，返回此错误码    |
| -1004   | 调用接听呼叫接口时，如果已经接听过了，返回此错误码    |
| -1005   | 调用挂断呼叫接口时，如果门铃呼叫没有被接听，返回此错误码    |
| -1006   | 不支持此类型设备，比如`doorbell`类型设备    |