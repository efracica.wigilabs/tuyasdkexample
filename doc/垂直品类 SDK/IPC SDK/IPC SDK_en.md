Tuya Smart IPC SDK provides the interface package for communication with camera hardware and Tuya Cloud to accelerate the application development process. The later chapters are collectively called IPC SDK, including the following features:

- Preview the images captured by the camera in real-time.
- Play the video recorded on the camera's memory card.
- The phone side records the image collected by the camera.
- Play audio from the camera and talk to the camera equipment.
- The function of publishing or receive camera commands.
- Cloud storage.
- Detect alarm message.

## Tuya Smart Camera Capability

* Network Configuration
  * Quick connect mode (Smart Config)
  * Hotspot mode (AP mode)
  * QR code mode
* P2p video
  * Live video
  * Memory card recording video playback
  * Video original data
* Low power doorbell
  * Battery management
  * Sleep and wake
  * Doorbell call
* Cloud storage
  * Cloud storage service purchase
  * Event cloud storage
  * Cloud video playback
* Alarm message
  * Message list
  * Audio and video message playback
* Expansion ability
  * PTZ control
  * Memory card management
  * Detect alarm
  * Other

The above capabilities are partially different depending on the device manufacturer.