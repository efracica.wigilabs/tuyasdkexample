
时间轴组件用视频回放或者云存储视频回放时，线性展示可以播放的视频录像时间点。可以通过滑动时间轴来精准的定位开始播放的时间点和片段。

![Timeline View](https://images.tuyacn.com/fe-static/docs/img/76766a21-209d-45ad-a2c0-2a1ce992c714.png)

## 集成

```groovy
dependencies {
    implementation 'com.tuya.smart:tuyasmart-ipc-camera-timeline:1.0.1'
}
```

**类和接口**

|                类（接口）名                |       描述        |
| -------------------------------------- | ---------------- |
| TuyaTimelineView                         | 时间轴控件         |
| OnBarMoveListener      | 时间轴拖动回调接口   |
| OnSelectedTimeListener | 时间轴选择框回调接口 |


## 接口说明

### TuyaTimelineView

**自定义属性**


|       名称        |     说明      |
| ----------------- | ----------- |
| topTextMargin    | 文字顶部间距   |
| bottomTextMargin | 文字底部间距   |
| smallRulerHeight | 小刻度线高度   |
| timeScaleColor   | 时间文字颜色   |
| linesColor       | 刻度线颜色     |
| bubbleColor      | 气泡背景颜色   |
| bubbleTextColor  | 气泡文字颜色   |


**接口说明**

设置时间轴中的数据渲染颜色

```java
void setContentShader(Shader contentShader)
```

**接口说明**

指定时区

```java
void setTimeZone(TimeZone zone)
```

**接口说明**

拖动时和选择模式时会有气泡文字显示当前所在的时间

设置是否显示气泡文字

```java
void setShowBubbleWhenDrag(boolean show)
```

**接口说明**

设置一个时间单位的宽，单位 px

```java
void setSpacePerUnit(int spacePerUnit)
```

**接口说明**

设置时间轴时间单位模式，目前有三种，一个时间单位为 60 秒，600 秒，3600 秒

```java
void setUnitMode(TimelineUnitMode mode)
```

**参数说明**

|             枚举             |        说明        |
| --------------------------- | ----------------- |
| TimelineUnitMode.Mode_60   | 一个时间单位为60s   |
| TimelineUnitMode.Mode_600  | 一个时间单位为600s  |
| TimelineUnitMode.Mode_3600 | 一个时间单位为3600s |

**接口说明**

是否显示时间文本

```java
void setShowTimeText(boolean showTimeText)
```

**接口说明**

是否显示短刻度线

```java
void setShowShortMark(boolean showShortMark)
```

**接口说明**

时间轴选择模式开关

```java
void showSelectTimeArea(boolean open)
```

**接口说明**

设置选择框能选中的最大、最小时长，单位 s

```java
void setSelectTimeAreaRange(long min, long max)
```

**接口说明**

设置时间轴选择框的颜色

```java
void setSelectionBoxColor(int color)
```

**接口说明**

设置时间轴选择框竖线的颜色

```java
void setSelectCenterColor(int selectCenterColor)
```

**接口说明**

是否在选择模式中

```java
boolean isSelectionEnabled()
```

**接口说明**

配置时间轴参数

```java
void setCurrentTimeConfig(long currentTimeInMillisecond)
```

**接口说明**

设置时间轴的当前时间，单位 ms

```java
void setCurrentTimeInMillisecond(long currentTimeInMillisecond)
```

**接口说明**

设置气泡文字在 12 小时制时显示文字

```java
void setTimeAmPmString(String timeAmString, String timePmString)
```

**接口说明**

设置时间轴片段数据列表

```java
void setRecordDataExistTimeClipsList(List<TimeBean> source)
```

**接口说明**

开启自动寻找下一个播放的片段

```java
public void setCanQueryData()
```

**接口说明**

设置是否允许寻找下一个播放的片段

```java
public synchronized void setQueryNewVideoData(boolean var1);
```

**接口说明**

设置时间轴样式

| 时间轴样式 | 值 |
| ---- | --- |
| 默认样式。时间刻度值在上方，显示小刻度线 | 1 |
| 时间刻度值在下方，不显示小刻度线 | 2 |

> 样式 2 的气泡文字在控件上方，因此要正常显示需要为时间轴组件的父布局设置 android:clipChildren="false"

```java
public void setStyle(int style);
```

**接口说明**

设置中央刻度线颜色

```java
public void setMiddleCursorColor(int color)
```


### OnBarMoveListener

**接口说明**

开始拖拽时回调

```java
void onBarActionDown()
```

**接口说明**

拖拽中回调

```java
void onBarMove(long screenLeftTime, long screenRightTime, long currentTime)
```

**参数说明**

|      参数       |      说明      |
| --------------- | ------------- |
| screenLeftTime | 屏幕最左侧的时间 |
| screenRightTime | 屏幕最右侧的时间 |
| currentTime     | 当前时间        |

**接口说明**

时间轴拖拽结束时回调，拖动结束时会自动寻找当前位置可以播放的片段数据

```java
void onBarMoveFinish(long startTime, long endTime, long currentTime)
```

**参数说明**

|    参数     |                说明                 |
| ----------- | ---------------------------------- |
| startTime   | 播放片段数据的开始时间，无片段数据返回 -1 |
| endTime     | 播放片段数据的结束时间，无片段数据返回 -1 |
| currentTime | 拖拽结束时的时间，无片段数据返回 -1      |

### OnSelectedTimeListener

**接口说明**

时间轴选择模式拖拽时回调

```java
void onDragging(long selectStartTime, long selectEndTime)
```

**参数说明**

|      参数       |         说明         |
| --------------- | --------------------- |
| selectStartTime | 选择框选中的最左侧的时间 |
| selectEndTime   | 选择框选中的最右侧的时间 |