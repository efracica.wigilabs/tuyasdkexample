云台作为网络摄像机（IPC）一个重要的功能点，一直被广泛使用在各类 IPC 设备上。而对于云台业务的接入，一直缺少一个垂直化封装，给开发者对接带来了困难。为了解决这个问题，让开发者更快更方便的接入云台功能，涂鸦对云台相关功能进行了封装。

## 云台管理类

云台相关功能包括：

- 云台控制
- 焦距调整
- 收藏点
- 巡航
- 移动追踪
- 预设点

如上所有云台相关功能，皆通过 `ITuyaIPCPTZ` 云台管理类来管理。同时，因为云台功能涉及到大量DP（即功能点，以下统称DP）操作，所以云台相关DP操作也在 `ITuyaIPCPTZ` 类中封装了统一接口。

### 云台管理类获取

**示例代码**

```java
ITuyaIPCPTZ tuyaIPCPTZ = TuyaIPCSdk.getPTZInstance(mDevId)
```

### 注册云台监听

**接口说明**

```java
void addPTZListener(@NonNull ITuyaIPCPTZListener listener);
```

**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| listener | 云台监听者 |

**`ITuyaIPCPTZListener`**

云台监听者，监听云台相关DP变化，详见下表：

| 接口名 | 参数列表 | 返回值 | 描述 |
| :---- | :---- | :---- | :---- |
| onPTZDeviceDpUpdate | dpCode | void |当云台相关DP更新时回调，开发者可根据`dpCode`查询DP当前数据（使用云台 DP 封装中 [数据查询](#dp_data_get) 接口）|

**示例代码**

```java
if (tuyaIPCPTZ != null) {
    tuyaIPCPTZ.addPTZListener(new ITuyaIPCPTZListener() {
        @Override
        public void onPTZDeviceDpUpdate(String dpCode) {

        }
    });
}
```

### 移除云台监听

移除云台监听。推荐开发者在页面销毁时移除监听，如果监听未移除，将会导致内存泄漏发生。

**接口说明**

```java
void removePTZListener(@NonNull ITuyaIPCPTZListener listener);
```

**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| listener | 要移除的云台监听者 |

**示例代码**

```java
if (tuyaIPCPTZ != null) {
    tuyaIPCPTZ.removePTZListener(listener);
}
```

<a id="ptz_dp_kit"></a>

## 云台 DP 封装

由于云台功能大量使用 DP（即功能点，功能点概念参考 [设备控制](https://developer.tuya.com/cn/docs/app-development/andoird_device_control?id=Kaixh4pfm8f0y)），所以，对云台相关的DP，在 [设备管理](https://developer.tuya.com/cn/docs/app-development/devicemanage?id=Ka6ki8r2rfiuu) 及 [设备控制](https://developer.tuya.com/cn/docs/app-development/andoird_device_control?id=Kaixh4pfm8f0y) 的基础上进行了二次封装。

### 状态查询

通过该方法可以判断设备是否支持该DP，可用于判断对应功能相关UI是否展示。

**接口说明**

```java
boolean querySupportByDPCode(String dpCode);
```

**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| dpCode | DP标识符 |

**示例代码**

```java
if (tuyaIPCPTZ != null) {
    tuyaIPCPTZ.querySupportByDPCode("motion_tracking");
}
```

<a id="dp_data_get"></a>

### 数据查询

获取DP当前数据信息，直接返回值对象。比如值为布尔类型，`tClass`参数直接传入`Boolean.class`，返回值即为`Boolean`对象类型，其他类型类推。如果不支持该DP，将返回空值。

**接口说明**

```java
<T> T getCurrentValue(String dpCode, @NonNull Class<T> tClass);
```

**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| dpCode | DP标识符 |
| tClass | DP值类对象，`Boolean.class`、`Integer.class`、`String.class`、`JSONBean.class`等|

**示例代码**

```java
if (tuyaIPCPTZ != null) {
    tuyaIPCPTZ.getCurrentValue("motion_tracking", Boolean.class);
}
```

<a id="dp_get_device"></a>

### 从设备查询数据

获取DP当前数据信息，针对不主动发送数据的设备DP。结果将会通过 `ITuyaIPCPTZListener` 监听返回。

**接口说明**

```java
void getCurrentValueFromDevice(String dpCode);
```

**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| dpCode | DP 标识符 |

**示例代码**

```java
if (tuyaIPCPTZ != null) {
    tuyaIPCPTZ.getCurrentValueFromDevice("cruise_status");
}
```

### 功能点属性查询

查询DP属性信息。比如查询枚举型（Enum）DP的功能点属性。如果不支持该DP，将返回空值。

**接口说明**

```java
<T> T getSchemaProperty(String dpCode, @NonNull Class<T> tClass);
```

**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| dpCode | DP标识符 |
| tClass | DP属性类对象，`com.tuya.smart.android.device.bean.EnumSchemaExBean.class`、` com.tuya.smart.android.device.bean.ValueSchemaBean.class`、`String.class`等|

**示例代码**

```java
if (tuyaIPCPTZ != null) {
    tuyaIPCPTZ.getSchemaProperty("ptz_control", EnumSchemaBean.class);
}
```

### 数据下发

向设备发送DP，改变设备状态或功能，来达到设备控制的目的。

**接口说明**

```java
void publishDps(@NonNull String dpCode, @NonNull Object value, IResultCallback callback);
```

**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| dpCode | DP标识符 |
| value | DP新的值的对象 |
| callback | 结果回调 |

**IResultCallback**

| 接口名 | 参数列表 | 返回值 | 描述 |
| :---- | :---- | :---- | :---- |
| onSuccess | void | void |成功回调|
| onError | String , String |void |错误码及错误信息 |

您可以在 `onSuccess()` 回调中处理下发成功逻辑。比如在回调中通过 [数据查询](#dp_data_get) 接口获取 DP 最新数据来更新 UI。

**示例代码**

```java
if (tuyaIPCPTZ != null) {
    tuyaIPCPTZ.publishDps("ptz_stop", true, new IResultCallback() {
        @Override
        public void onError(String code, String error) {

        }

        @Override
        public void onSuccess() {

        }
    });
}
```

### 设备 DP 上报

当支持上报的 DP 数据变化时，`ITuyaIPCPTZListener` 监听将会通知 DP 发生变化。DP变化的情况包含但不限于以下情况：

1. 别的客户端操作设备
2. 数据下发
3. 从设备查询数据

## 云台控制

|功能点名称|标识符| 数据传输类型|数据类型|功能点属性|备注|
| --- | --- |--- |--- |--- |--- |
|云台转动|ptz_control |可下发可上报（rw） |枚举型（Enum） |枚举值: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 |direction：方向，共8个;0-上，1-右上，2-右，3-右下，4-下，5-左下，6-左，7-左上，8-已经转到头了，9-云台正在校准中，请稍后。|
|停止转动|ptz_stop |可下发可上报（rw）| 布尔型（Bool）| - |true：停止转动  <br> false：不停止转动|

云台控制涉及如上 DP，DP 相关操作参考上文 [云台 DP 封装](#ptz_dp_kit)。

## 焦距控制

|功能点名称|标识符| 数据传输类型|数据类型|功能点属性|备注|
| --- | --- | --- | --- | --- | --- |
|焦距控制|zoom_control|可下发可上报（rw）|枚举型（Enum|枚举值: 0, 1|机械变焦, 0 减焦距 ，1  加焦距|
|停止变焦|zoom_stop|可下发可上报（rw）|布尔型（Bool）||true：停止变焦|

焦距调整涉及如上 DP，DP 相关操作参考上文 [云台 DP 封装](#ptz_dp_kit)。

## 收藏点

|功能点名称|标识符| 数据传输类型|数据类型|
| --- | --- | --- | --- |
|收藏点|memory_point_set|可下发可上报（rw）|字符型（String）|

收藏点涉及如上 DP。

### 获取收藏点列表

**接口说明**

```java
void requestCollectionPointList(ITuyaResultCallback<List<CollectionPointBean>> callback);
```

**参数说明**

|参数| 说明 |
| :--- | :--- |
| callback | 成功返回收藏点列表 <br> 失败返回错误码及错误信息 |

**CollectionPointBean**

| 参数 | 描述|
| --- | --- |
|id|收藏点 ID |
|devId|设备 ID |
|pos|预设点位置信息|
|name|预设点名字|
|pic|图的地址|
|mpId|设备内唯一 ID，设备生成|
|encryption|图片解密 Key 的存放 Map，为一个 JSON String。|

收藏点加密图片展示请参考 [`DecryptImageView`](https://developer.tuya.com/cn/docs/app-development/encryptimage?id=Ka6nxw2hetr2y#title-1-DecryptImageView)。

**示例代码**

```java
if (tuyaIPCPTZ != null) {
    tuyaIPCPTZ.requestCollectionPointList(new ITuyaResultCallback<List<CollectionPointBean>>() {
        @Override
        public void onSuccess(List<CollectionPointBean> result) {

        }

        @Override
        public void onError(String errorCode, String errorMessage) {

        }
    });
}
```

### 添加收藏点

全景巡航、收藏点巡航中不能添加收藏点，否则 `callback` 失败回调将会返回错误码 [`-2001`](#ptz_error_code)。

**接口说明**

```java
void addCollectionPoint(@NonNull String name, IResultCallback callback);
```

**参数说明**

|参数| 说明 |
| :--- | :--- |
| name | 自定义的收藏点名字 |
| callback | 结果回调 |

**示例代码**

```java
if (tuyaIPCPTZ != null) {
    tuyaIPCPTZ.addCollectionPoint("name", new IResultCallback() {
        @Override
        public void onError(String code, String error) {

        }

        @Override
        public void onSuccess() {

        }
    });
}
```

### 修改收藏点名称

**接口说明**

```java
void modifyCollectionPoint(@NonNull CollectionPointBean pointBean, @NonNull String name, IResultCallback callback);
```

**参数说明**

|参数| 说明 |
| :--- | :--- |
| pointBean | 要修改名字的收藏点Bean |
| name | 新名称 |
| callback | 结果回调 |

**示例代码**

```java
if (tuyaIPCPTZ != null) {
    tuyaIPCPTZ.modifyCollectionPoint(collectionPointBean, "NewTitle", new IResultCallback() {
        @Override
        public void onError(String code, String error) {
        }

        @Override
        public void onSuccess() {
        }
    });
}
```

### 删除收藏点

**接口说明**

```java
void deleteCollectionPoints(@NonNull List<CollectionPointBean> points, IResultCallback callback);
```

**参数说明**

|参数| 说明 |
| :--- | :--- |
| points | 需要删除的收藏点集合 |
| callback | 结果回调 |

**示例代码**

```java
if (tuyaIPCPTZ != null) {
    tuyaIPCPTZ.deleteCollectionPoints(points, new IResultCallback() {
        @Override
        public void onError(String code, String error) {

        }

        @Override
        public void onSuccess() {

        }
    });
}
```

### 预览收藏点

**接口说明**

```java
void viewCollectionPoint(@NonNull CollectionPointBean collectionPointBean, IResultCallback callback);
```

**参数说明**

|参数| 说明 |
| :--- | :--- |
| collectionPointBean | 需要预览的收藏点 |
| callback | 结果回调 |

**示例代码**

```java
if (tuyaIPCPTZ != null) {
    tuyaIPCPTZ.viewCollectionPoint(collectionPointBean, new IResultCallback() {
        @Override
        public void onError(String code, String error) {

        }

        @Override
        public void onSuccess() {

        }
    });
}
```

## 巡航模式

|功能点名称|标识符| 数据传输类型|数据类型|功能点属性|备注|
| --- | --- | --- | --- | --- | --- |
|巡航开关|cruise_switch|可下发可上报（rw）|布尔型（Bool）|-|-|
|巡航模式|cruise_mode|可下发可上报（rw）|枚举型（Enum）|枚举值: 0, 1|0 ：全景巡航 <br> 1：收藏点巡航
|巡航状态|cruise_status|可下发可上报（rw）|枚举型（Enum）|枚举值: 0, 1, 2|0：全景巡航中 <br> 1：收藏点巡航 <br> 2：非巡航模式
|巡航时间模式|cruise_time_mode|可下发可上报（rw）|枚举型（Enum）|枚举值: 0, 1 |0：全天巡航 <br> 1：自定义巡航|
|巡航时间|cruise_time|可下发可上报（rw）|字符型（String）||数据格式： <br> t_start：开始时间 <br> t_end：结束时间 <br> 多个时间由 `;` 分隔|

巡航涉及如上DP，设置巡航模式及设置定时巡航操作有单独接口封装，其他DP相关操作参考上文 [云台 DP 封装](#ptz_dp_kit)。

:::important
巡航状态（`cruise_status`）DP 设备不会主动上报，要获取此 DP 当前数据时，需要使用云台 DP 封装中[从设备查询数据](#dp_get_device)接口来查询。除了巡航状态 DP，其他云台相关 DP 皆为常规 DP，使用云台 DP 封装中[数据查询](#dp_data_get)接口来获取当前值即可。
:::

### 设置巡航模式

设置巡航模式，对应 DP `cruise_mode`。如果设置收藏点巡航，当收藏点个数小于 `2` 时，将返回错误码 [-2002](#ptz_error_code)。

**接口说明**

```java
void setCruiseMode(String mode, IResultCallback callback);
```

**参数说明**

|参数| 说明 |
| :--- | :--- |
| mode | 只支持两个值： <br> 0：全景巡航 <br> 1：收藏点巡航 |
| callback | 结果回调 |

**示例代码**

```java
if (tuyaIPCPTZ != null) {
    tuyaIPCPTZ.setCruiseMode("0", new IResultCallback() {
        @Override
        public void onError(String code, String error) {
        }

        @Override
        public void onSuccess() {

        }
    });
}
```

### 设置定时巡航

设置定时巡航。对应 DP `cruise_time_mode` 及 `cruise_time`。此接口会先设置 `cruise_time_mode` 值为定时巡航，再设置 `cruise_time` 的巡航时间。

**接口说明**

```java
void setCruiseTiming(@NonNull String startTime, @NonNull String endTime, IResultCallback callback);
```

**参数说明**

|参数| 说明 |
| :--- | :--- |
| startTime | 开始时间，格式：`00:00` |
| endTime | 结束时间，格式：`00:00` |
| callback | 结果回调 |

**示例代码**

```java
if (tuyaIPCPTZ != null) {
    tuyaIPCPTZ.setCruiseTiming("08:00", "10:00", new IResultCallback() {
        @Override
        public void onError(String code, String error) {

        }

        @Override
        public void onSuccess() {

        }
    });
}
```

## 移动追踪

|功能点名称|标识符| 数据传输类型|数据类型|
| --- | --- | --- | --- |
|移动追踪开关| motion_tracking | 可下发可上报（rw） | 布尔型 （Bool）|

移动追踪涉及如上 DP，DP 相关操作参考上文 [云台 DP 封装](#ptz_dp_kit)。

## 预设点

|功能点名称|标识符| 数据传输类型|数据类型|功能点属性|备注|
| --- | --- | --- | --- | --- | --- |
|预设点设置|ipc_preset_set|可下发可上报（rw）|枚举型（Enum）|枚举值: 1, 2, 3, 4|预设点 1、2、3、4|

预设点涉及如上 DP，DP 相关操作参考上文 [云台 DP 封装](#ptz_dp_kit)。

<a id="ptz_error_code"></a>

## 错误码

云台相关错误码

| 错误码 | 说明 |
| :---- | :---- |
| -2001 | 全景巡航、收藏点巡航中不能添加收藏点 |
| -2002 | 收藏点小于 2 个，无法开启收藏点巡航 |