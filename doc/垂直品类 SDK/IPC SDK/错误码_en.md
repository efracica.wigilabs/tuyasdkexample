This topic describes the operation status code for P2P (Peer-to-Peer) streaming features.

| Code | Description |
| ---- | ---- |
| 0 | No errors |
| -3 | Link timed out |
| -8 | Broken link |
| -12 | The link was closed by the device |
| -13 | Link no response timed out |
| -10001 | When the device is not connected [or if the session is broken] |
| -10002 | Invalid session |
| -10003 | Time out |
| -10004 | P2P connection was canceled halfway |
| -10006 | Device is offline |
| -20001 | Invalid command |
| -20002 | Invalid parameter |
| -20003 | Invalid data |
| -20004 | Cloud video during startup was interrupted by stop operation |
| -10000 | SDK is not initialized |
| -20005 | Operation not allowed |
| -20006 | This protocol is not supported by the current app version [app needs to be upgraded] |
| -20007 | Equipment service is busy [multi-person intercom is occupied, and more.] |
| -30001 | Failed to download file |
| -30002 | Not in playback state |
| -30003 | Failed to set playback speed |
| -30004 | Not playing in cloud storage |
| -30005 | Failed to delete playback data |
| -30006 | The download time of the playback cable is too long or 0 |
| -30007 | The playback image only supports JPG files |
| -30008 | Playback download status error |
| -30011 | No file recording is currently in progress |
| -30012 | Wrong audio and video parameters |
| -30013 | MP4 file header write failed |
| -30014 | MP4 file tracer write failure |