## Create message manager ITYCameraMessage

**Declaration**

Create the entity object of message manager.

```java
ITYCameraMessage createCameraMessage();
```

**Example**

```java
private ITYCameraMessage mTyCameraMessage;

ITuyaIPCMsg message = TuyaIPCSdk.getMessage();
if (message != null) {
    cameraMessage = message.createCameraMessage();
}
```

## Message calendar

You can use the SDK to look up the date of the alarm message in a certain year and a certain month so that it can be displayed visually on the calendar.

**Declaration**

The camera message center records the picture data, video data of the camera detection alarm, and data of someone visiting.

```java
void queryMotionDaysByMonth(String devId, int year, int month, ITuyaResultCallback<List<String>> callback);
```

**Parameter**

| Parameter | Description |
| ---- | ---- |
| devId | Device Id |
| year | Year |
| month | Month |
| callback | Operation callback |

**Example**

```java
mTyCameraMessage.queryMotionDaysByMonth(mDeviceBean.getDevId(), year, month, new ITuyaResultCallback<List<String>>() {
    @Override
    public void onSuccess(List<String> result) {
        
    }

    @Override
    public void onError(String errorCode, String errorMessage) {
        
    }
});
```

## Message type

There are many types of detection alarm messages according to the trigger mode, and some types can be divided into a large category. The IPC SDK provides a list of default classifications to facilitate querying alarm messages by classification.

**Declaration**

Get message type.

```java
void queryAlarmDetectionClassify(String devId, ITuyaResultCallback<List<CameraMessageClassifyBean>> callback);
```

**Parameter**

| Parameter | Description |
| --- | ---- |
| devId | Device id |
| msgCode | Message type array |
| callback | Operation callback |


**Example**

```java
mTyCameraMessage.queryAlarmDetectionClassify(devId, new ITuyaResultCallback<List<CameraMessageClassifyBean>>() {
@Override
public void onSuccess(List<CameraMessageClassifyBean> result) {

        }

@Override
public void onError(String errorCode, String errorMessage) {

        }
        });
```

**CameraMessageClassifyBean**

| Parameter | Description |
| ---- | ---- |
| describe | message description |
| msgCode | message type array |

The message type represents the trigger form of the alarm message, which is reflected in the code as the `msgCode` attribute of the alarm message data model.

**Message Type**

| Type | Description |
| ---- | ---- |
| ipc_motion | Motion detecting |
| ipc_doorbell | Doorbell ring |
| ipc_dev_link | Devices linkage |
| ipc_passby | Someone passby |
| ipc_linger | Someone linger |
| ipc_leave_msg | Doorbell message |
| ipc_connected | Doorbell ring has answered |
| ipc_unconnected | Doorbell not answered |
| ipc_refuse | Doorbell resisted |
| ipc_human | Human detection |
| ipc_cat | Pet detection |
| ipc_car | Car detection |
| ipc_baby_cry | Baby cry |
| ipc_bang | Abnormal sound |
| ipc_antibreak | Forced demolition alarm |
| ipc_low_battery | Low power alarm |

> Depending on the device's capabilities, the types of messages that can be triggered can vary.

## Message list

### Get a message list

**Declaration**

```java
void getAlarmDetectionMessageList(String devId, int startTime, int endTime, String[] msgCodes, int offset, int limit, ITuyaResultCallback<List<CameraMessageBean>> callback);
```
**Parameter**

| Parameter | Description |
| ---- | ---- |
| devId | Device id |
| startTime | Start timestamp, in second |
| endTime | End timestamp, in second |
| msgCodes | Message type array. If you dont't pass, all types of data are requested by default, if they are, then the corresponding type of message data is requested (for the message array, please refer to the message type interface introduction) |
| offset | Offset |
| limit | Limit |

**Example**

```java
mTyCameraMessage.getAlarmDetectionMessageList(devId, startTime, endTime, selectClassify.getMsgCode(), 0, 30, new ITuyaResultCallback<List<CameraMessageBean>>() {
@Override
public void onSuccess(List<CameraMessageBean> result) {

        }

@Override
public void onError(String errorCode, String errorMessage) {

        }
        });
```

**CameraMessageBean data model**

| Parameter | Description |
| ---- | ---- |
| dateTime | message create date |
| time | message create time stamp |
| attachPics | Picture of message event, array format, take the first data |
| attachVideos | Video data of message event, array format, take the first data |
| id | event ID |
| msgSrcId | message ID |

### Delete alarm messages

**Declaration**

delete Message.

```java
void deleteMotionMessageList(List<String> ids, ITuyaResultCallback<Boolean> callback);
```

**Parameter**

| Parameter | Description |
| ---- | ---- |
| ids | Delete the id list of the message |

**Example**

```java
cameraMessage.deleteMotionMessageList(ids, new ITuyaResultCallback<Boolean>() {
    @Override
    public void onSuccess(Boolean result) {
        
    }

    @Override
    public void onError(String errorCode, String errorMessage) {
        
    }
});
```

## Destroy

**Parameter**

```java
void destroy();
```

**Example**

```java
if (null != mTyCameraMessage) {
    mTyCameraMessage.destroy();
}
```
