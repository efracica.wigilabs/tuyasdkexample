# Detection alarm
Tuya smart camera usually has the ability to detect alarm. There are two main types of alarm detection, sound detection, and motion detection. When the device detects a sound or object moving, it sends a warning message, and if your app is integrated with push, it will also receive a push notification. For more information, see [Integrate Push](https://developer.tuya.com/en/docs/app-development/android-app-sdk/integrate-push/push?id=Ka6ki8ipunvb8).

The alarm message usually comes with a screenshot of the current video.

Direct-powered doorbell equipment has the ability to provide video messages. When someone rings the doorbell, the device can upload a video message, which is also picked up by an alarm message with a six-second encrypted video.