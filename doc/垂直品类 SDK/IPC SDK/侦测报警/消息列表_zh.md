## 创建消息管理器 ITYCameraMessage

**接口说明**

创建消息管理器的实体对象。

```java
ITYCameraMessage createCameraMessage();
```

**示例代码**

```java
private ITYCameraMessage mTyCameraMessage;

ITuyaIPCMsg message = TuyaIPCSdk.getMessage();
if (message != null) {
    cameraMessage = message.createCameraMessage();
}
```

## 消息日历

可以通过 IPC SDK 查询到某年某月有报警消息的日期，以便于在日历上直观展示。

**接口说明**

获取摄像机消息中心指定月含有消息的具体日期列表。

```java
void queryMotionDaysByMonth(String devId, int year, int month, ITuyaResultCallback<List<String>> callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| devId | 设备 id |
| year | 年 |
| month | 月 |
| callback | 回调方法 |

**示例代码**

```java
mTyCameraMessage.queryMotionDaysByMonth(mDeviceBean.getDevId(), year, month, new ITuyaResultCallback<List<String>>() {
    @Override
    public void onSuccess(List<String> result) {
        
    }

    @Override
    public void onError(String errorCode, String errorMessage) {
        
    }
});
```

## 消息类型

侦测报警消息根据触发方式定义有多种类型，部分类型又可以划分为一个大的分类。IPC SDK 提供获取默认分类的列表，以便于分类查询报警消息。

**接口说明**

获取消息类型。

```java
void queryAlarmDetectionClassify(String devId, ITuyaResultCallback<List<CameraMessageClassifyBean>> callback);
```

**参数说明**

| 参数 | 说明 |
| --- | ---- |
| devId | 设备 id |
| callback | 回调方法 |

**示例代码**

```java
mTyCameraMessage.queryAlarmDetectionClassify(devId, new ITuyaResultCallback<List<CameraMessageClassifyBean>>() {
    @Override
    public void onSuccess(List<CameraMessageClassifyBean> result) {
        
    }

    @Override
    public void onError(String errorCode, String errorMessage) {
        
    }
});
```

**CameraMessageClassifyBean 数据模型**

| 参数 | 说明 |
| ---- | ---- |
| describe | 消息描述 |
| msgCode | 消息类型数组 |

消息类型表示报警消息的触发形式，体现为报警消息数据模型的`msgCode`属性。

**消息类型说明**

| 类型 | 说明 |
| ---- | ---- |
| ipc_motion | 移动侦测 |
| ipc_doorbell | 门铃呼叫 |
| ipc_dev_link | 设备联动 |
| ipc_passby | 有人经过 |
| ipc_linger | 有人徘徊 |
| ipc_leave_msg | 门铃消息留言 |
| ipc_connected | 门铃已接听 |
| ipc_unconnected | 门铃未接听 |
| ipc_refuse | 门铃拒接 |
| ipc_human | 人形检测 |
| ipc_cat | 宠物检测 |
| ipc_car | 车辆检测 |
| ipc_baby_cry | 婴儿哭声 |
| ipc_bang | 异响 |
| ipc_antibreak | 强拆报警 |
| ipc_low_battery | 低电量告警 |

> 由于设备能力的不同，能触发的消息类型会有差别。消息分类和消息类型不同，消息类型表示报警消息的触发方式，消息分类是将一个或多个类型的消息组合成一个大类，比如 `ipc_passby`、`ipc_linger`、`ipc_motion` 可以组合成一个分类为移动侦测。

## 消息列表

### 获取侦测报警消息列表

**接口说明**

```java
void getAlarmDetectionMessageList(String devId, int startTime, int endTime, String[] msgCodes, int offset, int limit, ITuyaResultCallback<List<CameraMessageBean>> callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| devId | 设备 id |
| startTime | 起始时间戳，单位秒 |
| endTime | 结束时间戳，单位秒 |
| msgCodes | 消息类型数组。如果不传,默认请求所有类型的数据,如果传了,则请求对应类型的消息数据(消息数组可以查看消息类型接口介绍) |
| offset | 请求的偏移量，用来做分页 |
| limit | 请求的数量 |

>**说明**：如果要查询某一天的全部消息数据，可以将起始时间和结束时间间隔一天的时间戳即可。

**示例代码**

```java
mTyCameraMessage.getAlarmDetectionMessageList(devId, startTime, endTime, selectClassify.getMsgCode(), 0, 30, new ITuyaResultCallback<List<CameraMessageBean>>() {
    @Override
    public void onSuccess(List<CameraMessageBean> result) {
        
    }

    @Override
    public void onError(String errorCode, String errorMessage) {
        
    }
});
```

**CameraMessageBean 数据模型**

| 参数 | 说明 |
| ---- | ---- |
| dateTime | 发生日期 |
| time | 发生事件的时间戳 |
| attachPics | 消息事件的图片，数组格式，取第一个数据 |
| attachVideos | 消息事件的视频数据，数组格式，取第一个数据 |
| id | 事件 ID |
| msgSrcId | 消息 ID |


### 批量删除报警消息

**接口说明**

```java
void deleteMotionMessageList(List<String> ids, ITuyaResultCallback<Boolean> callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| ids | 删除消息的 id 列表 |

**示例代码**

```java
cameraMessage.deleteMotionMessageList(ids, new ITuyaResultCallback<Boolean>() {
    @Override
    public void onSuccess(Boolean result) {
        
    }

    @Override
    public void onError(String errorCode, String errorMessage) {
        
    }
});
```

## 销毁

**接口说明**

```java
void destroy();
```

**示例代码**

```java
if (null != mTyCameraMessage) {
    mTyCameraMessage.destroy();
}
```
