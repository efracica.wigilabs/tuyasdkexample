**TYCloudVideoPlayer** 是视频消息播放器的容器的实体对象，提供了播放视频消息相关的 API。

## 创建视频消息播放器 createVideoMessagePlayer

**接口说明**

创建视频消息播放器容器的实体对象

```java
ITYCloudVideo createVideoMessagePlayer();
```

**示例代码**

```java
private ITYCloudVideo mCloudVideo;

ITuyaIPCMsg message = TuyaIPCSdk.getMessage();
if (message != null) {
    mCloudVideo = message.createVideoMessagePlayer();
}
```

## 注册监听器 registerP2PCameraListener

**接口说明**

注册监听器，只有注册之后才能获取到视频播放的回调数据

```java
void registerP2PCameraListener(AbsP2pCameraListener listener);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| AbsP2pCameraListener | 视频播放回调数据 |

**示例代码**

```java
private ITYCloudVideo mCloudVideo;

mCloudVideo.registerP2PCameraListener(new AbsP2pCameraListener() {
    @Override
    public void onSessionStatusChanged(Object camera, int sessionId, int sessionStatus) {
        super.onSessionStatusChanged(camera, sessionId, sessionStatus);
    }
});
```

## 绑定播放器 generateCloudCameraView

**接口说明**

注入播放器 View，用来渲染视频画面

```java
void generateCloudCameraView(IRegistorIOTCListener view);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| IRegistorIOTCListener | 播放器组件 |

## 创建云视频播放对象 createCloudDevice

**接口说明**

创建云视频播放设备

```java
void createCloudDevice(String cachePath, String devId, OperationDelegateCallBack callBack);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| cachePath | 缓存文件地址 |
| devId | 设备 id |
| OperationDelegateCallBack | 操作回调 |

**示例代码**

```java
cloudVideo.createVideoMessagePlayer().createCloudDevice(cachePath, devId, new OperationDelegateCallBack() {
            @Override
            public void onSuccess(int sessionId, int requestId, String data) {
                mHandler.sendMessage(MessageUtil.getMessage(ICameraVideoPlayModel.MSG_CLOUD_MEDIA_DEVICE, ICameraVideoPlayModel.OPERATE_SUCCESS, data));
            }

            @Override
            public void onFailure(int sessionId, int requestId, int errCode) {
                mHandler.sendMessage(MessageUtil.getMessage(ICameraVideoPlayModel.MSG_CLOUD_MEDIA_DEVICE, ICameraVideoPlayModel.OPERATE_FAIL, errCode));
            }
});
```

## 播放视频 playVideo

**接口说明**

播放报警消息中的视频

```java
void playVideo(String videoUrl, int startTime, String encryptKey, OperationCallBack callback, OperationCallBack playFinishedCallBack);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| videoUrl | 视频播放地址 |
| startTime | 开始播放时间，初始值：0 |
| encryptKey | 播放视频的秘钥 |
| callback | 操作回调 |
| playFinishedCallBack | 结束播放的操作回调 |

**示例代码**

```java
public void playVideo(String videoUrl, int startTime, String encryptKey) {
        if (cloudVideo == null) {
            return;
        }
        cloudVideo.playVideo(videoUrl, startTime, encryptKey, new OperationCallBack() {
            @Override
            public void onSuccess(int sessionId, int requestId, String data, Object camera) {
                playState = CloudPlayState.STATE_PLAYING;
                mHandler.sendMessage(MessageUtil.getMessage(ICameraVideoPlayModel.MSG_CLOUD_VIDEO_PLAY, ICameraVideoPlayModel.OPERATE_SUCCESS));
            }

            @Override
            public void onFailure(int sessionId, int requestId, int errCode, Object camera) {
                playState = CloudPlayState.STATE_ERROR;
                mHandler.sendMessage(MessageUtil.getMessage(ICameraVideoPlayModel.MSG_CLOUD_VIDEO_PLAY, ICameraVideoPlayModel.OPERATE_FAIL));
            }
        }, new OperationCallBack() {
            @Override
            public void onSuccess(int sessionId, int requestId, String data, Object camera) {
                playState = CloudPlayState.STATE_COMPLETED;
                cloudVideo.audioClose();
                mHandler.sendMessage(MessageUtil.getMessage(ICameraVideoPlayModel.MSG_CLOUD_VIDEO_STOP, ICameraVideoPlayModel.OPERATE_SUCCESS));
            }

            @Override
            public void onFailure(int sessionId, int requestId, int errCode, Object camera) {
                cloudVideo.audioClose();
                mHandler.sendMessage(MessageUtil.getMessage(ICameraVideoPlayModel.MSG_CLOUD_VIDEO_STOP, ICameraVideoPlayModel.OPERATE_FAIL));
            }
        });
}
```

## 暂停播放 pauseVideo

**接口说明**

暂停播放

```java
void pauseVideo(OperationCallBack callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| OperationDelegateCallBack | 操作回调 |

**示例代码**

```java
public void pauseVideo() {
        if (cloudVideo == null) {
            return;
        }
        cloudVideo.pauseVideo(new OperationCallBack() {
            @Override
            public void onSuccess(int sessionId, int requestId, String data, Object camera) {
                playState = CloudPlayState.STATE_PAUSED;
                mHandler.sendMessage(MessageUtil.getMessage(ICameraVideoPlayModel.MSG_CLOUD_VIDEO_PAUSE, ICameraVideoPlayModel.OPERATE_SUCCESS));
            }

            @Override
            public void onFailure(int sessionId, int requestId, int errCode, Object camera) {
                mHandler.sendMessage(MessageUtil.getMessage(ICameraVideoPlayModel.MSG_CLOUD_VIDEO_PAUSE, ICameraVideoPlayModel.OPERATE_FAIL));
            }
        });
}
```

## 恢复播放 resumeVideo

**接口说明**

恢复播放

```java
void resumeVideo(OperationCallBack callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| OperationDelegateCallBack | 操作回调 |

**示例代码**

```java
public void  resumeVideo() {
        if (cloudVideo == null) {
            return;
        }
        cloudVideo.resumeVideo(new OperationCallBack() {
            @Override
            public void onSuccess(int sessionId, int requestId, String data, Object camera) {
                playState = CloudPlayState.STATE_PLAYING;
                mHandler.sendMessage(MessageUtil.getMessage(ICameraVideoPlayModel.MSG_CLOUD_VIDEO_RESUME, ICameraVideoPlayModel.OPERATE_SUCCESS));
            }

            @Override
            public void onFailure(int sessionId, int requestId, int errCode, Object camera) {
                mHandler.sendMessage(MessageUtil.getMessage(ICameraVideoPlayModel.MSG_CLOUD_VIDEO_RESUME, ICameraVideoPlayModel.OPERATE_FAIL));
            }
        });
}
```

## 停止播放 stopVideo

**接口说明**

停止播放

```java
void stopVideo(OperationCallBack callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| OperationDelegateCallBack | 操作回调 |

**示例代码**

```java
public void stopVideo() {
        if (cloudVideo == null) {
            return ;
        }
        cloudVideo.stopVideo(new OperationCallBack() {
            @Override
            public void onSuccess(int sessionId, int requestId, String data, Object camera) {
                playState = CloudPlayState.STATE_STOP;
                mHandler.sendMessage(MessageUtil.getMessage(ICameraVideoPlayModel.MSG_CLOUD_VIDEO_STOP, ICameraVideoPlayModel.OPERATE_SUCCESS));
            }

            @Override
            public void onFailure(int sessionId, int requestId, int errCode, Object camera) {
                mHandler.sendMessage(MessageUtil.getMessage(ICameraVideoPlayModel.MSG_CLOUD_VIDEO_STOP, ICameraVideoPlayModel.OPERATE_FAIL));
            }
        });
}
```

## 设置声音开关 setCloudVideoMute

**接口说明**

设置视频播放的声音开关

```java
void setCloudVideoMute(int mute, OperationDelegateCallBack callBack);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| mute | 拾音器模式: ICameraP2P.MUTE/ICameraP2P.UNMUTE（静音/非静音） |
| OperationDelegateCallBack | 操作回调 |

**示例代码**

```java
private void setCloudVideoMute(int voiceMode) {
        if (cloudVideo == null) {
            return ;
        }
        cloudVideo.setCloudVideoMute(voiceMode,  new  OperationDelegateCallBack() {

            @Override
            public void onSuccess(int sessionId, int requestId, String data) {
                mHandler.sendMessage(MessageUtil.getMessage(ICameraVideoPlayModel.MSG_CLOUD_VIDEO_MUTE, IPanelModel.ARG1_OPERATE_SUCCESS, data));
            }

            @Override
            public void onFailure(int sessionId, int requestId, int errCode) {
                mHandler.sendMessage(MessageUtil.getMessage(ICameraVideoPlayModel.MSG_CLOUD_VIDEO_MUTE, IPanelModel.ARG1_OPERATE_FAIL));
            }
        });
}
```

## 销毁播放对象 deinitCloudVideo

**接口说明**

当云视频不再使用的时候，销毁云视频播放设备

```java
void deinitCloudVideo();
```

## 视频数据 onReceiveFrameYUVData

**接口说明**

视频数据回调

```java
public void onReceiveFrameYUVData(int sessionId, ByteBuffer y, ByteBuffer u, ByteBuffer v, int width, int height, int nFrameRate, int nIsKeyFrame, long timestamp, long nProgress, long nDuration, Object camera);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| sessionId | \ |
| Y | 视频Y数据 |
| u | 视频U数据 |
| v | 视频V数据 |
| width | 视频画面的宽 |
| height | 视频画面的高 |
| nFrameRate | 帧率 |
| nIsKeyFrame | 是否I帧 |
| timestamp | 时间戳 |
| nProgress | 时间进度(消息中心视频播放的进度) |
| nDuration | 时长(消息中心视频播放时长) |
| camera | \ |

  > 在这里的视频播放只要关心 nProgress，nDuration 即可

**示例代码**

  ```java
public void onReceiveFrameYUVData(int sessionId, ByteBuffer y, ByteBuffer u, ByteBuffer v, int width, int height, int nFrameRate, int nIsKeyFrame, long timestamp, long nProgress, long nDuration, Object camera) {
        Map map = new HashMap<String, Long>(2);
        map.put("progress", nProgress);
        map.put("duration", nDuration);
        mHandler.sendMessage(MessageUtil.getMessage(ICameraVideoPlayModel.MSG_CLOUD_VIDEO_INFO, ICameraVideoPlayModel.OPERATE_SUCCESS, map));
}
  
  ```

## 流程图

![流程图](https://images.tuyacn.com/fe-static/docs/img/5efc0a3b-1f24-4837-994e-d1c6f66bf664.png)

## 报警消息与存储卡回放

报警消息和存储卡回放没有直接联系，唯一的关联是在存储卡事件录制模式的情况下，报警消息和存储卡视频录制的触发原因和时间点是一样的。

报警消息保存在涂鸦云端，存储卡视频录像保存在摄像机的存储卡中，且存储卡中的视频在容量不足时，可能会被覆盖。存储卡录制的开关和侦测报警的开关也没有关联，所以即使在存储卡事件录制的模式下，报警消息和存储卡中的视频录像也不是一一对应的。

但是存在报警消息发生的时间点有视频录像的情况，IPC SDK 并不提供这种关联查找的接口，开发者可以通过报警消息的触发时间，在当天的存储卡录像视频片段中查找是否有对应的视频录像来建立这种关联。