
**TYCloudVideoPlayer** is the entity object of the container of the video message player and provides API methods related to playing video messages.

## createVideoMessagePlayer

**Description**

Create the entity object of the container of the video message player.

```java
ITYCloudVideo createVideoMessagePlayer();
```

**Example**

```java
private ITYCloudVideo mCloudVideo;

ITuyaIPCMsg message = TuyaIPCSdk.getMessage();
if (message != null) {
    mCloudVideo = message.createVideoMessagePlayer();
}
```

## registerP2PCameraListener

**Description**

Register the listener. Only after registration can you get the callback data for video playback.

```java
void registerP2PCameraListener(AbsP2pCameraListener listener);
```

**Parameter**

| Parameter | Description |
| ---- | ---- |
| AbsP2pCameraListener | Video play callback data |

**Example**

```java
private ITYCloudVideo mcloudCamera;

mcloudCamera = new TYCloudVideoPlayer();
mcloudCamera.registerP2PCameraListener(new AbsP2pCameraListener() {
    @Override
    public void onSessionStatusChanged(Object camera, int sessionId, int sessionStatus) {
        super.onSessionStatusChanged(camera, sessionId, sessionStatus);
    }
});
```

## generateCloudCameraView

**Description**

Inject player view to render video.

```java
void generateCloudCameraView(IRegistorIOTCListener view);
```

**Parameter**

| Parameter | Description |
| ---- | ---- |
| IRegistorIOTCListener | Video play callback data |

## createCloudDevice

**Description**

Create Message VideoPlayer device.

```java
void createCloudDevice(String cachePath, String devId, OperationDelegateCallBack callBack);
```
**Parameter**

| Parameter | Description |
| ---- | ---- |
| cachePath | cache Video file path |
| devId | device ID |
| OperationDelegateCallBack | operation callback |

**Example**

```java
cloudVideo.createVideoMessagePlayer().createCloudDevice(cachePath, devId, new OperationDelegateCallBack() {
            @Override
            public void onSuccess(int sessionId, int requestId, String data) {
                mHandler.sendMessage(MessageUtil.getMessage(ICameraVideoPlayModel.MSG_CLOUD_MEDIA_DEVICE, ICameraVideoPlayModel.OPERATE_SUCCESS, data));
            }

            @Override
            public void onFailure(int sessionId, int requestId, int errCode) {
                mHandler.sendMessage(MessageUtil.getMessage(ICameraVideoPlayModel.MSG_CLOUD_MEDIA_DEVICE, ICameraVideoPlayModel.OPERATE_FAIL, errCode));
            }
});
```

## playVideo

**Description**

Play video in the alarm message.

```java
void playVideo(String videoUrl, int startTime, String encryptKey, OperationCallBack callback, OperationCallBack playFinishedCallBack);
```

**Parameter**

| Parameter | Description |
| ---- | ---- |
| videoUrl | Video url |
| startTime | Start playback time, initial value: 0. |
| encryptKey | Key for playing video |
| callback | Operation callback |
| playFinishedCallBack | Finish play callback |

**Example**

```java
public void playVideo(String videoUrl, int startTime, String encryptKey) {
        if (cloudVideo == null) {
            return;
        }
        cloudVideo.playVideo(videoUrl, startTime, encryptKey, new OperationCallBack() {
            @Override
            public void onSuccess(int sessionId, int requestId, String data, Object camera) {
                playState = CloudPlayState.STATE_PLAYING;
                mHandler.sendMessage(MessageUtil.getMessage(ICameraVideoPlayModel.MSG_CLOUD_VIDEO_PLAY, ICameraVideoPlayModel.OPERATE_SUCCESS));
            }

            @Override
            public void onFailure(int sessionId, int requestId, int errCode, Object camera) {
                playState = CloudPlayState.STATE_ERROR;
                mHandler.sendMessage(MessageUtil.getMessage(ICameraVideoPlayModel.MSG_CLOUD_VIDEO_PLAY, ICameraVideoPlayModel.OPERATE_FAIL));
            }
        }, new OperationCallBack() {
            @Override
            public void onSuccess(int sessionId, int requestId, String data, Object camera) {
                playState = CloudPlayState.STATE_COMPLETED;
                cloudVideo.audioClose();
                mHandler.sendMessage(MessageUtil.getMessage(ICameraVideoPlayModel.MSG_CLOUD_VIDEO_STOP, ICameraVideoPlayModel.OPERATE_SUCCESS));
            }

            @Override
            public void onFailure(int sessionId, int requestId, int errCode, Object camera) {
                cloudVideo.audioClose();
                mHandler.sendMessage(MessageUtil.getMessage(ICameraVideoPlayModel.MSG_CLOUD_VIDEO_STOP, ICameraVideoPlayModel.OPERATE_FAIL));
            }
        });
}
```

**Description**

Pause playing.

```java
void pauseVideo(OperationCallBack callback);
```

**Parameter**

| Parameter | Description |
| ---- | ---- |
| OperationDelegateCallBack | operation callback |

**Example**

```java
public void pauseVideo() {
        if (cloudVideo == null) {
            return;
        }
        cloudVideo.pauseVideo(new OperationCallBack() {
            @Override
            public void onSuccess(int sessionId, int requestId, String data, Object camera) {
                playState = CloudPlayState.STATE_PAUSED;
                mHandler.sendMessage(MessageUtil.getMessage(ICameraVideoPlayModel.MSG_CLOUD_VIDEO_PAUSE, ICameraVideoPlayModel.OPERATE_SUCCESS));
            }

            @Override
            public void onFailure(int sessionId, int requestId, int errCode, Object camera) {
                mHandler.sendMessage(MessageUtil.getMessage(ICameraVideoPlayModel.MSG_CLOUD_VIDEO_PAUSE, ICameraVideoPlayModel.OPERATE_FAIL));
            }
        });
}
```

## resumeVideo

**Description**

Resume playing.

```java
void resumeVideo(OperationCallBack callback);
```

**Parameter**

| Parameter | Description |
| ---- | ---- |
| OperationDelegateCallBack | operation callback |

**Example**

```java
public void resumeVideo() {
        if (cloudVideo == null) {
            return;
        }
        cloudVideo.resumeVideo(new OperationCallBack() {
            @Override
            public void onSuccess(int sessionId, int requestId, String data, Object camera) {
                playState = CloudPlayState.STATE_PLAYING;
                mHandler.sendMessage(MessageUtil.getMessage(ICameraVideoPlayModel.MSG_CLOUD_VIDEO_RESUME, ICameraVideoPlayModel.OPERATE_SUCCESS));
            }

            @Override
            public void onFailure(int sessionId, int requestId, int errCode, Object camera) {
                mHandler.sendMessage(MessageUtil.getMessage(ICameraVideoPlayModel.MSG_CLOUD_VIDEO_RESUME, ICameraVideoPlayModel.OPERATE_FAIL));
            }
        });
}
```

## stopVideo

**Description**

Stop playing.

```java
void stopVideo(OperationCallBack callback);
```

**Parameter**

| Parameter | Description |
| ---- | ---- |
| OperationDelegateCallBack | operation callback |

**Example**

```java
public void stopVideo() {
        if (cloudVideo == null) {
            return ;
        }
        cloudVideo.stopVideo(new OperationCallBack() {
            @Override
            public void onSuccess(int sessionId, int requestId, String data, Object camera) {
                playState = CloudPlayState.STATE_STOP;
                mHandler.sendMessage(MessageUtil.getMessage(ICameraVideoPlayModel.MSG_CLOUD_VIDEO_STOP, ICameraVideoPlayModel.OPERATE_SUCCESS));
            }

            @Override
            public void onFailure(int sessionId, int requestId, int errCode, Object camera) {
                mHandler.sendMessage(MessageUtil.getMessage(ICameraVideoPlayModel.MSG_CLOUD_VIDEO_STOP, ICameraVideoPlayModel.OPERATE_FAIL));
            }
        });
}
```

## setCloudVideoMute

**Description**

Mute the cloud video.

```java
void setCloudVideoMute(int mute, OperationDelegateCallBack callBack);
```

**Parameter**

| Parameter | Description |
| ---- | ---- |
| mute | Pickup mode: ICameraP2P.MUTE / ICameraP2P.UNMUTE (mute / non-mute) |
| OperationDelegateCallBack | operation callback |

**Example**

```java
private void setCloudVideoMute(int voiceMode) {
        if (cloudVideo == null) {
            return ;
        }
        cloudVideo.setCloudVideoMute(voiceMode,  new  OperationDelegateCallBack() {

            @Override
            public void onSuccess(int sessionId, int requestId, String data) {
                mHandler.sendMessage(MessageUtil.getMessage(ICameraVideoPlayModel.MSG_CLOUD_VIDEO_MUTE, IPanelModel.ARG1_OPERATE_SUCCESS, data));
            }

            @Override
            public void onFailure(int sessionId, int requestId, int errCode) {
                mHandler.sendMessage(MessageUtil.getMessage(ICameraVideoPlayModel.MSG_CLOUD_VIDEO_MUTE, IPanelModel.ARG1_OPERATE_FAIL));
            }
        });
}
```

## deinitCloudVideo

**Description**

When cloud video is no longer used, deinit cloud video.

```java
void deinitCloudVideo();
```

**Description**

Callback video YUV data.

```java
void onReceiveFrameYUVData(int sessionId, ByteBuffer y, ByteBuffer u, ByteBuffer v, int width, int height, int nFrameRate, int nIsKeyFrame, long timestamp, long nProgress, long nDuration, Object camera);
```

**Parameter**

| Parameter | Description |
| ---- | ---- |
| sessionId | p2p session ID |
| y | Video Y data |
| u | Video U data |
| v | Video V data |
| width | Video width |
| height | Video height |
| timestamp | Time stamp |
| nFrameRate | Frame rate |
| nIsKeyFrame | Whether I frame |
| nProgress | Time progress (the progress of the message center video) |
| nDuration | Duration (message center video duration) |
| camera | N/A |

As long as the video here is concerned about nProgress, nDuration.

**Sample code:**

```java
public void onReceiveFrameYUVData(int sessionId, ByteBuffer y, ByteBuffer u, ByteBuffer v, int width, int height, int nFrameRate, int nIsKeyFrame, long timestamp, long nProgress, long nDuration, Object camera) {
        Map map = new HashMap<String, Long>(2);
        map.put("progress", nProgress);
        map.put("duration", nDuration);
        mHandler.sendMessage(MessageUtil.getMessage(ICameraVideoPlayModel.MSG_CLOUD_VIDEO_INFO, ICameraVideoPlayModel.OPERATE_SUCCESS, map));
}
```

## Flow chart

![flow chart](https://images.tuyacn.com/fe-static/docs/img/5efc0a3b-1f24-4837-994e-d1c6f66bf664.png)

## Alarm message and playback

There is no direct correlation between the alarm message and the record videos in the memory card, the only correlation is that in the memory card event recording mode, the trigger reason and time point of the alarm message and record video are the same.

The alarm message is saved in the Tuya cloud, and the video recording of the memory card is saved in the camera's memory card, and the video in the memory card may be overwritten when the capacity is insufficient. The switch recorded by the memory card is not related to the switch that detects the alarm, so even in the mode of recording the memory card event, the alarm message and the video recording in the memory card are not one-to-one.

However, there are cases where video footage at the point in time when the alarm message occurs. The IPC SDK does not provide an interface to find this association, so you can establish this association by looking for the corresponding video footage on the memory card at the time the alarm message is triggered.