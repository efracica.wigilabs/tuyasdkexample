
## SDK 架构

### 简介

**IPC SDK** 基于 **涂鸦 IoT App SDK** 封装了智能摄像机的相关功能。整个 SDK 架构组成分为三个层级：涂鸦全屋智能 SDK、IPC 网络通信层、摄像机业务层。

- 涂鸦 IoT App SDK 提供了以家庭为单位，对硬件设备、涂鸦云通讯等接口封装。
- IPC 网络通信层提供了 P2P 网络通道实现。
- 摄像机业务层提供了音视频通讯和展示、设备功能点、报警消息、云存储视频管理等业务功能。

> Note: IPC 是IP Camera 的简称，中文称为网络摄像机，由网络编码模块和 [模拟摄像机](https://baike.baidu.com/item/模拟摄像机/4419990) 组合而成。

### 架构图

![image.png](https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/content-platform/hestia/163782410059520ca586c.png)

IPC SDK 依赖通信层的组件包括 P2P SDK、Audio-engine 等，以及 IPC SDK 提供的功能组件包括视频直播、存储卡回放、云视频等，在导入 IPC SDK 时都会自动导入。
