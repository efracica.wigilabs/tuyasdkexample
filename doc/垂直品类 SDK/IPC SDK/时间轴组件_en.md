
When the timeline component uses video playback or cloud storage video playback, it linearly displays the video recording time points that can be played. You can slide the timeline to accurately locate the time point and segment to start playing.

![Timeline View](https://images.tuyacn.com/fe-static/docs/img/d8baf638-b317-463d-98a2-532b251c883d.png)

### Integrate

```groovy
dependencies {
    implementation 'com.tuya.smart:tuyasmart-ipc-camera-timeline:1.0.1'
}
```

**Class and Interface**

|                Class（Interface）            |       Description    |
| ---- | ---- |
| TuyaTimelineView                     | Timeline view   |
| OnBarMoveListener  | Timeline drag listener  |
| OnSelectedTimeListener | Timeline select listener |

## Introduction

### TuyaTimelineView

**Attrs**


|       Name    |     Description  |
| ---- | ---- |
| topTextMargin | Margin top of text |
| bottomTextMargin | Margin bottom of text  |
| smallRulerHeight | Height of short tick mark |
| timeScaleColor  | Color of time text string  |
| linesColor   | Color of tick mark    |
| bubbleColor  | Background color of bubble  |
| bubbleTextColor | Color of bubble text  |


**Declaration**

Set the colors of the data rendering in the timeline

```java
void setContentShader(Shader contentShader)
```

**Declaration**

Set time zone

```java
void setTimeZone(TimeZone zone)
```

**Declaration**

There will be a bubble text showing the current time When dragging

Set whether to display bubble text

```java
void setShowBubbleWhenDrag(boolean show)
```

**Declaration**

Set the width of a time unit in px, two long tick marks represent a time unit

```java
void setSpacePerUnit(int spacePerUnit)
```

**Declaration**

Time unit mode, currently there are three, one time unit is 60 seconds, 600 seconds, 3600 seconds

```java
void setUnitMode(TimelineUnitMode mode)
```

**Parameters**

|             Enum         |        Description    |
| ---- | ---- |
| TimelineUnitMode.Mode_60  | one time unit is 60 seconds  |
| TimelineUnitMode.Mode_600 | one time unit is 600 seconds |
| TimelineUnitMode.Mode_3600 | one time unit is 3600 seconds |

**Declaration**

Set whether to display time text

```java
void setShowTimeText(boolean showTimeText)
```

**Declaration**

Set whether to show short tick marks

```java
void setShowShortMark(boolean showShortMark)
```

**Declaration**

Selection mode switch

```java
void showSelectTimeArea(boolean open)
```

**Declaration**

Set the length of time that the selection box can be selected 

```java
void setSelectTimeAreaRange(long min, long max)
```

|      Parameter       |      Description      |
| --------------- | ------------- |
| min | The minimum length of time |
| max | The maximum length of time |


**Declaration**

Set the background color of the timeline selection box

```java
void setSelectionBoxColor(int color)
```

**Declaration**

Set the color of the vertical line of the timeline selection box

```java
void setSelectCenterColor(int selectCenterColor)
```

**Declaration**

Is in selection mode    

```java
boolean isSelectionEnabled()
```

**Declaration**

timeline Config,

```java
void setCurrentTimeConfig(long currentTimeInMillisecond)
```

**Declaration**

Set the current time of the timeline in ms

```java
void setCurrentTimeInMillisecond(long currentTimeInMillisecond)
```

**Declaration**

Set am/pm string of bubble text in 12-hour format

```java
void setTimeAmPmString(String timeAmString, String timePmString)
```

**Declaration**

Set the timeline source data array  

```java
void setRecordDataExistTimeClipsList(List<TimeBean> source)
```

**Declaration**

Turn on automatically find the next segment to play

```java
public void setCanQueryData()
```

**Declaration**

Set whether to allow searching for the next segment

```java
public synchronized void setQueryNewVideoData(boolean var1)
```

**Declaration**

Set timeline style

| Timeline style | value |
| ---- | --- |
| Default style. The time scale value is at the top, show small ruler | 1 |
| The time scale value is at the bottom, hide smarll ruler | 2 |

> The style 2 bubble text is above the view, so you need to set android:clipChildren="false" for the parent of the timeline view to display normally.

```java
public void setStyle(int style);
```

**Declaration**

Set the color of the center cursor

```java
public void setMiddleCursorColor(int color)
```


### OnBarMoveListener

**Declaration**

Callback when dragging starts

```java
void onBarActionDown()
```

**Declaration**

Callback when dragging

```java
void onBarMove(long screenLeftTime, long screenRightTime, long currentTime)
```

**Parameters**

|      Parameter   |      Description  |
| ---- | ---- |
| screenLeftTime | The time represented on the far left of the screen |
| screenRightTime | The time represented on the far right of the screen |
| currentTime | Current time    |

**Declaration**

Callback when the timeline drag is over, when the drag is over, it will automatically find the segment data that can be played

```java
void onBarMoveFinish(long startTime, long endTime, long currentTime)
```

**Parameters**

|    Parameter |                Description             |
| ---- | ---- |
| startTime  | The start time of the fragment data, if there is no fragment data, it will return -1 |
| endTime | The end time of the fragment data, if there is no fragment data, it will return -1 |
| currentTime | Time when dragging stopped, if there is no fragment data, it will return -1 |

### OnSelectedTimeListener

**Declaration**

Callback when dragging in selection mode

```java
void onDragging(long selectStartTime, long selectEndTime)
```

**Parameters**

|      Parameter   |         Description     |
| ---- | ---- |
| selectStartTime | Selected start time |
| selectEndTime   | Selected end time |