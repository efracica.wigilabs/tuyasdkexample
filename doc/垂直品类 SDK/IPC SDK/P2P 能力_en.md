In traditional solutions, a server forwards data between an app and a device. During peak hours, this transmission mode consumes a high number of space and traffic resources on the service and results in costly operations and maintenance (O&M). Tuya's peer-to-peer (P2P) connection solution is an ideal option to resolve this issue and achieve the best price performance in O&M.

## Integration

Add the following code block to the project file `build.gradle`:

	```groovy
	allprojects {
	    repositories {
	        ...
	        maven {
	            url "https://maven-other.tuya.com/repository/maven-releases/"
	        }
		...
	    }
	}
	```

Add the following codes to the app build.gradle file:

	```groovy
	dependencies {
		...
		implementation 'com.tuya.smart:tuyasmart-p2p-channel-sdk:3.4.15'
		...
	}
	```

	>**Note**: IPC SDK already includes P2P capability and no further integration is required.

## Get a P2P object

Returns a P2P object that is a singleton.

**API description**
```java
ITuyaP2P p2pSDK = TuyaP2PSdk.getP2P();
```

## Initialize the SDK

Initializes the P2P SDK.

**API description**

```java
int init(String localId);
```

**Parameters**

| Parameter | Description |
| ---- | ---- |
| localId | The user's UID. The value for a logged-in account can be obtained from `User` of the data model in [User Account Management (Android)](https://developer.tuya.com/en/docs/app-development/usermanage?id=Ka69qtzy9l8nc#title-1-User). |

**Example**

```java
ITuyaP2P p2p = TuyaIPCSdk.getP2P();
if (p2p != null) {
    p2p.init("xxx");
}
```

## Deinitialize the SDK

Deinitializes the P2P SDK. This API method is called to exit an account in most cases.

**API description**

```java
int deInit();
```

## Create a P2P connection

**API description**

Creates a P2P connection to a specific device. The value of `traceId` in the response indicates the connection identifier that can be used to cancel an ongoing connection attempt. In the callback, the handle of the successful connection is returned.

This API method is supported by `v3.4.8`.

```java
 String connect(String remoteId, int lanMode, int timeout, ITuyaP2PCallback callback);
```

**Parameters**

| Parameter | Description |
| ---- | ---- |
| remoteId | The device ID. |
| lanMode | The prioritized connection mode. |
| timeout | The connection timeout value. |
| callback | The connection result. When the value is larger than or equal to `0`, the P2P connection is successful and the session handle of the connection is returned. If the value is less than `0`, the connection failed and an error message is returned. |

| lanMode | Description |
| ---- | --- |
| 0 | Automatically selected |
| 1 | Internet connections prioritized |
| 2 | Local area network (LAN) connections prioritized |

**Return values**

| Return value | Description |
| ---- | --- |
| traceId | The unique identifier of the connection. It can be used to cancel an ongoing connection attempt. |

## Cancel an ongoing connection attempt

Cancels an ongoing connection attempt. The existing connections are not affected.

**API description**

```java
int connectBreak(String traceId);
```

**Parameters**

| Parameter | Description |
| ---- | --- |
| traceId | The unique identifier of the connection. |

## Close a connection

**API description**

```java
int disConnect(int handle);
```

**Parameters**

| Parameter | Description |
| ---- | --- |
| handle | The handle of the connection. |

## Receive data

**API description**

```java
int recvData(int handle, int channel, byte[] data, int dataLen, int timeout);
```

**Parameters**

| Parameter | Description |
| ---- | --- |
| handle | The handle of the connection. |
| channel | The channel number. |
| data | The buffer that receives data. |
| dataLen | The length of the received data. |
| timeout | The timeout value. Unit: milliseconds. |

**Return values**

| Return value | Description |
| ---- | --- |
| < 0 | An error has occurred. A value of `-3` indicates a timeout error. Other values indicate that the connection is closed. |
| >= 0 | The length of the received data. |

## Send data to the peer

**API description**

```java
int sendData(int handle, int channel, byte[] data, int dataLen, int timeout);
```

**Parameters**

| Parameter | Description |
| ---- | --- |
| handle | The handle of the connection. |
| channel | The channel number. |
| data | The content to be sent |
| dataLen | The length of the data to be sent. |
| timeout | The timeout value. Unit: milliseconds. |

**Return values**

| Return value | Description |
| ---- | --- |
| < 0 | An error has occurred. A value of `-3` indicates a timeout error. Other values indicate that the connection is closed. |
| >= 0 | The length of the transmitted data. |

## Check the connection status

**API description**

```java
int activeCheck(int handle);
```

**Parameters**

| Parameter | Description |
| ---- | --- |
| handle | The handle of the connection. |

**Return values**

| Return value | Description |
| ---- | --- |
| < 0 | The connection is closed. |
| >= 0 | The connection is normal. |

## Query the P2P SDK version number

**API description**

```java
String getVersion();
```

## Channel numbers

In the existing data protocols for `Powered by Tuya` (PBT) devices, eight data channels are initialized for each P2P session and six of these channels are defined and used.

| Channel number | Description |
| ---- | ---- |
| 0 | The channel to send and receive control commands. |
| 1 | The channel to transmit video data. |
| 2 | The channel to transmit audio data. |
| 3 | The channel to play back and transmit video footage on an SD card. |
| 4 | The data pass-through channel. |
| 5 | The channel to download and transmit files. |

## Error codes

| Error code | Description |
| ---- | ---- |
| -1001 | Other networking errors. |
| -1002 | The P2P type is not supported. |
| -1003 | Parameter errors. |
| | |