除了实时视频直播，存储卡录像播放以外，IPC SDK 还提供了一些额外的音视频能力。

## 本地录制

当视频成功开始播放以后（可以是视频直播，也可以是录像回放），可以将当前正在播放的视频录制到手机中。

> **说明**：在视频录制的过程中，请不要再切换视频清晰度，开关声音及对讲。
> 
> 如果需要将录制的视频保存至系统相册，需要开发者自己实现。注意Android10开始采用[分区存储](https://developer.android.com/training/data-storage?hl=zh-cn#scoped-storage)机制（可禁用，但Android11强制使用），保存媒体文件至系统相册需使用[MediaStore](https://developer.android.com/reference/android/provider/MediaStore?hl=zh-cn) API。

### 开启视频录制

**接口说明**

```java
int startRecordLocalMp4(String folderPath, Context context, OperationDelegateCallBack callBack);
```

> 注：录制视频需要写存储卡权限

**参数说明**

|    参数 |       说明 |
| ---- | ---- |
| folderPath | 保存视频的文件路径 |
| context | 上下文 |
| callBack | 操作回调 |

**示例代码**

```java
private ITuyaSmartCameraP2P mCameraP2P;

if (Constants.hasStoragePermission()) {
    String picPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Camera/";
    File file = new File(picPath);
    if (!file.exists()) {
        file.mkdirs();
    }
    mCameraP2P.startRecordLocalMp4(picPath, CameraPanelActivity.this, new OperationDelegateCallBack() {
        @Override
        public void onSuccess(int sessionId, int requestId, String data) {
            isRecording = true;
            mHandler.sendEmptyMessage(MSG_VIDEO_RECORD_BEGIN);
        }

        @Override
        public void onFailure(int sessionId, int requestId, int errCode) {
            mHandler.sendEmptyMessage(MSG_VIDEO_RECORD_FAIL);
        }
    });
    recordStatue(true);
} else {
    Constants.requestPermission(CameraPanelActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE, Constants.EXTERNAL_STORAGE_REQ_CODE, "open_storage");
} 
```

### 停止视频录制

**接口说明**

```java
int stopRecordLocalMp4(OperationDelegateCallBack callBack);
```

**参数说明**

|   参数 |   说明 |
| ---- | ---- |
| callBack | 操作回调 |

**示例代码**

```java
mCameraP2P.stopRecordLocalMp4(new OperationDelegateCallBack() {
    @Override
    public void onSuccess(int sessionId, int requestId, String data) {
        //成功
    }

    @Override
    public void onFailure(int sessionId, int requestId, int errCode) {
        //失败
    }
});
```

## 视频截图

截取实时视频的影像图片存储到手机 SD 卡上。

> **说明**：如果需要将截图保存至系统相册，需要开发者自己实现。注意Android10开始采用[分区存储](https://developer.android.com/training/data-storage?hl=zh-cn#scoped-storage)机制（可禁用，但Android11强制使用），保存媒体文件至系统相册需使用[MediaStore](https://developer.android.com/reference/android/provider/MediaStore?hl=zh-cn) API。

**接口说明**

```java
int snapshot(String absoluteFilePath, Context context, OperationDelegateCallBack callBack);
```

**参数说明**

|       参数 |    说明 |
| ---- | ---- |
| absoluteFilePath | 图片存储路径 |
| context | 上下文 |
| callBack | 操作回调 |

**示例代码**

```java
if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
    String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Camera/";
    File file = new File(path);
    if (!file.exists()) {
        file.mkdirs();
    }
    picPath = path;
}
mCameraP2P.snapshot(picPath, CameraPanelActivity.this, new OperationDelegateCallBack() {
    @Override
    public void onSuccess(int sessionId, int requestId, String data) {
     //data 返回的是文件路径
        mHandler.sendMessage(MessageUtil.getMessage(MSG_SCREENSHOT, ARG1_OPERATE_SUCCESS, data));
    }

    @Override
    public void onFailure(int sessionId, int requestId, int errCode) {
        mHandler.sendMessage(MessageUtil.getMessage(MSG_SCREENSHOT, ARG1_OPERATE_FAIL));
    }
});
```

## 视频声音

当视频成功开始播放以后（可以是视频直播，也可以是录像回放），可以开启视频声音，默认声音是关闭状态。

**接口说明**

开启/关闭视频声音

```
void setMute(int mute, OperationDelegateCallBack callBack);
```

| 音频模式 | 值 |
| ---- | --- |
| 静音 | 1 |
| 非静音 | 0 |

**示例代码**

```java
mCameraP2P.setMute(1, new OperationDelegateCallBack() {
  @Override
  public void onSuccess(int sessionId, int requestId, String data) {
    //data 返回的是对应操作之后的结果值
    previewMute = Integer.valueOf(data);
    mHandler.sendMessage(MessageUtil.getMessage(MSG_MUTE, ARG1_OPERATE_SUCCESS));
  }

  @Override
  public void onFailure(int sessionId, int requestId, int errCode) {
    mHandler.sendMessage(MessageUtil.getMessage(MSG_MUTE, ARG1_OPERATE_FAIL));
  }
});
```
**接口说明**

扬声器和听筒模式的切换，p2p 1.0 不支持此接口。

```
void setLoudSpeakerStatus(boolean enable);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| enable | `true` 为切换成扬声器播放，`false`为切换成听筒播放 |

## 实时对讲

在 p2p 连接成功后，可以开启与设备的实时通话功能，在开始对讲前，需要确保 App 已获得手机麦克风的访问权限。

### 开启对讲

打开手机声音传输给摄像机操作。

**示例代码**

```java
if (Constants.hasRecordPermission()) {
    mCameraP2P.startAudioTalk(new OperationDelegateCallBack() {
        @Override
        public void onSuccess(int sessionId, int requestId, String data) {
            isSpeaking = true;
            mHandler.sendMessage(MessageUtil.getMessage(MSG_TALK_BACK_BEGIN, ARG1_OPERATE_SUCCESS));
            ToastUtil.shortToast(CameraPanelActivity.this, "start talk success");
        }

        @Override
        public void onFailure(int sessionId, int requestId, int errCode) {
            isSpeaking = false;
            mHandler.sendMessage(MessageUtil.getMessage(MSG_TALK_BACK_BEGIN, ARG1_OPERATE_FAIL));
            ToastUtil.shortToast(CameraPanelActivity.this, "operation fail");

        }
    });
} else {
    Constants.requestPermission(CameraPanelActivity.this, Manifest.permission.RECORD_AUDIO, Constants.EXTERNAL_AUDIO_REQ_CODE, "open_recording");
}
```

### 停止对讲

关闭手机声音传输给摄像机操作。

**示例代码**

```java
mCameraP2P.stopAudioTalk(new OperationDelegateCallBack() {
    @Override
    public void onSuccess(int sessionId, int requestId, String data) {
        isSpeaking = false;
        mHandler.sendMessage(MessageUtil.getMessage(MSG_TALK_BACK_OVER, ARG1_OPERATE_SUCCESS));
    }

    @Override
    public void onFailure(int sessionId, int requestId, int errCode) {
        isSpeaking = false;
        mHandler.sendMessage(MessageUtil.getMessage(MSG_TALK_BACK_OVER, ARG1_OPERATE_FAIL));

    }
});
```

> 注：对讲和录制是互斥的，而且只有在预览时可以开启对讲。

### 双向对讲

在实时视频直播时，打开视频声音，此时播放的声音即为摄像机实时采集的人声与环境声音，此时打开 App 到摄像机的声音通道，即可实现双向对讲功能。

> 部分摄像机可能没有扬声器或者拾音器，此类摄像机无法实现双向对讲。

### 单向对讲

单向对讲功能需要开发者来实现控制。在开启对讲的时候，关闭视频声音，关闭对讲后，再打开视频声音即可。

## 清晰度切换

在实时视频直播时，可以切换清晰度（少数摄像机只支持一种清晰度），目前只有高清和标清两种清晰度，且只有实时视频直播时才支持。存储卡视频录像在录制时只保存了一种清晰度的视频流。

**清晰度模式**

| 模式 | 值 |
| ---- | --- |
| 标清 | 2 |
| 高清 | 4 |

### 获取清晰度

获取摄像机传过来的影像清晰度。

**示例代码**

```java
mCameraP2P.getVideoClarity(new OperationDelegateCallBack() {
​    @Override
​    public void onSuccess(int sessionId, int requestId, String data) {

​    }

​    @Override
​    public void onFailure(int sessionId, int requestId, int errCode) {

​    }

});

```

> 注意：预览画面出来后，调取该函数

### 设置清晰度

设置摄像机播放的影像清晰度。

**示例代码**

```java
 mCameraP2P.setVideoClarity(2, new OperationDelegateCallBack() {

​    @Override

​    public void onSuccess(int sessionId, int requestId, String data) {
​        videoClarity = Integer.valueOf(data);
​        mHandler.sendMessage(MessageUtil.getMessage(MSG_GET_CLARITY, ARG1_OPERATE_SUCCESS));
​    }

​    @Override

​    public void onFailure(int sessionId, int requestId, int errCode) {
​        mHandler.sendMessage(MessageUtil.getMessage(MSG_GET_CLARITY, ARG1_OPERATE_FAIL));
​    }

});
```

## 其他功能

### 获取视频码率

**接口说明**

获取视频码率。

```java
double getVideoBitRateKbps();
```

**示例代码**

```java
double rate = mCameraP2P.getVideoBitRateKbps();
```

### 渲染器设置是否支持双击放大

**接口说明**

默认支持双击放大。

```java
public void setCameraViewDoubleClickEnable(boolean enable);
```

**示例代码**

```java
TuyaCameraView mVideoView = findViewById(R.id.camera_video_view);
mVideoView.setCameraViewDoubleClickEnable(false);
```

## 裸流数据

IPC SDK 提供访问视频裸流数据的回调方法，此方法返回视频帧的 YUV 数据，颜色编码格式为 YUV 420sp。

**接口说明**

接收视频帧回调需要向 ITuyaSmartCameraP2P 注册监听器，开发者只需要重写自己关心的回调

```
void registerP2PCameraListener(AbsP2pCameraListener listener);
```

**AbsP2pCameraListener** 主要方法：

**接口说明**

回调视频 YUV 数据

```java
public void onReceiveFrameYUVData(int sessionId, ByteBuffer y, ByteBuffer u, ByteBuffer v, int width, int height, int nFrameRate, int nIsKeyFrame, long timestamp, long nProgress, long nDuration, Object camera)
```
**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| sessionId | session Id |
| Y | 视频Y数据 |
| u | 视频U数据 |
| v | 视频V数据 |
| width | 视频画面的宽 |
| height | 视频画面的高 |
| nFrameRate | 帧率 |
| nIsKeyFrame | 是否I帧 |
| timestamp | 时间戳 |
| nProgress | 时间进度(消息中心视频播放的进度) |
| nDuration | 时长(消息中心视频播放时长) |

**接口说明**

p2p 的链接状态回调。

```java
public void onSessionStatusChanged(Object camera, int sessionId, int sessionStatus)
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| sessionId | session Id |
| sessionStatus | session 状态 |

| session 状态码 |       说明 |
| ---- | ---- |
| 0 | 连接成功 |
| -3 | 连接超时 |
| -12 | 连接被设备关闭 |
| -13 | 连接无响应超时关闭 |

**示例代码**

```java
private ITuyaSmartCameraP2P mCameraP2P;

@Override
protected void onResume() {
    super.onResume();
    if (null != mCameraP2P) {
        //注册 P2P 监听器
        mCameraP2P.registerP2PCameraListener(p2pCameraListener);
    }
}

@Override
protected void onPause() {
    super.onPause();
    if (null != mCameraP2P) {
        //注销 P2P 监听器
        mCameraP2P.removeOnP2PCameraListener();
    }
}

private AbsP2pCameraListener p2pCameraListener = new AbsP2pCameraListener() {
    @Override
    public void onSessionStatusChanged(Object o, int i, int i1) {
        super.onSessionStatusChanged(o, i, i1);
        //连接状态变化时回调
    }
};
```

## 音频数据

如果您需要对 APP 采集的音频数据做额外处理，例如变声等，IPC SDK 提供 APP 采集音频数据的回调方法，且音频已经过回声消除处理。接收音频回调需要向 ITuyaSmartCameraP2P 注册监听器。

**接口说明**

注册监听器

```
void registerSpeakerEchoProcessor(ISpeakerEchoProcessor processor);
```

**接口说明**

注销监听器

```
void unregisterSpeakerEchoProcessor();
```

**接口说明**

音频数据处理后，需要传给设备播放。

```java
void sendAudioTalkData(byte[] outbuf, int length);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| outbuf | 音频数据字节数组 |
| length | 长度 |

**示例代码**

```java
mCameraP2P.registerSpeakerEchoProcessor(new ISpeakerEchoProcessor() {
    @Override
    public void receiveSpeakerEchoData(ByteBuffer pcm, int sampleRate) {
        // pcm 数据; 采样率
    }
});
```

## 智能画框

开启智能画框功能后，在视频直播的过程中，如果设备检查到移动物体，会在对应的物体上画一个白色的矩形框。

首先需要开启设备的智能画框功能，在开启后，设备会随着视频帧发送移动物体的坐标，通过 DP 点 "198" 来开启设备端的智能画框功能。设备控制接口功能详见 [设备控制](https://developer.tuya.com/cn/docs/app-development/android-app-sdk/device-management/devicemanage)。

在设备端智能画框功能开启的前提下，直播视频播放时，需要打开 IPC SDK 的智能画框功能，SDK 会根据设备发送的移动物体坐标在视频图像上绘制矩形框。

**接口说明**

智能画框功能开关

```java
void setEnableIVA(boolean enableIVA);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| enable | 是否开启智能画框 |

## 设备能力

**接口说明**

获取设备能力类。

```java
ICameraConfigInfo getCameraConfig(String devId);
```

**示例代码**

```java
ITuyaIPCCore cameraInstance = TuyaIPCSdk.getCameraInstance();
if (cameraInstance != null) {
    ICameraConfigInfo cameraConfig = cameraInstance.getCameraConfig(devId);
    if (cameraConfig != null) {
        int videoNum = cameraConfig.getVideoNum();
    }
}
```

### 设备能力类 `ICameraConfigInfo`

**接口说明**

设备支持的码流数量，如果等于 1 ，表示设备只支持高清或者标清，通过默认清晰度来获取设备支持的码流。

```java
int getVideoNum();
```

**接口说明**

设备默认清晰度，如果设备只支持 1 路码流，也表示设备唯一支持的清晰度。

```java
int getDefaultDefinition();
```

**接口说明**

设备是否有扬声器，有扬声器，表示设备支持对讲。

```java
boolean isSupportSpeaker();
```
**接口说明**

设备是否有拾音器，有拾音器，表示设备的视频有声音。

```java
boolean isSupportPickup();
```

**接口说明**

获取默认对讲方式。

```java
int getDefaultTalkBackMode();
```

**接口说明**

是否可以切换对讲方式，可以切换，表示设备即可以单向对讲，也可以双向对讲。

```java
boolean isSupportChangeTalkBackMode();
```

**接口说明**

获取 p2p config 原始数据。

```java
String getRawDataJsonStr();
```

**接口说明**

获取设备支持的播放倍速。

```java
List<Integer> getSupportPlaySpeedList();
```

| 值 | 说明 |
| ---- | ---- |
| 0 | 0.5 倍速 |
| 1 | 1 倍速 |
| 3 | 2 倍速 |