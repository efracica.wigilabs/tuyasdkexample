For live video, you need to create an `ITuyaSmartCameraP2P` object, and then make a P2P connection to play the live video, make screenshots, record videos, and initiate real-time intercom data transfers.

## Initialization

### Create ITuyaSmartCameraP2P object

**Description**

```java
ITuyaSmartCameraP2P createCameraP2P(String devId);
```

**Parameter**

| Parameter | Description |
| ---- | ---- |
| devId | device ID |

**Example**

```java
ITuyaIPCCore cameraInstance = TuyaIPCSdk.getCameraInstance();
if (cameraInstance != null) {
    cameraInstance.createCameraP2P(devId));
}
```

### Use TuyaCameraView in XML layout

TuyaCameraView is a video rendering view provided by the IPC SDK. If you need to use your own video rendering view, you only need to implement the IRegistorIOTCListener interface, and then bind your own video rendering view to ITuyaSmartCameraP2P.

**Example**

```xml
<com.tuya.smart.camera.middleware.widget.TuyaCameraView
  android:id="@+id/camera_video_view"
  android:layout_width="match_parent"
  android:layout_height="match_parent"
  />
```

### Set callback for TuyaCameraView

**Description**

```java
public void setViewCallback(AbsVideoViewCallback callback);
```

**Parameter**

| Parameter | Description |
| ---- | ---- |
| callback | Callback interface |

**Example**

```java
TuyaCameraView mVideoView = findViewById(R.id.camera_video_view);
mVideoView.setViewCallback(new AbsVideoViewCallback() {
	@Override
	public void onCreated(Object view) {
		super.onCreated(view);
        //Callback when rendering view construction is completed
	}
});
```

**AbsVideoViewCallback**

In the callback of TuyaCameraView, developers only need to overwrite the callbacks they care about, and generally only need to overwrite the ʻonCreated` method.

**Description**

Callback when TuyaCameraView is ready to render view

```java
public void onCreated(Object view);
```

**Description**

Callback when clicking on the view

```java
public void videoViewClick();
```

**Description**

Callback when scrolling on the view.

>**Note**: This interface only supports P2P 1.0 devices (`p2ptype` = 1), other devices use the `setOnRenderDirectionCallback` interface.

```java
public void startCameraMove(String cameraDirection)
```

**Parameter**

| Parameter | Description |
| ---- | ---- |
| cameraDirection | direction. "0" means up，"2" means right，"4" means down，"6" means left |

**Parameter**

Callback when action up

```java
public void onActionUP()
```

### Create render view

**Description**

```java
public void createVideoView(String devId);
```

**Parameters**

| Parameter | Description |
| ---- | ---- |
| devId | Device id |

**Example**

```
TuyaCameraView mVideoView = findViewById(R.id.camera_video_view);
mVideoView.createVideoView(devId);
```

**Description**

Get render view

> if the render view is not constructed, will return null

```java
public Object createdView();
```
**Example**

```java
TuyaCameraView mVideoView = findViewById(R.id.camera_video_view);
mVideoView.createdView()
```

### Bind the render view for ITuyaSmartCameraP2P

**Description**

```java
void generateCameraView(T view);
```

**Example**

```java
mCameraP2P.generateCameraView(mVideoView.createdView());
```

### Register listener

register a listener with ITuyaSmartCameraP2P, otherwise, the render view will not work. 

**Description**

```
void registerP2PCameraListener(AbsP2pCameraListener listener);
```

### Link code

**Example**

```java
// 1. create ITuyaSmartCameraP2P object
ITuyaSmartCameraP2P mCameraP2P = null;
ITuyaIPCCore cameraInstance = TuyaIPCSdk.getCameraInstance();
if (cameraInstance != null) {
    mCameraP2P = cameraInstance.createCameraP2P(devId));
}
// 2. set callback for TuyaCameraView
mVideoView.setViewCallback(new AbsVideoViewCallback() {
	@Override
	public void onCreated(Object view) {
		super.onCreated(view);
		//4. when render view construction is completed, bind render view for ITuyaSmartCameraP2P
		if (null != mCameraP2P){
			mCameraP2P.generateCameraView(view);
		}
	}
});
// 3. create render view
mVideoView.createVideoView(devId);
// 4. register P2P listener
if (null != mCameraP2P){
    mCameraP2P.registerP2PCameraListener(new AbsP2pCameraListener() {
        @Override
        public void onSessionStatusChanged(Object camera, int sessionId, int sessionStatus) {
            super.onSessionStatusChanged(camera, sessionId, sessionStatus);
        }
    });
}
```

## P2P connection

Before starting video playback, you need to connect to the P2P channel. The P2P status needs to be maintained by the user. The SDK is only responsible for issuing instructions and receiving camera response results.

**Description**

connect P2P

```java
void connect(String devId, OperationDelegateCallBack callBack);
```

**Description**

connect P2P (Specify preferred connection mode)

| Mode | Description |
| ---- | ---- |
| 0 | automatically choose |
| 1 | internet connection priority |
| 2 | LAN connection priority |

```java
void connect(String devId, int mode, OperationDelegateCallBack callBack);
```

**Description**

disconnect P2P

```java
void disconnect(String devId, OperationDelegateCallBack callBack);
```

**Parameter**

| Parameter | Description |
| ---- | ---- |
| callBack | result callback |

**Example**

```java
mCameraP2P.connect(devId, new OperationDelegateCallBack() {
    @Override
    public void onSuccess(int sessionId, int requestId, String data) {

    }

    @Override
    public void onFailure(int sessionId, int requestId, int errCode) {

    }
});
```

## Live video

After the p2p channel is successfully connected, you can start playing the live video.

**Description**

Start playing the live video.

```java
void startPreview(int clarity, OperationDelegateCallBack callBack);
```

Stop playing the live video.

```java
int stopPreview(OperationDelegateCallBack callBack);
```

**Parameter**

| Parameter | Description |
| ---- | ---- |
| clarity | sharpness |
| callBack | result callback |

**Clarity model**

| mode | value |
| ---- | --- |
| SD | 2 |
| HD | 4 |

**Example**

```java
mCameraP2P.startPreview(new OperationDelegateCallBack() {
    @Override
    public void onSuccess(int sessionId, int requestId, String data) {

    }

    @Override
    public void onFailure(int sessionId, int requestId, int errCode) {

    }
});
```

> **Note**: After the startPreview callback is successful, the onReceiveFrameYUVData callback will start receiving video data and throw it to the business layer.

## Delete ITuyaSmartCameraP2P

Destroy the object, when you no longer use the camera function, you must remove the P2P listener and call destroy.

**Description**

remove P2P listener

```
void removeOnP2PCameraListener();
```

**Description**

destroy camera object

```java
void destroyP2P();
```

**Example**

```java
if (null != mCameraP2P) {
    mCameraP2P.removeOnP2PCameraListener();
    mCameraP2P.destroyP2P();
}
```

## Flow chart

![image.png](https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/content-platform/hestia/16378246319a5fa5d66bb.png)