
涂鸦 IPC 摄像机支持 SD卡录制功能，智能摄像机插入存储卡后，可以查看存储卡的信息和状态，并设置录像开关和模式，详情可以参考 [存储卡管理功能](https://developer.tuya.com/cn/docs/app-development/android-app-sdk/extension-sdk/ipc-sdk/extension-features/sdcard?id=Ka6nxw2eufia3)。

设备在存储卡中保存视频录像后，可以通过 IPC SDK 在 App 端播放视频录像，同实时视频直播一样，需要创建 `ITuyaSmartCameraP2P` 对象，连接上 p2p 通道。p2p 通道连接成功后，可以获取到设备端存储卡中录制的视频片段时间信息，然后播放视频片段。

## 视频片段

设备端保存在存储卡中的视频片段，IPC SDK 支持以天为单位查看和播放视频录像，并提供查询某年某月中，哪几天保存有视频录像，以便于用户查看。

### 获取有回放视频记录的日期

在开始回放前，需要获取到回放视频记录的信息。首先获取有回放视频记录的日期

**接口说明**

```java
void queryRecordDaysByMonth(int year, int month, OperationDelegateCallBack callBack);
```

**参数说明**


|      参数  |   说明 |
| ---- | ---- |
| year       | 查询的年份 |
| month      | 查询的月份 |
| monthcallBack | 结果回调  |

**示例代码**

```java
int year = Integer.parseInt(substring[0]);
int mouth = Integer.parseInt(substring[1]);
queryDay = Integer.parseInt(substring[2]);
mCameraP2P.queryRecordDaysByMonth(year, mouth, new OperationDelegateCallBack() {
  @Override
  public void onSuccess(int sessionId, int requestId, String data) {
    //data是获取到的月份数据
    MonthDays monthDays = JSONObject.parseObject(data, MonthDays.class);
    mBackDataMonthCache.put(mCameraP2P.getMonthKey(), monthDays.getDataDays());
    mHandler.sendMessage(MessageUtil.getMessage(MSG_DATA_DATE, ARG1_OPERATE_SUCCESS, data));
  }

  @Override
  public void onFailure(int sessionId, int requestId, int errCode) {
    mHandler.sendMessage(MessageUtil.getMessage(MSG_DATA_DATE, ARG1_OPERATE_FAIL));
  }
}); 
```

### 获取某日的视频回放信息

获取到有用回放记录的日期后，根据日期获取当日的视频回放记录

**接口说明**

```java
void queryRecordTimeSliceByDay(int year, int month, int day, OperationDelegateCallBack callBack);
```

**参数说明**


|      参数  |   说明 |
| ---- | ---- |
| year       | 查询的年份 |
| month      | 查询的月份 |
| day      | 查询的天 |
| callBack | 结果回调  |

**示例代码**

```java
int year = Integer.parseInt(substring[0]);
int mouth = Integer.parseInt(substring[1]);
int day = Integer.parseInt(substring[2]);
mCameraP2P.queryRecordTimeSliceByDay(year, mouth, day, new OperationDelegateCallBack() {
  @Override
  public void onSuccess(int sessionId, int requestId, String data) {					
    //data是获取到指定日期的视频片段数据集
    parsePlaybackData(data);
  }

  @Override
  public void onFailure(int sessionId, int requestId, int errCode) {
    mHandler.sendEmptyMessage(MSG_DATA_DATE_BY_DAY_FAIL);
  }
});
```
`parsePlaybackData(data)`方法参考：
```java
private void parsePlaybackData(Object obj) {
        RecordInfoBean recordInfoBean = JSONObject.parseObject(obj.toString(), RecordInfoBean.class);
        if (recordInfoBean.getCount() != 0) {
            List<TimePieceBean> timePieceBeanList = recordInfoBean.getItems();
            if (timePieceBeanList != null && timePieceBeanList.size() != 0) {
                mBackDataDayCache.put(mCameraP2P.getDayKey(), timePieceBeanList);
            }
            mHandler.sendMessage(MessageUtil.getMessage(MSG_DATA_DATE_BY_DAY_SUCC, ARG1_OPERATE_SUCCESS));
        } else {
            mHandler.sendMessage(MessageUtil.getMessage(MSG_DATA_DATE_BY_DAY_FAIL, ARG1_OPERATE_FAIL));
        }
    }
```
`RecordInfoBean`数据模型：

|      参数  |  说明 |
| ---- | ---- |
| count | 片段个数|
|List<TimePieceBean>|视频片段集合|

`TimePieceBean`数据模型：

|      参数  |  说明 |
| ---- | ---- |
| startTime | 视频片段开始时间|
|endTime|视频片段结束时间|
|playTime|视频片段播放时间|


## 视频播放

### 开启回放


**接口说明**

```java
void startPlayBack(int startTime, int stopTime, int playTime, OperationDelegateCallBack callBack, OperationDelegateCallBack finishCallBack);
```

**参数说明**


|      参数  |   说明 |
| ---- | ---- |
| startTime | 开始时间（10 位时间戳） |
| stopTime | 结束时间 |
| playTime | 播放时间 |
| callBack | 开启回放回调 |
| finishcallBack | 结束回放回调 |

**示例代码**

```java
mCameraP2P.startPlayBack(timePieceBean.getStartTime(),
                         timePieceBean.getEndTime(),
                         timePieceBean.getStartTime(), new OperationDelegateCallBack() {
                           @Override
                           public void onSuccess(int sessionId, int requestId, String data){
                             isPlayback = true;
                           }

                           @Override
                           public void onFailure(int sessionId, int requestId, int errCode){
                             isPlayback = false;
                           }
                         }, new OperationDelegateCallBack() {
                           @Override
                           public void onSuccess(int sessionId, int requestId, String data){
                             isPlayback = false;
                           }

                           @Override
                           public void onFailure(int sessionId, int requestId, int errCode){
                             isPlayback = false;
                           }
                         });
```

### 暂停回放


**接口说明**

```java
void pausePlayBack(OperationDelegateCallBack callBack);
```

**参数说明**


|      参数  |   说明 |
| ---- | ---- |
| callBack | 操作回调 |

**示例代码**

```java
mCameraP2P.pausePlayBack(new OperationDelegateCallBack() {
  @Override
  public void onSuccess(int sessionId, int requestId, String data) {
    isPlayback = false;
  }

  @Override
  public void onFailure(int sessionId, int requestId, int errCode) {

  }
});
```

### 恢复回放


**接口说明**

```java
void resumePlayBack(OperationDelegateCallBack callBack);
```

**参数说明**


|      参数  |   说明 |
| ---- | ---- |
| callBack | 操作回调 |

**示例代码**

```java
mCameraP2P.resumePlayBack(new OperationDelegateCallBack() {
  @Override
  public void onSuccess(int sessionId, int requestId, String data) {
    isPlayback = true;
  }

  @Override
  public void onFailure(int sessionId, int requestId, int errCode) {

  }
});
```

### 结束回放


**接口说明**

```java
void stopPlayBack(OperationDelegateCallBack callBack);
```

**参数说明**


|      参数  |   说明 |
| ---- | ---- |
| callBack | 操作回调 |

**示例代码**

```java
mCameraP2P.stopPlayBack(new OperationDelegateCallBack() {
  @Override
  public void onSuccess(int sessionId, int requestId, String data) {

  }

  @Override
  public void onFailure(int sessionId, int requestId, int errCode) {

  }
});
```

### 倍速播放

**接口说明**

倍速播放。需要设备支持，可以使用[设备能力类ICameraConfigInfo][https://developer.tuya.com/cn/docs/app-development/android-app-sdk/extension-sdk/ipc-sdk/camera-functions/avfunction]获取设备支持的播放倍速。

```java
void setPlayBackSpeed(int speed, OperationDelegateCallBack callBack);
```

**参数说明**

| 参数          | 说明                                 |
| ------------- | ------------------------------------ |
| speed | 播放倍速，指定的播放倍数需要设备支持 |
| callBack | 操作回调 |

## 视频下载

部分 IPC 设备支持将存储卡回放视频下载到 App 上，可以通过 IPC SDK 提供的接口判断设备是否支持下载存储卡视频。

**接口说明**

判断设备是否支持存储卡视频回放下载，需要在 p2p 连接成功之后调用

```java
boolean isSupportPlaybackDownload();
```

**示例代码**

```java
ITuyaIPCCore cameraInstance = TuyaIPCSdk.getCameraInstance();
if (cameraInstance != null) {
    ICameraConfigInfo cameraConfig = cameraInstance.getCameraConfig(devId);
    if (cameraConfig != null) {
        boolean supportPlaybackDownload = cameraConfig.isSupportPlaybackDownload();
    }
}
```

**接口说明**

开始下载存储卡回放视频

```java
void startPlayBackDownload(int startTime, int stopTime, String folderPath, String fileName, OperationDelegateCallBack callBack, ProgressCallBack progressCallBack, OperationDelegateCallBack finishCallBack);
```

**参数说明**

| 参数          | 说明                                 |
| ------------- | ------------------------------------ |
| startTime | 开始时间 |
| stopTime | 结束时间 |
| folderPath | 存储路径 |
| fileName | 文件名称 |
| callBack | 操作回调 |
| progressCallBack | 下载回调 |
| finishCallBack | 结束回调 |

> 由于 IPC 设备对回放视频文件的检索能力限制，视频下载的范围，起始点都必须分别在某个视频片段内。即期望下载的视频的开始时间：`startTime` ，结束时间：`stopTime`，需要在某两个视频片段的时间范围内。


**接口说明**

暂停下载存储卡回放视频

```java
void pausePlayBackDownload(OperationDelegateCallBack callBack);
```

**参数说明**

| 参数  | 说明  |
| ---- | ------ |
| callBack | 操作回调 |


**接口说明**

恢复下载存储卡回放视频

```java
void resumePlayBackDownload(OperationDelegateCallBack callBack);
```

**参数说明**

| 参数  | 说明  |
| ---- | ------ |
| callBack | 操作回调 |


**接口说明**

停止下载存储卡回放视频

```java
void stopPlayBackDownload(OperationDelegateCallBack callBack);
```

**参数说明**

| 参数  | 说明  |
| ---- | ------ |
| callBack | 操作回调 |

## 视频删除

部分 IPC 设备支持通过 App 控制删除存储卡上某天的回放视频，目前只支持按日期删除全天的视频。

**接口说明**

判断设备是否支持删除存储卡上的回放视频，需要在 p2p 连接成功之后调用

```java
boolean isSupportPlaybackDelete();
```

**示例代码**

```java
ITuyaIPCCore cameraInstance = TuyaIPCSdk.getCameraInstance();
if (cameraInstance != null) {
    ICameraConfigInfo cameraConfig = cameraInstance.getCameraConfig(devId);
    if (cameraConfig != null) {
        boolean supportPlaybackDelete = cameraConfig.isSupportPlaybackDelete();
    }
}
```

**接口说明**

删除设备存储卡上指定日期的回放视频

```java
void deletePlaybackDataByDay(String day, OperationDelegateCallBack callBack, OperationDelegateCallBack finishCallBack);
```

**参数说明**

| 参数          | 说明                                 |
| ------------- | ------------------------------------ |
| day | 期望删除的日期，格式为 “yyyyMMdd” |
| callBack | 操作回调 |
| finishCallBack | 结束回调 |


## 流程图

<img src="https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/content-platform/hestia/16378243706636ce717d7.png" style="zoom:50%;" />

开启回放成功后，可以使用音视频功能，例如开启/停止视频录制、视频截图、开启/关闭视频声音，详情可参考 [音视频功能](https://developer.tuya.com/cn/docs/app-development/android-app-sdk/extension-sdk/ipc-sdk/camera-functions/avfunction?id=Ka6nuvucjujar)。