Tuya IPC camera supports SD card recording function. After the smart camera inserts the memory card, you can view the information and status of the memory card, and set the recording switch and mode. For details, refer to the [memory card management function](https://developer.tuya.com/en/docs/app-development/android-app-sdk/extension-sdk/ipc-sdk/extension-features/sdcard?id=Ka6nxw2eufia3).

After the device saves the video on the memory card, it can play the video on the app through the IPC SDK. Like the real-time video live broadcast, it needs to create the ITuyaSmartCameraP2P object and connect to the P2P channel. After the P2P channel is connected successfully, the time information of the video clip recorded in the device side memory card can be obtained, and then the video clip can be played

## Video clip

The device-side stores the video clips in the memory card. IPC SDK supports viewing and playing video recordings by day, and provides queries on which days of a year, a month, and a few days to save video recordings for users to view.

### Get a date with playback video record

Before starting playback, you need to get the information of the playback video record. First, get the date with the playback video record.

**Description**

```java
void queryRecordDaysByMonth(int year, int month, OperationDelegateCallBack callBack);
```

**Parameter**

| Parameter | Description |
| ---- | ---- |
| year | query year |
| month | query month |
| monthcallBack | result callback |

**Example**

```java
int year = Integer.parseInt(substring[0]);
int mouth = Integer.parseInt(substring[1]);
queryDay = Integer.parseInt(substring[2]);
mCameraP2P.queryRecordDaysByMonth(year, mouth, new OperationDelegateCallBack() {
  @Override
  public void onSuccess(int sessionId, int requestId, String data) {
    MonthDays monthDays = JSONObject.parseObject(data, MonthDays.class);
    mBackDataMonthCache.put(mCameraP2P.getMonthKey(), monthDays.getDataDays());
    mHandler.sendMessage(MessageUtil.getMessage(MSG_DATA_DATE, ARG1_OPERATE_SUCCESS, data));
  }

  @Override
  public void onFailure(int sessionId, int requestId, int errCode) {
    mHandler.sendMessage(MessageUtil.getMessage(MSG_DATA_DATE, ARG1_OPERATE_FAIL));
  }
});
```

### Get video playback information for a certain day

After getting the date of the useful playback record, get the video playback record of the day according to the date.

**Description**

```java
void queryRecordTimeSliceByDay(int year, int month, int day, OperationDelegateCallBack callBack);
```

**Parameter**

| Parameter | Description |
| ---- | ---- |
| year | query year |
| month | query month |
| day | query day |
| callBack | result callback |

**Example**

```java
int year = Integer.parseInt(substring[0]);
int mouth = Integer.parseInt(substring[1]);
int day = Integer.parseInt(substring[2]);
mCameraP2P.queryRecordTimeSliceByDay(year, mouth, day, new OperationDelegateCallBack() {
  @Override
  public void onSuccess(int sessionId, int requestId, String data) {
    parsePlaybackData(data);
  }

  @Override
  public void onFailure(int sessionId, int requestId, int errCode) {
    mHandler.sendEmptyMessage(MSG_DATA_DATE_BY_DAY_FAIL);
  }
});
```

`parsePlaybackData(data)`refer this：
```java
private void parsePlaybackData(Object obj) {
        RecordInfoBean recordInfoBean = JSONObject.parseObject(obj.toString(), RecordInfoBean.class);
        if (recordInfoBean.getCount() != 0) {
            List<TimePieceBean> timePieceBeanList = recordInfoBean.getItems();
            if (timePieceBeanList != null && timePieceBeanList.size() != 0) {
                mBackDataDayCache.put(mCameraP2P.getDayKey(), timePieceBeanList);
            }
            mHandler.sendMessage(MessageUtil.getMessage(MSG_DATA_DATE_BY_DAY_SUCC, ARG1_OPERATE_SUCCESS));
        } else {
            mHandler.sendMessage(MessageUtil.getMessage(MSG_DATA_DATE_BY_DAY_FAIL, ARG1_OPERATE_FAIL));
        }
    }
```
`RecordInfoBean`：
|      Parameter  |  Description |
| ---- | ---- |
| count | video count|
|List<TimePieceBean>|video list|

`TimePieceBean`：
|      Parameter  |  Description |
| ---- | ---- |
| startTime | starting time|
|endTime|end time|
|playTime|play time|


## Video playback

### Start Playback

**Description**

```java
void startPlayBack(int startTime, int stopTime, int playTime, OperationDelegateCallBack callBack, OperationDelegateCallBack finishCallBack);
```

**Parameter**

| Parameter | Description |
| ---- | ---- |
| startTime | starting time (10-bit timestamp) |
| stopTime | end time |
| playTime | play time |
| callBack | playback callback |
| finishcallBack | callback when playback ends |

**Example**

```java
mCameraP2P.startPlayBack(timePieceBean.getStartTime(),
                         timePieceBean.getEndTime(),
                         timePieceBean.getStartTime(), new OperationDelegateCallBack() {
                           @Override
                           public void onSuccess(int sessionId, int requestId, String data){
                             isPlayback = true;
                           }

                           @Override
                           public void onFailure(int sessionId, int requestId, int errCode){
                             isPlayback = false;
                           }
                         }, new OperationDelegateCallBack() {
                           @Override
                           public void onSuccess(int sessionId, int requestId, String data){
                             isPlayback = false;
                           }

                           @Override
                           public void onFailure(int sessionId, int requestId, int errCode){
                             isPlayback = false;
                           }
                         });
```

### Pause playback

**Description**

```java
void pausePlayBack(OperationDelegateCallBack callBack);
```

**Parameter**

| Parameter | Description |
| ---- | ---- |
| callBack | result callback |

**Example**

```java
mCameraP2P.pausePlayBack(new OperationDelegateCallBack() {
  @Override
  public void onSuccess(int sessionId, int requestId, String data) {
    isPlayback = false;
  }

  @Override
  public void onFailure(int sessionId, int requestId, int errCode) {

  }
});
```

### Resume playback

**Description**

```java
void resumePlayBack(OperationDelegateCallBack callBack);
```

**Parameter**

| Parameter | Description |
| ---- | ---- |
| callBack | result callback |

**Example**

```java
mCameraP2P.resumePlayBack(new OperationDelegateCallBack() {
  @Override
  public void onSuccess(int sessionId, int requestId, String data) {
    isPlayback = true;
  }

  @Override
  public void onFailure(int sessionId, int requestId, int errCode) {

  }
});
```

### Stop playback

**Description**

```java
void stopPlayBack(OperationDelegateCallBack callBack);
```

**Parameter**

| Parameter | Description |
| ---- | ---- |
| callBack | result callback |

**Example**

```java
mCameraP2P.stopPlayBack(new OperationDelegateCallBack() {
  @Override
  public void onSuccess(int sessionId, int requestId, String data) {

  }

  @Override
  public void onFailure(int sessionId, int requestId, int errCode) {

  }
});
```

**Description**

Multiple speed playback. Need device support, you can use [device capability class ICameraConfigInfo](https://developer.tuya.com/en/docs/app-development/android-app-sdk/extension-sdk/ipc-sdk/camera-functions/avfunction) to get the playback speed supported by the device.

```java
void setPlayBackSpeed(int speed, OperationDelegateCallBack callBack);
```

**Parameters**

| Parameter | Description |
| ------------- | ------------------------------------ |
| speed | Playback speed, the specified playback peed needs device support |
| callBack | operation callback |

## Flow chart

<img src="https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/content-platform/hestia/1637824480e6389c9765d.png" style="zoom:50%;" />