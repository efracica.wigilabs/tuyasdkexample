视频直播需要创建 `ITuyaSmartCameraP2P` 对象，然后进行 P2P 连接，连接成功后就可以播放实时视频、截图、录制视频和实时对讲数据传输。

## 初始化

### 创建 `ITuyaSmartCameraP2P` 对象

**接口说明**

支持子设备预览

```java
ITuyaSmartCameraP2P createCameraP2P(String devId);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| devId | 设备 ID |

**示例代码**

```java
ITuyaIPCCore cameraInstance = TuyaIPCSdk.getCameraInstance();
if (cameraInstance != null) {
    cameraInstance.createCameraP2P(devId));
}
```

### 页面布局文件中引入渲染视图容器

`TuyaCameraView` 是 IPC SDK 提供的视频渲染视图，如果您需要使用自己的视频渲染视图，只需要实现 `IRegistorIOTCListener` 接口，再将您自己的视频渲染视图和 `ITuyaSmartCameraP2P` 绑定即可。

```xml
<com.tuya.smart.camera.middleware.widget.TuyaCameraView
  android:id="@+id/camera_video_view"
  android:layout_width="match_parent"
  android:layout_height="match_parent"
  />
```

### 为渲染视图容器设置回调

**接口说明**

```java
public void setViewCallback(AbsVideoViewCallback callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| callback | 回调接口 |

**示例代码**

```java
TuyaCameraView mVideoView = findViewById(R.id.camera_video_view);
mVideoView.setViewCallback(new AbsVideoViewCallback() {
	@Override
	public void onCreated(Object view) {
		super.onCreated(view);
        //渲染视图构造完成时回调
	}
});
```

**AbsVideoViewCallback**

渲染视图回调抽象类，开发者只需要重写自己关心的回调，一般只需要重写 `onCreated` 方法。

**接口说明**

渲染视图构造完成时回调。

```java
public void onCreated(Object view);
```

**接口说明**

点击视图时回调。

```java
public void videoViewClick();
```

**接口说明**

触发视图滑动操作时回调。
**该接口仅支持 p2p 1.0 的设备**，其他设备使用 `setOnRenderDirectionCallback` 接口。

```java
public void startCameraMove(String cameraDirection);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| cameraDirection | 滑动方向。"0"代表上，"2"代表右，"4"代表下，"6"代表左 |

**接口说明**

点击视图后手指抬起时回调

```java
public void onActionUP();
```

### 构造渲染视图

**接口说明**

```java
public void createVideoView(String devId);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| devId | 设备 id |

**示例代码**

```
TuyaCameraView mVideoView = findViewById(R.id.camera_video_view);
mVideoView.createVideoView(devId);
```

**接口说明**

获取视频渲染视图

> 需要注意如果未构造视频渲染视图会返回 null

```java
public Object createdView();
```

**示例代码**

```java
TuyaCameraView mVideoView = findViewById(R.id.camera_video_view);
mVideoView.createdView();
```

### 为 ITuyaSmartCameraP2P 绑定渲染视图

**接口说明**

```java
void generateCameraView(T view);
```

**示例代码**

```java
mCameraP2P.generateCameraView(mVideoView.createdView());
```

### 注册 P2P 监听

向 ITuyaSmartCameraP2P 注册监听器，否则无法正常显示画面。

**接口说明**

```
void registerP2PCameraListener(AbsP2pCameraListener listener);
```

### 链路

**示例代码**

```java
// 1. 创建 ITuyaSmartCameraP2P
ITuyaSmartCameraP2P mCameraP2P = null;
ITuyaIPCCore cameraInstance = TuyaIPCSdk.getCameraInstance();
if (cameraInstance != null) {
    mCameraP2P = cameraInstance.createCameraP2P(devId));
}
TuyaCameraView mVideoView = findViewById(R.id.camera_video_view);
// 2. 为渲染视图容器设置回调
mVideoView.setViewCallback(new AbsVideoViewCallback() {
	@Override
	public void onCreated(Object view) {
		super.onCreated(view);
		//4. 渲染视图构造完成时，为 ITuyaSmartCameraP2P 绑定渲染视图
		if (null != mCameraP2P){
			mCameraP2P.generateCameraView(view);
		}
	}
});
// 3. 构造渲染视图
mVideoView.createVideoView(devId);
// 4. 注册 P2P 监听
if (null != mCameraP2P){
    mCameraP2P.registerP2PCameraListener(new AbsP2pCameraListener() {
        @Override
        public void onSessionStatusChanged(Object camera, int sessionId, int sessionStatus) {
            super.onSessionStatusChanged(o, i, i1);
        }
    });   
}
```

## P2P 连接

在开始视频播放之前，需要先连接 P2P 通道。P2P 状态需要使用者自己维护，SDK 只负责下发指令和接收摄像机响应结果。

**接口说明**

连接 P2P 通道。

```java
void connect(String devId, OperationDelegateCallBack callBack);
```

**接口说明**

连接 P2P 通道（指定优先连接方式）。

| Mode | 说明 |
| ---- | --- |
| 0 | 自动选择 |
| 1 | 外网连接优先 |
| 2 | 局域网连接优先 |

```java
void connect(String devId, int mode, OperationDelegateCallBack callBack);
```

**接口说明**

断开 P2P 通道。

```java
void disconnect(String devId, OperationDelegateCallBack callBack);
```

**参数说明**

| 参数 | 说明 |
| ---- | --- |
| callBack | 操作结果回调 |

**示例代码**

```java
mCameraP2P.connect(devId, new OperationDelegateCallBack() {
    @Override
    public void onSuccess(int sessionId, int requestId, String data) {
        //连接成功
    }

    @Override
    public void onFailure(int sessionId, int requestId, int errCode) {
        //连接失败
    }
});
```

## 实时播放视频

P2P连接成功之后，就能进行实时视频播放了。

**接口说明**

开始播放实时视频。

```java
void startPreview(int clarity, OperationDelegateCallBack callBack);
```

停止播放实时视频。

```java
int stopPreview(OperationDelegateCallBack callBack);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| clarity | 清晰度模式 |
| callBack | 操作结果回调 |

**清晰度模式**

| 模式 | 值 |
| ---- | --- |
| 标清 | 2 |
| 高清 | 4 |

**示例代码**

```java
mCameraP2P.startPreview(new OperationDelegateCallBack() {
    @Override
    public void onSuccess(int sessionId, int requestId, String data) {
        //开始播放实时视频成功
    }

    @Override
    public void onFailure(int sessionId, int requestId, int errCode) {
        //开始播放实时视频失败
    }
});
```

> **注意**：startPreview 成功回调之后，onReceiveFrameYUVData 回调会开始接收视频数据，并抛给业务层。

## 销毁 ITuyaSmartCameraP2P 对象

不再使用 camera 功能的时候，一定要注销 P2P 监听器、销毁 P2P 对象。

**接口说明**

注销 P2P 监听器。

```java
void removeOnP2PCameraListener();
```

**接口说明**

删除 P2P 对象。

```java
void destroyP2P();
```

**示例代码**

```java
@Override
public void onDestroy() {
    if (null != mCameraP2P) {
        mCameraP2P.removeOnP2PCameraListener();
        mCameraP2P.destroyP2P();
    }
}
```

## 流程图

<img src="https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/content-platform/hestia/1637824574e4a39a09d22.png" style="zoom:60%;" />