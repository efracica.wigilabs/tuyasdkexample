## Cloud storage

Tuya IoT provides a cloud storage service for the smart cameras, which can upload the video recorded by the device to the Tuya cloud.

## Flow chart

Request the cloud storage service status first. If the cloud storage service is not activated or has expired and the cloud video has been completely deleted (after the cloud storage service expires, the uploaded cloud video will be retained for some days), you need to first purchase cloud storage services. If the cloud storage service is in the validity period, first request the dates of the cloud storage video, and then request the relevant data of the specified date, including cloud storage events, timeline data, authentication information, etc. After that, you can choose a cloud storage event or a point in time to start playing cloud video.

![image.png](https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/content-platform/hestia/1637822806f766fe24e00.png)

## Cloud service 

The [billed cloud service of biz bundle](https://developer.tuya.com/en/docs/app-development/android-bizbundle-sdk/cloudstorage?id=Ka8qhzjzay7fx) provides H5 pages and order display functions for cloud storage.

## Cloud video

**ITYCloudCamera** provides API interfaces related to cloud storage playback.

**Declaration**

create the entity object of cloud storage playback.

```java
ITYCloudCamera createCloudCamera();
```

**Example**

```java
private ITYCloudCamera cloudCamera;

ITuyaIPCCloud cloud = TuyaIPCSdk.getCloud();
if (cloud != null) {
    cloudCamera = cloud.createCloudCamera();
}
```

## Cloud storage data

### Get cloud storage current status value (with or without purchase)

Before using cloud storage, you must get the current state value, otherwise, the cloud storage will not work.

```java
void queryCloudServiceStatus(String devId, ITuyaResultCallback<CloudStatusBean> callback);
```

**Parameter**

| Parameter | Description |
| ---------- | ----------- |
| devId | Device id |
| callback | Callback |

**Example**

```java
cloudCamera.queryCloudServiceStatus(devId, new ITuyaResultCallback<CloudStatusBean>() {
    @Override
    public void onSuccess(CloudStatusBean result) {
        //Get cloud storage status
    }

    @Override
    public void onError(String errorCode, String errorMessage) {

    }
});
```

**CloudStatusBean data model**

| Parameter | Description                  |
|-----------|------------------------------|
| status    | Status code                  |
| key       | Cloud storage encryption key |

**Status code description**

| Code | Description |
| ---- | ---- |
| 10001 | Service not opened |
| 10010 | Service opened |
| 10011 | Service expired |

In the cloud storage event, the value of the event screenshot (TimeRangeBean.snapshotUrl) is encrypted picture path, and the key is used to display the encrypted picture.

### Get the date when cloud storage data is available

```java
void getCloudDays(String devId, ITuyaResultCallback<List<CloudDayBean>> callback);
```

**Parameter**

| Parameter | Description |
| --------- | ----------- |
| devId | Device id |

**Example**

```java
cloudCamera.getCloudDays(devId, new ITuyaResultCallback<List<CloudDayBean>>() {
    @Override
    public void onSuccess(List<CloudDayBean> result) {

    }

    @Override
    public void onError(String errorCode, String errorMessage) {

    }
});
```

**CloudDayBean data model**

| Parameter | Description |
| ---- | ---- |
| uploadDay | Date string |
| currentDayStart | Start time, used to get time slice at a specified time |
| currentDayEnd | End time, used to get time slice at a specified time |

### Get time slice at a specified time

```java
void getTimeLineInfo(String devId, long timeGT, long timeLT, ITuyaResultCallback<List<TimePieceBean>> callback);
```

**Parameter**

| Parameter | Description |
| --------- | ----------------------------- |
| devId | Device id |
| timeGT | Start time (10-bit timestamp) |
| timeLT | End Time (10-bit timestamp) |
| callback | Callback |

**Example**

```java
CloudDayBean dayBean = dayBeanList.get(0);
cloudCamera.getTimeLineInfo(devId, dayBean.getCurrentStartDayTime(), dayBean.getCurrentDayEndTime(), new ITuyaResultCallback<List<TimePieceBean>>() {
    @Override
    public void onSuccess(List<TimePieceBean> result) {

    }

    @Override
    public void onError(String errorCode, String errorMessage) {

    }
});
```

### Get the corresponding motion detection data according to the beginning and end of the time segment

```java
void getMotionDetectionInfo(String devId, long timeGT, long timeLT, int offset, int limit, ITuyaResultCallback<List<TimeRangeBean>> callback);
```

**Parameter**

| Parameter | Description |
| --------- | ----------------------------------------------------------- |
| devId | device Id |
| timeGT | Start time (10-bit timestamp) |
| timeLT | End Time (10-bit timestamp) |
| offset | Page number, default 0 |
| limit | Number of pulls at a time, default -1, which means all data |
| callback | callback |

**Example**

```java
CloudDayBean dayBean = dayBeanList.get(0);
cloudCamera.getMotionDetectionInfo(devId, dayBean.getCurrentStartDayTime(), dayBean.getCurrentDayEndTime(), offset, limit, new ITuyaResultCallback<List<TimeRangeBean>>() {
    @Override
    public void onSuccess(List<TimeRangeBean> result) {

    }

    @Override
    public void onError(String errorCode, String errorMessage) {

    }
});
```

The real-time screenshots carried in the cloud storage event are encrypted and need to be displayed through the encrypted image component DecryptImageView. For details, refer to the [Encrypted Image chapter](https://developer.tuya.com/en/docs/app-development/android-app-sdk/extension-sdk/ipc-sdk/encryptimage).

### ~~Cloud storage purchase address interface~~

No longer in maintenance, please refer to the cloud storage service package.

```java
public void buyCloudStorage(Context mContext, DeviceBean deviceBean, String homeId, ICloudManagerCallback callback) {
  CameraCloudManager.getInstance().getCloudStorageUrl(mContext, deviceBean, homeId);
}
```

**Parameter**

| Parameter | Description |
| ---------- | ----------- |
| deviceBean | device Info |
| homeId | home Id |
| callback | callback |

**Example**

```java
cameraCloudSDK.buyCloudStorage(CameraCloudStorageActivity.this,
                               TuyaHomeSdk.getDataInstance().getDeviceBean(devId),
                               String.valueOf(FamilyManager.getInstance().getCurrentHomeId()), new ICloudManagerCallback() {
                                 @Override
                                 public void onError(int i) {

                                 }

                                 @Override
                                 public void onSuccess(Object o) {
                                   String uri = (String) o;
                                   Intent intent = new Intent(CameraCloudStorageActivity.this, WebViewActivity.class);
                                   intent.putExtra("Uri",uri);
                                   startActivity(intent);
                                 }
                               });
```

### Error code

| error code | Description |
| ---------- | ------------------------------------------------------------ |
| 10100 | Get SECRET failed |
| 10101 | Failed to get AUTH |
| 10110 | Other exceptions |
| 10010 | Failed to obtain authentication information |
| 10011 | Failed to get time slice (authentication information) for a certain day |

## Cloud video playback

### Initialize and create a device

```java
void createCloudDevice(String cachePath, String devId)
```

**Parameter**

| Parameter | Description |
| --------- | ----------- |
| cachePath | Cache path |
| devId | device Id |

**Example**

```java
cloudCamera.createCloudDevice(cachePath, devId);
```

### Register for cloud camera listening

```java
void registerP2PCameraListener(AbsP2pCameraListener listener);
```

**Parameter**

| Parameter | Description |
| --------- | ----------- |
| listener | Callback |

**Example**

```java
cloudCamera.registerP2PCameraListener(new AbsP2pCameraListener() {
    @Override
    public void onSessionStatusChanged(Object camera, int sessionId, int sessionStatus) {
        super.onSessionStatusChanged(camera, sessionId, sessionStatus);
    }
});
```

### Unregister cloud camera listening

```java
void removeOnP2PCameraListener();
```

**Example**

```java
cloudCamera.removeOnP2PCameraListener(this);
```

### Binder video view

```java
void generateCloudCameraView(IRegistorIOTCListener view);
```

**Parameter**

| Parameter | Description |
| --------- | ----------- |
| view | Video view |

**Example**

```java
cloudCamera.generateCloudCameraView(mVideoView);
```

### Cloud storage playing

#### Start playing

```java
void playCloudDataWithStartTime(long mStartTime, long mEndTime, boolean isEvent, OperationCallBack callback, OperationCallBack playFinishedCallBack);
```

**Parameter Description**

| Parameter | Description |
| -------------------- | ----------------------------------------- |
| mStartTime | Starting time (10-bit timestamp) |
| mEndTime | End time, usually the end time of the day |
| isEvent | Whether to detect events, default false |
| callBack | play callback |
| playFinishedCallBack | Play finish callback |

**Sample Code**

```java
cloudCamera.playCloudDataWithStartTime(startTime, endTime, isEvent,
                                       new OperationCallBack() {
                                         @Override
                                         public void onSuccess(int sessionId, int requestId, String data, Object camera) {

                                         }

                                         @Override
                                         public void onFailure(int sessionId, int requestId, int errCode, Object camera) {

                                         }
                                       }, new OperationCallBack() {
                                         @Override
                                         public void onSuccess(int sessionId, int requestId, String data, Object camera) {

                                         }

                                         @Override
                                         public void onFailure(int sessionId, int requestId, int errCode, Object camera) {
                                         }
                                       });
```

#### Pause playing

```java
int pausePlayCloudVideo(OperationDelegateCallBack callBack);
```

**Parameter**

| Parameter | Description |
| --------- | ----------- |
| callBack | call back |

**Example**

```java
cloudCamera.pausePlayCloudVideo(new OperationDelegateCallBack() {
  @Override
  public void onSuccess(int sessionId, int requestId, String data) {
  }

  @Override
  public void onFailure(int sessionId, int requestId, int errCode) {

  }
});
```

#### Resume playing

```java
int resumePlayCloudVideo(OperationDelegateCallBack callBack);
```

**Parameter**

| Parameter | Description |
| --------- | ----------- |
| callBack | call back |

**Example**

```java
cloudCamera.resumePlayCloudVideo(new OperationDelegateCallBack() {
  @Override
  public void onSuccess(int sessionId, int requestId, String data) {
  }

  @Override
  public void onFailure(int sessionId, int requestId, int errCode) {

  }
});
```

#### Stop playing

```java
int stopPlayCloudVideo(OperationDelegateCallBack callBack);
```

**Parameter**

| Parameter | Description |
| --------- | ----------- |
| callBack | call back |

**Example**

```java
cloudCamera.stopPlayCloudVideo(new OperationDelegateCallBack() {
  @Override
  public void onSuccess(int sessionId, int requestId, String data) {
  }

  @Override
  public void onFailure(int sessionId, int requestId, int errCode) {

  }
});
```

**Declaration**

download cloud videos.

```java
void startCloudDataDownload(long startTime, long stopTime, String folderPath, String mp4FileName,
                                OperationCallBack callback, ProgressCallBack progressCallBack, OperationCallBack finishedCallBack);
```

**Parameters**

| Parameter | Description |
| --------- | ------- |
| startTime | start time, 10-bit timestamp |
| stopTime | end time, 10-bit timestamp |
| folderPath | file path to save video |
| mp4FileName | file name |
| callback | operation callback |
| progressCallBack | download progress callback |
| finishedCallBack | download finish callback |

**Declaration**

stop download cloud video.

```java
void stopCloudDataDownload(OperationCallBack callBack);
```

**Parameters**

| Parameter | Description |
| --------- | ----------|
| callBack | operation callback |

**Declaration**

delete cloud video

```java
void deleteCloudVideo(long timeGT, long timeLT, String timeZone, final ITuyaResultCallback<String> callback);
```

**Parameters**

| Parameter | Description |
| :------- | :---------------------------------- |
| timeGT | start time (10-bit timestamp) |
| timeLT | end time |
| timeZone | time Zone |
| callback | callback|

### Other functions

Cloud storage video playback also provides functions such as sound switch, local video recording, and screenshots.

#### Get sound status

```java
int getCloudMute();
```

**Example**

```java
cloudCamera.getCloudMute()
```

#### Set sound status

``` java
void setCloudMute(final int mute, OperationDelegateCallBack callBack);
```

**Parameter**

| Parameter | Description |
| --------- | ----------- |
| mute | is mute |
| callBack | call back |

**Example**

```java
cloudCamera.setCloudMute(mute, new OperationDelegateCallBack() {
  @Override
  public void onSuccess(int sessionId, int requestId, String data) {
    soundState = Integer.valueOf(data);
  }

  @Override
  public void onFailure(int sessionId, int requestId, int errCode) {
  }
});
```

#### snapshot

```java
int snapshot(String absoluteFilePath, OperationDelegateCallBack callBack);
```

**Parameter**

| Parameter | Description |
| ---------------- | ----------- |
| absoluteFilePath | file path |
| callBack | call back |

**Example**

```java
cloudCamera.snapshot(IPCCameraUtils.recordSnapshotPath(devId), new OperationDelegateCallBack() {
  @Override
  public void onSuccess(int sessionId, int requestId, String data) {
    Toast.makeText(CameraCloudStorageActivity.this, "snapshot success", Toast.LENGTH_SHORT).show();
  }

  @Override
  public void onFailure(int sessionId, int requestId, int errCode) {
  }
});
```

### Video Recording

#### Start recording

```java
int startRecordLocalMp4(String folderPath, String fileName, OperationDelegateCallBack callBack);
```

**Parameter**

| Parameter | Description |
| ---------- | ----------- |
| folderPath | folder path |
| fileName | file name |
| callBack | Call back |

**Example**

```java
cloudCamera.startRecordLocalMp4(IPCCameraUtils.recordPath(devId), String.valueOf(System.currentTimeMillis()), new OperationDelegateCallBack() {
  @Override
  public void onSuccess(int sessionId, int requestId, String data) {
    Toast.makeText(CameraCloudStorageActivity.this, "record start success", Toast.LENGTH_SHORT).show();
  }

  @Override
  public void onFailure(int sessionId, int requestId, int errCode) {
  }
});
```

#### Stop recording

```java
int stopRecordLocalMp4(OperationDelegateCallBack callBack);
```

**Parameter**

| Parameter | Description |
| --------- | ----------- |
| callBack | call back |

**Example**

```java
cloudCamera.stopRecordLocalMp4(new OperationDelegateCallBack() {
  @Override
  public void onSuccess(int sessionId, int requestId, String data) {
    Toast.makeText(CameraCloudStorageActivity.this, "record end success", Toast.LENGTH_SHORT).show();
  }

  @Override
  public void onFailure(int sessionId, int requestId, int errCode) {
  }
});
```

### Deinit

```java
void deinitCloudCamera();
```

**Example**

```java
cloudCamera.deinitCloudCamera();
```

### Destroy

```java
public void onDestroy()
```

**Example**

```java
cloudCamera.onDestroy();
```
