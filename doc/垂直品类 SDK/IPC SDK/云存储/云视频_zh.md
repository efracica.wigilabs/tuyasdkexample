## 云存储

涂鸦智能为智能摄像机提供云存储的服务，可以将设备录制的视频上传到涂鸦云端。

## 流程图

先获取云存储服务状态，如果云存储服务未开通或者已经过期并且云视频已经被全部删除（云存储服务过期后，已经上传的云视频还会保留一段时间，通常是 7 天），就需要先购买云存储服务。如果云存储服务在有效期，先获取有云存储视频的日期，然后获取指定日期的相关数据，包括云存储事件，时间轴数据，鉴权信息等。之后就可以选择一个云存储事件或者一个时间点开始播放云视频了。

![image.png](https://airtake-public-data-1254153901.cos.ap-shanghai.myqcloud.com/content-platform/hestia/1637822570da67721f5b8.png)


## 服务购买

云存储服务购买需要使用 [云存储业务包组件](https://developer.tuya.com/cn/docs/app-development/android-bizbundle-sdk/cloudstorage?id=Ka8qhzjzay7fx)，该组件提供了云存储购买的 H5 页面和订单展示功能。

## 云视频

**ITYCloudCamera** 提供了云存储相关的API接口。

**接口说明**

创建云存储的实例对象。

```java
ITYCloudCamera createCloudCamera();
```

**示例代码**

```java
private ITYCloudCamera cloudCamera;

ITuyaIPCCloud cloud = TuyaIPCSdk.getCloud();
if (cloud != null) {
    cloudCamera = cloud.createCloudCamera();
}
```

## 云存储数据

### 获取云存储当前状态值（有无购买等）

使用云视频前需要先获取当前状态值，否则云存储无法正常显示画面。

```java
void queryCloudServiceStatus(String devId, ITuyaResultCallback<CloudStatusBean> callback);
```

**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| devId | 设备 id |
| callback | 回调方法|

**示例代码**

```java
cloudCamera.queryCloudServiceStatus(devId, new ITuyaResultCallback<CloudStatusBean>() {
    @Override
    public void onSuccess(CloudStatusBean result) {
        //Get cloud storage status
    }

    @Override
    public void onError(String errorCode, String errorMessage) {
        
    }
});
```

**CloudStatusBean 数据模型**

| 参数 | 说明 |
| ---- | ---- |
| status | 状态码 |
| key | 云存储密钥 |

**状态码说明**

| 类型 | 说明 |
| ---- | ---- |
| 10001 | 未购买云存储服务 |
| 10010 | 已开通云存储服务 |
| 10011 | 云存储服务已过期 |

云存储事件中，事件截图 TimeRangeBean.snapshotUrl 的值是一个完整的加密过的图片地址。云存储密钥用于显示该加密图片。

### 获取有云存储数据的日期

```java
void getCloudDays(String devId, ITuyaResultCallback<List<CloudDayBean>> callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| devId | 设备 id |

**示例代码**

```java
cloudCamera.getCloudDays(devId, new ITuyaResultCallback<List<CloudDayBean>>() {
    @Override
    public void onSuccess(List<CloudDayBean> result) {
        
    }

    @Override
    public void onError(String errorCode, String errorMessage) {
        
    }
});
```

**CloudDayBean 数据模型**

| 参数 | 说明 |
| ---- | ---- |
| uploadDay | 日期 |
| currentDayStart | 对应日期的开始时间，用于获取指定时间内的视频片段 |
| currentDayEnd | 对应日期的结束时间，用于获取指定时间内的视频片段 |

### 获取指定时间内的视频片段

```java
void getTimeLineInfo(String devId, long timeGT, long timeLT, ITuyaResultCallback<List<TimePieceBean>> callback);
```

**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| devId | 设备 id |
| timeGT | 开始时间（10位时间戳）|
| timeLT | 结束时间（10位时间戳） |
| callback | 回调方法 |

**示例代码**

```java
CloudDayBean dayBean = dayBeanList.get(0);
cloudCamera.getTimeLineInfo(devId, dayBean.getCurrentStartDayTime(), dayBean.getCurrentDayEndTime(), new ITuyaResultCallback<List<TimePieceBean>>() {
    @Override
    public void onSuccess(List<TimePieceBean> result) {
        
    }

    @Override
    public void onError(String errorCode, String errorMessage) {
        
    }
});
```

### 获取指定时间内的移动侦测数据

```java
void getMotionDetectionInfo(String devId, long timeGT, long timeLT, int offset, int limit, ITuyaResultCallback<List<TimeRangeBean>> callback);
```

**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| devId | 设备 id |
| timeGT | 开始时间（10位时间戳）|
| timeLT | 结束时间（10位时间戳）|
| offset | 第几页，默认0 |
| limit | 每次拉取条数，默认-1，表示所有数据 |
| callback | 回调方法 |

**示例代码**

```java
CloudDayBean dayBean = dayBeanList.get(0);
cloudCamera.getMotionDetectionInfo(devId, dayBean.getCurrentStartDayTime(), dayBean.getCurrentDayEndTime(), offset, limit, new ITuyaResultCallback<List<TimeRangeBean>>() {
    @Override
    public void onSuccess(List<TimeRangeBean> result) {

    }

    @Override
    public void onError(String errorCode, String errorMessage) {

    }
});
```

云存储事件中携带的事件截图是经过加密的，需要通过加密图片组件展示，详情参考[加密图片-云存储事件](https://developer.tuya.com/cn/docs/app-development/android-app-sdk/extension-sdk/ipc-sdk/encryptimage?id=Ka6nxw2hetr2y)章节。

### ~~云存储购买地址接口~~

已不再维护，参考 云存储业务包

```java
public void buyCloudStorage(Context mContext, DeviceBean deviceBean, String homeId, ICloudManagerCallback callback) {
        CameraCloudManager.getInstance().getCloudStorageUrl(mContext, deviceBean, homeId);
    }
```

**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| mContext | 上下文 |
| deviceBean | 设备信息）|
| homeId | 家庭id|
| callback | 回调方法|

**示例代码**

```java
cameraCloudSDK.buyCloudStorage(CameraCloudStorageActivity.this,
                               TuyaHomeSdk.getDataInstance().getDeviceBean(devId),
                               String.valueOf(FamilyManager.getInstance().getCurrentHomeId()), new ICloudManagerCallback() {
                                 @Override
                                 public void onError(int i) {

                                 }

                                 @Override
                                 public void onSuccess(Object o) {
                                   String uri = (String) o;
                                   Intent intent = new Intent(CameraCloudStorageActivity.this, WebViewActivity.class);
                                   intent.putExtra("Uri",uri);
                                   startActivity(intent);
                                 }
                               });
```

### 错误码

| 错误码 | 说明 |
| :---- | :---- |
| 10100 | 获取 SECRET 失败 |
| 10101 | 获取 AUTH 失败 |
| 10110 | 其他异常 |
| 10010 | 获取认证信息失败 |
| 10011 | 获取某天的视频片段（鉴权信息）失败 |

## 云存储播放

### 初始化

创建设备对象

```java
void createCloudDevice(String cachePath, String devId)
```

**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| cachePath | 缓存路径 |
| devId | 设备id |

**示例代码**

```java
cloudCamera.createCloudDevice(cachePath, devId);
```

### 注册云存储监听

向 `ITYCloudCamera` 注册监听器，否则无法正常显示画面。

```java
void registerP2PCameraListener(AbsP2pCameraListener listener);
```
**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| listener | p2p 回调 |

**示例代码**

```java
cloudCamera.registerP2PCameraListener(new AbsP2pCameraListener() {
    @Override
    public void onSessionStatusChanged(Object camera, int sessionId, int sessionStatus) {
        super.onSessionStatusChanged(camera, sessionId, sessionStatus);
    }
});
```

### 反注册云存储监听

```java
void removeOnP2PCameraListener();
```
**示例代码**

```java
cloudCamera.removeOnP2PCameraListener();
```

### 绑定播放组件 view

```java
void generateCloudCameraView(IRegistorIOTCListener view);
```
**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| view | 播放器组件 |

**示例代码**

```java
cloudCamera.generateCloudCameraView(mVideoView);
```

### 云存储视频播放

#### 开始播放

```java
void playCloudDataWithStartTime(long mStartTime, long mEndTime, boolean isEvent, OperationCallBack callback, OperationCallBack playFinishedCallBack);
```
**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| mStartTime | 开始时间（10位时间戳） |
| mEndTime | 结束时间，一般是这一天的结束时间 |
| isEvent | 是否是侦测事件，默认false |
| callBack | 播放回调 |
| playFinishedCallBack | 播放结束回调 |

**示例代码**

```java
cloudCamera.playCloudDataWithStartTime(startTime, endTime, isEvent,
                                       new OperationCallBack() {
                                         @Override
                                         public void onSuccess(int sessionId, int requestId, String data, Object camera) {
                                           // 播放中的回调, playing
                                         }

                                         @Override
                                         public void onFailure(int sessionId, int requestId, int errCode, Object camera) {

                                         }
                                       }, new OperationCallBack() {
                                         @Override
                                         public void onSuccess(int sessionId, int requestId, String data, Object camera) {
                                           //播放完成的回调, playCompleted
                                         }

                                         @Override
                                         public void onFailure(int sessionId, int requestId, int errCode, Object camera) {
                                         }
                                       });
```

#### 暂停播放

```java
int pausePlayCloudVideo(OperationDelegateCallBack callBack);
```
**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| callBack | 操作回调 |

**示例代码**

```java
cloudCamera.pausePlayCloudVideo(new OperationDelegateCallBack() {
  @Override
  public void onSuccess(int sessionId, int requestId, String data) {
  }

  @Override
  public void onFailure(int sessionId, int requestId, int errCode) {

  }
});
```

#### 继续播放

```java
int resumePlayCloudVideo(OperationDelegateCallBack callBack);
```
**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| callBack | 操作回调 |

**示例代码**

```java
cloudCamera.resumePlayCloudVideo(new OperationDelegateCallBack() {
  @Override
  public void onSuccess(int sessionId, int requestId, String data) {
  }

  @Override
  public void onFailure(int sessionId, int requestId, int errCode) {

  }
});
```

#### 停止播放

```java
int stopPlayCloudVideo(OperationDelegateCallBack callBack);
```
**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| callBack | 操作回调 |

**示例代码**

```java
cloudCamera.stopPlayCloudVideo(new OperationDelegateCallBack() {
  @Override
  public void onSuccess(int sessionId, int requestId, String data) {
  }

  @Override
  public void onFailure(int sessionId, int requestId, int errCode) {

  }
});
```

**接口说明**

下载云存储视频。

```java
void startCloudDataDownload(long startTime, long stopTime, String folderPath, String mp4FileName,
                                OperationCallBack callback, ProgressCallBack progressCallBack, OperationCallBack finishedCallBack);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| startTime | 需要下载的视频片段的开始时间，10位时间戳 |
| stopTime | 需要下载的视频片段的结束时间，10位时间戳 |
| folderPath | 存储路径 |
| mp4FileName | 文件名 |
| callback | 操作回调 |
| progressCallBack | 下载进度回调 |
| finishedCallBack | 下载完成回调 |

**接口说明**

停止下载云存储视频。

```java
void stopCloudDataDownload(OperationCallBack callBack);
```

**参数说明**

| 参数 | 说明 |
| ---- | ----|
| callBack | 操作回调 |

**接口说明**

删除云存储接口

```java
void deleteCloudVideo(long timeGT, long timeLT, String timeZone, final ITuyaResultCallback<String> listener);
```

**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| timeGT | 开始时间（10位时间戳）|
| timeLT | 结束时间|
| timeZone | 时区 |
| listener | 回调方法|

### 其他功能

云存储视频播放也提供有声音开关，本地视频录制，截图等功能。

#### 获取静音状态

```java
int getCloudMute();
```

**示例代码**

```java
cloudCamera.getCloudMute()
```

#### 设置静音状态

``` java
void setCloudMute(final int mute, OperationDelegateCallBack callBack);
```
**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| mute | 是否静音 |
| callBack | 操作回调 |

**示例代码**

```java
cloudCamera.setCloudMute(mute, new OperationDelegateCallBack() {
            @Override
            public void onSuccess(int sessionId, int requestId, String data) {
                soundState = Integer.valueOf(data);
            }

            @Override
            public void onFailure(int sessionId, int requestId, int errCode) {
            }
        });
```

#### 截图

```java
int snapshot(String absoluteFilePath, OperationDelegateCallBack callBack);
```
**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| absoluteFilePath | 文件地址 |
| callBack | 操作回调 |

**示例代码**

```java
cloudCamera.snapshot(IPCCameraUtils.recordSnapshotPath(devId), new OperationDelegateCallBack() {
  @Override
  public void onSuccess(int sessionId, int requestId, String data) {
    Toast.makeText(CameraCloudStorageActivity.this, "snapshot success", Toast.LENGTH_SHORT).show();
  }

  @Override
  public void onFailure(int sessionId, int requestId, int errCode) {
  }
});
```

### 视频录制

#### 开始录制

```java
int startRecordLocalMp4(String folderPath, String fileName, OperationDelegateCallBack callBack);
```
**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| folderPath | 文件夹路径 |
| fileName | 文件名称 |
| callBack | 操作回调 |

**示例代码**

```java
cloudCamera.startRecordLocalMp4(IPCCameraUtils.recordPath(devId), String.valueOf(System.currentTimeMillis()), new OperationDelegateCallBack() {
  @Override
  public void onSuccess(int sessionId, int requestId, String data) {
    Toast.makeText(CameraCloudStorageActivity.this, "record start success", Toast.LENGTH_SHORT).show();
  }

  @Override
  public void onFailure(int sessionId, int requestId, int errCode) {
  }
});
```

#### 结束录制

```java
int stopRecordLocalMp4(OperationDelegateCallBack callBack);
```

**参数说明**

| 参数 | 说明 |
| :---- | :---- |
| callBack | 操作回调 |

**示例代码**

```java
cloudCamera.stopRecordLocalMp4(new OperationDelegateCallBack() {
  @Override
  public void onSuccess(int sessionId, int requestId, String data) {
    Toast.makeText(CameraCloudStorageActivity.this, "record end success", Toast.LENGTH_SHORT).show();
  }

  @Override
  public void onFailure(int sessionId, int requestId, int errCode) {
  }
});
```

### 反初始化

```java
void deinitCloudCamera();
```

**示例代码**

```java
cloudCamera.deinitCloudCamera();
```

### 销毁

```java
public void onDestroy();
```

**示例代码**

```java
cloudCamera.onDestroy();
```