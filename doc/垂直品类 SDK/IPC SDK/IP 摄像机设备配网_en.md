This topic introduces information on device networking for the development of the IP camera app. For networking of other devices, please refer to [Device Networking (Android)](https://developer.tuya.com/cn/docs/app-development/wifinetwork?id=Ka6ki8lbwu82c).

## Networking methods

The camera hardware Wi-Fi mainly supports the following networking methods, among which **Wi-Fi Fast Connect mode** and **Hotspot mode** are the same as the networking methods for other devices.

* Wi-Fi Fast Connect mode (also known as Smart Config mode)
* Hotspot mode (also known as AP mode)
* QR code scanning method

	> **Note**: Because QR code scanning is easier, we recommend you use it. If the IP camera device can not scan the code, you can try Wi-Fi fast connection method as a priority.

* Cloud Development Docking

	Due to the cloud development and SDK docking, the integrated distribution network SDK is different, the process and code of device activation will be a little different.

	> **Note**: Cloud development is connected to a cut-down version of the wiring SDK, please visit the [tuya-android-activator-sdk](https://github.com/tuya/tuya-android-activator-sdk) GitHub repository for details.

## Binding mode

Tuya smart devices support three binding modes: strong, medium, and weak. After the device is successfully activated into the family of the corresponding account, different binding modes and different verification methods are required to unbind.

* **Strong Binding**:

	It requires the previous user to remove the device in the App before it can be re-matched to bind to another account.

	> **Note**: Since IPC has audio and video transmission and involves more privacy, the default is the strong binding mode, and you can't switch to other modes. If you need to switch to other modes, please evaluate the impact in detail and then [submit work order](https://service.console.tuya.com/) to handle it.

* **Ordinary Binding**:

	No need for the previous user to remove the device in the App to re-bind the network to another account, but a PUSH notification message will be sent to the previous account's home group/default group administrator.

* **Weak Binding**:

	You can re-bind to another account without the previous user removing the device from the App. Translated with www.DeepL.com/Translator (free version)