在传统的方案中，App 和设备的数据传输都是经过服务器进行转发，当传输数据较大时需要占用服务器大量的空间和流量，运营成本较高。涂鸦 P2P 连接方案可以有效地解决这个问题，降低运营成本。

## 集成

开发项目目录下的 build.gradle 配置如下：

	```groovy
	allprojects {
	    repositories {
	        ...
	        maven {
	            url "https://maven-other.tuya.com/repository/maven-releases/"
	        }
		...
	    }
	}
	```

`app` 目录下的 build.gradle 配置：

	```groovy
	dependencies {
		...
		implementation 'com.tuya.smart:tuyasmart-p2p-channel-sdk:3.4.15'
		...
	}
	```

	>**注意**：IPC SDK 已包含 P2P 能力，不需要再额外集成。


## 获取 P2P

获取 P2P 对象，该对象是单例

**接口说明**
```java
ITuyaP2P p2pSDK = TuyaP2PSdk.getP2P();
```

## 初始化

P2P SDK 初始化。

**接口说明**

```java
int init(String localId);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| localId | 用户 UID，登录账号从 [用户数据模型 `User`](https://developer.tuya.com/cn/docs/app-development/usermanage?id=Ka69qtzy9l8nc#title-1-User) 中查询 |

**示例代码**

```java
ITuyaP2P p2p = TuyaIPCSdk.getP2P();
if (p2p != null) {
    p2p.init("xxx");
}
```

## 反初始化

P2P 反初始化，一般退出登录的时候调用。

**接口说明**

```java
int deInit();
```

## 建立 P2P 连接

**接口说明**

与指定设备建立 P2P 连接，方法调用返回 `traceId` 为连接的唯一标识符，可用于取消正在尝试的连接。通过 callback 返回成功建立连接的句柄。

支持版本  `v3.4.8`

```java
 String connect(String remoteId, int lanMode, int timeout, ITuyaP2PCallback callback);
```

**参数说明**

| 参数 | 说明 |
| ---- | ---- |
| remoteId | 设备 ID |
| lanMode | 优先连接方式 |
| timeout | 连接超时时间 |
| callback | 连接结果，>= 0，连接成功，返回 P2P 本次连接的会话句柄；< 0，连接失败，返回具体失败错误码 |

| lanMode | 说明 |
| ---- | --- |
| 0 | 自动选择 |
| 1 | 外网连接优先 |
| 2 | 局域网连接优先 |

**返回值**

| 返回值 | 说明 |
| ---- | --- |
| traceId | 连接的唯一标识符，用于取消正在尝试的连接 |

## 取消正在尝试的连接

已经建立成功的连接不受影响。

**接口说明**

```java
int connectBreak(String traceId);
```

**参数说明**

| 参数 | 说明 |
| ---- | --- |
| traceId | 连接的唯一标识符 |

## 关闭连接

**接口说明**

```java
int disConnect(int handle);
```

**参数说明**

| 参数 | 说明 |
| ---- | --- |
| handle | 连接句柄 |

## 接收数据

**接口说明**

```java
int recvData(int handle, int channel, byte[] data, int dataLen, int timeout);
```

**参数说明**

| 参数 | 说明 |
| ---- | --- |
| handle | 连接句柄 |
| channel | 通道号 |
| data | 接收数据的缓冲区 |
| dataLen | 接收数据的长度 |
| timeout | 超时时间，单位毫秒 |

**返回值说明**

| 返回值 | 说明 |
| ---- | --- |
| < 0 | 接收数据出错，-3 表示超时，其他错误表示连接已断开 |
| >= 0 | 实际接收到的数据长度 |

## 向对方发送数据

**接口说明**

```java
int sendData(int handle, int channel, byte[] data, int dataLen, int timeout);
```

**参数说明**

| 参数 | 说明 |
| ---- | --- |
| handle | 连接句柄 |
| channel | 通道号 |
| data | 需要发送的内容 |
| dataLen | 需要发送的数据长度 |
| timeout | 超时时间，单位毫秒 |

**返回值**

| 返回值 | 说明 |
| ---- | --- |
| < 0 | 发送数据出错，-3 表示超时，其他错误表示连接已断开 |
| >= 0 | 发送成功的数据长度 |

## 检查连接是否正常

**接口说明**

```java
int activeCheck(int handle);
```

**参数说明**

| 参数 | 说明 |
| ---- | --- |
| handle | 连接句柄 |

**返回值**

| 返回值 | 说明 |
| ---- | --- |
| < 0 | 连接已断开 |
| >= 0 | 连接正常 |

## 查询 P2P SDK 版本号

**接口说明**

```java
String getVersion();
```

## 通道号

目前在涂鸦智能设备的数据协议中，每一个 P2P 会话中初始化 8 个数据通道，已经定义和使用的通道有 6 个。

| 通道号 | 说明 |
| ---- | ---- |
| 0 | 控制命令收发通道 |
| 1 | 视频数据传输通道 |
| 2 | 音频数据传输通道 |
| 3 | 存储卡回放视频传输通道 |
| 4 | 数据透传通道 |
| 5 | 文件下载传输通道 |

## 错误码

| 错误码 | 说明 |
| ---- | ---- |
| -1001 | 网络连接其他错误 |
| -1002 | P2P 类型不支持 |
| -1003 | 参数错误 |
| | |