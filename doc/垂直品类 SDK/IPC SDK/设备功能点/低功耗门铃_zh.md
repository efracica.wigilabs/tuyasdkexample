## 判断是否是低功耗设备

**接口说明**

设备是否是低功耗设备。

```java
boolean isLowPowerDevice(String devId);
```

如果设备即是摄像机设备又是低功耗设备，可以认为设备是低功耗门铃。

**示例代码**

```java
ITuyaIPCCore cameraInstance = TuyaIPCSdk.getCameraInstance();
if (cameraInstance != null) {
    cameraInstance.isLowPowerDevice(devId));
}
```

## 休眠唤醒

低功耗门铃由电池供电，为了节省电量，在一定时间内没有 p2p 连接会休眠，休眠后无法直接连接 p2p，需要先唤醒设备，再连接 p2p 通道。

**接口说明**

```java
void wirelessWake(String devId);
```

**示例代码**

```java
ITuyaIPCDoorbell doorbell = TuyaIPCSdk.getDoorbell();
if (doorbell != null) {
    doorbell.wirelessWake(devId);
}
```

可以使用通过查询 DP 点 "149" 信息查看低功耗门铃设备是否唤醒。如何查询 DP 点信息见[设备控制](https://developer.tuya.com/cn/docs/app-development/android-app-sdk/device-management/devicemanage?id=Ka6ki8r2rfiuu). 

## 门铃呼叫

设备成功绑定到家庭且在线状态下，有人按门铃，IPC SDK 将收到门铃呼叫的事件。

### 集成推送通知

[集成推送](https://developer.tuya.com/cn/docs/app-development/android-app-sdk/integrate-push/push?id=Ka6ki8ipunvb8)，按门铃，app 端收到推送消息。

### 注册涂鸦推送协议

> 前提:  APP 进程处于 active 状态

**registerCameraPushListener**

在登录 tuya 账号后，注册 registerCameraPushListener ，调用方法：

```java
 void registerCameraPushListener(ITuyaGetBeanCallback<CameraPushDataBean> callback);
```

**参数说明**

| 参数             | 说明           |
| ---- | ---- |
| ITuyaGetBeanCallback | 收到消息数据的回调 |
| CameraPushDataBean  | 消息体数据模型 |

**CameraPushDataBean**

| 参数  | 描述   |
| ---- | ---- |
| timestamp | 消息时间戳 |
| devid | 设备 id |
| msgid | 消息 id |
| etype | 消息类型  |

> **注意**：门铃来电消息的 etype=doorbell 

**unRegisterCameraPushListener**

在退出 tuya 账号后，反注册，调用 unRegisterCameraPushListener

```java
void unRegisterCameraPushListener(ITuyaGetBeanCallback<CameraPushDataBean> callback);
```

>**说明**：当app进程被杀死的时候，该监听无效。当app登录成功之后，注册监听；app退出登录，取消监听。

## 电池管理

低功耗门铃有两种供电方式，插电和电池供电。通过 IPC SDK 可以查询到设备当前的供电模式以及当前的电量。还可以设置一个低电量报警阈值，当电量过低时，会触发一个报警通知。

### 电池锁的数据下发

控制打开/关闭电池锁的功能点 ID 是 "153"。

|   值   | 描述 |
| ----- | ---- |
| true  | 上锁 |
| false | 解锁 |

### 电池及设备状态信息的数据下发

设备电池及状态信息的功能点是 "145"，设备电量是上报0-100的整数。

> **注意**：该 DP 点下发不需要带参数，所以传null值即可。

### 低电量报警阈值的数据下发

低电量报警阈值功能点 ID 是 "147"。当设备电池小于设置的报警阈值时则报警。

### 设备供电方式

设备供电方式的功能点 ID 是 "146"，供电状态发生变化时会上报。

|  值  |   描述   |
| --- | ------- |
| 0   | 电池供电 |
| 1   | 插电供电 |

> **注意**：该 DP 点下发不需要带参数，所以传null值即可。