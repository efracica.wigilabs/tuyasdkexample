
## Determine if it is a low power device

**Declaration**

Determine if it is a low-power device.

```java
boolean isLowPowerDevice(String devId);
```

If the device is both a camera device and a low-power device, the device can be considered a low-power doorbell.

**Example**

```java
ITuyaIPCCore cameraInstance = TuyaIPCSdk.getCameraInstance();
if (cameraInstance != null) {
    cameraInstance.isLowPowerDevice(devId));
}
```

## Sleep and wake

Low power doorbell is powered by the battery. In order to save power, the camera will sleep when no p2p connection for a certain period of time. After sleeping, it cannot be directly connected to p2p. You need to wake up the device and then connect to the p2p channel after waking up.

**Declaration**

```java
void wirelessWake(String devId);
```

**Parameter**

| Parameter |   Description  |
| ---- | ---- |
| devId | device Id   |

**Example**

```java
ITuyaIPCDoorbell doorbell = TuyaIPCSdk.getDoorbell();
if (doorbell != null) {
    doorbell.wirelessWake(devId);
}
```

You can check whether the low-power doorbell device wakes up by querying the DP point "149" information. Please refer to [Device Control](https://developer.tuya.com/en/docs/app-development/android-app-sdk/device-management/devicemanage?id=Ka6ki8r2rfiuu) for how to query DP point information.

## Doorbell call

When the device is successfully bound to the home and online, when someone rings the doorbell, the SDK will receive the event of the doorbell call.

### Integrated push notification

[Integrated push](https://developer.tuya.com/en/docs/app-development/android-app-sdk/integrate-push/push?id=Ka6ki8ipunvb8), Press the doorbell and the app receives the push message.

### Registration Tuya push protocol

> **Note**: The premise is that the APP process is active.

**registerCameraPushListener**

After logging in to your tuya account, register registerCameraPushListener.

```java
 void registerCameraPushListener(ITuyaGetBeanCallback<CameraPushDataBean> callback);
```

**Parameter Description**

| Parameter        | Description   |
| ---- | ---- |
| ITuyaGetBeanCallback | get data callback |
| CameraPushDataBean   | data bean    |

**CameraPushDataBean**

| Parameter | Description    |
| ---- | ---- |
| timestamp | message time stamp |
| devid | device id      |
| msgid | message id     |
| etype | message Type   |

> **Note**: etype = doorbell for doorbell  message

**unRegisterCameraPushListener**

After logging out of the tuya account, unregister and call unRegisterCameraPushListener

```java
void unRegisterCameraPushListener(ITuyaGetBeanCallback<CameraPushDataBean> callback);
```

> **Note**: When the app process is killed, the listener is invalid. When the app logs in successfully, register for monitoring; when the app logs out, cancel the monitoring.



## Battery management

There are two ways to power low-power doorbells, plug-in, and battery-powered. The SDK can query the current power supply mode and current power of the device. You can also set a low battery alarm threshold. When the battery is too low, an alarm notification will be triggered.

### Battery lock data publish

The DP point ID tha controls the opening/closing of the battery lock control is "153".

|   Value   | Description |
| ----- | ---- |
| true  | locked |
| false | unlock |

### Battery and device status info data publish

The DP point ID of the device battery and status information is "145", the power of the device is an integer from 0-100.

> **Note**: publishing this DP data only needs null value, and require no parameters. 

### Low battery alarm value data publish

The DP point ID of the low battery alarm threshold is "147". When the device battery is less than the set alarm threshold, it will alarm.

### Power supply method data publish

The DP point ID of the device`s power supply method is "146", it will be reported when the power supply status changes.

|  Value  |   Description   |
| --- | ------- |
| 0   | battery power supply |
| 1   | plug power supply |

> **Note**: publishing this DP data only needs null value, and require no parameters.