Memory card management use data point, for related data point refer to [Data point id](https://developer.tuya.com/en/docs/app-development/android-app-sdk/extension-sdk/ipc-sdk/extension-features/datapointid?id=Ka6nxw2ln5k4h).

It is recommended to refer to [Device Management](https://developer.tuya.com/en/docs/app-development/android-app-sdk/device-management/devicemanage?id=Ka6ki8r2rfiuu) to implement query and publish DP point.

## Status

Before starting to manage the memory card or perform the video playback, you need to obtain the status of the memory card. If the device does not detect the memory card, you cannot proceed to the next step. If the memory card is abnormal, you need to format the memory card first.

The DP point ID for obtaining the status of the memory card is "110".

| Value | Description |
| ---- | ---- |
| 1 | normal |
| 2 | abnormal (SD card is broken or under the wrong format) |
| 3 | insufficient space |
| 4 | formatting |
| 5 | no SD card inserted |

> **Note**: publish this DP point data only needs null value (does not require parameters).

## Format

When formatting the memory card, there are two cases according to the implementation of the camera manufacturer. The firmware implemented by some manufacturers will actively report the progress of formatting, and will also actively report the current capacity status after formatting is completed. However, there are a few manufacturers' firmware that will not actively report, so it is necessary to periodically and actively check the format Progress, when the progress reaches 100, then actively query the current capacity status. DpSDFormat data delivery can start the format operation.
The DP point ID for formatting the memory card is "111", publish this DP point to start the format operation.

## Format status

The DP point ID for obtaining the formatting progress is "117", publish this DP point to query the formatting progress. When the progress reaches 100, the formatting ends. You can check the memory card capacity again.

## Get memory card capacity

The DP point ID for obtaining memory card capacity is "109". 
Precondition: confirm whether the data-point&sdcard are existed before data is published

> The string format: total capacity|used capacity|free capacity, unit `KB`.

## Recording switch

The DP point ID for switching the memory card recording is "150", publish this DP point to control whether the Tuya smart camera has the recording function enabled.

## Memory card recording

After the Tuya camera is inserted into the memory card, the captured image recording can be saved in the memory card, and the video recording switch and mode can be set through the SDK. There are two recording modes:

* **Continuous recording**: The camera will continuously record the collected audio and video recordings on the memory card. When the capacity of the memory card is insufficient, the oldest recorded video data will be overwritten.
* **Event recording**: The camera will only start recording video when the detection alarm is triggered. The length of the video will change according to the type of event and the duration of the event.

The DP point ID for modifying the recording mode of the memory card is "151".

| Value | Description |
| --- | ---- |
| 1 | event record (start to record once movement is detected) |
| 2 | continuous record |