涂鸦蓝牙有以下几种技术方案，帮助您实现通过安卓版智能生活 SDK 控制蓝牙系列设备：

| 蓝牙类型 | 说明 |设备应用示例 |
| ---- | ---- | ---- |
| 蓝牙单点 | 蓝牙设备与手机一对一连接单点设备（蓝牙或蓝牙 LE） | 体脂秤、手环、温控器、电动牙刷、门锁等 |
| 蓝牙 Mesh | 蓝牙技术联盟发布的蓝牙拓扑通信 | 一路、二路、五路等灯泡、插座、传感器等子设备 |
| 涂鸦 Mesh | 涂鸦自研的蓝牙拓扑通信 | 与蓝牙 Mesh 产品类似，通信协议为涂鸦自研 |
| 双模设备 | 一些多协议设备也会使用到蓝牙技术，例如同时具备 Wi-Fi 能力和蓝牙能力的 **双模设备** | 蓝牙 Mesh 网关、IPC 设备、新版多协议 Wi-Fi 设备等 |

智能生活 App SDK 蓝牙部分所具备的功能如下：

- **蓝牙设备配网**
	- 扫描发现设备
	- 设备配网
- **蓝牙设备管理**
	- 检查设备连接状态
	- 连接设备
	- 设备操作
	- 解绑设备
- **设备升级固件**
	- 检测设备版本
	- 升级设备固件 OTA