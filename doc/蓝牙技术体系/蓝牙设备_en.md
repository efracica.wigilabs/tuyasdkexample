Tuya Bluetooth has 3 technology solutions.

- **Bluetooth**: Bluetooth single point device, one-to-one connection between Bluetooth device and phone.
- **Tuya mesh**: Mesh released by Tuya.
- **Bluetooth mesh**: Mesh released by SIG (Bluetooth Special Interest Group).

	In addition to the above three, there are some multi-protocol devices, such as **Dual-mode Device** with Wi-Fi and Bluetooth capabilities. You can use both Bluetooth and Wi-Fi capabilities.

	|Category | Product examples |
	| ---- | ---- |
	| Bluetooth | Body fat scale, bracelet, electric toothbrush, door lock, etc. |
	| Bluetooth mesh | Light bulb, socket, sensor, etc.|
	| Tuya mesh | Light bulb, socket, sensor, etc. This agreement is self-developed by Tuya. |
	| Dual-mode Device | Bluetooth mesh gateways, IPC devices, and new multi-protocol Wi-Fi devices are all possible. |

The Bluetooth part has the following functions:

- **Activate Device**
	- Scan Bluetooth devices
	- Device activation
- **Device Function**
	- Check the device connection status
	- Connect To Bluetooth Device
	- Device Function
	- Unbind Device
- **Upgrade Firmware**
	- Check device new version
	- OTA(Upgrade device firmware)