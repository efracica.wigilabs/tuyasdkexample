Tuya Bluetooth has 3 technology lines.

- **Bluetooth**: Bluetooth single point device, one-to-one connection between Bluetooth device and phone;
- **Tuya mesh**: Mesh released by Tuya.
- **Bluetooth mesh**: Mesh released by SIG (Bluetooth Special Interest Group).
In addition to the above three, there are some multi-protocol devices, such as **Dual-mode device** with Wi-Fi and Bluetooth capabilities. You can use both Bluetooth and Wi-Fi capabilities.

	|Category | Product examples |
	| ---- | ---- |
	| Bluetooth | Body fat scale, bracelet, electric toothbrush, door lock, etc. |
	| Bluetooth mesh | Light bulb, socket, sensor, and more.|
	| Tuya mesh | Light bulb, socket, sensor, and more. This protocol is self-developed by Tuya. |
	| Dual-mode Device | Bluetooth mesh gateways, IPC devices, and new multi-protocol Wi-Fi devices are all possible. <br><br> Dual-mode's Bluetooth distribution network part, Use Bluetooth technology to network the device, Will be explained in the Bluetooth section.|

>**Note**: Please learn about TuyaHomeSdk first and develop Bluetooth mesh. All operations of mesh are based on the initialization of family data. A family can have multiple meshes (recommend one family create one).

## Prepare

**Mobile System Requirements**

TuyaHomesdk has been supported since Android 4.4.   API>=19

**Manifest Permissions**

```java
<uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
<uses-permission android:name="android.permission.BLUETOOTH" />
<uses-permission android:name="android.permission.BLUETOOTH_ADMIN" />
```

**Permissions Check**

* Before the APP uses the Bluetooth connection or scanning operation, it needs to check whether the APP positioning permission is allowed.
* Check if the Bluetooth status is on.

## Concetps

| Term |Description|
|--|--|
|pcc|Each mesh device corresponds to a product, and each product has its own size class label. The SDK uses pcc and type as the size class label.|
|mesh node Id |	The node id is used to distinguish the "unique identifier" of each mesh device in the mesh network. For example, if you want to control a device, you can send the nodeId command corresponding to this device to the mesh network|
|mesh group local Id|	The local Id is used to distinguish the `unique identifier` of each mesh group in the mesh network. For example, if you want to control the devices in a group, you can send the localId command corresponding to this group to the mesh network.|
|Local Connection |	The networked device is connected via Bluetooth to control mesh and command operations|
|Gateway Connection|	The networked devices are connected through the gateway (the gateway needs to be with the device, and the distance cannot be too far) to control the mesh and command operations.|

## Management

Contains MeshId creation, destruction

### Create Bluetooth mesh

MeshId is created by the cloud. Only one Mesh can be created in a family.

**Description**

```java
void createSigMesh(ITuyaResultCallback<SigMeshBean> callback);
```

**Example**

```java
TuyaHomeSdk.newHomeInstance(123xxx) // homeId
	.createSigMesh(new ITuyaResultCallback<SigMeshBean>() {

	@Override
	public void onError(String errorCode, String errorMsg) {

	}

	@Override
	public void onSuccess(SigMeshBean sigMeshBean) {

	}
});
```

### Delete Bluetooth mesh

Delete SigMesh MeshId.

**Description**

```java
void removeMesh(IResultCallback callback);
```

**Example**

```java
TuyaHomeSdk.newSigMeshDeviceInstance(meshId).removeMesh(new IResultCallback() {
	@Override
	public void onError(String errorCode, String errorMsg) {

	}

	@Override
	public void onSuccess() {

	}
});
```

### Get the list of Bluetooth mesh in the family

**Description**

```java
List<SigMeshBean> getSigMeshList();
```
**Example**
```java
ITuyaHome mTuyaHome = TuyaHomeSdk.newHomeInstance(123xxx); // homeId

if (mTuyaHome.getHomeBean() != null){
	List<SigMeshBean> meshList = TuyaHomeSdk.getSigMeshInstance().getSigMeshList()
}
```

### Get Sub-device Data

**Description**

```java
List<DeviceBean> getMeshSubDevList();
```

**Example**

```java
List<DeviceBean> meshSubDevList = TuyaHomeSdk.newSigMeshDeviceInstance("meshIdxxxxx").getMeshSubDevList();
```

### Init Bluetooth mesh

Initialize the mesh network to monitor the status changes from the cloud.

**Description**

```java
void initMesh(String meshId);
```

**Example**

```java
TuyaHomeSdk.getTuyaSigMeshClient().initMesh("meshIdxxxxx");
```

### Bluetooth mesh destory

It is recommended to destroy the current mesh and re-initialize the mesh when the family switches.

**Description**

```java
void destroyMesh();
```

**Example**

```java
TuyaHomeSdk.getTuyaSigMeshClient().destroyMesh();
```

### Bluetooth mesh sub-device connect and disconnect

`ITuyaBlueMeshClient` provides connect, disconnect, scan, stop the scan.

**Example**

```java
// start connect
TuyaHomeSdk.getTuyaSigMeshClient().startClient(mSigMeshBean);
// start connect with timeout
TuyaHomeSdk.getTuyaSigMeshClient().startClient(mSigMeshBean,searchTime);

// disconnect
TuyaHomeSdk.getTuyaSigMeshClient().stopClient();

// start scan
TuyaHomeSdk.getTuyaSigMeshClient().startSearch()

// stop scan
TuyaHomeSdk.getTuyaSigMeshClient().stopSearch();
```

>**Important**:
>- `startClient(mSigMeshBean)` After calling, it will continuously scan the surrounding connectable devices in the background until the connection is successful.
>- Scanning in the background always consumes resources. You can control background scanning by starting and stopping
>- If not call `startClient()` , `startSearch()` and `stopSearch()` is not working
>- When connected to the Mesh network, calling startSearch and stopSearch is not working.

## Activation

### Scanning Bluetooth mesh devices

Keep Bluetooth ON and check location permissions before scanning.

**Description**

```java
// start scanning
void startSearch();
// stop scanning
void stopSearch();
```

**Example**

```java
ITuyaBlueMeshSearchListener iTuyaBlueMeshSearchListener=new ITuyaBlueMeshSearchListener() {
	@Override
	public void onSearched(SearchDeviceBean deviceBean) {

	}

	@Override
	public void onSearchFinish() {

	}
};

// The UUID of the Bluetooth mesh device to be configured is fixed
UUID[] MESH_PROVISIONING_UUID = {UUID.fromString("00001827-0000-1000-8000-00805f9b34fb")};

SearchBuilder searchBuilder = new SearchBuilder()
								.setServiceUUIDs(MESH_PROVISIONING_UUID)
				.setTimeOut(100)
				.setTuyaBlueMeshSearchListener(iTuyaBlueMeshSearchListener).build();

ITuyaBlueMeshSearch mMeshSearch = TuyaHomeSdk.getTuyaBlueMeshConfig().newTuyaBlueMeshSearch(searchBuilder);

// start
mMeshSearch.startSearch();

// stop
mMeshSearch.stopSearch();
```

### Query device information

After scanning to the target device, the name and icon of the product configuration can be displayed by query.

**Description**

```java
void getActivatorDeviceInfo(String productId, String uuid, String mac, ITuyaDataCallback<ConfigProductInfoBean> callback);
```

**Parameters**

| Parameter	 | Type | **Description** |
| ---- | ---- | ---- |
| productId | String | `SearchDeviceBean.productId`, Note that you need to convert `byte[]` to `String` and then pass in |
| uuid | String | Mesh Device is `null` |
| mac | String | `SearchDeviceBean.productId` |

**Example**

```java
TuyaHomeSdk.getActivatorInstance().getActivatorDeviceInfo(
	// btye[] to String
	new String(bean.getProductId(), StandardCharsets.UTF_8),
	// uuid
	null,
	// mac
	scanDeviceBean.getMacAdress(),
	// callback
	new ITuyaDataCallback<ConfigProductInfoBean>() {
		@Override
		public void onSuccess(ConfigProductInfoBean result) {

		}

		@Override
		public void onError(String errorCode, String errorMessage) {

		}
});
```

**Callback Description**

`ConfigProductInfoBean` description

| Attributes | Type | **Description** |
| ---- | ---- | ---- |
| name | String | The product's name |
| icon | String | The product's icon |

### Active Bluetooth mesh device by Bluetooth

Two types of sub-devices Activation, App activation, and mesh gateway activation.

**Description**

```java
void startActivator();
void stopActivator();
```
**Example**

```java
TuyaSigMeshActivatorBuilder tuyaSigMeshActivatorBuilder = new TuyaSigMeshActivatorBuilder()
			.setSearchDeviceBeans(mSearchDeviceBeanList)
			.setSigMeshBean(sigMeshBean) // Bluetooth mesh info
			.setTimeOut(100)
			.setTuyaBlueMeshActivatorListener(new ITuyaBlueMeshActivatorListener() {
	 @Override
	 public void onSuccess(String mac, DeviceBean deviceBean) {
		 L.d(TAG, "subDevBean onSuccess: " + deviceBean.getName());
	 }
	 @Override
	 public void onError(String mac, String errorCode, String errorMsg) {
		 L.d(TAG, "config mesh error" + errorCode + " " + errorMsg);
	 }
	 @Override
	 public void onFinish() {
		L.d(TAG, "config mesh onFinish");
	 });

ITuyaBlueMeshActivator iTuyaBlueMeshActivator = TuyaHomeSdk.getTuyaBlueMeshConfig().newSigActivator(tuyaSigMeshActivatorBuilder);

// Start Activator
iTuyaBlueMeshActivator.startActivator();

// Stop
iTuyaBlueMeshActivator.stopActivator();
```

**Parameters**

|Field| Description |
|--|--|
|mSearchDeviceBeanList|List of devices to be activated|
|timeout|Activation timeout, default 100s.|
|sigMeshBean|SigMeshBean|

**Data Model**

- `DeviceBean`: See [DeviceBean Data Model](https://developer.tuya.com/en/docs/app-development/android-app-sdk/device-management/devicemanage?id=Ka6ki8r2rfiuu#title-0-DeviceBean%20data%20model)
- `errorCode` : See Error Code

### Active Bluetooth mesh gateway device

See [ZigBee Sub-devices Activation](https://developer.tuya.com/en/docs/app-development/android-app-sdk/wifinetwork#title-7-Sub-device%20Configuration).

### Active Bluetooth mesh device by the gateway

Bluetooth mesh gateway essentially a dual-mode device:

- Activation as a Wi-Fi device. See [Wi-Fi EZ Activation](https://developer.tuya.com/en/docs/app-development/android-app-sdk/wifinetwork?id=Ka6ki8lbwu82c#title-3-Quick%20connection%20mode)
- Activation as a Bluetooth device. See [Dual-mode Activation](https://developer.tuya.com/en/docs/app-development/android-app-sdk/tuya-bluetooth/ble?id=Ka6km4855a1pa#title-8-Dual-mode%20Device%20Activation)

### Error code

| Code | Description |
| ---- | ---- |
| 21002 | invite failed |
| 21004 | provisions failed |
| 21006 | send public key failed |
| 21008 | conform failed |
| 210010 | random conform failed |
| 210014 | send data failed |
| 210016 | composition data failed |
| 210018 | add appkey failed |
| 210020 | bind model failed |
| 210022 | publication model failed |
| 210024 | network transmit failed |
| 210026 | Cloud registration failed |
| 210027 | Device address allocation is full |
| 210034 | notify failed |
| 20021 | timeout |

## Device

`ITuyaBlueMeshDevice` provides all operations for Mesh devices.

### Get device instance

```java
ITuyaBlueMeshDevice mTuyaBlueMeshDevice = TuyaHomeSdk.newSigMeshDeviceInstance("meshIdxxxx");
```

### Query SIGMesh connection status

**Example**

```java
DeviceBean deviceBean=TuyaHomeSdk.getDataInstance().getDeviceBean(mDevId);

// online status (Including local online and gateway online)
boolean online=deviceBean.getIsOnline()
// local online
boolean localOnline=deviceBean.getIsLocalOnline()
// gateway online
boolean wifiOnline=deviceBean.isCloudOnline() && gwBean.getIsOnline()
```

### Bluetooth mesh device and gateway judgment method

**Example**

```java
DeviceBean deviceBean=TuyaHomeSdk.getDataInstance().getDeviceBean(mDevId);

if(deviceBean.isSigMesh()){
	// This device is sigmesh device"
}

if(deviceBean.isSigMeshWifi()){
	// This device is sigmesh wifi device
}
```

### Bluetooth mesh sub-devices rename

**Description**

```java
void renameMeshSubDev(String devId, String name, IResultCallback callback);
```
**Parameters**

|Parameter | Description |
|--|--|
|devId|Device Id|
|name|New name|
|callback|Callback|

**Example**

```java
ITuyaBlueMeshDevice mTuyaBlueMeshDevice= TuyaHomeSdk.newSigMeshDeviceInstance("meshIdxxxx");

mTuyaBlueMeshDevice.renameMeshSubDev("devIdxxxx","New Name", new IResultCallback() {
	 @Override
	 public void onError(String code, String errorMsg) {

	 }

	 @Override
	 public void onSuccess() {

	 }
});
```

### Get device status

Get DP data from cloud maybe not real-time data, can use it search real-time data and `IMeshDevListener`'s `onDpUpdate` will be callback.

**Description**

```java
void querySubDevStatusByLocal(String pcc, final String nodeId, final IResultCallback callback);
```

**Parameters**

|Parameter | Description |
|--|--|
|pcc|Device type|
|nodeId|Device nodeId|
|callback|Callback|

**Example**

```java
ITuyaBlueMeshDevice mTuyaBlueMeshDevice = TuyaHomeSdk.newSigMeshDeviceInstance("meshIdxxxx");

mTuyaBlueMeshDevice.querySubDevStatusByLocal(devBean.getCategory(), devBean.getNodeId(), new IResultCallback() {
	@Override
	public void onError(String code, String errorMsg) {
		// query error
	}
	@Override
	public void onSuccess() {
		// query success
	}
});
```

### Remove device

**Description**

```java
void removeMeshSubDev(String devId, IResultCallback callback);
```

**Parameters**

| Parameter | Description |
| ---- | ---- |
| devId | Device Id |
| pcc | Device type |
| callback | Callback |

**Example**

```java
ITuyaBlueMeshDevice mTuyaBlueMeshDevice = TuyaHomeSdk.newSigMeshDeviceInstance("meshIdxxxx");

mTuyaBlueMeshDevice.removeMeshSubDev(devBean.getDevId(), devBean.getCategory(), new IResultCallback() {
	@Override
	public void onError(String code, String errorMsg) {

	}
	@Override
	public void onSuccess() {

	}
});
```

## Group

`ITuyaGroup` provides operations for Mesh group.

### Find mesh group

A Mesh group or a Wi-Fi group can be distinguished by whether it has a MeshId.

**Example**

```java
GroupBean groupBean=TuyaHomeSdk.getDataInstance().getGroupBean("groupId");
if(!TextUtils.isEmpty(groupBean.getMeshId())){
	// This group is mesh group"
}
```

### Add group

16128 groups can be created in a mesh network. The id range when returning is 0xC000-0xFEFF . It is maintained locally.

**Description**

```java
void addGroup(String name, String pcc, String localId,IAddGroupCallback callback);
```

**Parameters**

|Parameter | Description |
|--|--|
|name|group name|
|pcc|device type|
|localId|LocalId (0xC000 - 0xFFFF)|
|callback|Callback|

**Example**

```java
ITuyaBlueMeshDevice mTuyaSigMeshDevice= TuyaHomeSdk.newSigMeshDeviceInstance("meshId");

mTuyaSigMeshDevice.addGroup("Group Name","1510", "C001", new IAddGroupCallback() {
	@Override
	public void onError(String errorCode, String errorMsg) {
		// Failed to create
	}

	@Override
	public void onSuccess(long groupId) {
		// Created successfully
	}
});
```

### Add device to a group

**Description**

```java
void addDevice(String devId,IResultCallback callback);
```

**Parameters**

|Parameter | Description |
|--|--|
|devId|device Id|
|callback|callback|

**Example**

```java
ITuyaGroup mGroup = TuyaHomeSdk.newSigMeshGroupInstance(groupId);

mGroup.addDevice("devId", new IResultCallback() {
	@Override
	public void onError(String code, String errorMsg) {
		// Failed to add to group
	}

	@Override
	public void onSuccess() {
		// Added to group successfully
	}
});
```

### Remove the device from a group

**Description**

```java
void removeDevice(String devId,IResultCallback callback);
```

**Parameters**

|Parameter | Description |
|--|--|
|devId|Device ID |
|callback|Callback|

**Example**

```java
ITuyaGroup mGroup = TuyaHomeSdk.newSigMeshGroupInstance(groupId);

mGroup.removeDevice("devId", new IResultCallback() {
	@Override
	public void onError(String code, String errorMsg) {
		// Failed to remove device
	}

	@Override
	public void onSuccess() {
		// Device removed successfully
	}
});
```

### Disband group

**Description**

```java
void dismissGroup(IResultCallback callback);
```

**Parameters**

|Parameter | Description |
|--|--|
|callback|Callback|

**Example**

```java
ITuyaGroup mGroup = TuyaHomeSdk.newSigMeshGroupInstance(groupId);
mGroup.dismissGroup(new IResultCallback() {
	@Override
	public void onError(String code, String errorMsg) {
		// Failed to disband the group
	}

	@Override
	public void onSuccess() {
		// Successfully disbanded the group
	}
});
```

### Group rename

**Description**

```java
void renameGroup(String groupName,IResultCallback callback);
```

**Parameters**

|Parameter | Description |
|--|--|
|groupName|new name|
|callback|Callback|

**Example**

```java
ITuyaGroup mGroup = TuyaHomeSdk.newSigMeshGroupInstance(groupId);
mGroup.renameGroup("Group name",new IResultCallback() {
	@Override
	public void onError(String code, String errorMsg) {
		// Rename failed
	}

	@Override
	public void onSuccess() {
		// Renamed successfully
	}
});
```

## Control

`ITuyaBlueMeshDevice` provides operations for Mesh devices.

### Control devices

**Format**:

```java
{
	"(dpId1)":(dpValue1),
	"(dpId2)":(dpValue2)
}
```

**Description**

```java
void publishDps(String nodeId, String pcc, String dps, IResultCallback callback);
```

**Parameters**

|Parameter | Description |
|--|--|
|nodeId|Sub-devices No.|
|pcc|Devices type|
|dps|dps|
|callback|Callback|

**Example**

```java
String dps = {"1":false};

ITuyaBlueMeshDevice mTuyaSigMeshDevice=TuyaHomeSdk.newSigMeshDeviceInstance("meshIdxxxx");

mTuyaSigMeshDevice.publishDps(devBean.getNodeId(), devBean.getCategory(), dps, new IResultCallback() {
	@Override
	public void onError(String s, String s1) {
		// Failed to send DPS
	}

	@Override
	public void onSuccess() {
		// DPS sent successfully
	}
});
```

### Control group

**Description**

```java
void multicastDps(String localId, String pcc, String dps, IResultCallback callback)
```

**Parameters**

|Parameter | Description |
|--|--|
|localId|Group No.|
|pcc|Device type|
|dps|dps|
|callback|Callback|

**Example**

```java
String dps = {"1":false};

ITuyaBlueMeshDevice mTuyaSigMeshDevice= TuyaHomeSdk.newSigMeshDeviceInstance("meshId");

mTuyaSigMeshDevice.multicastDps(groupBean.getLocalId(), devBean.getCategory(), dps, new IResultCallback() {
	@Override
	public void onError(String errorCode, String errorMsg) {
		// Failed to send DPS
	}

	@Override
	public void onSuccess() {
		// DPS sent successfully
	}
});
```

### Data monitor

Mesh info (DP data, status change, device name, device remove) will real-time sync to `IMeshDevListener`.

**Example**

```java
mTuyaSigMeshDevice.registerMeshDevListener(new IMeshDevListener() {

	@Override
	public void onDpUpdate(String nodeId, String dps,boolean isFromLocal) {
		// You can find the corresponding DeviceBean through Node
		DeviceBean deviceBean = mTuyaBlueMeshDevice.getMeshSubDevBeanByNodeId(nodeId);
	}

	@Override
	public void onStatusChanged(List<String> online, List<String> offline,String gwId) {

	}

	@Override
	public void onNetworkStatusChanged(String devId, boolean status) {

	}

	@Override
	public void onRawDataUpdate(byte[] bytes) {

	}

	@Override
	public void onDevInfoUpdate(String devId) {

	}

	@Override
	public void onRemoved(String devId) {

	}
});
```

## Firmware updates

Sub-device updates are classified into generic sub-device updates and mesh gateway updates.
:::important
Sigmesh low-power devices need to wake up before upgrading, and the specific wakeup method varies from device to device.
:::

### Query firmware information

**Description**

```java
void requestUpgradeInfo(String devId, IRequestUpgradeInfoCallback callback);
```

**Parameters**

|Parameter | Type| Description |
|--|--|--|
|devId|String|devId|
|callback|IRequestUpgradeInfoCallback|callback|

**Example**

```java
TuyaHomeSdk.getMeshInstance().requestUpgradeInfo(mDevID, new IRequestUpgradeInfoCallback() {
	@Override
	public void onSuccess(ArrayList<BLEUpgradeBean> bleUpgradeBeans) {

	}

	@Override
	public void onError(String errorCode, String errorMsg) {

	}
});
```

`BLEUpgradeBean` data model

|Parameter | Type | Description |
|----|----|----|
|upgradeStatus|int|0: No new version 1: New version available 2: In upgrade|
|version| String|Latest version|
|currentVersion| String |Current version|
|timeout| int| timeout, Unit s|
|upgradeType|int| Valid values: <ul><li>`0`: update notifications</li><li>`2`: forced updates</li><li>`3`: check for updates</li></ul> |
|type|int| <ul><li>`0`: Wi-Fi</li><li>`1`: Bluetooth</li><li>`2`: GPRS</li><li>`3`: Zigbee</li><li>9: MCU</li></ul> |
|typeDesc| String |Update description |
|lastUpgradeTime|long| The duration of the latest update. |
|**url**|String|New firmware download URL, `type` =` 1` or `9` has value|
|fileSize|long|New firmware size|
|md5|String|New firmware MD5 value|

### Sub-device update

**Description**

```java
void startOta();
```

**Example**

```java
private MeshUpgradeListener mListener = new MeshUpgradeListener() {
	@Override
	public void onUpgrade(int percent) {
		// Update progress.
	}

	@Override
	public void onSendSuccess() {
		// Data sent successfully.
	}

	@Override
	public void onUpgradeSuccess() {
		// Updated successfully.
		mMeshOta.onDestroy();
	}

	@Override
	public void onFail(String errorCode, String errorMsg) {
		// Failed to update.
		mMeshOta.onDestroy();
	}
};

// Get the byte stream of the specified file
byte data[] = getFromFile(path);

TuyaBlueMeshOtaBuilder build = new TuyaBlueMeshOtaBuilder()
		.setData(data)
		.setMeshId(mDevBean.getMeshId())
                 .setMac(mDevBean.getMac())
		.setProductKey(mDevBean.getProductId())
		.setNodeId(mDevBean.getNodeId())
		.setDevId(mDevID)
		.setVersion("version")
		.setTuyaBlueMeshActivatorListener(mListener)
		.bulid();
ITuyaBlueMeshOta mMeshOta = TuyaHomeSdk.newMeshOtaManagerInstance(build);


// Starts OTA updates.
mMeshOta.startOta();
```

**Parameter**

`TuyaBlueMeshOtaBuilder`

| Parameter | Type | Description |
|--|--|--|
|data|byte[]|Byte stream of the firmware to be updated|
|meshId|String|Device MeshId|
|mac|String|Device Mac address|
|productKey|String|Product Id|
|mNodeId|String|Device NodeId|
|devId|String|Device Id|
|version|String|Version|

### Mesh gateway update

The following is the sample code for a gateway update.

**Example**

```java
private IOtaListener iOtaListener = new IOtaListener() {
	@Override
	public void onSuccess(int otaType) {
		// Updated successfully.
	}

	@Override
	public void onFailure(int otaType, String code, String error) {
		// Failed to update.
	}

	@Override
	public void onProgress(int otaType, final int progress) {
		// Update progress
	}
};

ITuyaOta iTuyaOta = TuyaHomeSdk.newOTAInstance(devId);
iTuyaOta.setOtaListener(mOtaListener);
// Starts OTA updates.
iTuyaOta.startOta();

// Destroy an update
iTuyaOta.onDestroy();
```