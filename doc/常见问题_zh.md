## 接口提示签名错误/权限校验失败

* 确认 appKey、appSecret、安全图片是否与 IoT 平台上的信息一致，任意一个不匹配都将校验失败。具体请按照 [集成准备](https://developer.tuya.com/en/docs/app-development/preparation/preparation?id=Ka69nt983bhh5) 和 [集成 SDK](https://developer.tuya.com/en/docs/app-development/android-app-sdk/integration/integrated?id=Ka69nt96cw0uj) 章节来进行检查。

## 升级 SDK 后出现如下问题

```
java.lang.IllegalAccessError: Class okhttp3.EventListener extended by class com.tuya.smart.android.network.http.HttpEventListener is inaccessible (declaration of 'com.tuya.smart.android.network.http.HttpEventListener'***)
```

请使用以下命令升级 SDK 依赖的 okhttp 版本号：

`implementation 'com.squareup.okhttp3:okhttp-urlconnection:3.12.3'`

<!-- ## 配网常见问题

1. [Wi-Fi 设备配网常见问题请参照文档](Activator_wifi_faq.md) -->

## 升级到3.34.5版本的SDK后无法获取到面板信息（ProductPanelInfoBean）？

由于SDK内部升级改造，现在请求设备数据时不会再默认拉取面板信息。如需要获取面板信息，在请求数据之前调用以下方法
```java
//调用此方法后会在请求设备列表数据时自动拉取面板信息
TuyaHomeSdk.getDataInstance().setAutoLoadPanelInfo(true)
```