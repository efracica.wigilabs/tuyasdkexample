## Permission verification failed

 Please check the `appKey`, `appSecret`, `Security img`(t_s.bmp), any one of the mismatches will fail the verification. Please refer to [Preparation for integration](https://developer.tuya.com/en/docs/app-development/preparation/preparation?id=Ka69nt983bhh5) and [Integrate SDK](https://developer.tuya.com/en/docs/app-development/android-app-sdk/integration/integrated?id=Ka69nt96cw0uj) section.

## After upgrading the SDK, the following problems occur

```
java.lang.IllegalAccessError: Class okhttp3.EventListener extended by class com.tuya.smart.android.network.http.HttpEventListener is inaccessible (declaration of 'com.tuya.smart.android.network.http.HttpEventListener' ***)
```

Please upgrade the OkHttp version by using the following command.
	 
`implementation 'com.squareup.okhttp3: okhttp-urlconnection: 3.12.3'`

<!--
## Configuration Network FAQ

1. [Summary of Problems for Wi-Fi Configuration](Activator_wifi_faq.md)
-->

## Can't get panel info (ProductPanelInfoBean) after upgrading to 3.34.5
Since 3.34.5, the sdk does not load panel info by default. if you need panel information, add code below before your device list request  
```
TuyaHomeSdk.getDataInstance().setAutoLoadPanelInfo(true)
```